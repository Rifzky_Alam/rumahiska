<?php 
session_start();
// include_once $_SERVER['DOCUMENT_ROOT'].'/classes/views/class-global.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/library/controller/member.controller.php';

// carts
/*if (isset($_SESSION['ri_items'])) {
	$data->items = $_SESSION['ri_items'];
	$data->countmycart = count($_SESSION['ri_items']);
} else {
	$data->items = array();
	$data->countmycart=0;
}*/

if (isset($_SERVER['PATH_INFO'])) {
	$_SERVER['PATH_INFO'] = str_replace('.php', '', $_SERVER['PATH_INFO']);
	@$url_segment = explode('/', trim($_SERVER['PATH_INFO'],'/'));
	$key = array_shift($url_segment);

	switch ($key) {
		case 'login':
			if (isset($_POST['in'])) {
				Memberctrl::Loginmemberpost($_POST['in']);
			} else {
				Memberctrl::Loginmember();	
			}	
			break;
		case 'registrasi':
			if (isset($_POST['in'])) {
				Memberctrl::CommonRegistration($_POST['in']);
			} elseif(isset($_GET['email'])&&!empty($_GET['email'])) {
				// email harusnya dalam bentuk hash md5
				Memberctrl::Registerpernahbeli($_GET['email']);	
			}elseif (isset($_GET['valid'])&&$_GET['valid']=='0') {
				Memberctrl::Registrasi($_GET['valid']);
			}else{
				Memberctrl::Registrasi();
			}
			break;
		case 'registerexistingmember':
			if (isset($_POST['in'])) {
				Memberctrl::registerpernahbelipost($_POST['in']);
			}
			break;
		case 'dasbor':
			Memberctrl::Dasbor();
			break;
		case 'pembayaran':
			echo $_COOKIE['sal_member'];
			Memberctrl::Payment();
			break;
		case 'alamatapi':
			if (isset($_GET['hash'])) {
				Memberctrl::AlamatAPI($_GET['hash']);
			}
			break;
		case 'tesalamatapi':
			Memberctrl::TambahDaftarDetailAlamatAPI();
			break;
		case 'historitransaksi':
			
			break;
		case 'tambahalamat':
			Memberctrl::TambahAlamat();
			break;
		case 'ubahpassword':
			Memberctrl::GantiPassword();
			break;
		case 'sendemail':
			if (isset($_GET['v'])) {
				Memberctrl::SendEmail($_GET['v']);
			}
			break;
		case 'aktifasi':
			if (isset($_GET['usr'])) {
				Memberctrl::EmailSuccess($_GET['usr']);	
			}
			break;
		case 'logout':
			session_unset($_SESSION['smember']);
			if (isset($_COOKIE['sal_member'])) {
    			unset($_COOKIE['sal_member']);
    			setcookie('sal_member', '', time() - 3600, '/'); // empty value and old timestamp
			}
			header('location: http://'.$_SERVER['SERVER_NAME'].'/member/login');
			break;
		case 'resendemail':
			
			break;
		case '/':
			header('location:'.$_SERVER['HTTP_REFERER'].'/'.basename(__FILE__,'.php'));
			break;
		case '':
			header('location:'.$_SERVER['HTTP_REFERER'].'/'.basename(__FILE__,'.php'));
			break;
	}
} else {
	header('location:'.$_SERVER['HTTP_REFERER'].'/'.'member/login');
}
