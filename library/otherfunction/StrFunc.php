<?php 
class IString{
    
    public function GetDefaultValHypen($value){
    	if ($value!='') {
    		return $value;
    	} else {
    		return '-';
    	}
    }

    public function GetDefaultValNull($value){
    	if ($value!='') {
    		return $value;
    	} else {
    		return '';
    	}
    }

    public function GetDefaultValZero($value){
    	if ($value!='') {
    		return $value;
    	} else {
    		return '0';
    	}
    }
}

?>