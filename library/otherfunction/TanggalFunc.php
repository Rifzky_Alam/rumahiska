<?php 

function GetTanggal($value){
	if (preg_match('/0000/i', $value)) {
		return 'tidak ada tanggal tersedia';
	} else {
		$value = explode(' ', $value);
		$tgl = $value[0];
		$tgl = explode('-', $tgl);
		return $tgl[2].IndoTanggal($tgl[1]).$tgl[0];	
	}
	
}

function cariNamaHari($value){
	// input type = date('l')
    if ($value=='Sunday') {
        return 'ahad';
    } else if($value=='Monday'){
        return 'senin';
    } else if($value=='Tuesday'){
        return 'selasa';
    } else if($value=='Wednesday'){
        return 'rabu';
    } else if($value=='Thursday'){
        return 'kamis';
    } else if($value=='Friday'){
        return 'jumat';
    } else if($value=='Saturday'){
        return 'sabtu';
    }
}

function IndoTanggal($value){
	if ($value==1) {
		return " Januari ";
	}elseif ($value==2) {
		return " Februari ";
	}elseif ($value==3) {
		return " Maret ";
	}elseif ($value==4) {
		return " April ";
	}elseif ($value==5) {
		return " Mei ";
	}elseif ($value==6) {
		return " Juni ";
	}elseif ($value==7) {
		return " Juli ";
	}elseif ($value==8) {
		return " Agustus ";
	}elseif ($value==9) {
		return " September ";
	}elseif ($value==10) {
		return " Oktober ";
	}elseif ($value==11) {
		return " November ";
	}elseif ($value==12) {
		return " Desember ";
	}
}	

?>