<?php 
function GetDefaultValHypen($value){
    if ($value!='') {
    	return $value;
    } else {
    	return '-';
    }
}

function GetDefaultValNull($value){
    if ($value!='') {
    	return $value;
    } else {
    	return '';
	}
}

function GetDefaultValZero($value){
	if ($value!='') {
    	return $value;
	} else {
    	return '0';
	}
}

function GetDefaultValTidakAda($value){
    if ($value!='') {
    	return $value;
    } else {
    	return 'Tidak Ada Data';
    }
}

?>