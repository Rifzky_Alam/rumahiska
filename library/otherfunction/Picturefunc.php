<?php 

function DefaultProductPicPath($value,$baseurl){
	if ($value=='') {
		return $baseurl.'media/default/none.jpg';
	} else {
		return $baseurl.'uploads/images/produk/'.$value;
	}
}

function GetFileExtension($value){
	$filetype = pathinfo($value['name'],PATHINFO_EXTENSION);
	return $filetype;
}

function BuktiTransaksiFolder($basedir){
	return $basedir.'uploads/images/transaksi/';
}

function UploadPic($gambar,$folder){
	if (file_exists($folder.$gambar['name'])) {
		unlink($folder.$gambar['name']);
	}

	if (move_uploaded_file($gambar['tmp_name'], $folder.$gambar['name'])) {
		return true;
	}else {
		return false;
	}
}

?>