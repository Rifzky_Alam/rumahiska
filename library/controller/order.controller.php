<?php 
include_once '/home/salvinai/public_html/classes/views/class-global.php';
class Order{

    public static function cektoken($token){
        $data = new DataView();
        $token = md5($data->securitycode($token,'d'));
        if ($token==$_SESSION['ri_token']) {
            // echo 'oke';
            return true;
        }else{
            header('Location:'.$data->base_url);
        }

        if (empty($token)) {
            echo 'empty';
        }

        
    }

    public static function SendingInvoiceViaClient($id){
        $data = new DataView();
        $data->Model('Transaksi');
        $data->Model('Mail');
        $data->Model('Querybuilder');

        $db = new QueryBuilder();
        $email = new RumahIskaMail();
        $trans = new Transaksi();
        $id = $data->securitycode($id,'d');
        $token = md5($id);

        /* useless when using email library
        if ($token!=$_SESSION['ri_token']) {
            header('Location: '.$data->base_url);
        }

        if (empty($id)) {
            header('Location: '.$data->base_url);
        }
        */
        $trans->setId($id);
        #region
        // data for email template
        $data->Lib('otherfunction/TanggalFunc');
        $dataemail = $trans->FetchForEmail();
        $data->carts=$trans->FetchCarts();
        $data->customer=$dataemail->rc_nama;
        $data->resi=$dataemail->rk_resi;
        $data->ongkir=$dataemail->rk_ongkir;
        $data->idtrans=$dataemail->tr_id;
        $data->tglkirim=GetTanggal($dataemail->rk_tgl_kirim);
        $data->petugas = 'Web System';
        #end region

        // print_r($data->carts);
        include_once $data->homedir.'view/administrasi/transaksi/email/vkontenemail.php';
        // echo Email($data);
        $data->email_to = $dataemail->rc_email;
        $data->mailsubject = 'Invoice Transaksi '.$dataemail->rc_nama;
        $data->mailbody = Email($data);

        if ($email->KirimFaktur($data)) {
            $db->UpdateData('ri_transaksi',['tr_status'=>'1'],array($db->GetCond('tr_id','=',$id)));
            header('Location: '.$data->base_url.'order/invoice?tr='.$id);
        }else{
            echo "<script>alert('Invoice gagal Dikirim');</script>";
            echo "<script>location.replace('".$data->base_url."order/invoice?tr=".$id."');</script>";
        }
    }

    public static function ResendingInvoiceViaClient($id){
        $data = new DataView();
        $data->Model('Transaksi');
        $data->Model('Mail');
        $data->Model('Querybuilder');

        $db = new QueryBuilder();
        $email = new RumahIskaMail();
        $trans = new Transaksi();
        $id = $data->securitycode($id,'d');
        $trans->setId($id);
        #region
        // data for email template
        $data->Lib('otherfunction/TanggalFunc');
        $dataemail = $trans->FetchForEmail();
        $data->carts=$trans->FetchCarts();
        $data->customer=$dataemail->rc_nama;
        $data->resi=$dataemail->rk_resi;
        $data->ongkir=$dataemail->rk_ongkir;
        $data->idtrans=$dataemail->tr_id;
        $data->tglkirim=GetTanggal($dataemail->rk_tgl_kirim);
        $data->petugas = 'Web System';
        #end region

        // print_r($data->carts);
        include_once $data->homedir.'view/administrasi/transaksi/email/vkontenemail.php';
        // echo Email($data);
        $data->email_to = $dataemail->rc_email;
        $data->mailsubject = 'Invoice Transaksi '.$dataemail->rc_nama;
        $data->mailbody = Email($data);

        if ($email->KirimFaktur($data)) {
            $db->UpdateData('ri_transaksi',['tr_status'=>'1'],array($db->GetCond('tr_id','=',$id)));
            header('Location: '.$data->base_url.'order/invoice?tr='.$id);
        }else{
            echo "<script>alert('Invoice gagal Dikirim');</script>";
            echo "<script>location.replace('".$data->base_url."order/invoice?tr=".$id."');</script>";
        }
    }

    public static function ShowPDFInvoice($id){
        $data = new DataView();
        $data->Model('Transaksi');
        $transaksi = new Transaksi();
        $transaksi->setId($id);
        $detailcust = $transaksi->DetailTransaction();
        $data->listbarang = $transaksi->GetCartsOfCust();

        if (count($data->listbarang)=='0') {
            $data->subtotal = 0;
        }else{
            $data->subtotal = 0;
            foreach ($data->listbarang as $key) {
                $data->subtotal += $key->th_harga * $key->qty;                
            }
        }



        // data transaksi
        $data->labeltrans = str_replace('Rit-', '', $id);
        $data->transtanggaltimestamp = strtotime($detailcust->tr_tanggal);
        $data->tanggalexpired = date('d-m-Y H:m:s', $data->transtanggaltimestamp + 36000);
        $data->namacust=$detailcust->rc_nama;
        $data->emailcust=$detailcust->rc_email;
        $data->telepon=$detailcust->rc_telepon;
        $data->alamat=$detailcust->adr_alamat;
        $data->jasakurir=$detailcust->rk_kode_jasa;
        $data->no_resi=$detailcust->rk_resi;
        $data->service=$detailcust->rjk_kd_svc;
        $data->ongkir=$detailcust->rk_ongkir;
        $data->kecamatan = $detailcust->rk_kec;
        $data->kota = $detailcust->rk_kota;
        $data->prov=$detailcust->rk_prov;
        $data->potongan = 0;
        $data->totalbayar = $data->subtotal + $data->ongkir - $data->potongan; 
        $data->terbayar = 0;
        // end data transaksi

        $data->View('order/invoicepdf.order.php',$data);
    }

    public static function ShowHtmlInvoice($id){
        $data = new DataView();
        $data->Model('Transaksi');
        $transaksi = new Transaksi();
        $transaksi->setId($id);
        $detailcust = $transaksi->DetailTransaction();
        $data->listbarang = $transaksi->GetCartsOfCust();

        
        

        // data transaksi
        $data->id_transaksi = $id;
        $data->labeltrans = str_replace('Rit-', '', $id);
        $data->transtanggaltimestamp = strtotime($detailcust->tr_tanggal);
        $data->tanggalexpired = date('d-m-Y H:m:s', $data->transtanggaltimestamp + 36000);
        $data->namacust=$detailcust->rc_nama;
        $data->emailcust=$detailcust->rc_email;
        $data->telepon=$detailcust->rc_telepon;
        $data->alamat=$detailcust->adr_alamat;
        $data->jasakurir=$detailcust->rk_kode_jasa;
        $data->no_resi=$detailcust->rk_resi;
        $data->service=$detailcust->rjk_kd_svc;
        $data->ongkir=$detailcust->rk_ongkir;
        $data->kecamatan = $detailcust->adr_kec;
        $data->kota = $detailcust->adr_kota;
        $data->prov=$detailcust->adr_prov;
        $data->notealamat = $detailcust->adr_notes;
        $data->potongan = 0;
        $data->terbayar = 0;
        // end data transaksi

        // print_r($detailcust);
        $data->View('order/invoice-view.php',$data);
        // print_r($data->listbarang);
    }

	public static function ClientOrder($idjenisbarang){
        $data = new DataView();
        $data->Model('Barang');
        $data->Model('Querybuilder');
        $barang = new Barang();
        $db = new QueryBuilder();
        if (isset($_SESSION['ri_items'])) {
            $data->items = $_SESSION['ri_items'];
            $data->countmycart = count($_SESSION['ri_items']);
        } else {
            $data->items = array();
            $data->countmycart=0;
        }
        // print_r($_SESSION['ri_agen']);

        if (isset($_POST['in'])) {
            //setting id detail barang for submitting into trolley
            $barang->setID($_POST['in']['id_detail']);
            // selecting data for produk terkait
            if(isset($_SESSION['ri_agen'])&&!empty($_SESSION['ri_agen'])){
                $data->item = $barang->SelectForChartAgen();     
            }else{
                $data->item = $barang->SelectForChart(); 
            }

            if ($_POST['qty']>$data->item->jumlah_barang) {
                echo "<script>alert('Jumlah Barang tidak bisa melebihi jumlah stok kami :(');</script>";
            }elseif ($_POST['qty']=='0') {
                echo "<script>alert('Jumlah Barang tidak bisa nol :(');</script>";
            }else {
                if (!isset($_SESSION['ri_items'])) {
                    $_SESSION['ri_items'] = [
                        $_POST['in']['id_detail'] => array(
                            'ib' => $_POST['in']['id_detail'],
                            'kat' => $data->item->kb_ket,
                            'jns'=>$data->item->jenis_barang,
                            'nb' => $data->item->detail_jenis,
                            'bb' => $data->item->brg_berat,
                            'qty' => $_POST['in']['qty'],
                            'priceid' => $data->item->th_id,
                            'price' => $data->item->brg_harga,
                            'foto' => $data->item->bg_name
                            )
                        ];
                }else{ // else session is not set
                    if (isset($_SESSION['ri_items'][$_POST['in']['id_detail']])) {
                            $_SESSION['ri_items'][$_POST['in']['id_detail']] = [
                                'ib' => $_POST['in']['id_detail'],
                                'kat' => $data->item->kb_ket,
                                'jns'=>$data->item->jenis_barang,
                                'nb' => $data->item->detail_jenis,
                                'bb' => $data->item->brg_berat,
                                'qty' => $_POST['qty'],
                                'priceid' => $data->item->th_id,
                                'price' => $data->item->brg_harga,
                                'foto' => $data->item->bg_name
                            ];
                            // echo "<script>alert('barang ada!');</script>";
                        } else {
                            $_SESSION['ri_items'][$_POST['in']['id_detail']] = [
                                'ib' => $_POST['in']['id_detail'],
                                'kat' => $data->item->kb_ket,
                                'jns'=>$data->item->jenis_barang,
                                'nb' => $data->item->detail_jenis,
                                'bb' => $data->item->brg_berat,
                                'qty' => $_POST['qty'],
                                'priceid' => $data->item->th_id,
                                'price' => $data->item->brg_harga,
                                'foto' => $data->item->bg_name
                            ];
                            // echo "<script>alert('barang baru!');</script>";
                        }
                        echo "<script>location.replace('".$data->base_url."produk/ordernew/".$idjenisbarang."');</script>";
                }
            } // end else checkng qty
        } // end if isset

        $data->item = $barang->FetchKetJenisBarang($idjenisbarang);
        // print_r($data->item);
        $data->jenis_barang = $idjenisbarang;
        
        // data galeri
        $data->galeri = $barang->FetchBarangByJenisBarangID($idjenisbarang); // rifzky 21 Mei 2018

        // sidebar data ( categories )
        $data->sidebardata = $barang->FetchSidebarClient(); //rifzky 13 mei 2018

        $data->listprodukterkait = $db->FetchJoinWhere(
            ['db.id AS id','jb.id AS id_jb','kb_ket','jenis_barang','detail_jenis','th_harga','bg_name'],
           'jenis_barang AS jb',
           array(
               $db->getJoin('INNER','kategori_barang AS kb','kb.kb_id','jb.kb_id'),
               $db->getJoin('INNER','data_barang AS db','db.id_jenis','jb.id'),
               $db->getJoin('LEFT','tabel_harga AS th','th.th_id','db.brg_harga'),
               $db->getJoin('LEFT','barang_gambar AS bg','bg.bg_id','db.brg_pic')
           ),
           array(
               $db->GetCond('jb.id','=',$idjenisbarang)
           )
        );
        $data->judul = $data->company.' || Produk Kami';
        $data->View('homepage/vneworder.homepage.php',$data);
    }
} //end class