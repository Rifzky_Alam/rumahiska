<?php 
include_once '/home/salvinai/public_html/classes/views/class-global.php';
class ProdukService{
	public static function GetDetailJumlahHarga($jenis){
	    $data = new DataView();
    	$data->Model('Troli');
    	$troli = new Troli();
    	// print_r($troli);
    	if (isset($jenis)&&!empty($jenis)) {
    		return json_encode($troli->getTroliData($jenis));
    	} else {
    		return 'No data can be shown!';
    	}   	
	}

  public static function GetTroliItems(){
    // $arr = array();
    // array_push($arr, $_SESSION['ri_items']);
    foreach ($_SESSION['ri_items'] as $key => $value) {
      // echo $key . "<br>";
      echo json_encode($value);
    }
    // return json_encode($arr);
  }

  public static function PostTroli(){
    // echo '<div id="jmlbrg">2</div>';
    echo '<div id="tabel"><tr><td>1</td><td>Ciput Kupluk Anti Pusing</td><td>35000</td></tr></div>';
  }

  public static function CheckData($id){
      $data = new DataView();
      $data->Model('Querybuilder');
      $db = new QueryBuilder();
      $r = $db->FetchWhere(['id'],'data_barang',array($db->GetCond('id','=',$id)));
      echo count($r);
  }

  public static function ReturnTable(){
    $arr = array();
    // $a = ['aidi'=>'12','nb'=>'baju bekas','jml'=>'2','hrg'=>'20000'];
    // $b = ['aidi'=>'13','nb'=>'baju dikasih','jml'=>'3','hrg'=>'25000'];
    // $c = ['aidi'=>'14','nb'=>'baju jadul','jml'=>'4','hrg'=>'27000'];
    if (isset($_SESSION['ri_items'])&&!empty($_SESSION['ri_items'])) {
      if (count($_SESSION['ri_items'])!='0') {
        foreach ($_SESSION['ri_items'] as $key => $value) {
          array_push($arr, ['aidi'=>$value['ib'],'nb'=>$value['kat'].' '.$value['jns'].' '. $value['nb'],'jml'=>$value['qty'],'hrg'=>$value['price']]);
        }
      }
    }

    
    echo json_encode($arr);
  }

  public static function tesJoin($idbarang){
    $data = new DataView();
    $data->Model('Querybuilder');
    $db = new QueryBuilder();
    $r = $db->FetchOneRowJoinWhere(['jumlah_barang','th_harga'],'data_barang AS db',array(
      $db->getJoin('LEFT','tabel_harga AS th','db.brg_harga','th.th_id')
    ),array(
      $db->GetCond('db.id','=',$idbarang)
    ));
    echo json_encode($r);
  }

  public static function GetTotalItem($idbarang){
    $data = new DataView();
    $data->Model('Querybuilder');
    $db = new QueryBuilder();

    $r = $db->FetchOneRowJoinWhere(['jumlah_barang','th_harga'],'data_barang AS db',array(
      $db->getJoin('LEFT','tabel_harga AS th','db.brg_harga','th.th_id')
    ),array(
      $db->GetCond('db.id','=',$idbarang)
    ));

    $arr = [
      'status' => ['code'=>'17','description' => 'ok'],
      'totaldata' => $r['jumlah_barang'],
      'price'=>$r['th_harga']
    ];
    echo json_encode($arr);
  }

  public static function RemoveItem($idbarang){
    // unset($_SESSION['ri_items'][$idbarang]);
    if (isset($_SESSION['ri_items'])) {
      unset($_SESSION['ri_items'][$idbarang]);
      $jumlahdata = count($_SESSION['ri_items']);
      $dt = array();

      if ($jumlahdata=='0') {
        foreach ($_SESSION['ri_items'] as $key => $value) {
          array_push($dt,['aidi'=>$value['ib'],'nb'=>$value['kat'].' '.$value['jns'].' '. $value['nb'],'jml'=>$value['qty'],'hrg'=>$value['price']]);        
        }
        $arr = [
          'status' => ['code'=>'17','description' => 'ok'],
          'totaldata' => $jumlahdata,
          'data' => $dt
        ];
      } else {
        foreach ($_SESSION['ri_items'] as $key => $value) {
          array_push($dt,['aidi'=>$value['ib'],'nb'=>$value['kat'].' '.$value['jns'].' '. $value['nb'],'jml'=>$value['qty'],'hrg'=>$value['price']]);        
        }
        $arr = [
          'status' => ['code'=>'17','description' => 'ok'],
          'totaldata' => $jumlahdata,
          'data' => $dt
        ];
      }
      echo json_encode($arr);
    }else{
      
      $arr = [
        'status' => ['code'=>'0','description' => 'no item can be displayed'],
        'totaldata' => '0',
        'data' => []
      ];
      echo json_encode($arr);
    }
  }
   
  public static function CurrentItems(){
    if (isset($_SESSION['ri_items'])) {
      $jumlahdata = count($_SESSION['ri_items']);
      $dt = array();

      if ($jumlahdata=='0') {
        foreach ($_SESSION['ri_items'] as $key => $value) {
          array_push($dt,['aidi'=>$value['ib'],'nb'=>$value['kat'].' '.$value['jns'].' '. $value['nb'],'jml'=>$value['qty'],'hrg'=>$value['price']]);        
        }
        $arr = [
          'status' => ['code'=>'17','description' => 'ok'],
          'totaldata' => $jumlahdata,
          'data' => $dt
        ];
      } else {
        foreach ($_SESSION['ri_items'] as $key => $value) {
          array_push($dt,['aidi'=>$value['ib'],'nb'=>$value['kat'].' '.$value['jns'].' '. $value['nb'],'jml'=>$value['qty'],'hrg'=>$value['price']]);        
        }
        $arr = [
          'status' => ['code'=>'17','description' => 'ok'],
          'totaldata' => $jumlahdata,
          'data' => $dt
        ];
      }
      echo json_encode($arr);
    }else{
      
      $arr = [
        'status' => ['code'=>'0','description' => 'no item can be displayed'],
        'totaldata' => '0',
        'data' => []
      ];
      echo json_encode($arr);
    }    
    
  }

  public static function SetItemToSession($item_id,$qty){
    $data = new DataView();
    $data->Model('Barang');
    $data->Model('Querybuilder');
    $db = new QueryBuilder();
    $barang = new Barang();
    $barang->setID($item_id);


    @$r = $db->FetchWhere(['id'],'data_barang',array($db->GetCond('id','=',$item_id)));
    
    if (count(@$r)!='0') {
      if(isset($_SESSION['ri_agen'])&&!empty($_SESSION['ri_agen'])){
        $data->item = $barang->SelectForChartAgen();     
      }else{
        $data->item = $barang->SelectForChart(); 
      }

      // this line is supposed to check if qty is less than the item stock

      //end check line

      if (!isset($_SESSION['ri_items'])) {
        $_SESSION['ri_items'] = [
          $item_id => array(
            'ib' => $item_id,
            'kat' => $data->item->kb_ket,
            'jns'=>$data->item->jenis_barang,
            'nb' => $data->item->detail_jenis,
            'bb' => $data->item->brg_berat,
            'qty' => $qty,
            'priceid' => $data->item->th_id,
            'price' => $data->item->brg_harga,
            'foto' => $data->item->bg_name
          )
        ];
      }else{ // else session is not set
        if (isset($_SESSION['ri_items'][$item_id])) { // if item exist on session
          $_SESSION['ri_items'][$item_id] = [
            'ib' => $item_id,
            'kat' => $data->item->kb_ket,
            'jns'=>$data->item->jenis_barang,
            'nb' => $data->item->detail_jenis,
            'bb' => $data->item->brg_berat,
            'qty' => $qty,
            'priceid' => $data->item->th_id,
            'price' => $data->item->brg_harga,
            'foto' => $data->item->bg_name
          ];
          // echo "<script>alert('barang ada!');</script>";
        } else {
          $_SESSION['ri_items'][$item_id] = [
            'ib' => $item_id,
            'kat' => $data->item->kb_ket,
            'jns'=>$data->item->jenis_barang,
            'nb' => $data->item->detail_jenis,
            'bb' => $data->item->brg_berat,
            'qty' => $qty,
            'priceid' => $data->item->th_id,
            'price' => $data->item->brg_harga,
            'foto' => $data->item->bg_name
          ];
          // echo "<script>alert('barang baru!');</script>";
        }  
      } //end else if item exist on session

      // return json when session has been successfully set 
      $dt = array();

      foreach ($_SESSION['ri_items'] as $key => $value) {
        array_push($dt,['aidi'=>$value['ib'],'nb'=>$value['kat'].' '.$value['jns'].' '. $value['nb'],'jml'=>$value['qty'],'hrg'=>$value['price']]);        
      }
      $jumlahdata = count($_SESSION['ri_items']);
      $arr = [
        'status' => ['code'=>'17','description' => 'ok'],
        'totaldata' => $jumlahdata,
        'data' => $dt
      ];

      echo json_encode($arr);

    }else{ //else id is unrecognized
      $arr = [
          'status' => ['code'=>'404','description' => 'no data can be displayed'],
          'totaldata' => '0',
          'data' => [] 
      ];

      echo json_encode($arr);
    }

          
  }

   public static function GetPrice($id_detail_barang){
       $data = new DataView();
       $data->Model('Troli');
       $troli = new Troli();
       
       
       if (isset($id_detail_barang)&&!empty($id_detail_barang)) {
            return json_encode($troli->getHarga($id_detail_barang));    
       }else{
            return "No Data Shown Here!";
       }
   }

   public static function RemoveOneItem($item){
       $result = [
           'totalcurrentitem' => '1',
           'items' => [
                ['nb'=>'tahu','harga'=>'12000','qty'=>'5'],['nb'=>'tempe','harga'=>'24000','qty'=>'10']
           ]
       ];
       return json_encode($result);
   }
}