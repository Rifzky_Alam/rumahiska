<?php 
// include_once '/Applications/XAMPP/xamppfiles/htdocs/rikza/classes/views/class-global.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/classes/views/class-global.php';
class Distributor{
    //@url /distributor/tambahalamat, POST
    public static function TambahAlamatPost($masukdata){
        $data = new DataView();
        $data->JustInclude('library/session/sessions');
        $data->Model('Querybuilder');
        $db = new QueryBuilder();
        $sesi = new MySession();
        
        $sesi->OnlyAgen();

        $r = $db->InsertData('ri_alamat',
            [
                'adr_id' => md5($masukdata['alamat']), 
                'adr_source' => $_SESSION['ri_agen']['email'], 
                'adr_alamat' => $masukdata['alamat'],
                'adr_kec' => $masukdata['kec'],
                'adr_kota' => $masukdata['kota'],
                'adr_prov' => $masukdata['prov'],
                'adr_kode_pos' => $masukdata['kodepos'],
                'adr_notes' => $masukdata['note']
            ]
        );
        $inputdaftarprov = $db->InsertData('dftr_prov',
            [
                'idprov' => $masukdata['prov'], 
                'namaprov' => $masukdata['namaprov']
            ]
        );
        $inputdaftarkota = $db->InsertData('dftr_kota',
            [
                'idkota' => $masukdata['kota'], 
                'namakota' => $masukdata['namakota']
            ]
        );
        $inputdaftarkec = $db->InsertData('dftr_kec',
            [
                'idkec' => $masukdata['kec'], 
                'namakec' => $masukdata['namakec']
            ]
        );

        if ($r) {
            echo "<script>alert('Data alamat berhasil disimpan.');</script>";
            echo "<script>location.replace('".$data->base_url."distributor/pembayaran');</script>";
        }else{
            echo "<script>alert('DBError: Data alamat gagal disimpan.');</script>";
            echo "<script>location.replace('".$data->base_url."member/tambahalamat');</script>";
        }
    }

    public static function HapusAlamat($idaddr){
        $data = new DataView();
        $data->Model('Querybuilder');
        $db = new QueryBuilder();

        $r = $db->UpdateData('ri_alamat',['adr_status'=>'0'],array($db->GetCond('adr_id','=',$idaddr)));

        if ($r) {
            header('location:'.$data->base_url.'distributor/tambahalamat');
        }else{
            header('location:'.$data->base_url.'distributor/tambahalamat');
        }
    }
    
    //@url /distributor/tambahalamat, GET
    public static function TambahAlamat(){
        $data = new DataView();
        $data->JustInclude('library/session/sessions');
        $data->Model('Querybuilder');
        $data->Model('Customer');
        
        $db = new QueryBuilder();
        $cust = new Customer();
        $sesi = new MySession();
        
        $sesi->OnlyAgen();
        
        $data->listalamat = $cust->getDaftarAlamat($_SESSION['ri_agen']['email'],'1');
        // print_r($data->listalamat);
        
        if(isset($_POST['in'])){
            self::TambahAlamatPost($_POST['in']);
        }
        
        
        $data->judul = 'Tambah Alamat || Agen '.$data->company;
        $data->View('distributor/dasbor/vtambahalamat.distributor.php',$data);
    }
    
    //@url /distributor/pembayaran, GET
    public static function Pembayaran(){
        $data = new DataView();
        $data->Model('Querybuilder');
        $data->Model('Transaksi');
        $trans = new Transaksi();
        $db = new QueryBuilder();

        if (isset($_POST['in'])) {
            // print_r($_POST['in']);
            $id = $trans->CreateIDTr();
            $_SESSION['ri_token'] = md5($id);
            $rtrans = $db->InsertData('ri_transaksi',['tr_id' => $id, 'tr_id_cust' => $_SESSION['ri_agen']['email'],'tr_status'=>'0']);
            $rkur = $db->InsertData('ri_kurir',
                [
                    'rk_id_trans' => $id,
                    'rk_kode_jasa' => $_POST['in']['svc'],
                    'rjk_kd_svc' => $_POST['in']['ds'],
                    'rk_ongkir' => $_POST['in']['ongkir'],
                    'rk_alamat' => $_POST['in']['alamat']
                ]
            );
            // echo 'trans: '.$id.' '.$_SESSION['smember']['email'].'<br/>';
            // echo 'kurir: '.$id.' '.$_POST['in']['svc'].' '.$_POST['in']['ds'].' '.$_POST['in']['ongkir'].'<br/>';
            // print_r($_SESSION['ri_items']);
            foreach ($_SESSION['ri_items'] as $key) {
                // echo $key['ib'].' '.$key['qty'].' '.$key['price'].'<br/>';
                $ritem = $db->InsertData('ri_carts',
                    [
                        'cart_source' => $id,
                        'cart_items' => $key['ib'],
                        'qty' => $key['qty'],
                        'cart_itm_price' => $key['price'],
                    ]
                );  
            }
            if ($rtrans&&$rkur&&$ritem) {
                unset($_SESSION['ri_items']);
                echo "<script>location.replace('".$data->base_url."order/invoice/sending?tr=".$data->securitycode($id,'e')."');</script>";
            }
        }

        //$_SESSION['smember']['email'] = 'rifzky.mail@gmail.com';
        $data->listalamat = $db->FetchWhere(['*'],
            'ri_alamat',
            array(
                $db->GetCond('adr_source','=',$_SESSION['ri_agen']['email']),
                $db->GetCond('adr_status','!=','0')
            )
        );
        // print_r($listalamat);
        @$data->listbarang=$_SESSION['ri_items'];
        $data->View('distributor/dasbor/vpembayaran.distributor.php',$data);
    }
    
    public static function Login(){
    	$data = new DataView();
    	$data->Model('Querybuilder');
    	$db = new QueryBuilder();
        if (isset($_POST['in'])) {
            // print_r($_POST['in']);
            $r = $db->FetchWhere(
                ['rc_username','rc_password','rc_nama','rc_email','rc_telepon'],
                'ri_customer',
                array(
                    $db->GetCond('rc_username','=',$_POST['in']['username']),
                    $db->GetCond('rc_password','=',md5($_POST['in']['password']),
                    $db->GetCond('rc_status','=','23')
                    )
                )
            );
            if (empty($r)||count($r)>1) {
                // print_r($_POST['in']);
                echo "<script>alert('Anda tidak teregistrasi dalam sistem kami');</script>";
            }else{
                $_SESSION['ri_agen'] = [
                    'username' => $r[0]['rc_username'],
                    'password' => $r[0]['rc_password'],
                    'nama' => $r[0]['rc_nama'],
                    'email' => $r[0]['rc_email']
                ];

                echo "<script>alert('Welcome ".$r[0]['rc_nama']."');</script>";
                echo "<script>location.replace('".$data->base_url."distributor/dasbor');</script>";
            }
        }
            
    	$data->View('distributor/login/vlogin.distributor.php',$data);
    }

    public static function Logout(){
        session_unset($_SESSION['ri_agen']);
        header('Location: login');
    }

    public static function SuccessInputAgen($kode){
        $data = new DataView();
        $data->judul = 'Data Agen';
        $data->nama_admin = 'Rifzky Alam';
        $data->link = $data->base_url."distributor/register/".$kode;

        $data->View('administrasi/agen/vsuccesinput.agen.php',$data);
    }

    public static function ListAgen($offset='0',$limit='30'){
        $data = new DataView();
        $data->Model('Querybuilder');
        $db = new QueryBuilder();
        $data->judul = 'Data Agen';
        $data->nama_admin = 'Rifzky Alam';
        //$data->lists = $db->Fetch('agen_dist',$offset,$limit);
        $data->lists = $db->FetchWhere(['*'],'ri_customer',array($db->GetCond('rc_status','>','19')),$offset,$limit);
        $data->View('administrasi/agen/vlist.agen.php',$data);
    }

    public static function Verifikasi($username,$token){
        $data = new DataView();
        $data->Model('Querybuilder');
        $db = new QueryBuilder();
        // update after sending email
        $db->UpdateData('agen_dist',
            [
                'ad_status' => '3'
            ],
            array(
                $db->GetCond('ad_id','=',$data->securitycode($token,'d'))
            )
        );
    }

    public static function Produk(){
        $data = new DataView();
        $data->JustInclude('library/session/sessions');
        $data->Model('Querybuilder');
        $db = new QueryBuilder();
        $sesi = new MySession();
        
        $sesi->OnlyAgen();
        $data->judul = 'Dasbor || Agen '.$data->company;
        $data->nama_agen = $_SESSION['ri_agen']['nama'];
        // print_r($data->pengumuman);
        $data->View('distributor/produk/vtabelproduk.dist.php',$data);
    }

    public static function Dasbor(){
    	$data = new DataView();
        $data->JustInclude('library/session/sessions');
        $data->Model('Querybuilder');
        $data->model('Transaksi');
        $trans = new Transaksi();
        $db = new QueryBuilder();
        $sesi = new MySession();
        
        $sesi->OnlyAgen();
        $data->judul = 'Dasbor || Agen '.$data->company;
        $data->nama_agen = $_SESSION['ri_agen']['nama'];
        $data->pengumuman = $db->FetchWhere(['n_subject AS subjek','n_pengumuman AS conten'],'notif',array($db->GetCond('n_status','=','1')));
        $data->historitrans = $trans->GetHistoriTransaksi($_SESSION['ri_agen']['email']);
    	$data->View('distributor/dasbor/vdasbor.distributor.php',$data);
    }

    public static function SuccessReg($idagent){
        $data = new DataView();
        $data->Model('Querybuilder');
        $data->Model('Mail');
        $email = new RumahIskaMail();
        $db = new QueryBuilder();

        $r = $db->FetchOneRow(['rc_status','rc_email','rc_nama','rc_telepon'],'ri_customer',array($db->GetCond('rc_hash','=',$idagent))); 
        
        /*
        harusnya:
        
        */

        if ($r['rc_status']!='21'):
            header('Location: '.$data->base_url);            
        endif;

        //send email here!
        include_once $data->homedir.'view/distributor/email/vnotifkodereg.php';
        $data->email_to = $r['rc_email'];
        $data->mailsubject = 'Registrasi Agen Distributor salvina.id';
        $data->akun = $data->securitycode($idagent,'e');
        $data->mailbody = Notifikasi($data);
        $email->sendMail($data);

        // update after sending email
        $db->UpdateData('ri_customer',
            [
                'ad_status' => '22'
            ],
            array(
                $db->GetCond('ri_hash','=',$idagent)
            )
        );
        
        /*
        harusnya:
        $db->UpdateData('ri_customer',
            [
                'rc_status' => '22'
            ],
            array(
                $db->GetCond('rc_hash','=',$idagent)
            )
        );
        */

        $data->judul = 'Registrasi Agen '.$data->company;
        $data->View('distributor/registrasi/vsuccessregister.distributor.php',$data);
    }

    public static function Validasi($idagen){
        $data = new DataView();
        $data->Model('Querybuilder');
        $db = new QueryBuilder();
        // update after sending email
        $db->UpdateData('ri_customer',
            [
                'rc_status' => '23'
            ],
            array(
                $db->GetCond('rc_hash','=',$data->securitycode($idagen,'d'))
            )
        );
        
        /*
        harusnya:
        $db->UpdateData('ri_customer',
            [
                'rc_status' => '23'
            ],
            array(
                $db->GetCond('rc_hash','=',$data->securitycode($idagen,'d'))
            )
        );
        */

        header('Location: '.$data->base_url.'distributor/login');
    }

    public static function Register($idagent){
    	$data = new DataView();
    	$data->Model('Querybuilder');
    	$db = new QueryBuilder();
        $r = $db->FetchOneRow(['rc_username'],'ri_customer',array($db->GetCond('rc_hash','=',$idagent)));

        if ($r['rc_username']!='-'):
            header('Location: '.$data->base_url);            
        endif;

        if (isset($_POST['in'])) {
            // $pr = $db->UpdateData('agen_dist',
            //     [
            //         'ad_username' => @$_POST['in']['username'],
            //         'ad_password' => @md5($_POST['in']['password']),
            //         'ad_nama' => @$_POST['in']['nama'],
            //         'ad_email' => @$_POST['in']['email'],
            //         'ad_telepon' => @$_POST['in']['telepon'],
            //         'ad_status' => '1'
            //     ],
            //     array(
            //         $db->GetCond('ad_id','=',$idagent)
            //     )
            // );
            
            
                $pr = $db->UpdateData('ri_customer',
                [
                    'rc_username' => @$_POST['in']['username'],
                    'rc_password' => @md5($_POST['in']['password']),
                    'rc_nama' => @$_POST['in']['nama'],
                    'rc_email' => @$_POST['in']['email'],
                    'rc_telepon' => @$_POST['in']['telepon'],
                    'rc_status' => '21'
                ],
                array(
                    $db->GetCond('rc_hash','=',$idagent)
                )
            );
            

            if ($pr) {
                header('Location: '.$data->base_url.'distributor/registersuccess/'.$idagent);
            } else {
                echo "<script>alert('Registrasi gagal :(');</script>";
            }

        }
        $data->judul = 'Registrasi agen '.$data->company;
        $data->View('distributor/dasbor/vregister.distributor.php',$data);             	
    }

    public static function ListNotif(){
        $data = new DataView();
        $data->Model('Querybuilder');
        $db = new QueryBuilder();

        $data->lists = $db->FetchColumns(['n_id','n_subject','n_status'],'notif');

        $data->judul = 'Input Agen Baru';
        $data->nama_admin = 'Rifzky Alam';
        $data->View('administrasi/agen/vlistnotif.agen.php',$data); 
    }

    public static function DeleteNotif($id){
        $data = new DataView();
        $data->Model('Querybuilder');
        $db = new QueryBuilder();

        $r = $db->DeleteWhere('notif',array($db->GetCond('n_id','=',$id)));
        if ($r) {
            echo "<script>location.replace('".$data->base_url."administrasi/agen/listnotif');</script>";
        }else{
            echo "<script>alert('Data tidak dapat dihapus, hubungi admin sistem.');</script>";
        }
    }

    public static function Changestatus($id,$status){
        $data = new DataView();
        $data->Model('Querybuilder');
        $db = new QueryBuilder();
        
        // check if status has more than 1

        $total = $db->FetchWhere(['n_id'],'notif',array($db->GetCond('n_status','=','1')));
        if (count($total)>1) {
            echo "<script>alert('Hanya 1 pengumuman yang bisa diaktifkan.');</script>";
            echo "<script>location.replace('".$data->base_url."administrasi/agen/listnotif');</script>";
        }else{
            if ($status!='0'&&$status!='1') {
                echo "<script>alert('Status tidak dapat dikenali!');</script>";
            
            }else{
                $db->UpdateData('notif',
                    [
                        'n_status' => $status
                    ],
                    array(
                        $db->GetCond('n_id','=',$id)
                    )
                );
                echo "<script>location.replace('".$data->base_url."administrasi/agen/listnotif');</script>";
            }
        }
    }

    public static function InputPeluang(){
        $data = new DataView();
        $data->Model('Querybuilder');
        $db = new QueryBuilder();

        if (isset($_POST['in'])) {
            // print_r($_POST['in']);
            $r = $db->UpdateData('agen_data',
                    [
                        'agdt_content' => $_POST['in']['apalah']
                    ],
                    array(
                        $db->GetCond('agdt_id','=','peluanglaman')
                    )
            );
            
            
            if ($r) {
                echo "<script>alert('Konten Laman berhasil disimpan!');</script>";
                echo "<script>location.replace('".$data->base_url."administrasi/agen/list');</script>";
            }else {
                echo "<script>alert('Konten Laman gagal disimpan!');</script>";
            }
        }
        
        $r = $db->FetchOneRow(['agdt_content'],'agen_data',array($db->GetCond('agdt_id','=','peluanglaman')));
        $data->konten = $r['agdt_content'];

        $data->judul = 'Input Agen Baru';
        $data->nama_admin = 'Rifzky Alam';
        $data->View('administrasi/agen/vpeluang.agen.php',$data); 

    }

    public static function Peluang(){
        $data = new DataView();
        $data->Model('Querybuilder');
        $db = new QueryBuilder();
        $data->judul = 'Registrasi agen '.$data->company;
        $r = $db->FetchOneRow(['agdt_content'],'agen_data',array($db->GetCond('agdt_id','=','peluanglaman')));
        $data->konten = $r['agdt_content'];
        // echo $data->konten;
        $data->View('distributor/dasbor/vpeluang.distributor.php',$data);
    }

    public static function Notifikasi(){
        $data = new DataView();
        $data->Model('Querybuilder');
        $db = new QueryBuilder();
        if (isset($_POST['in'])) {
            $r = $db->InsertData('notif',
                [
                    'n_subject' => $_POST['in']['subject'],
                    'n_pengumuman' => $_POST['in']['pengumuman']
                ]
            );
            if ($r) {
                echo "<script>alert('Pengumuman Untuk agen berhasil disimpan!');</script>";
            } else {
                echo "<script>alert('Pengumuman untuk agen gagal disimpan!');</script>";
            }
        }

        $data->judul = 'Input Agen Baru';
        $data->nama_admin = 'Rifzky Alam';
        $data->View('administrasi/agen/vnotif.agen.php',$data); 
    }

    public static function BaseUrl(){
        $data = new DataView;
        return $data->base_url;
    }

    public static function Newagent(){
    	$data= new DataView();
    	$data->Model('Querybuilder');
    	$db = new QueryBuilder();

        if (isset($_POST['in'])) {
            //$r = $db->InsertData('agen_dist',
                //['ad_id' => $_POST['in']['kodeakses']]
            //); // ubah ke ri_customer status(flag) = 20 (20 is not registered yet)
            
            $r = $db->InsertData('ri_customer',
                ['rc_hash' => $_POST['in']['kodeakses'],'rc_status' => '20']
            );
            
            if ($r) {
                echo "<script>alert('kode akses berhasil disimpan, anda dapat berikan kode akses terkait untuk registrasi agen anda.');</script>";
                echo "<script>location.replace('".$data->base_url.'administrasi/agen/kodesuccess?kode='.$_POST['in']['kodeakses']."');</script>";
            } else {
                echo "<script>alert('kode akses gagal disimpan.');</script>";
            }
        }

        $data->judul = 'Input Agen Baru';
        $data->nama_admin = 'Rifzky Alam';
        $data->View('administrasi/agen/vinput.agen.php',$data);	
    }
    
}
?>