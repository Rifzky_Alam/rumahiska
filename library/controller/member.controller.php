<?php 
// include_once '/Applications/XAMPP/xamppfiles/htdocs/rikza/classes/views/class-global.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/classes/views/class-global.php';
class Memberctrl{
    private static function CekSesi(){
        $data = new DataView();
        $data->JustInclude('library/session/sessions');
        $sesi = new MySession();
        $sesi->OnlyMember();
    }

    public static function TambahDaftarDetailAlamatAPI(){
        $data = new DataView();
        self::CekSesi();
        $data->Model('Querybuilder');       
        $db = new QueryBuilder();
        $listprov = $db->FetchColumns(['idkota','namakota'],'dftr_kota');
        // print_r($listprov);
        foreach ($listprov as $value) {
            // echo 'id: '.$value['idkota'].', namakota: '.$value['namakota'].'<br>';
            $curl = curl_init();

            curl_setopt_array($curl, array(
              CURLOPT_URL => "https://pro.rajaongkir.com/api/subdistrict?city=".$value['idkota'],
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "GET",
              CURLOPT_HTTPHEADER => array(
                "key: 1b0b4fcff1d4f1e01c68c9e7fccbeea5"
              ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
              echo "cURL Error #:" . $err;
            } else {
              $result = json_decode($response);
              // print_r($result->rajaongkir->results);
              // echo $result->results;
              // print_r($dataprov);
              foreach ($result->rajaongkir->results as $key) {
                // echo $key->subdistrict_id.' : '.$key->subdistrict_name.'<br>';
                $db->InsertData('dftr_kec',
                    [
                        'idkec' => $key->subdistrict_id, 
                        'namakec' => $key->subdistrict_name
                    ]
                );
              }
            }
        } // end foreach
            
    }

    //@url = /member/alamatapi?hash={value}, method=GET
    public static function AlamatAPI($hash){
        header('Content-Type: application/json; charset=ISO-8859-1');
        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        $data = new DataView();
        $data->Model('Querybuilder');   
        $data->Model('Transaksi');
        $trans = new Transaksi();    
        $db = new QueryBuilder();
        
        $r = $trans->GetDataAlamatbyHash($hash);
        if (count($r)>'0') {
            $arr = [
                'status' => ['code'=>'1','description'=>'data is available'],
                'data'=>$r
            ];
        }else{
            $arr = [
                'status' => ['code'=>'0','description'=>'Cannot retrieve requested data.'],
                'data' => [] 
            ];
        }
        echo json_encode($arr);
    }
    //@url = /member/tambahalamat, method=POST
    public static function TambahAlamatPost($masukdata){
        $data = new DataView();
        self::CekSesi();
        $data->Model('Querybuilder');       
        $db = new QueryBuilder();
        //$_SESSION['smember']['email']='rifzky.mail@gmail.com';

        $r = $db->InsertData('ri_alamat',
            [
                'adr_id' => md5($masukdata['alamat']), 
                'adr_source' => $_SESSION['smember']['email'], 
                'adr_alamat' => $masukdata['alamat'],
                'adr_kec' => $masukdata['kec'],
                'adr_kota' => $masukdata['kota'],
                'adr_prov' => $masukdata['prov'],
                'adr_kode_pos' => $masukdata['kodepos']
            ]
        );
        $inputdaftarprov = $db->InsertData('dftr_prov',
            [
                'idprov' => $masukdata['prov'], 
                'namaprov' => $masukdata['namaprov']
            ]
        );
        $inputdaftarkota = $db->InsertData('dftr_kota',
            [
                'idkota' => $masukdata['kota'], 
                'namakota' => $masukdata['namakota']
            ]
        );
        $inputdaftarkec = $db->InsertData('dftr_kec',
            [
                'idkec' => $masukdata['kec'], 
                'namakec' => $masukdata['namakec']
            ]
        );

        if ($r) {
            echo "<script>alert('Data alamat berhasil disimpan.');</script>";
            echo "<script>location.replace('".$data->base_url."member/tambahalamat');</script>";
        }else{
            echo "<script>alert('Data alamat gagal disimpan.');</script>";
            echo "<script>location.replace('".$data->base_url."member/tambahalamat');</script>";
        }

    }

    //@url = /member/tambahalamat, method=GET
    public static function TambahAlamat(){
        $data = new DataView();
        self::CekSesi();
        $data->Model('Querybuilder');       

        if (isset($_POST['in'])) {
            // echo 'ok';
            // print_r($_POST['in']);
            self::TambahAlamatPost($_POST['in']);
        }

        $data->View('member/valamatbaru.member.php',$data); 
    }

    public static function GantiPasswordpost($masukdata){
        
    }

    public static function GantiPassword(){
        $data = new DataView();
        $data->Model('Querybuilder');       
        $data->View('member/vgantipass.member.php',$data); 
    }

    // @url = "/member/dasbor/" method = GET
    public static function Dasbor(){
        $data = new DataView();
        self::CekSesi();
        // print_r($_COOKIE);
        // if (isset($_COOKIE['sal_member'])&&!empty($_COOKIE['sal_member'])) {
        //     echo $_COOKIE['sal_member'];
        // }
        $data->Model('Querybuilder'); 
        $data->model('Transaksi');
        $trans = new Transaksi();
        @$data->listbarang=$_SESSION['ri_items'];
        if(isset($_POST['vcr'])){
            $jml = $trans->Cekvoucher($_POST['vcr']['kode']);
            if($jml=='0'){
                echo "<script>alert('Tidak ada voucher dengan kode yang anda masukkan');</script>";
            }else{
                setcookie('salvina_vcr', $_POST['vcr']['kode'], time() + (86400 * 1), "/");
                echo "<script>location.replace('".$data->base_url."member/dasbor');</script>";
            }
            
        }
        $data->historitrans = $trans->GetHistoriTransaksi($_SESSION['smember']['email']);
        $data->View('member/vdasbor.member.php',$data); 
    }    

    // @url = "/member/sendemail/" method = GET
    public static function SendEmail($hash){
        $data = new DataView();
        $data->Model('Querybuilder');
        $data->Model('Mail');
        $db = new QueryBuilder();
        $surat = new RumahIskaMail();
        $data->JustInclude('view/member/emailverif.member');
        $userdata = $db->FetchOneRowJoinWhere(['*'],
            'ri_customer',
            array(
                $db->getJoin('LEFT','ri_validasi','ri_customer.rc_hash','ri_validasi.rv_source')
            ),array($db->GetCond('ri_customer.rc_hash','=',$hash)));
        $data->kode = $userdata['rv_kode'];
        $data->nama = $userdata['rc_nama'];
        $data->hash = $hash;
        $content = VerifikasiMember($data);
        // echo $content;
        $data->email_to = $userdata['rc_email'];
        $data->mailsubject = 'Verifikasi Akun '.$userdata['rc_nama'];
        $data->mailbody = $content;

        if ($surat->KirimEmailWithoutCC($data)) {
            header('location:'.$data->base_url.'member/aktifasi?usr='.$hash);
        } else {
            header('location:'.$data->base_url.'member/aktifasi?usr='.$hash);
        }

        // print_r($data->userdata);
        // $data->View('member/emailverif.member.php',$data);
    }

    // @url = "/member/login/" method = POST
    public static function Loginmemberpost($masukdata){
        $data = new DataView();
        $data->Model('Querybuilder');
        $db = new QueryBuilder();
        // $password = password_hash($masukdata['password'],PASSWORD_BCRYPT);
        // echo $masukdata['email'];
        // echo $password;
        $datauser = $db->FetchOneRow(['rc_hash','rc_email','rc_password','rc_nama','rc_status'],
            'ri_customer',
            array(
                $db->GetCond('rc_email','=',$masukdata['email']),
                $db->GetCond('rc_status','=','1')
            )
        );
        // print_r($masukdata);
        // print_r($datauser);
        echo $datauser['rc_password'];
        if (password_verify($masukdata['password'],$datauser['rc_password'])) {
            $_SESSION['smember'] = [
                'hash' => $datauser['rc_hash'],
                'nama' => $datauser['rc_nama'],
                'email' => $datauser['rc_email'],
                'role' => 'member'
            ];
            setcookie('sal_member', $datauser['rc_hash'], time() + (86400 * 30), "/");
            // print_r($_SESSION['smember']);
            echo "<script>location.replace('".$data->base_url."member/dasbor');</script>";
            // header('location:'.$data->base_url.'member/dasbor');
        }else{
            echo "<script>alert('Maaf, sistem tidak mengenali akun anda.');</script>";
            echo "<script>location.replace('".$data->base_url."member/login');</script>";
        }

    }
    // @url = "/member/login/" method = GET
    public static function Loginmember(){
        $data = new DataView();
        $data->Model('Querybuilder');
        $db = new QueryBuilder();
        
        if (isset($_COOKIE['sal_member'])&&!empty($_COOKIE['sal_member'])&&$_COOKIE['sal_member']!='deleted'&&$_COOKIE['sal_member']!='') {
            $datauser = $db->FetchOneRow(['rc_hash','rc_email','rc_password','rc_nama','rc_status'],
                'ri_customer',
                array(
                    $db->GetCond('rc_hash','=',$_COOKIE['sal_member'])
                )
            );
            if (count($datauser)>0) {
                $_SESSION['smember'] = [
                    'hash' => $datauser['rc_hash'],
                    'nama' => $datauser['rc_nama'],
                    'email' => $datauser['rc_email'],
                    'role' => 'member'
                ];
                header('location:'.$data->base_url.'member/pembayaran');
            }
        }

        $data->View('member/vlogin.member.php',$data);
    }

    // @url = '/member/pembayaran' method=GET
    public static function Payment(){
        $data = new DataView();
        self::CekSesi();
        $data->Model('Querybuilder');
        $data->Model('Transaksi');
        $trans = new Transaksi();
        $db = new QueryBuilder();

        if (isset($_POST['in'])) {
            // print_r($_POST['in']);
            $id = $trans->CreateIDTr();
            $_SESSION['ri_token'] = md5($id);
            $rtrans = $db->InsertData('ri_transaksi',['tr_id' => $id, 'tr_id_cust' => $_SESSION['smember']['email'],'tr_status'=>'0']);
            $rkur = $db->InsertData('ri_kurir',
                [
                    'rk_id_trans' => $id,
                    'rk_kode_jasa' => $_POST['in']['svc'],
                    'rjk_kd_svc' => $_POST['in']['ds'],
                    'rk_ongkir' => $_POST['in']['ongkir'],
                    'rk_alamat' => $_POST['in']['alamat']
                ]
            );
            // echo 'trans: '.$id.' '.$_SESSION['smember']['email'].'<br/>';
            // echo 'kurir: '.$id.' '.$_POST['in']['svc'].' '.$_POST['in']['ds'].' '.$_POST['in']['ongkir'].'<br/>';
            // print_r($_SESSION['ri_items']);
            foreach ($_SESSION['ri_items'] as $key) {
                // echo $key['ib'].' '.$key['qty'].' '.$key['price'].'<br/>';
                $ritem = $db->InsertData('ri_carts',
                    [
                        'cart_source' => $id,
                        'cart_items' => $key['ib'],
                        'qty' => $key['qty'],
                        'cart_itm_price' => $key['price'],
                    ]
                );  
            }
            if ($rtrans&&$rkur&&$ritem) {
                unset($_SESSION['ri_items']);
                echo "<script>location.replace('".$data->base_url."order/invoice/sending?tr=".$data->securitycode($id,'e')."');</script>";
            }
        }

        $_SESSION['smember']['email'] = 'rifzky.mail@gmail.com';
        $data->listalamat = $db->FetchWhere(['*'],'ri_alamat',array($db->GetCond('adr_source','=',$_SESSION['smember']['email'])));
        // print_r($listalamat);
        @$data->listbarang=$_SESSION['ri_items'];
        $data->View('member/vpembayaran.member.php',$data);
    }

    // @url = '/member/aktifasi' method=POST
    public static function Aktifasipost($masukdata,$userdata){
        $data = new DataView();
        $data->Model('Querybuilder');
        $db = new QueryBuilder();
        if ($masukdata['kode']==$userdata['rv_kode']) {
            $r = $db->UpdateData('ri_customer',
                ['rc_status'=>'1'],
                array(
                    $db->GetCond('rc_hash','=',$userdata['rc_hash'])
                )
            );
            if ($r) {
                echo "<script>alert('Akun berhasil di verifikasi, silahkan login untuk masuk ke dalam menu member :)');</script>";
                echo "<script>location.replace('".$data->base_url."member/login');</script>";
            } else {
                echo "<script>alert('Akun gagal di verifikasi, silahkan hubungi support kami.');</script>";
                echo "<script>location.replace('".$data->base_url."member/aktifasi?usr=".$userdata['rc_hash']."');</script>";
            }
        }else {
            echo "<script>alert('Kode validasi tidak valid.');</script>";
                echo "<script>location.replace('".$data->base_url."member/aktifasi?usr=".$userdata['rc_hash']."');</script>";
        }
        // print_r($masukdata);
    }

    // @url = '/member/aktifasi' method=GET   
    public static function EmailSuccess($hash){
        $data = new DataView();
        $data->Model('Querybuilder');
        $db = new QueryBuilder();
        $userdata = $db->FetchOneRowJoinWhere(['*'],
            'ri_customer',
            array(
                $db->getJoin('LEFT','ri_validasi','ri_customer.rc_hash','ri_validasi.rv_source')
            ),array($db->GetCond('ri_customer.rc_hash','=',$hash)));

        if (isset($_POST['in'])) {
            self::Aktifasipost($_POST['in'],$userdata);
        }else{
            $data->id = $userdata['rc_hash'];
            $data->View('member/vtungguverif.member.php',$data);    
        }

        
    }

    // @url = '/member/registrasi' method=POST
    public static function Registrasipost($masukdata){
        $data = new DataView();
        $data->Model('Querybuilder');
        $db = new QueryBuilder();
        $dt = array();
        $dt = $db->FetchOneRow(['rc_email','rc_nama','rc_telepon'],'ri_customer',array($db->GetCond('rc_email','=','rifzky.mail@gmail.com'))); 
        $jumlahdata = count($dt);

        if ($masukdata['password']==$masukdata['passwordconfirm']) {
            if ($jumlahdata>0) {
                $r = $db->UpdateData('ri_customer',[
                    'rc_nama' => $masukdata['nama'],
                    'rc_telepon' => $masukdata['telepon'],
                    'rc_password' => password_hash($masukdata['password'],PASSWORD_BCRYPT),
                    'status' => '1'
                ],array($db->GetCond('rc_email','=',$masukdata['email'])));

                if ($r) {
                    echo "<script>alert('Data berhasil disimpan');</script>";        
                } else {
                    echo "<script>alert('Data gagal disimpan, harap hubungi operator kami.');</script>";
                    echo "<script>location.replace('".$data->base_url."member/registrasi');</script>";
                }

            }

        }else{
            echo "<script>alert('Password konfirmasi tidak sama.');</script>";
            echo "<script>location.replace('".$data->base_url."member/registrasi');</script>";
        }
    }

    public static function registerpernahbelipost($masukdata){
        $data = new DataView();
        $data->Model('Querybuilder');
        $db = new QueryBuilder();

        if ($masukdata['password']==$masukdata['passwordconfirm']) {
            $r = $db->UpdateData(
                'ri_customer',
                [
                    'rc_password'=>password_hash($masukdata['password'],PASSWORD_BCRYPT)
                ],
                array($db->GetCond('rc_hash','=',$masukdata['v']))
            );

            if ($r) {
                echo "<script>alert('Data berhasil disimpan');</script>";   
                echo "<script>location.replace('".$data->base_url."member/sendemail?v=".$masukdata['v']."');</script>";
            }
        }else {
            echo "<script>alert('Kombinasi password tidak sesuai');</script>";   
            echo "<script>location.replace('".$data->base_url."member/registrasi?email=".$masukdata['v']."');</script>";   
        }
    }

    // @url = '/member/registrasi?email={value}' method=GET
    public static function Registerpernahbeli($email){
        $data = new DataView();
        $data->Model('Querybuilder');
        $db = new QueryBuilder();

        $cust = $db->FetchWhere(['*'],'ri_customer',array($db->GetCond('rc_hash','=',$email)));
        // echo count($cust);
        if (count($cust)=='1') {
            $data->namacust = $cust[0]['rc_nama'];
            $data->hash = $email;
            if (isset($_POST['in'])) {
                self::registerpernahbelipost($_POST['in'],$email);
            }


            $data->View('member/vreg2.member.php',$data);
        } else {
            header('location:'.$data->base_url.'member/registrasi');
        }
        
    }

    // @url = '/member/registrasi' method=POST
    public static function CommonRegistration($masukdata){
        $data = new DataView();
        $data->Model('Querybuilder');
        $db = new QueryBuilder();
        if ($masukdata['passwordconfirm']==$masukdata['password']) {
            $r = $db->InsertData(
                'ri_customer',
                [
                    'rc_hash' => md5($masukdata['email']),
                    'rc_email' => $masukdata['email'],
                    'rc_password' => password_hash($masukdata['password'],PASSWORD_BCRYPT),
                    'rc_telepon' => $masukdata['telepon'],
                    'rc_nama' => $masukdata['nama'],
                ]
            );
            $r2 = $db->InsertData(
                'ri_validasi',
                [
                    'rv_source' => md5($masukdata['email']),
                    'rv_kode' => rand(100000,999999)
                ]
            );
            if ($r) {
                echo "<script>alert('Data berhasil disimpan');</script>";   
                echo "<script>location.replace('".$data->base_url."member/sendemail?v=".md5($masukdata['email'])."');</script>";
            } else {
                echo "<script>alert('Data gagal disimpan, harap hubungi operator kami.');</script>";
                echo "<script>location.replace('".$data->base_url."member/registrasi');</script>";
            }
        }else{
            header('location:'.$data->base_url.'member/registrasi/?valid=0');
        }
    }


    // @url = '/member/registrasi' method=GET
    public static function Registrasi($valid='1'){
    	$data = new DataView();
    	$data->Model('Querybuilder');
    	$db = new QueryBuilder();
        
        $data->namalengkap = '';
        $data->telepon = '';
        $data->email = '';

        $data->validation = $valid;

        $data->View('member/vreg.member.php',$data);
    }

   
    
}