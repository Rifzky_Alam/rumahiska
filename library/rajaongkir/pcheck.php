<?php 

$curl = curl_init();

if (isset($_GET['id'])&&!empty($_GET['id'])) {
	$myurl = "https://pro.rajaongkir.com/api/province?id=".$_GET['id'];
}else{
	$myurl = "https://pro.rajaongkir.com/api/province";
}

curl_setopt_array($curl, array(
  CURLOPT_URL => $myurl,
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_HTTPHEADER => array(
    "key: 1b0b4fcff1d4f1e01c68c9e7fccbeea5"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  echo $response;
}