<?php 
// session_start();
class MySession{
	public function Keluar(){
		session_start();
		// session_destroy();
		unset($_SESSION['admin']);
		header('location: login.php');
	}

	public function SetPage($value){
		$_SESSION['page'] = $value;
	}

	public function OnlyAdmin($pathtogo){
		if (!isset($_SESSION['admin'])) {
			header('location: '.$pathtogo);
			// echo "<script>location.replace('$pathtogo')</script>";
		}
	}

	public function OnlyAgen(){
		if (!isset($_SESSION['ri_agen']['username'])||$_SESSION['ri_agen']['nama']=='') {
			header('location: logout');
		}
	}

	public function OnlyMember(){
		if (!isset($_SESSION['smember']['email'])||$_SESSION['smember']['email']=='') {
			header('location: http://'.$_SERVER['SERVER_NAME'].'/member/login');
		}
	}

	public function AdminMarketing(){
		if (!isset($_SESSION['admin'])||($_SESSION['admin']['tipe']!='admin'&&$_SESSION['admin']['tipe']!='marketing')) {
			header('location: logout.php');
		}
	}

	public function Staff($user){
		if (!isset($_SESSION['admin'])||empty($_SESSION['admin']['username'])) {
			header('location: logout.php');
		}
	}

	public function Login(){
		if (isset($_SESSION['admin'])) {
			header('Location: ../administrasi/index.php');
		}
	}

}

?>