<?php 
$base_url = 'http://'.$_SERVER['HTTP_HOST'].'/';
// print_r($_SERVER);
?>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="<?= $base_url ?>assets/common/bootstrap/css/bootstrap.min.css">
	<title>Invoice Transaksi - salvina.id</title>
</head>
<body>

	<div class="container" style="padding-bottom:40px;">
		<div class="row">
			<div class="page-header">
				<h2>Invoice Transaksi</h2>
			</div>
		</div>
	
		<div class="row">
			<div class="col-md-12">
				<form action="" method="post" accept-charset="utf-8">
					<a href="#" class="btn btn-lg btn-warning" title="Cetak dengan ukuran kertas A5" target="_blank">Cetak Invoice</a>	
					<input type="hidden" name="token" value="">
					<button class="btn btn-lg btn-info" title="Klik bila anda tidak mendapatkan pesan apapun di email anda.">Kirim Ulang Invoice ke Email Saya</button>
					<a href="#" class="btn btn-lg btn-success">Konfirmasi Whatsapp</a>
				</form>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<section>
					<h3>SALVINA.ID</h3>
					<p>
						Bintara IV Bekasi Barat <br>
						admin@salvina.id <br>
						081279222250
					</p>
				</section>
				
			</div>
			<div class="col-md-6">
				<br><br>
				<div class="row">
					<div class="col-md-6">
						No Invoice
					</div>
					<div class="col-md-6">
						#000000001
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						Tanggal
					</div>
					<div class="col-md-6">
						23 Sept 2018
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						Expired
					</div>
					<div class="col-md-6">
						24 Sept 2018 23:00:00
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<section>
					<h3>Kepada</h3>
					<p>
						Rifzky Alam <br>
						Jl Jatiwaringin No 8 Rt 01 Rw 04 <br>
						Pangkalan Jati Makassar <br>
						Jakarta Timur DKI Jakarta <br>
						rifzky.mail@gmail.com <br>
						<a href="tel://081279222250" title="Nomor Kontak">081279222250</a>
					</p>
				</section>
				<br>
				<table class="table table-bordered">
					<thead>
						<tr>
							<th style="text-align:center;">Nama Produk</th>
							<th style="text-align:center;">Harga</th>
							<th style="text-align:center;">Qty</th>
							<th style="text-align:center;">Jumlah</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Jilbab bla2</td>
							<td style="text-align:right;"><?= number_format(300000).' IDR' ?></td>
							<td style="text-align:center;">1</td>
							<td style="text-align:right;"><?= number_format(300000).' IDR' ?></td>
						</tr>
						<tr>
							<td>Jilbab bla3</td>
							<td style="text-align:right;"><?= number_format(400000).' IDR' ?></td>
							<td style="text-align:center;">1</td>
							<td style="text-align:right;"><?= number_format(400000).' IDR' ?></td>
						</tr>
						<tr>
							<td>Jilbab bla4</td>
							<td style="text-align:right;"><?= number_format(300000).' IDR' ?></td>
							<td style="text-align:center;">1</td>
							<td style="text-align:right;"><?= number_format(300000).' IDR' ?></td>
						</tr>
						<tr>
							<td colspan="3"><b>Total</b></td>
							<td style="text-align:right;"><b><?= number_format(1000000).' IDR' ?></b></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<h4>Pembayaran</h4>
				<ul>
					<li>Transfer Bank BCA A/N Rifzky Alam 801245325</li>
					<li>Transfer Bank MANDIRI A/N Rifzky Alam 801245325</li>
					<li>Transfer Bank BNI A/N Rifzky Alam 801245325</li>
				</ul>
			</div>
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-6">
						<b>Subtotal</b>
					</div>
					<div class="col-md-6" style="text-align:right;">
						<?= number_format(1000000).' IDR' ?>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-6">
						<b>Potongan</b>
					</div>
					<div class="col-md-6" style="text-align:right;">
						<?= number_format(0).' IDR' ?>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-6">
						<b>Terbayar</b>
					</div>
					<div class="col-md-6" style="text-align:right;">
						<?= number_format(0).' IDR' ?>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-6">
						<b>Total</b>
					</div>
					<div class="col-md-6" style="text-align:right;">
						<b><?= number_format(1000000).' IDR' ?></b>
					</div>
				</div>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-6">
				<a href="#" title="Upload Bukti Pembayaran" class="btn btn-lg btn-primary">Upload Bukti Pembayaran</a>
			</div>
		</div>

	</div>

<script src="<?= $base_url ?>assets/common/bootstrap/js/jquery.js"></script>
<script src="<?= $base_url ?>assets/common/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>