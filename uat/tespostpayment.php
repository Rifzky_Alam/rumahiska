<?php session_start(); ?>
<?php include_once '/home/salvinai/public_html/classes/views/class-global.php'; ?>
<?php $base_url = 'http://salvina.id/'; ?>

<?php 
$data = new DataView();
$data->Model('Querybuilder');
$data->Model('Transaksi');
$db = new QueryBuilder();
$trans = new Transaksi();
if (isset($_POST['zz'])) {
  
  // print_r($_POST['zz']);
}

$aidi = $trans->CreateIDTr();
$user = 'lulu@rifzky.com';
$idalamat = md5('tesalamat');

$dua = 2;
if ($dua > 9) {
  // if transaction has no data cust before.
  //first, create new customer
  $iscustdone = $db->InsertData('ri_customer',['rc_email' => $user,'rc_nama' => 'Rifzky Alam','rc_telepon' => '0123425345']);
  //second, create new address
  $isaddrdone = $db->InsertData('ri_alamat',['adr_id' => $idalamat, 'adr_source' => $user, 'adr_alamat' => 'tesalamat']);
  // third, create new transaction
  $istransdone = $db->InsertData('ri_transaksi',['tr_id' => $aidi, 'tr_id_cust' => $user,'tr_status'=>'0']);
  // fouth, create new kurir
  $iskurirdone = $db->InsertData('ri_kurir',['rk_id_trans' => $aidi,'rk_kode_jasa' => 'jne', 'rk_prov' => '2', 'rk_kota' => '20','rk_kec' => '200','rk_ongkir' => '8000','rk_alamat' => $idalamat]);
  // fifth, create new carts
  $arr = [
      '12' => ['qty' => '1','price'=>'50000'],
      '55' => ['qty' => '1','price'=>'22000'],
      '77' => ['qty' => '1','price'=>'40000'] 
  ];

  foreach ($arr as $key => $value) {
    $iscartdone = $db->InsertData('ri_carts',['cart_source' => $aidi, 'cart_items' => $key,'qty' => $value['qty'],'cart_itm_price' => $value['price']]);
  }
}
  
  
?>




<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="<?= $base_url ?>assets/common/bootstrap/css/bootstrap.min.css">
</head>
<body>
    



<div class="container" style="padding-bottom:50px;">
  <div class="row">
      <h4>Form Tes Troli</h4>
      <form action="" method="post" accept-charset="utf-8">
        <div class="form-group">
          <label>Email</label>
          <input id="idbrg" type="email" name="zz[email]" class="form-control">
        </div>
        <div class="form-group">
          <label>Nama Lengkap</label>
          <input id="iqty" type="text" name="zz[namalengkap]" class="form-control">
        </div>
        <div class="form-group">
          <label>Telepon</label>
          <input type="text" name="zz[telepon]" class="form-control">
        </div>
        <div class="form-group">
          <label>Alamat Rumah</label>
          <input type="text" name="zz[alamat]" class="form-control">
        </div>
        <div class="form-group">
          <label>Provinsi</label>
          <select name="zz[prov]" class="form-control">
            <option value="">-- PILIH PROVINSI --</option>
            <option value="23">Jawa Barat</option>
            <option value="29">Jawa Timur</option>
          </select>
        </div>
        <div class="form-group">
          <label>Kota</label>
          <select name="zz[kota]" class="form-control">
            <option value="">-- PILIH KOTA --</option>
            <option value="11">Bogor</option>
            <option value="12">Ponorogo</option>
          </select>
        </div>
        <div class="form-group">
          <label>Kecamatan</label>
          <select name="zz[kec]" class="form-control">
            <option value="">-- PILIH KECAMATAN --</option>
            <option value="33">Bogor Utara</option>
            <option value="41">Mlarak /option>
          </select>
        </div>
        <div class="form-group">
          <label>Jasa Kurir</label>
          <select name="zz[kurir]" class="form-control">
            <option value="">-- PILIH KURIR --</option>
            <option value="jne">JNE</option>
            <option value="pos">POS Indonesia</option>
          </select>
        </div>
        <div class="form-group">
          <label>Layanan</label>
          <select name="zz[layanan]" class="form-control">
            <option value="">-- PILIH LAYANAN --</option>
            <option value="1">Reguler</option>
            <option value="2">Eksekutif</option>
          </select>
        </div>
        <button id="btns" class="btn btn-lg btn-primary" style="width:100%">Submit</button>
        </form>      
    </div>
</div>

<script src="<?= $base_url ?>assets/common/bootstrap/js/jquery.js"></script>
<script src="<?= $base_url ?>assets/common/bootstrap/js/bootstrap.min.js"></script>

</body>
</html>
