<?php 
require $_SERVER['DOCUMENT_ROOT'].'/library/fpdf/fpdf.php';

class MyPDF{

	private $nilaiY;

	public function setNilaiy($value){
		$this->nilaiY = $value;
	}

	public function getNilaiy($value){
		return $this->nilaiY;
	}

    public function Invoice(){
    	$pdf = new PDF_HTML();
		$pdf->AddPage('L','A5');
		$pdf->SetAuthor('Rifzky Alam - Software Dev');
		$this->Header($pdf);
		$this->Tujuan($pdf);
		$this->TableItem($pdf);
		$this->Pembayaran($pdf);
		$this->TotalPembayaran($pdf);
		$pdf->Output();
    }

    function Header($pdf){
    	// Brand
    	$pdf->SetFont('Arial','B',24);
		$pdf->Cell(40,10,'SALVINA');
		
		$pdf->setXY(10,20);
		$pdf->SetFont('Arial','',8);
		$pdf->Cell(30,2,'Bintara 4 Bekasi Barat');

		$pdf->setXY(10,24);
		$pdf->SetFont('Arial','',8);
		$pdf->Cell(30,2,'admin@salvina.id');

		$pdf->setXY(10,28);
		$pdf->SetFont('Arial','',8);
		$pdf->Cell(30,2,'081279222250');

		//Invoice Number
		$pdf->setXY(120,15);
		$pdf->SetFont('Arial','',9);
		$pdf->Cell(20,2,'No Invoice');
		$pdf->Cell(10,2,':');
		$pdf->Cell(40,2,'#000000001',0,0,'R');

		// tanggal invoice
		$pdf->setXY(120,20);
		$pdf->Cell(20,2,'Tanggal');
		$pdf->Cell(10,2,':');
		$pdf->Cell(40,2,'23 Sept 2018',0,0,'R');

		// terakhir bayar
		$pdf->setXY(120,25);
		$pdf->Cell(20,2,'Expired');
		$pdf->Cell(10,2,':');
		$pdf->Cell(40,2,'23 Sept 2018 23:00:00',0,0,'R');

		// Total Bayar
		$pdf->setXY(120,30);
		$pdf->SetFillColor(0,0,0);
		$pdf->Rect(121,30,20,7,'F');
		$pdf->SetTextColor(255,255,255);

		$pdf->setXY(121,30);
		// $pdf->SetFillColor(255,255,255);
		$pdf->Cell(20,7,'Total',1,0,'C');
		$pdf->SetTextColor(0,0,0);
		$pdf->Cell(48,7,'1.000.000 IDR',1,0,'R');

    }

    function Tujuan($pdf){
    	$pdf->setXY(10,40);
    	$pdf->SetFont('Arial','B',9);
		$pdf->Cell(30,2,'Kepada :');

		$pdf->setXY(10,45);
		$pdf->SetFont('Arial','',8);
		$pdf->Cell(30,2,'Rifzky Alam');

		$pdf->setXY(10,49);
		$pdf->Cell(30,2,'Jl Jatiwaringin no 8 Pangkalan Jati Jakarta Timur');

		$pdf->setXY(10,53);
		$pdf->Cell(30,2,'rifzky.mail@gmail.com');

		$pdf->setXY(10,57);
		$pdf->Cell(30,2,'081279222250');
    }

    function TableItem($pdf){
    	$pdf->setXY(10,70);
    	$pdf->SetFont('Arial','B',8);
    	$pdf->Cell(90,5,'Nama Produk',1,0,'C');
		$pdf->Cell(50,5,'Harga',1,0,'C');
		$pdf->Cell(10,5,'Qty',1,0,'C');
		$pdf->Cell(30,5,'Jumlah',1,0,'C');
		$pdf->Ln();

		$pdf->SetFont('Arial','',8);
		//loop iteration here
		$pdf->SetWidths(array(90,50,10,30));
		$pdf->SetAligns(array('L','R','C','R'));
		$pdf->Row(array('Jilbab bla2',number_format(300000),'1',number_format(300000).' IDR'));

		$pdf->SetWidths(array(90,50,10,30));
		$pdf->SetAligns(array('L','R','C','R'));
		$pdf->Row(array('Jilbab bla3',number_format(400000),'1',number_format(400000).' IDR'));

		$pdf->SetWidths(array(90,50,10,30));
		$pdf->SetAligns(array('L','R','C','R'));
		$pdf->Row(array('Jilbab bla4',number_format(300000),'1',number_format(300000).' IDR'));
		// end loop

		// total
		$pdf->SetFont('Arial','B',8);
		$pdf->SetWidths(array(150,30));
		$pdf->SetAligns(array('L','R'));

		$pdf->Row(array('Total',number_format(1000000).' IDR'));
		
		$this->setNilaiy($pdf->GetY());
    }

    function Pembayaran($pdf){
    	$pdf->setXY(10,$this->nilaiY + 10);
    	$pdf->SetFont('Arial','B',9);
		$pdf->Cell(30,2,'PEMBAYARAN');
		$pdf->setXY(10,$this->nilaiY + 15);
		$pdf->SetFont('Arial','',8);
		$pdf->Cell(30,2,'- Transfer Bank BCA A/N Rifzky Alam 801245325');
		$pdf->setXY(10,$this->nilaiY + 19);
		$pdf->Cell(30,2,'- Transfer Bank MANDIRI A/N Rifzky Alam 801245325');
		$pdf->setXY(10,$this->nilaiY + 23);
		$pdf->Cell(30,2,'- Transfer Bank BNI A/N Rifzky Alam 801245325');
    }

    function TotalPembayaran($pdf){
    	$pdf->setXY(140,$this->nilaiY + 10);
    	$pdf->SetFont('Arial','B',8);
		$pdf->Cell(30,2,'Subtotal');
		$pdf->SetFont('Arial','',8);
		$pdf->Cell(20,2,'1,000,000 IDR',0,0,'R');
		$pdf->setXY(140,$this->nilaiY + 15);
		$pdf->SetFont('Arial','B',8);
		$pdf->Cell(30,2,'Potongan');
		$pdf->SetFont('Arial','',8);
		$pdf->Cell(20,2,'0 IDR',0,0,'R');
		$pdf->SetFont('Arial','B',8);
		$pdf->setXY(140,$this->nilaiY + 20);
		$pdf->Cell(30,2,'Terbayar');
		$pdf->SetFont('Arial','',8);
		$pdf->Cell(20,2,'0 IDR',0,0,'R');
		$pdf->setXY(140,$this->nilaiY + 25);
		$pdf->SetFont('Arial','B',8);
		$pdf->Cell(30,2,'Total');
		$pdf->Cell(20,2,'1,000,000 IDR',0,0,'R');
    }

}

class PDF_HTML extends FPDF{
	var $B=0;
	var $I=0;
	var $U=0;
	var $HREF='';
	var $ALIGN='';
	var $widths;
	var $aligns;

	function WriteHTML($html){
		//HTML parser
		$html=str_replace("\n",' ',$html);
		$a=preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
		foreach($a as $i=>$e)
		{
			if($i%2==0)
			{
				//Text
				if($this->HREF)
					$this->PutLink($this->HREF,$e);
				elseif($this->ALIGN=='center')
					$this->Cell(0,5,$e,0,1,'C');
				else
					$this->Write(5,$e);
			}
			else
			{
				//Tag
				if($e[0]=='/')
					$this->CloseTag(strtoupper(substr($e,1)));
				else
				{
					//Extract properties
					$a2=explode(' ',$e);
					$tag=strtoupper(array_shift($a2));
					$prop=array();
					foreach($a2 as $v)
					{
						if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
							$prop[strtoupper($a3[1])]=$a3[2];
					}
					$this->OpenTag($tag,$prop);
				}
			}
		}
	}

	function OpenTag($tag,$prop){
		//Opening tag
		if($tag=='B' || $tag=='I' || $tag=='U')
			$this->SetStyle($tag,true);
		if($tag=='A')
			$this->HREF=$prop['HREF'];
		if($tag=='BR')
			$this->Ln(5);
		if($tag=='P')
			$this->ALIGN=$prop['ALIGN'];
		if($tag=='HR')
		{
			if( !empty($prop['WIDTH']) )
				$Width = $prop['WIDTH'];
			else
				$Width = $this->w - $this->lMargin-$this->rMargin;
			$this->Ln(2);
			$x = $this->GetX();
			$y = $this->GetY();
			$this->SetLineWidth(0.4);
			$this->Line($x,$y,$x+$Width,$y);
			$this->SetLineWidth(0.2);
			$this->Ln(2);
		}
	}

	function CloseTag($tag){
		//Closing tag
		if($tag=='B' || $tag=='I' || $tag=='U')
			$this->SetStyle($tag,false);
		if($tag=='A')
			$this->HREF='';
		if($tag=='P')
			$this->ALIGN='';
	}

	function SetStyle($tag,$enable){
		//Modify style and select corresponding font
		$this->$tag+=($enable ? 1 : -1);
		$style='';
		foreach(array('B','I','U') as $s)
			if($this->$s>0)
				$style.=$s;
		$this->SetFont('',$style);
	}

	function PutLink($URL,$txt){
		//Put a hyperlink
		$this->SetTextColor(0,0,255);
		$this->SetStyle('U',true);
		$this->Write(5,$txt,$URL);
		$this->SetStyle('U',false);
		$this->SetTextColor(0);
	}

	//start mc_table
function SetWidths($w){
	//Set the array of column widths
	$this->widths=$w;
}

function SetAligns($a){
	//Set the array of column alignments
	$this->aligns=$a;
}

function Row($data){
	//Calculate the height of the row
	$nb=0;
	for($i=0;$i<count($data);$i++)
		$nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
	$h=5*$nb;
	//Issue a page break first if needed
	$this->CheckPageBreak($h);
	//Draw the cells of the row
	for($i=0;$i<count($data);$i++){
		$w=$this->widths[$i];
		$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
		//Save the current position
		$x=$this->GetX();
		$y=$this->GetY();
		//Draw the border
		$this->Rect($x,$y,$w,$h);
		//Print the text
		$this->MultiCell($w,5,$data[$i],0,$a);
		//Put the position to the right of the cell
		$this->SetXY($x+$w,$y);
	}
	//Go to the next line
	$this->Ln($h);
}

function CheckPageBreak($h){
	//If the height h would cause an overflow, add a new page immediately
	if($this->GetY()+$h>$this->PageBreakTrigger)
		$this->AddPage($this->CurOrientation);
}

function NbLines($w,$txt){
	//Computes the number of lines a MultiCell of width w will take
	$cw=&$this->CurrentFont['cw'];
	if($w==0)
		$w=$this->w-$this->rMargin-$this->x;
	$wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
	$s=str_replace("\r",'',$txt);
	$nb=strlen($s);
	if($nb>0 and $s[$nb-1]=="\n")
		$nb--;
	$sep=-1;
	$i=0;
	$j=0;
	$l=0;
	$nl=1;
	while($i<$nb)
	{
		$c=$s[$i];
		if($c=="\n")
		{
			$i++;
			$sep=-1;
			$j=$i;
			$l=0;
			$nl++;
			continue;
		}
		if($c==' ')
			$sep=$i;
		$l+=$cw[$c];
		if($l>$wmax){
			if($sep==-1){
				if($i==$j)
					$i++;
			}
			else
				$i=$sep+1;
			$sep=-1;
			$j=$i;
			$l=0;
			$nl++;
		}
		else
			$i++;
	}
	return $nl;
}

function myFooter(){
    // Go to 1.5 cm from bottom
    $this->SetY(-15);
    // Select Arial italic 8
    $this->SetFont('Arial','',10);
    // Print centered page number
    $this->SetTextColor(255, 0, 0);
	$this->Cell(175,3,'FAC Institute',0,1,'L');
	$this->SetTextColor(0, 0, 0);
	$this->Cell(175,3,'Lembaga Pendidikan Komputer Sistem Akuntansi ACCURATE',0,1,'L');
	$this->Cell(175,3,'Jl. Raya Jatiwaringin, No. 8 Pangkalan Jati Jakarta Timur',0,1,'L');
	$this->Cell(175,3,'Telp. 0812 900 83983 / 0821 220 48075 /​ 0823 1194 4359',0,1,'L');
	$this->SetTextColor(0, 0, 255);
	$this->Cell(175,3,'Email: training.facinstitute@gmail.com, training@fac-institute.com, cs.facinstitute@gmail.com',0,1,'L');
	$this->Cell(175,3,'Website: http://fac-institute.com, http://fac-institute.blogspot.com',0,1,'L');
}


}


class PDF_MC_Table extends FPDF
{
var $widths;
var $aligns;

function SetWidths($w){
	//Set the array of column widths
	$this->widths=$w;
}

function SetAligns($a){
	//Set the array of column alignments
	$this->aligns=$a;
}

function Row($data){
	//Calculate the height of the row
	$nb=0;
	for($i=0;$i<count($data);$i++)
		$nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
	$h=5*$nb;
	//Issue a page break first if needed
	$this->CheckPageBreak($h);
	//Draw the cells of the row
	for($i=0;$i<count($data);$i++){
		$w=$this->widths[$i];
		$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
		//Save the current position
		$x=$this->GetX();
		$y=$this->GetY();
		//Draw the border
		$this->Rect($x,$y,$w,$h);
		//Print the text
		$this->MultiCell($w,5,$data[$i],0,$a);
		//Put the position to the right of the cell
		$this->SetXY($x+$w,$y);
	}
	//Go to the next line
	$this->Ln($h);
}

function CheckPageBreak($h){
	//If the height h would cause an overflow, add a new page immediately
	if($this->GetY()+$h>$this->PageBreakTrigger)
		$this->AddPage($this->CurOrientation);
}

function NbLines($w,$txt){
	//Computes the number of lines a MultiCell of width w will take
	$cw=&$this->CurrentFont['cw'];
	if($w==0)
		$w=$this->w-$this->rMargin-$this->x;
	$wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
	$s=str_replace("\r",'',$txt);
	$nb=strlen($s);
	if($nb>0 and $s[$nb-1]=="\n")
		$nb--;
	$sep=-1;
	$i=0;
	$j=0;
	$l=0;
	$nl=1;
	while($i<$nb)
	{
		$c=$s[$i];
		if($c=="\n")
		{
			$i++;
			$sep=-1;
			$j=$i;
			$l=0;
			$nl++;
			continue;
		}
		if($c==' ')
			$sep=$i;
		$l+=$cw[$c];
		if($l>$wmax)
		{
			if($sep==-1)
			{
				if($i==$j)
					$i++;
			}
			else
				$i=$sep+1;
			$sep=-1;
			$j=$i;
			$l=0;
			$nl++;
		}
		else
			$i++;
	}
	return $nl;
}


}

$ipdf = new MyPDF();
$ipdf->Invoice();




?>