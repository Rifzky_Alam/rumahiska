<?php $base_url = 'http://salvina.id/'; ?>
<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="<?= $base_url ?>assets/common/bootstrap/css/bootstrap.min.css">
</head>
<body>
    
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">IntelliSys</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Dasbor <span class="sr-only">(current)</span></a></li>
        <li><a href="#">Profil</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">PR Menu <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Separated link</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">One more separated link</a></li>
          </ul>
        </li>
      </ul>

      <ul class="nav navbar-nav navbar-right">
        <li><a href="#">Report Bug</a></li>
        <li class="dropdown">
          <!-- <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Ak <span class="caret"></span></a> -->
          <!-- <ul class="dropdown-menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Separated link</a></li>
          </ul>
        </li>
      </ul> -->
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>


<div class="container" style="padding-bottom:50px;">
  <div class="row">
    <div class="col-md-12">
      <div class="page-header">
        <h2>Payment Request - Admin</h2>
      </div>
    </div>
  </div>


  <div class="row">
    <div class="col-md-12">
      <table class="table table-bordered">
            <thead>
              <tr>
                <th>Requester</th>
                <th>Items</th>
                <th>Total</th>
                <th>Due</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td colspan="5">Belum Ada data di database kami</td>
              </tr>
            </tbody>
      </table>  
    </div>      
  </div>
</div>

<script src="<?= $base_url ?>assets/common/bootstrap/js/jquery.js"></script>
<script src="<?= $base_url ?>assets/common/bootstrap/js/bootstrap.min.js"></script>

</body>
</html>