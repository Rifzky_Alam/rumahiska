<?php session_start(); ?>
<?php $base_url = 'http://salvina.id/'; ?>
<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="<?= $base_url ?>assets/common/bootstrap/css/bootstrap.min.css">
</head>
<body>
    

<div class="container-fluid" style="padding-top:50px;padding-bottom:20px;">
    <div class="row">
        <div class="col-sm-9 col-md-9">
            <select id="tes">
                <option id="tesopsi" value="" disabled>Tidak memilih apapun</option>
            </select>

        </div>   
    </div>
    <br>

    <div class="row">
      <div class="col-md-12">
        <span id="hehe"></span>
        <table class="table table-bordered">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama Barang</th>
              <th>Jumlah</th>
            </tr>
          </thead>
          <tbody id="itbody">
            
          </tbody>
        </table>
        <br>
        <br>
        <button id="coba">Click me!</button>
      </div>
    </div>

    <div class="row">
      <h4>Tabel Isi Troli</h4>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Nama Barang</th>
            <th>Jumlah Barang</th>
            <th>Harga Barang</th>
          </tr>
        </thead>
        <tbody id="itb">
          
        </tbody>
      </table>
    </div>
    <br><br><br>
    
</div>

<div class="container" style="padding-bottom:50px;">
  <div class="row">
      <h4>Form Tes Troli</h4>
      
        <div class="form-group">
          <label>ID Barang</label>
          <input id="idbrg" type="text" name="zz[idbarang]" class="form-control">
        </div>
        <div class="form-group">
          <label>Jumlah Barang yang dipesan</label>
          <input id="iqty" type="number" name="zz[qty]" class="form-control">
        </div>
        <button id="btns" class="btn btn-lg btn-primary" style="width:100%">Submit</button>
      
    </div>
</div>

<script src="<?= $base_url ?>assets/common/bootstrap/js/jquery.js"></script>
<script src="<?= $base_url ?>assets/common/bootstrap/js/bootstrap.min.js"></script>
<script>

  $('#coba').bind('click',function(){
    $.ajax({
      type: "GET",
      url: "<?= $base_url ?>uat/service",
      dataType: "html",
      data: {
        'ajax':'1'
      },
      cache: false,
      success: function(data){
          // $('#hehe').html($(data).filter('#jmlbrg').html());
          // console.log($(data).filter('#tabel'));
          $('#itbody').html(data);
          CountData();
      }
    }); //end ajax
  });

  $('#btns').bind('click',function(){
    $.ajax({
      type: "GET",
      url: "<?= $base_url ?>uat/service",
      dataType: "html",
      data: {
        'ib': $('#idbrg').val(),
        'bqty': $('#iqty').val()
      },
      cache: false,
      success: function(data){
        var myjson = JSON.parse(data);
        $('.hahah').remove();
        for (x in myjson.data) {
          $('#itb').append('<tr class="hahah"><td>' + myjson.data[x].nb + ' <a href="#" onclick="tesyah(this)" title="Hapus"  id="r'+myjson.data[x].aidi +'"><span class="glyphicon glyphicon-remove"></span></a></td><td>' + myjson.data[x].jml + '</td><td>'+myjson.data[x].hrg+'</td></tr>');
        }
      }
    }); //end ajax
  });

  function CountData() {
    $.ajax({
      type: "GET",
      url: "<?= $base_url ?>uat/service",
      dataType: "html",
      data: {
        'ajax2':'1'
      },
      cache: false,
      success: function(data){
          $('#hehe').html(data);
          alert(data);
          // console.log($(data).filter('#tabel'));
          // $('#itbody').html(data);
      }
    }); //end ajax
  }

  function tesyah(obj) {
    var idname = obj.id.replace('r','');
    // alert(idname);
    $.ajax({
      type: "GET",
      url: "<?= $base_url ?>uat/service",
      data: {
        'rmvitm':idname
      },
      cache: false,
      success: function(data){
        var myjson = JSON.parse(data);
        // console.log(myjson.status.code);
        $('.hahah').remove();
        for (x in myjson.data) {
          $('#itb').append('<tr class="hahah"><td>' + myjson.data[x].nb + ' <a href="#" onclick="tesyah(this)" title="Hapus"  id="r'+myjson.data[x].aidi +'"><span class="glyphicon glyphicon-remove"></span></a></td><td>' + myjson.data[x].jml + '</td><td>'+myjson.data[x].hrg+'</td></tr>');
        }
      }
    }); //end ajax
  }

  function ShowItems() {
    $.ajax({
            type: "GET",
            url: "<?= $base_url ?>uat/service",
            data: {
              'ajax2':'151b12ed4a3b78e742eead400e8040a1'
            },
            cache: false,
            success: function(data){
              var myjson = JSON.parse(data);
              // console.log(myjson.status.code);
              $('.hahah').remove();
              for (x in myjson.data) {
                $('#itb').append('<tr class="hahah"><td>' + myjson.data[x].nb + ' <a href="#" onclick="tesyah(this)" title="Hapus"  id="r'+myjson.data[x].aidi +'"><span class="glyphicon glyphicon-remove"></span></a></td><td>' + myjson.data[x].jml + '</td><td>'+myjson.data[x].hrg+'</td></tr>');
              }
            }
        }); //end ajax
  }


    $(document).ready(function(){
        ShowItems();
    });
</script>
</body>
</html>
