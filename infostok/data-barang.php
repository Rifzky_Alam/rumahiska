<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Data Barang Rumah Iska</title>
	<link rel="stylesheet" type="text/css" href="assets/bootstrap/css/bootstrap.css">
	<script type="text/javascript" src="assets/bootstrap/js/jquery-1.11.1.js"></script>
	<script type="text/javascript" src="assets/bootstrap/js/bootstrap.min.js"></script>

</head>
<body>

	<?php 
	include_once 'model/Pengguna.php';
	$user = new Pengguna(); //komentar
	$jnsBarang = json_decode($user->getJenisBarang());
	


	?>





	<div class="container" style="padding-top:40px;padding-bottom:40px;">
		<div class="row">
			<div class="page-header">
				<h3>Data Stok Barang Rumah Iska</h3>
			</div>
		</div>

		<div class="row">
			<div class="col-md-10" style="text-align:right;">
				<a class="btn btn-lg btn-primary" id="cari-data" href="#modal-cari" data-toggle="modal">Search <i class="glyphicon glyphicon-search"></i></a>
			</div>
		</div>

		<?php if (isset($_GET['v'])&&isset($_GET['nb'])&&!empty($_GET['v'])): ?>
			<?php $jenisBarang = $user->getJenisBarangByID($_GET['v'])?>			
			
			<div class="row">
				<div class="col-md-10">
					<h3><?php echo $jenisBarang->jenis_barang ?></h3>
				</div>
			</div>

			<?php $myDatas=json_decode($user->getDataBarangWithDetail($_GET['v'],$_GET['nb'])) ?>

		<div class="row">
			<div class="col-md-10">			
				<table class="table table-bordered">
					<thead>
						<tr>
							<th style="text-align:center;">No</th>
							<th style="text-align:center;">Jenis Barang</th>
							<th style="text-align:center;">Nama Barang</th>
							<th style="text-align:center;">Stok</th>
						</tr>
					</thead>
					
					<tbody>


					<?php for ($j=0; $j < count($myDatas); $j++) { ?>

						<tr>
							<td style="text-align:center;"><?php echo $j + 1 ?></td>
							<td><?php echo $jenisBarang->jenis_barang ?></td>
							<td><?php echo $myDatas[$j]->detail_jenis ?></td>
							<?php if (intval($myDatas[$j]->jumlah_barang)>0): ?>
								<td style="text-align:center;"><?= $myDatas[$j]->jumlah_barang ?></td>
								<?php else: ?>
								<td style="text-align: center;">-</td>
							<?php endif ?>
							
						</tr>							
					

					<?php }	?>

					</tbody>
				</table>
			</div>
		</div>

			<?php else: ?>


		<?php 
			for ($i=0; $i < count($jnsBarang) ; $i++) { 	
		?>

		<div class="row">
			<div class="col-md-10">
				<h3><?php echo $jnsBarang[$i]->jenis_barang ?></h3>
			</div>
		</div>

		<?php $datas = json_decode($user->getDataBarang($jnsBarang[$i]->id)); ?>
		<div class="row">
			<div class="col-md-10">			
				<table class="table table-bordered">
					<thead>
						<tr>
							<th style="text-align:center;">No</th>
							<th style="text-align:center;">Jenis Barang</th>
							<th style="text-align:center;">Nama Barang</th>
							<th style="text-align:center;">Stok</th>
						</tr>
					</thead>
					
					<tbody>


					<?php for ($j=0; $j < count($datas); $j++) { ?>

						<tr>
							<td style="text-align:center;"><?php echo $j + 1 ?></td>
							<td><?php echo $jnsBarang[$i]->jenis_barang ?></td>
							<td><?php echo $datas[$j]->detail_jenis ?></td>
							<?php if (intval($datas[$j]->jumlah_barang)>0): ?>
								<td style="text-align:center;"><?= $datas[$j]->jumlah_barang ?></td>
								<?php else: ?>
								<td style="text-align: center;">-</td>
							<?php endif ?>
							
						</tr>							
					

					<?php }	?>

					</tbody>
				</table>
			</div>
		</div>

		<?php } ?>

		<?php endif ?>
	</div>


<!-- Modal -->
  <div class='modal fade' id='modal-cari' role='dialog'>
    <div class='modal-dialog'>
    
      <!-- Modal content-->
      <div class='modal-content'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'>&times;</button>
          <h3 class="modal-title">Pencarian Data</h3>

        </div>
        <div class='modal-body'>
        <div style='padding-left:15px;padding-right:15px;padding-top:0px; height: 300px;' class='row'>
          
          <div class="col-md-12">
            
              
                <form action="" method="GET">
                    <div class="row">
                           
                        <div class="form-group">
                        	<label>Jenis Barang</label>
                            <select class='form-control' name='v'>
                                <option value="">-- Pilih jenis barang --</option>
                                <?php for ($i=0; $i < count($jnsBarang) ; $i++) { ?>
                                	<option value="<?php echo $jnsBarang[$i]->id ?>"><?php echo $jnsBarang[$i]->jenis_barang ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <label for="namaBarang">Nama / Warna Barang</label>
                            <input type="text" name="nb" id="namaBarang" class="form-control" />
                        </div>
                    </div>

                    <div class="row">
                        <button class="btn btn-success" style="width:100%">Cari</button>
                    </div>

                </form>
          </div>
             
          

        </div>
          

        </div>
        <div class='modal-footer'>
            <div class="row">
                <div class="col-md-6" style="text-align:left">
                    <span>Rumah iska 2017</span>
                </div>
                <div class="col-md-6">
                    <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                </div>
            </div>
          
        </div>
      </div>
      
    </div>
  </div>
  <!-- end modal -->

</body>
</html>