<?php 
include_once 'Koneksi.php';
class Pengguna extends Koneksi{
	
	function __construct(){
		$this->dbHost = $this->bukaKoneksi();
	}

	public function getUsername($objek){
		$query="SELECT * FROM `users` WHERE `username`=:user AND `password`=:pass";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':user',$objek->user,PDO::PARAM_STR,75);
		$statement->bindParam(':pass',$objek->pass,PDO::PARAM_STR,75);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function getDataBarangWithDetail($id,$detail){
		$query="SELECT * FROM `data_barang` WHERE `id_jenis`='$id' AND `detail_jenis` LIKE '%$detail%'";
		$statement=$this->dbHost->prepare($query);
		
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;
	}

	public function getDataBarang($id){
		$query="SELECT * FROM `data_barang` WHERE `id_jenis`=:id ORDER BY detail_jenis ASC";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':id',$id,PDO::PARAM_STR,75);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;
	}

	public function deleteBarang($id){
		$query="DELETE FROM `jenis_barang` WHERE `id`='$id';DELETE FROM `data_barang` WHERE `id_jenis`='$id'";
		$statement=$this->dbHost->prepare($query);

		if ($statement->execute()){
			return true;
		}else{
			return false;
		}
	}

	public function inputDataBarang($objek){
		$query="INSERT INTO `data_barang` (`id_jenis`, `detail_jenis`, `jumlah_barang`) 
		VALUES (:jenis, :detail, :jumlah)";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':jenis',$objek->jenis,PDO::PARAM_STR,75);
		$statement->bindParam(':detail',$objek->detail,PDO::PARAM_STR,75);
		$statement->bindParam(':jumlah',$objek->jumlah,PDO::PARAM_INT,8);

		if ($statement->execute()){
			return true;
		}else{
			return false;
		}
	}

	public function deleteDataBarang($id){
		$query = "DELETE FROM data_barang WHERE id=$id";
		$statement=$this->dbHost->prepare($query);

		if ($statement->execute()){
			return true;
		}else{
			return false;
		}

	}

	public function editDataBarang($objek){
		$query = "UPDATE `data_barang` SET `detail_jenis`=:jenis,`jumlah_barang`=:jumlah WHERE `id`=:id";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':jenis',$objek->jenis,PDO::PARAM_STR,75);
		$statement->bindParam(':id',$objek->id,PDO::PARAM_INT,8);
		$statement->bindParam(':jumlah',$objek->jumlah,PDO::PARAM_INT,8);

		if ($statement->execute()){
			return true;
		}else{
			return false;
		}
	}


	public function getdataBarangByID($id){
		$query="SELECT `data_barang`.`id` AS `id`, `jenis_barang`,`jumlah_barang`,`detail_jenis` FROM `data_barang`,`jenis_barang` WHERE `data_barang`.`id`=$id AND `data_barang`.`id_jenis`=`jenis_barang`.`id` ORDER BY detail_jenis ASC";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();

		return $statement->fetch(PDO::FETCH_OBJ);
	}

	public function getJenisBarang(){
		$query="SELECT * FROM `jenis_barang`";
		$statement=$this->dbHost->prepare($query);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;
	}


	public function getJenisBarangByID($id){
		$query = "SELECT * FROM `jenis_barang` WHERE id='$id'";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();

		return $statement->fetch(PDO::FETCH_OBJ);

	}

	public function editJenisBarang($objek){
		$query = "UPDATE `jenis_barang` SET `jenis_barang`=:nama WHERE `id`=:id";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':id',$objek->id,PDO::PARAM_STR,75);
		$statement->bindParam(':nama',$objek->nama,PDO::PARAM_STR,75);

		if ($statement->execute()){
			return true;
		}else{
			return false;
		}

	}


	public function inputJenisBarang($objek){
		$query = "INSERT INTO `jenis_barang` (`id`, `jenis_barang`) 
		VALUES (:id, :nama)";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':id',$objek->id,PDO::PARAM_STR,75);
		$statement->bindParam(':nama',$objek->nama,PDO::PARAM_STR,75);

		if ($statement->execute()){
			return true;
		}else{
			return false;
		}

	}

}




?>