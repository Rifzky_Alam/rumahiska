<?php 
session_start();
include_once '../../classes/views/class-global.php';
// include_once '/Applications/XAMPP/xamppfiles/htdocs/rikza/classes/views/class-global.php';
$data = new DataView();
$_SESSION['page'] = $data->base_url.'administrasi/produk/'.basename(__FILE__,'.php');
$data->Model('Transaksi');
$data->Lib('session/sessions');
$transaksi = new Transaksi();
$sesi = new MySession();
$sesi->OnlyAdmin($data->homedir.'administrasi/logout');

if (isset($_GET['act'])&&$_GET['act']=='new') {
	$data->Model('Customer');
	$cust = new Customer();


	if (isset($_POST['in'])) {
		// print_r($_POST['in']);
		$transaksi->setCustomer($_POST['in']['email']);
		if ($transaksi->NewAdminTransaction()) {
			echo "<script>alert('Data berhasil disimpan!');</script>";
			echo "<script>location.replace('".$data->base_url."administrasi/transaksi');</script>";
		} else {
			
		}
	}


	$data->judul="RumahIska - Data Transaksi";
	$data->subtitle="Transaksi Baru";
	$data->nama_admin=$_SESSION['admin']['nama'];
	$data->listcust = $cust->GetCustomer();
	$data->me=$data->base_url.'administrasi/transaksi/';
	$data->package="administrasi";
	$data->View('administrasi/transaksi/vinput.transaksi.php',$data);
}elseif (isset($_GET['edt'])&&!empty($_GET['edt'])) {
	$data->Model('Customer');
	$cust = new Customer();

	if (isset($_POST['in'])) {
		// print_r($_POST['in']);
		$transaksi->setId($_GET['edt']);
		$transaksi->setCustomer($_POST['in']['email']);

		if ($transaksi->UpdateCustTransaksi()) {
			echo "<script>alert('Data berhasil diubah!');</script>";
			echo "<script>location.replace('".$data->base_url."administrasi/transaksi');</script>";
		} else {
			echo "<script>alert('Data gagal diubah!');</script>";
		}

	}

	$data->judul="RumahIska - Transaksi - Edit Customer";
	$data->subtitle="Edit Transaksi";
	$data->nama_admin=$_SESSION['admin']['nama'];
	$transaksi->setId($_GET['edt']);
	$datatransaksi = $transaksi->FetchTransByID();
	$data->customer = $datatransaksi->tr_id_cust;
	$data->listcust = $cust->GetCustomer();
	$data->me=$data->base_url.'administrasi/transaksi/';
	$data->package="administrasi";
	$data->View('administrasi/transaksi/vedit.transaksi.php',$data);
} else {
	$data->judul="RumahIska - Data Transaksi";
	$data->subtitle="Data Transaksi";
	$data->nama_admin=$_SESSION['admin']['nama'];
	$data->lists = $transaksi->DisplayTransactions();
	$data->me=$data->base_url.'administrasi/transaksi/';
	$data->package="administrasi";
	$data->View('administrasi/transaksi/vdtransaksi.php',$data);
}

?>