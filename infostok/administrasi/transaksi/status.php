<?php 

session_start();
include_once '../../classes/views/class-global.php';
$data = new DataView();
$data->Model('Querybuilder');
$data->Model('Transaksi');
$trans = new Transaksi();
$db = new QueryBuilder();
@$url_segment = explode('/', trim($_SERVER['PATH_INFO'],'/'));
if (isset($url_segment)) {
	$key = array_shift($url_segment);
	switch ($key) {
		case 'cancel':
			$data->CheckSegment($url_segment[0]);
			if ($db->UpdateData('ri_transaksi',['tr_status'=>'-1'],array($db->GetCond('tr_id','=',$url_segment[0])))) {
				header('Location:'.$data->base_url.'administrasi/transaksi/');
			} else {
				echo "<script>alert('Terdapat kesalahan, Harap hubungi admin sistem :(');</script>";
				echo "<script>location.replace('".$data->base_url."administrasi/transaksi');</script>";
			}
			break;
		case 'kirimfaktur':
			$data->CheckSegment($url_segment[0]);
			if ($db->UpdateData('ri_transaksi',['tr_status'=>'1'],array($db->GetCond('tr_id','=',$url_segment[0])))) {
				header('Location:'.$data->base_url.'administrasi/transaksi/');
			} else {
				echo "<script>alert('Terdapat kesalahan, Harap hubungi admin sistem :(');</script>";
				echo "<script>location.replace('".$data->base_url."administrasi/transaksi');</script>";
			}
			break;
		case 'uploadbukti':
			$data->CheckSegment($url_segment[0]);
			if ($db->UpdateData('ri_transaksi',['tr_status'=>'2'],array($db->GetCond('tr_id','=',$url_segment[0])))) {
				header('Location:'.$data->base_url.'administrasi/transaksi/');
			} else {
				echo "<script>alert('Terdapat kesalahan, Harap hubungi admin sistem :(');</script>";
				echo "<script>location.replace('".$data->base_url."administrasi/transaksi');</script>";
			}
			break;
		case 'pengiriman':
			$data->CheckSegment($url_segment[0]);
			if ($db->UpdateData('ri_transaksi',['tr_status'=>'3'],array($db->GetCond('tr_id','=',$url_segment[0])))) {
				$carts = $db->FetchWhere(['cart_items','qty'],'ri_carts',array($db->GetCond('cart_source','=',$url_segment[0])));
				foreach ($carts as $key) {
					// echo $key['cart_items'].' '.$key['qty'];
					$trans->ReduceItem($key['cart_items'],$key['qty']);
				}
				header('Location:'.$data->base_url.'administrasi/transaksi/');
			} else {
				echo "<script>alert('Terdapat kesalahan, Harap hubungi admin sistem :(');</script>";
				echo "<script>location.replace('".$data->base_url."administrasi/transaksi');</script>";
			}
			break;
		case '':
			header('Location:'.$data->base_url.'administrasi/transaksi/');
			break;
	}
}