<?php 
@$url_segment = explode('/', trim($_SERVER['PATH_INFO'],'/'));
session_start();
include_once '/home/rumahisk/public_html/classes/views/class-global.php';
// include_once '/Applications/XAMPP/xamppfiles/htdocs/rikza/classes/views/class-global.php';
$data = new DataView();
$data->Model('Transaksi');
$trans = new Transaksi();
if (isset($url_segment)) {
	$key = array_shift($url_segment);
	switch ($key) {
		case 'upload':
			$data->CheckSegment($url_segment[0]);

			if (isset($_FILES['bukti'])) {
				$data->Lib('otherfunction/Picturefunc');
				$_FILES['bukti']['name'] = $url_segment[0] . '.' . GetFileExtension($_FILES['bukti']);
				$data->uploadfolder = BuktiTransaksiFolder($data->homedir);
				if (UploadPic($_FILES['bukti'],$data->uploadfolder)) {
					if ($trans->UpdateSingleData(['atr' => 'tr_bukti_upload','atrval' => $_FILES['bukti']['name'],'key' => 'tr_id','cond' => '=','keyval' => $url_segment[0]])) {
						echo "<script>alert('Data Berhasil disimpan.');</script>";
						echo "<script>location.replace('".$data->base_url."administrasi/transaksi/');</script>";
					} else {
						echo "<script>alert('Data Gagal disimpan.');</script>";
					}
				}else{
					echo "<script>alert('Terdapat kesalahan dalam menyimpan file gambar.');</script>";
				}
			}


			$data->judul="RumahIska - Data Transaksi";
			$data->subtitle="Data Transaksi";
			$data->nama_admin=$_SESSION['admin']['nama'];
			$data->me = $data->base_url.'administrasi/transaksi/bukti/upload/'.$url_segment[0];
			$data->package="administrasi";
			$data->View('administrasi/transaksi/uploadconfirm.php',$data);
			break;
	}
}