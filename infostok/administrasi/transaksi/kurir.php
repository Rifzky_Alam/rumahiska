<?php 
@$url_segment = explode('/', trim($_SERVER['PATH_INFO'],'/'));
session_start();
include_once '/home/rumahisk/public_html/classes/views/class-global.php';
// include_once '/Applications/XAMPP/xamppfiles/htdocs/rikza/classes/views/class-global.php';
$data = new DataView();
$data->Lib('session/sessions');
$sesi = new MySession();
$sesi->OnlyAdmin($data->base_url.'administrasi/logout');
$data->Model('Transaksi');
$trans = new Transaksi();
$data->Model('Kurir');
$kurir = new Kurir();

$data->nama_admin=$_SESSION['admin']['nama'];
$data->package="administrasi/transaksi/";
if (isset($url_segment)) {
	$key = array_shift($url_segment);
	switch ($key) {
		case 'input':

			if (isset($_FILES['in'])) {
				print_r($_FILES['in']);
			}

			$data->judul="RumahIska - Data Transaksi";
			$data->subtitle="Data Transaksi";
			$data->me = $data->base_url.'administrasi/transaksi/kurir/input';
			
			echo 'Input';
			break;
		case 'edit':
			$data->CheckSegment($url_segment[0]);
			$trans->setId($url_segment[0]);

			$data->me = $data->base_url.'administrasi/transaksi/kurir/edit/'.$url_segment[0];

			if (isset($_POST['in'])) {
				$kurir->setIdtrans($url_segment[0]);
				$kurir->setJasa($_POST['in']['jasa']);
				$kurir->setLayanan($_POST['in']['service']);
				$kurir->setAlamat($_POST['in']['alamat']);
				$kurir->setProvinsi($_POST['in']['prov']);
				$kurir->setKota($_POST['in']['kota']);
				$kurir->setOngkir($_POST['in']['ongkir']);
				if ($kurir->UpdateData()) {
					echo "<script>alert('Data Berhasil disimpan');</script>";
					echo "<script>location.replace('".$data->base_url."administrasi/transaksi/');</script>";
				} else {
					echo "<script>alert('Data Gagal disimpan');</script>";
				}
			}


			$data->listkurir=$kurir->FetchKurir();
			$data->judul="RumahIska - Data Kurir";
			$data->subtitle="Data Kurir";
			$data->listalamat = $trans->FetchAlamatForKurir();
			$data->totalhargabarang = 0;
			$data->beratbarang = $trans->FetchWeightOfItem();
			$data->listalamat = $trans->FetchAlamatForKurir();
			$datakurir = $kurir->FetchOneRow(['atr'=>'`rk_id_trans`','cond'=>'=','value'=>$url_segment[0]]);

			$data->idtrans = $url_segment[0];
			@$data->jasa=$datakurir->rk_kode_jasa;
			@$data->layanan=$datakurir->rjk_kd_svc;
			@$data->alamat = $datakurir->alamat;
			@$data->provinsi = $datakurir->rk_prov;
			@$data->kota = $datakurir->rk_kota;
			@$data->ongkir=$datakurir->rk_ongkir;
			@$data->resi=$datakurir->rk_resi;
			$data->View('administrasi/transaksi/kurir/vedit.kurir.php',$data);

			break;

		case 'editresi':
			$data->CheckSegment($url_segment[0]);
			echo "<script>location.replace('".$data->base_url.'administrasi/transaksi/resi?tr='.$url_segment[0]."');</script>";
			break;

		case 'view':
			$data->CheckSegment($url_segment[0]);
			
			$kurir->setIdtrans($url_segment[0]);

			if (isset($_POST['in'])) {
				
				$kurir->setJasa($_POST['in']['jasa']);
				$kurir->setLayanan($_POST['in']['service']);
				$kurir->setAlamat($_POST['in']['alamat']);
				$kurir->setProvinsi($_POST['in']['prov']);
				$kurir->setKota($_POST['in']['kota']);
				$kurir->setOngkir($_POST['in']['ongkir']);

				if ($kurir->NewData()) {
					echo "<script>alert('Data Berhasil disimpan');</script>";
					echo "<script>location.replace('".$data->base_url."administrasi/transaksi/');</script>";
				} else {
					echo "<script>alert('Data Gagal disimpan');</script>";
				}
				
			}
			$trans->setId($url_segment[0]);
			if ($trans->CountItem()=='0') {
				echo "<script>alert('Tidak ada Data barang, harap untuk memasukkan item ke transaksi ini terlebih dahulu.');</script>";
				echo "<script>location.replace('".$data->base_url."administrasi/transaksi/carts/add/".$url_segment[0]."');</script>";
			}

			$data->listkurir=$kurir->FetchKurir();
			// print_r($data->listkurir);
			$data->judul="RumahIska - Data Kurir";
			$data->subtitle="Data Kurir";
			$data->listalamat = $trans->FetchAlamatForKurir();
			$data->totalhargabarang = 0;
			$data->beratbarang = $trans->FetchWeightOfItem();
			if (count($data->listkurir)=='0') {
				$data->View('administrasi/transaksi/kurir/vdisplay.kurir.php',$data);
			}else{
				echo "<script>location.replace('".$data->base_url."administrasi/transaksi/kurir.php/edit/".$url_segment[0]."');</script>";			
			}
			
			break;
	}
}
?>