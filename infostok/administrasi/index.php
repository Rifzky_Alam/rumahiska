<?php 
session_start();
include_once '../baseurl.php';
include_once HomeDir().'library/session/sessions.php';
include_once HomeDir().'model/Querybuilder.php';
$db = new QueryBuilder();
$sesi = new MySession();
$sesi->OnlyAdmin(getBaseUrl().'administrasi/logout');

$data = array(
	'base_url' => getBaseUrl(),
	'judul' => 'RumahIska',
	'package' => 'administrasi',
	'nama_admin' => $_SESSION['admin']['nama'] 
);

$data['jumlahtransaksi'] = $db->FetchCount('tr_id','ri_transaksi');
$data['jumlahartikel'] = $db->FetchCount('a_id','artikel');;
$data['feedbacks'] = $db->Fetch('feedback_cust',0,30);

$data = (object) $data;
include_once HomeDir().'/view/administrasi/dasbor.php'; 

?>