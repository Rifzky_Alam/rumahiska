<?php 
session_start();
// include_once '/Applications/XAMPP/xamppfiles/htdocs/rikza/baseurl.php';
include_once '/home/rumahisk/public_html/baseurl.php';
$_SESSION['page'] = getBaseUrl().'administrasi/produk/'.basename(__FILE__,'.php');

include_once HomeDir().'library/session/sessions.php';
$sesi = new MySession();
$sesi->OnlyAdmin(getBaseUrl().'administrasi/logout');

include_once HomeDir().'model/Barang.php';
$barang = new Barang();





if (isset($_GET['idb'])&&!empty($_GET['idb'])) {
	$barang->setID($_GET['idb']);

	if (isset($_POST['edtimg'])) {
		$barang->setGambar($_POST['edtimg']['idimg']);
		$barang->setCatatan($_POST['edtimg']['ketimg']);
		if ($barang->EditKeteranganGambar()) {
			echo "<script>alert('Keterangan gambar telah diubah!');</script>";
			echo "<script>location.replace('barang-gallery?idb=".$barang->getID()."');</script>";
		} else {
			echo "<script>alert('Keterangan gambar gagal diubah!');</script>";
		}
	}



	if (isset($_POST['mkdef'])) {
		$barang->setGambar($_POST['mkdef']);
		if ($barang->SetDefaultImage()) {
			echo "<script>alert('Gambar default telah disimpan!');</script>";
			echo "<script>location.replace('barang-gallery?idb=".$barang->getID()."');</script>";
		} else {
			echo "<script>alert('Gambar default gagal disimpan!');</script>";
		}
	}


	if (isset($_POST['delimg'])){
		$barang->setGambar($_POST['delimg']);
		$datagambar = $barang->FetchGambarByID();
		$barang->setCatatan($datagambar->bg_name);
		if ($barang->DeleteImage()) {
			echo "<script>alert('Data berhasil dihapus');</script>";
			echo "<script>location.replace('barang-gallery?idb=".$barang->getID()."');</script>";
		} else {
			echo "<script>alert('Data gagal dihapus');</script>";
		}
	}


	if (isset($_FILES['gambar'])&&!empty($_FILES['gambar'])&&isset($_POST['in'])) {
		$barang->setCatatan($_POST['in']['ket']);
		$image_info = getimagesize($_FILES["gambar"]["tmp_name"]);
		$image_width = $image_info[0];
		$image_height = $image_info[1];
		if ($image_width<350||$image_height<400) {
			echo "<script>alert('Harap upload gambar lebar minimal:350px dan tinggi minimal: 400px');</script>";
		} else {
			$barang->setGambar($_FILES['gambar']);
			if ($barang->IsGambarValid()) {
				if ($barang->UploadImage()) {
					if ($barang->NewImage()) {
						echo "<script>alert('Data berhasil disimpan');</script>";
						echo "<script>location.replace('barang-gallery?idb=".$barang->getID()."');</script>";
					} else {
						echo "<script>alert('Data gagal disimpan, harap hubungi admin sistem kami..');</script>";
					}
				} else {
					echo "<script>alert('Gambar gagal di upload :(');</script>";
				}
			} else {
				echo "<script>alert('Hanya File .jpg/.png/jpeg yang boleh di upload');</script>";
			}
		}
	}

	$databrg=$barang->FetchBarangByID();
	$data = array(
		'base_url' => getBaseUrl(),
		'judul' => 'Data Gambar Barang',
		'package' => 'administrasi',
		'nama_admin' => $_SESSION['admin']['nama'],
		'me'=> getcwd().basename(__FILE__,'.php'),
		'pathfolder'=> getBaseUrl().'uploads/images/produk/',
		'kategori'=> $databrg->kb_ket,
		'jenisbarang'=> $databrg->jenis_barang,
		'namabarang'=> $databrg->detail_jenis,
		'defaultgbr'=> $databrg->brg_pic,
		'foto' => $barang->FetchDataGambar()
		// 'list' => $barang->DisplayTable(),
		// 'kategori' => $barang->FetchAllKategori()
	);
	$data = (object) $data;
	include_once HomeDir().'view/administrasi/produk/view-modal-produk.php';
	include_once HomeDir().'view/administrasi/produk/view-gallery-barang.php';
} else {
	
}

 ?>