<?php
session_start();
// include_once '/Applications/XAMPP/xamppfiles/htdocs/rikza/baseurl.php';
include_once '../../baseurl.php';
$_SESSION['page'] = getBaseUrl().'administrasi/produk/'.basename(__FILE__,'.php');
include_once HomeDir().'model/Barang.php';
include_once HomeDir().'library/session/sessions.php';
$sesi = new MySession();
$sesi->OnlyAdmin(getBaseUrl().'administrasi/logout');
$barang = new Barang();

if (isset($_GET['act'])&&$_GET['act']=='new') {

	if (isset($_POST['in'])) {
		// print_r($_POST['in']);
		$barang->setModeljenis($_POST['in']['jenis']);
		$barang->setKategori($_POST['in']['kategori']);
		$barang->setStatusbarang($_POST['in']['status']);

		if ($barang->NewJenisBarang()) {
			echo "<script>alert('data berhasil disimpan!');</script>";
			echo "<script>location.replace('".basename(__FILE__,'.php')."');</script>";
		} else {
			echo "<script>alert('data gagal disimpan!');</script>";
		}

	}

	$data = array(
		'base_url' => getBaseUrl(),
		'judul' => 'Input Model/Jenis Barang',
		'package' => 'administrasi',
		'me'=>basename(__FILE__,'.php'),
		'nama_admin' => $_SESSION['admin']['nama'],
		'datastatus' => $barang->FetchStatusJenisBarang(),
		'datakategori' => $barang->FetchAllKategori()
	);
	$data = (object) $data;
	include_once HomeDir().'/view/administrasi/produk/view-input-jenisbarang.php';

}elseif (isset($_GET['edt'])&&!empty($_GET['edt'])) {
	// echo "edit data here!";

	if (isset($_POST['in'])) {
		// print_r($_POST['in']);
		$barang->setData($_GET['edt']);
		$barang->setModeljenis($_POST['in']['jenis']);
		$barang->setKategori($_POST['in']['kategori']);
		$barang->setStatusbarang($_POST['in']['status']);
		$barang->setID($_POST['in']['defitem']);

		if ($barang->EditJenisBarang()) {
			echo "<script>alert('data berhasil diubah!');</script>";
			echo "<script>location.replace('".basename(__FILE__,'.php')."');</script>";
		} else {
			echo "<script>alert('data gagal diubah!');</script>";
		}

	}

	$barang->setModeljenis($_GET['edt']);
	$datamodel = $barang->FetchJenisBarangByID();
	$barang->setKategori($datamodel->kb_id);
	$data = array(
		'base_url' => getBaseUrl(),
		'judul' => 'Input Model/Jenis Barang',
		'package' => 'administrasi',
		'me'=>basename(__FILE__,'.php'),
		'nama_admin' => $_SESSION['admin']['nama'],
		'datakategori' => $barang->FetchAllKategori(),
		'datastatusjb' => $barang->FetchStatusJenisBarang(),
		'statusjb' => $datamodel->jb_status,
		'defitem' => $datamodel->jb_def_item,
		'kategori' => $datamodel->kb_id,
		'listbarang' => $barang->FetchDetailBarangByJenisBarang(),
		'jenisbarang'=>$datamodel->jenis_barang
	);

	// print_r($data['datakategori']);
	$data = (object) $data;
	include_once HomeDir().'/view/administrasi/produk/view-edit-jenisbarang.php';

} else {
	$data = array(
		'base_url' => getBaseUrl(),
		'judul' => 'Data Model/Jenis Barang',
		'package' => 'administrasi',
		'me'=>basename(__FILE__,'.php'),
		'nama_admin' => $_SESSION['admin']['nama'],
		'list'=> $barang->FetchJenisBarang()
	);
	$data = (object) $data;
	include_once HomeDir().'/view/administrasi/produk/view-jenis-barang.php';

}


?>
