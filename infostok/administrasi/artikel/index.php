<?php 
session_start();
include_once '../../baseurl.php';
include_once HomeDir().'library/session/sessions.php';
include_once HomeDir().'model/Artikel.php';
$sesi = new MySession();
$sesi->OnlyAdmin(getBaseUrl().'administrasi/logout');
$artikel = new Artikel();


$data = array(
	'base_url' => getBaseUrl(),
	'judul' => 'RumahIska',
	'package' => 'administrasi',
	'nama_admin' => $_SESSION['admin']['nama'],
	'list' => $artikel->FetchArtikelData()
);
// print_r($data['list']);
$data = (object) $data;
include_once HomeDir().'/view/administrasi/artikel/view-data-artikel.php'; 

?>