<?php 
session_start();
include_once '../../library/controller/distributor.controller.php';

if (isset($_SERVER['PATH_INFO'])) {
	$_SERVER['PATH_INFO'] = str_replace('.php', '', $_SERVER['PATH_INFO']);
	@$url_segment = explode('/', trim($_SERVER['PATH_INFO'],'/'));
	$key = array_shift($url_segment);

	switch ($key) {
		case 'changestatus':
			if (!isset($url_segment[0])||!isset($url_segment[1])) {
				header('Location: '.Distributor::BaseUrl().'administrasi/agen/listnotif');
			}
			Distributor::Changestatus($url_segment[0],$url_segment[1]);
			break;
		case 'delete':
			if (!isset($url_segment[0])) {
				header('Location: '.Distributor::BaseUrl().'administrasi/agen/listnotif');
			}
			Distributor::DeleteNotif($url_segment[0]);
			break;
		case '':
			Distributor::Notifikasi();
			break;

	}
}else{
	Distributor::Notifikasi();	
}
