<?php session_start(); ?>
<?php 
error_reporting(E_ALL);
ini_set('display_errors', 1);
?>
<?php if (!isset($_SESSION['admin'])&&empty($_SESSION['admin']['username'])): ?>
	<?php header('location: index.php'); ?>
<?php endif ?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Data Barang Rumah Iska</title>
	<link rel="stylesheet" type="text/css" href="assets/bootstrap/css/bootstrap.css">
	<script type="text/javascript" src="assets/bootstrap/css/jquery-1.11.1.js"></script>
	<script type="text/javascript" src="assets/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

	<?php include_once 'header.php'; ?>
	<div class="container">
		<div class="row">
			<div class="page-header">
				<h3>Data Stok Barang Rumah Iska</h3>
			</div>
		</div>

		<?php 
			include_once 'model/Pengguna.php';
			$user= new Pengguna();
		?>
		<?php if (isset($_GET['app'])&&$_GET['app']=='2'): ?>
			

			<?php 

			$jnsBarang = json_decode($user->getJenisBarang());
			if (isset($_POST['jn'])) {
				
				
				$input = array(
							'id' => md5($_POST['jn']['nama']), 
							'nama'=>$_POST['jn']['nama']
				);

				$input = (object) $input;

				if ($user->inputJenisBarang($input)) {
					echo "<script>alert('Data berhasil disimpan');</script>";
					echo "<script>location.replace('".$_SERVER['SCRIPT_NAME']."');</script>";	
				}else{
					echo "<script>alert('Data gagal disimpan');</script>";
				}
			}


			?>



		<form action="" method="post">
		
		<div class="row">
			<div class="col-md-10">
				<div class="form-group">
					<label>Jenis Barang</label>
					<input type="text" name="jn[nama]" class="form-control">
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-10">
				<button class="btn btn-lg btn-success">Submit</button>
			</div>
		</div>


		</form>		

		<br><br>
		<div class="row">
			<div class="col-md-12">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th style="text-align:center;">Jenis Barang</th>
							<th style="text-align:center;">Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?php for ($i=0; $i < count($jnsBarang) ; $i++) { ?>
							<tr>
								<td style="width:85%;"><?php echo $jnsBarang[$i]->jenis_barang ?></td>								
								<td style="text-align:center;">
									<a href="<?php echo 'editdeletedata.php?app=edtjns&id='.$jnsBarang[$i]->id ?>" class="btn btn-sm btn-warning">Edit</a>
									||
									<a href=<?php echo "'".$_SERVER['SCRIPT_NAME']."?app=4&id=".$jnsBarang[$i]->id."'"; ?> class="btn btn-sm btn-danger">Delete</a>
								</td>


							</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>

			<?php elseif(isset($_GET['app'])&&$_GET['app']=='3'&&isset($_GET['id'])): ?>


			<div class="row" style="display:none;">
				<div class="form-group col-md-10">
					<input type="text" name="ch[id]" class="form-control">
				</div>
			</div>

			<div class="row">
				<div class="form-group col-md-10">
					<label>Nama Item</label>
					<input type="text" name="ch[item]" class="form-control">
				</div>
			</div>

			<div class="row">
				<div class="col-md-10">
					<button class="btn btn-primary">submit</button>
				</div>
			</div>

		<?php elseif(isset($_GET['app'])&&$_GET['app']=='4'&&isset($_GET['id'])): ?>
			<?php if (isset($_SESSION['admin'])&&!empty($_SESSION['admin']['username'])) {
				if ($user->deleteBarang($_GET['id'])){
					echo "<script>alert('Data berhasil dihapus');</script>";
					echo "<script>location.replace('".$_SERVER['SCRIPT_NAME']."');</script>";
				}else{
					echo "<script>alert('Data gagal dihapus');</script>";
				}
			} ?>

			<?php else: ?>
				<?php 
				include_once 'model/Pengguna.php'; 
				$user = new Pengguna(); 
				$dataJenis = json_decode($user->getJenisBarang());


				if (isset($_POST['in'])) {
					$input = array(
						'jenis' => $_POST['in']['barang'],
						'detail'=> $_POST['in']['merk'],
						'jumlah'=>$_POST['in']['jumlah']
					 );

					$input = (object) $input;

					if ($user->inputDataBarang($input)) {
						echo "<script>alert('Data berhasil disimpan');</script>";
						echo "<script>location.replace('".$_SERVER['SCRIPT_NAME']."');</script>";
					}else{
						echo "<script>alert('Data gagal disimpan');</script>";
					}


				}



				?>

	<form action="" method="post">
		
		<div class="row">
			<div class="col-md-10">
				<div class="form-group">
					<label><a href= <?php echo "'".$_SERVER['SCRIPT_NAME']."?app=2'"; ?>>Jenis Barang (klik untuk tambah data)</a></label>
					<select name="in[barang]" class="form-control">
						<option value="">Pilih Jenis Barang</option>
						<?php for ($i=0; $i < count($dataJenis); $i++) { ?>
						<option value=<?php echo "'".$dataJenis[$i]->id."'"; ?>><?php echo $dataJenis[$i]->jenis_barang; ?></option>
						<?php } ?>
					</select>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-10">
				<div class="form-group">
					<label>Nama Barang</label>
					<input type="text" name="in[merk]" class="form-control">
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-10">
				<div class="form-group">
					<label>Jumlah Barang</label>
					<input type="number" name="in[jumlah]" class="form-control">
				</div>
			</div>
		</div>


		<div class="row">
			<div class="col-md-10">
				<button class="btn btn-lg btn-primary" style="width:100%">Submit</button>	
			</div>
			
		</div>


	</form>

		<?php endif ?>

	
	</div>
</body>
</html>