<?php session_start(); ?>

<?php if (!isset($_SESSION['admin'])&&empty($_SESSION['admin']['username'])): ?>
	<?php header('location: index.php'); ?>
<?php endif ?>


<!DOCTYPE html>
<html>
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Info Stok - Home</title>
	<link rel="stylesheet" type="text/css" href="assets/bootstrap/css/bootstrap.css">
	<script type="text/javascript" src="assets/bootstrap/css/jquery-1.11.1.js"></script>
	 <script type="text/javascript" src="assets/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<?php include_once 'header.php'; ?>

	<?php 
	include_once 'model/Pengguna.php';
	$user = new Pengguna(); //komentar
	$jnsBarang = json_decode($user->getJenisBarang());
	


	?>

	<div class="container" style="padding-top: 40px">
		
		<div class="row">
			<div class="page-header">
				<h3>Data Stok Barang Rumah Iska</h3>
			</div>
		</div>

		<?php if (isset($_GET['app'])&&$_GET['app']=='edit'&&isset($_GET['id'])&&!empty($_GET['id'])): ?>
			<?php $dataz = $user->getdataBarangByID($_GET['id']) ?>
			<h3><?php echo $dataz->jenis_barang ?></h3>

			<?php 
			if (isset($_POST['edit'])) {

				$objek = array('jenis' => $_POST['edit']['detailJenis'] ,'jumlah' => $_POST['edit']['jumlah'],'id' => $_POST['edit']['id']);
				$objek = (object) $objek;
				/*echo "id: ".$_POST['edit']['id']."<br>";
				echo "Detail Jenis: ".$_POST['edit']['detailJenis']."<br>";
				echo "Jumlah Barang: ".$_POST['edit']['jumlah']."<br>";*/
				if ($user->editDataBarang($objek)) {
					echo "<script>alert('Data berhasil di ubah');</script>";
					echo "<script>location.replace('".$_SERVER['SCRIPT_NAME']."');</script>";
				}else{
					echo "<script>alert('Data gagal di ubah');</script>";
				}


			}

			?>

			<form action="" method="post">
			<div class="row">
				<div class="form-group col-md-10">
					<label>Nama</label>
					<input type="text" name="edit[id]" style="display:none;" value="<?php echo $dataz->id ?>">
					<input type="text" name="edit[detailJenis]" class="form-control" value="<?php echo $dataz->detail_jenis ?>">
				</div>
			</div>

			<div class="row">
				<div class="form-group col-md-10">
					<label>Jumlah Barang</label>
					<input type="text" name="edit[jumlah]" class="form-control" value="<?php echo $dataz->jumlah_barang ?>">
				</div>
			</div>

			<div class="row">
				<div class="col-md-10">
					<button class="btn btn-lg btn-warning" style="width:100%">Ubah Data</button>
				</div>
			</div>
			</form>
			<?php else: ?>
		







		<?php 
			for ($i=0; $i < count($jnsBarang) ; $i++) { 	
		?>

		<div class="row">
			<div class="col-md-10">
				<h3><?php echo $jnsBarang[$i]->jenis_barang ?></h3>
			</div>
		</div>
		<?php $datas = json_decode($user->getDataBarang($jnsBarang[$i]->id)); ?>
		<div class="row">
			<div class="col-md-10">			
				<table class="table table-bordered">
					<thead>
						<tr>
							<th style="text-align:center;">No</th>
							<th style="text-align:center;">Jenis Barang</th>
							<th style="text-align:center;">Nama Barang</th>
							<th style="text-align:center;">Stok</th>
							<th style="text-align:center;">Aksi</th>
						</tr>
					</thead>
					
					<tbody>


					<?php for ($j=0; $j < count($datas); $j++) { ?>

						<tr>
							<td style="text-align:center;"><?php echo $j + 1 ?></td>
							<td><?php echo $jnsBarang[$i]->jenis_barang ?></td>
							<td><?php echo $datas[$j]->detail_jenis ?></td>
							<td style="text-align:center;"><?php echo $datas[$j]->jumlah_barang ?></td>
							<td style="text-align: center;">
								<a href="<?php echo $_SERVER['SCRIPT_NAME'].'?app=edit&id='.$datas[$j]->id ?>">
									Edit
								</a>
								||
								<a href="<?php echo 'editdeletedata.php?app=deldt&id='.$datas[$j]->id ?>">
									Delete
								</a>
							</td>
						</tr>							
					

					<?php }	?>

					</tbody>
				</table>
			</div>
		</div>


		<?php } ?>


		<?php endif ?>
	</div>

</body>
</html>