<?php session_start(); ?>
<?php 
error_reporting(E_ALL);
ini_set('display_errors', 1);
?>
<?php if (!isset($_SESSION['admin'])&&empty($_SESSION['admin']['username'])): ?>
	<?php header('location: index.php'); ?>
<?php endif ?>



<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Data Barang Rumah Iska</title>
	<link rel="stylesheet" type="text/css" href="assets/bootstrap/css/bootstrap.css">
	<script type="text/javascript" src="assets/bootstrap/css/jquery-1.11.1.js"></script>
	<script type="text/javascript" src="assets/bootstrap/js/bootstrap.min.js"></script>

</head>
<body>

	<?php 
	include_once 'model/Pengguna.php';
	$user = new Pengguna(); //komentar
	$jnsBarang = json_decode($user->getJenisBarang());
	


	?>

	<div class="container" style="padding-top:40px;padding-bottom:40px;">
		<div class="row">
			<div class="page-header">
				<h3>Ubah / Hapus Data Stok Barang Rumah Iska</h3>
			</div>
		</div>
		<?php if (isset($_GET['app'])&&$_GET['app']=='deldt'&&isset($_GET['id'])&&!empty($_GET['id'])): ?>
		<?php $dataz = $user->getdataBarangByID($_GET['id']) ?>	
		
		<?php 
		if (isset($_POST['del'])) {
			// echo $_POST['del']['id'];
			if ($user->deleteDataBarang($_POST['del']['id'])) {
				echo "<script>alert('Data berhasil di hapus');</script>";
				echo "<script>location.replace('stok-barang.php');</script>";
			}else{
				echo "<script>alert('Data gagal di hapus');</script>";
			}
		}

		?>

		<div class="row">
			<div class="col-md-10">
				<h3>Anda yakin ingin menghapus data ini?<h3>
			</div>
		</div>
		<form action="" method="post">
		<div class="row">
			<div class="col-md-10">			
				<table class="table table-hover">
					<thead>
						<tr>
							<th>Jenis Barang</th>
							<th>Nama Barang</th>
							<th>Jumlah Stok Barang</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><?php echo $dataz->jenis_barang ?></td>
							<td><?php echo $dataz->detail_jenis ?></td>
							<td><?php echo $dataz->jumlah_barang ?></td>
						</tr>
					</tbody>
				</table>
				<input  type="text" name="del[id]" value="<?php echo $dataz->id ?>" style="display:none;">
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-10">
				<button class="btn btn-lg btn-danger" style="width:100%">Delete Data</button>
			</div>
		</div>
		</form>

		<?php elseif(isset($_GET['app'])&&$_GET['app']=='edtjns'&&isset($_GET['id'])&&!empty($_GET['id'])): ?>
			<?php $mydata = $user->getJenisBarangByID($_GET['id']); ?>
			<?php if (isset($_POST['edt'])) {
				/*
				echo "ID Jenis Barang: ".$_POST['edt']['id']."<br>";
				echo "Nama Jenis Barang: ".$_POST['edt']['nama']."<br>";
				*/
				$objek = array('id' => $_POST['edt']['id'],'nama'=> $_POST['edt']['nama']);
				$objek = (object) $objek;
				if ($user->editJenisBarang($objek)) {
					echo "<script>alert('Data berhasil di ubah');</script>";
					echo "<script>location.replace('input-barang.php');</script>";
				}else{
					echo "<script>alert('Data gagal di ubah!');</script>";
				}

			} ?>


			<form action="" method="post">
			<div class="row">
				<div class="form-group col-md-10">
					<label>Nama Jenis Barang</label>
					<input type="text" name="edt[id]" value="<?php echo $mydata->id ?>" style="display:none;" >
					<input class="form-control" type="text" name="edt[nama]" value="<?php echo $mydata->jenis_barang ?>">
				</div>
			</div>

			<div class="row">
				<div class="col-md-10">
					<button class="btn btn-lg btn-warning" style="width:100%">Ubah Data</button>
				</div>
			</div>
			</form>
		<?php endif ?>
	</div>
</body>
</html>