<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<?php include_once 'baseurl.php'; ?>
<html>
<head>
<title></title>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<!-- Custom Theme files -->
<!--theme-style-->
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--theme-style-->
<link href="css/style4.css" rel="stylesheet" type="text/css" media="all" />
<link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
<!--//theme-style-->
<script src="js/jquery.min.js"></script>
<!--- start-rate---->
<script src="js/jstarbox.js"></script>
	<link rel="stylesheet" href="css/jstarbox.css" type="text/css" media="screen" charset="utf-8" />
		<script type="text/javascript">
			jQuery(function() {
			jQuery('.starbox').each(function() {
				var starbox = jQuery(this);
					starbox.starbox({
					average: starbox.attr('data-start-value'),
					changeable: starbox.hasClass('unchangeable') ? false : starbox.hasClass('clickonce') ? 'once' : true,
					ghosting: starbox.hasClass('ghosting'),
					autoUpdateAverage: starbox.hasClass('autoupdate'),
					buttons: starbox.hasClass('smooth') ? false : starbox.attr('data-button-count') || 5,
					stars: starbox.attr('data-star-count') || 5
					}).bind('starbox-value-changed', function(event, value) {
					if(starbox.hasClass('random')) {
					var val = Math.random();
					starbox.next().text(' '+val);
					return val;
					}
				})
			});
		});
		</script>
<!---//End-rate---->
<link href="css/form.css" rel="stylesheet" type="text/css" media="all" />
</head>
<body>
<!--header-->
<?php include_once 'header.php'; ?>
<!--banner-->
<div style="background: #f3f3f3 !important;" class="single">

<div class="container">
<div class="col-md-12">
<div class="col-md-4 grid">
<img class="myImg" onclick="reply_click(this)" src="images/ris/19114009_629964410533205_7693411940020148899_n.jpg" alt="" width="100%">
</div>

	<div class="col-md-3 grid">
		<img class="myImg"  onclick="reply_click(this)"  src="images/ris/19149058_629964413866538_3803655667581665984_n.jpg" alt="Trolltunga, Norway" width="100%">
	</div>


	<div id="myModal" class="modal">
	  <span class="close">&times;</span>
	  <img class="modal-content" id="img01">
	</div>







<div class="col-md-5 single-top-in">
		<div class="span_2_of_a1 simpleCart_shelfItem">
				<h3>HAYRA MOTIF</h3>
			   <div class="price_single">
				  <span class="reducedfrom item_price">IDR35.000</span>
				</div>
				<p class="quick_desc"> Jilbab motif berbahan double hycone ini lembut dan enak banget di pakai, dengan hiasan pinggir Rawis, bisa bikin kamu tambah manis lho... sangat cocok untuk keseharianmu </p>
			    <div class="wish-list">
				 	<ul>
				 		<li class="wish"><a href="#"><span class="glyphicon glyphicon-check" aria-hidden="true"></span>Add to Wishlist</a></li>
				 	</ul>
				 </div>
			    <a href="#" class="add-to item_add hvr-skew-backward">Add to cart</a>
			<div class="clearfix"> </div>
			</div>


<div class="tab-head">
			 <nav class="nav-sidebar">
		<ul class="nav tabs">
          <li class="active"><a href="#tab1" data-toggle="tab">Rincian Ukuran & Fit</a></li>
          <li class=""><a href="#tab2" data-toggle="tab">Perawatan</a></li>

		</ul>
	</nav>
	<div class="tab-content one">
<div class="tab-pane active text-style" id="tab1">
 <div class="facts">
	 <p>Bahan:&nbsp;Viole Cotton</p>
	 <p>Ukuran yang Dikenakan Model:<br />
Size: One Size<br />
Tinggi Model: 174 cm</p>
										<ul>
											<li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>Bahan viney</li>
											<li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>Uk. 115 x 115 cm</li>
											<li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>tidak terawang</li>
											<li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>pinggiran Rawis</li>
										</ul>
							        </div>

</div>
<div class="tab-pane text-style" id="tab2">

									<div class="facts">
	 <p>Perawatan :<br />
	 -Cuci terpisah<br />
	 -Gunakan detergen yang lembut<br />
	 -Jangan diputar dalam mesin cuci saat pengeringan<br />
	 -Jangan gunakan pemutih<br />
	 -Setrika suhu rendah&nbsp;</p>
							        </div>
</div>
  </div>
  <div class="clearfix"></div>
  </div>

	</div>

</div>
	</div>

			<!--brand-->
		<?php include_once 'brand.php'; ?>
			<!--//brand-->
		</div>

	<!--//content-->
		<!--//footer-->
	<?php include_once 'footer.php'; ?>
		<!--//footer-->
<script src="js/imagezoom.js"></script>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script defer src="js/jquery.flexslider.js"></script>
<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" />

<script>
// Can also be used with $(document).ready()
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide",
		slideshow: false,
    controlNav: "thumbnails"
  });
});
</script>


<script>
var modal = document.getElementById('myModal');
var modalImg = document.getElementById("img01");

function reply_click(obj) {
	modal.style.display = "block";
	modalImg.src = obj.src;
}


// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
		modal.style.display = "none";
}

</script>

	<script src="js/simpleCart.min.js"> </script>
<!-- slide -->
<script src="js/bootstrap.min.js"></script>


</body>
</html>
