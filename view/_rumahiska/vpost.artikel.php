<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
<title><?= $data->judul ?></title>
<link href="<?= $data->base_url ?>assets/homepage/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<!-- Custom Theme files -->
<!--theme-style-->
<link href="<?= $data->base_url ?>assets/homepage/css/style.css" rel="stylesheet" type="text/css" media="all" />
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--theme-style-->
<link href="<?= $data->base_url ?>assets/homepage/css/style4.css" rel="stylesheet" type="text/css" media="all" />
<link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
<!--//theme-style-->
<script src="<?= $data->base_url ?>assets/homepage/js/jquery.min.js"></script>
<!--- start-rate---->
<script src="<?= $data->base_url ?>assets/homepage/js/jstarbox.js"></script>
	<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage/css/jstarbox.css" type="text/css" media="screen" charset="utf-8" />
		<script type="text/javascript">
			jQuery(function() {
			jQuery('.starbox').each(function() {
				var starbox = jQuery(this);
					starbox.starbox({
					average: starbox.attr('data-start-value'),
					changeable: starbox.hasClass('unchangeable') ? false : starbox.hasClass('clickonce') ? 'once' : true,
					ghosting: starbox.hasClass('ghosting'),
					autoUpdateAverage: starbox.hasClass('autoupdate'),
					buttons: starbox.hasClass('smooth') ? false : starbox.attr('data-button-count') || 5,
					stars: starbox.attr('data-star-count') || 5
					}).bind('starbox-value-changed', function(event, value) {
					if(starbox.hasClass('random')) {
					var val = Math.random();
					starbox.next().text(' '+val);
					return val;
					}
				})
			});
		});
		</script>
<!---//End-rate---->
<link href="<?= $data->base_url ?>assets/homepage/css/form.css" rel="stylesheet" type="text/css" media="all" />
</head>
<body>
<!--header-->
<?php include_once $data->homedir.'view/homepage/header.php'; ?>
<!--banner-->

<!-- Page Content -->
<div style="background: #f3f3f3 !important;" class="single">
	<div id="blog"  class="container">
		<div class="row">
			<div class="col-sm-8">
				<h1>Post Title</h1>
				<p>  by
					<a href="#">Start Bootstrap</a>
				</p>
				<hr>
				<p>Posted on January 1, 2017 at 12:00 PM</p>
				<hr>
				<img width="100%" src="http://placehold.it/900x300" alt="">
				<hr>
				<p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, vero, obcaecati, aut, error quam sapiente nemo saepe quibusdam sit excepturi nam quia corporis eligendi eos magni recusandae laborum minus inventore?</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut, tenetur natus doloremque laborum quos iste ipsum rerum obcaecati impedit odit illo dolorum ab tempora nihil dicta earum fugiat. Temporibus, voluptatibus.</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, doloribus, dolorem iusto blanditiis unde eius illum consequuntur neque dicta incidunt ullam ea hic porro optio ratione repellat perspiciatis. Enim, iure!</p>
				<blockquote class="blockquote">
					<p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
					<footer class="blockquote-footer">Someone famous in
						<cite title="Source Title">Source Title</cite>
					</footer>
				</blockquote>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, nostrum, aliquid, animi, ut quas placeat totam sunt tempora commodi nihil ullam alias modi dicta saepe minima ab quo voluptatem obcaecati?</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum, dolor quis. Sunt, ut, explicabo, aliquam tenetur ratione tempore quidem voluptates cupiditate voluptas illo saepe quaerat numquam recusandae? Qui, necessitatibus, est!</p>
				<hr>
					</div>
					<?php include_once $data->homedir.'view/homepage/sidebar0.php'; ?>
				</div>
			</div>
</div>

<script src="<?= $data->base_url ?>assets/homepage/js/imagezoom.js"></script>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script defer src="<?= $data->base_url ?>assets/homepage/js/jquery.flexslider.js"></script>
<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage/css/flexslider.css" type="text/css" media="screen" />

<script>
// Can also be used with $(document).ready()
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide",
    controlNav: "thumbnails"
  });
});
</script>

	<script src="<?= $data->base_url ?>assets/homepage/js/simpleCart.min.js"> </script>
<!-- slide -->
<script src="<?= $data->base_url ?>assets/homepage/js/bootstrap.min.js"></script>


</body>
</html>
