<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
<title><?= $data->judul ?></title>
<link href="<?= $data->base_url ?>assets/homepage/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<!-- Custom Theme files -->
<!--theme-style-->
<link href="<?= $data->base_url ?>assets/homepage/css/style.css" rel="stylesheet" type="text/css" media="all" />
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--theme-style-->
<link href="<?= $data->base_url ?>assets/homepage/css/style4.css" rel="stylesheet" type="text/css" media="all" />
<link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
<!--//theme-style-->
<script src="<?= $data->base_url ?>assets/homepage/js/jquery.min.js"></script>
<!--- start-rate---->
<script src="<?= $data->base_url ?>assets/homepage/js/jstarbox.js"></script>
	<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage/css/jstarbox.css" type="text/css" media="screen" charset="utf-8" />
		<script type="text/javascript">
			jQuery(function() {
			jQuery('.starbox').each(function() {
				var starbox = jQuery(this);
					starbox.starbox({
					average: starbox.attr('data-start-value'),
					changeable: starbox.hasClass('unchangeable') ? false : starbox.hasClass('clickonce') ? 'once' : true,
					ghosting: starbox.hasClass('ghosting'),
					autoUpdateAverage: starbox.hasClass('autoupdate'),
					buttons: starbox.hasClass('smooth') ? false : starbox.attr('data-button-count') || 5,
					stars: starbox.attr('data-star-count') || 5
					}).bind('starbox-value-changed', function(event, value) {
					if(starbox.hasClass('random')) {
					var val = Math.random();
					starbox.next().text(' '+val);
					return val;
					}
				})
			});
		});
		</script>
<!---//End-rate---->
<link href="<?= $data->base_url ?>assets/homepage/css/form.css" rel="stylesheet" type="text/css" media="all" />
<style>
.list-group {border-radius: 0;}
.list-group .list-group-item {background-color: transparent;overflow: hidden;border: 0;border-radius: 0;padding: 0 16px;}
.list-group .list-group-item .row-picture,
.list-group .list-group-item .row-action-primary {float: left;display: inline-block;padding-right: 16px;padding-top: 8px;}
.list-group .list-group-item .row-picture img,
.list-group .list-group-item .row-action-primary img,
.list-group .list-group-item .row-picture i,
.list-group .list-group-item .row-action-primary i,
.list-group .list-group-item .row-picture label,
.list-group .list-group-item .row-action-primary label {display: block;width: 56px;height: 56px;}
.list-group .list-group-item .row-picture img,
.list-group .list-group-item .row-action-primary img {background: rgba(0, 0, 0, 0.1);padding: 1px;}
.list-group .list-group-item .row-picture img.circle,
.list-group .list-group-item .row-action-primary img.circle {border-radius: 100%;}
.list-group .list-group-item .row-picture i,
.list-group .list-group-item .row-action-primary i {background: rgba(0, 0, 0, 0.25);border-radius: 100%;text-align: center;line-height: 56px;font-size: 20px;color: white;}
.list-group .list-group-item .row-picture label,
.list-group .list-group-item .row-action-primary label {margin-left: 7px;margin-right: -7px;margin-top: 5px;margin-bottom: -5px;}
.list-group .list-group-item .row-content {display: inline-block;width: calc(100% - 92px);min-height: 66px;}
.list-group .list-group-item .row-content .action-secondary {position: absolute;right: 16px;top: 16px;}
.list-group .list-group-item .row-content .action-secondary i {font-size: 20px;color: rgba(0, 0, 0, 0.25);cursor: pointer;}
.list-group .list-group-item .row-content .action-secondary ~ * {max-width: calc(100% - 30px);}
.list-group .list-group-item .row-content .least-content {position: absolute;right: 16px;top: 0px;color: rgba(0, 0, 0, 0.54);font-size: 14px;}
.list-group .list-group-item .list-group-item-heading {color: rgba(0, 0, 0, 0.77);font-size: 20px;line-height: 29px;}
.list-group .list-group-separator {clear: both;overflow: hidden;margin-top: 10px;margin-bottom: 10px;}
.list-group .list-group-separator:before {content: "";width: calc(100% - 90px);border-bottom: 1px solid rgba(0, 0, 0, 0.1);float: right;}

.bg-profile{background-color: #3498DB !important;height: 150px;z-index: 1;}
.bg-bottom{height: 100px;margin-left: 30px;}
.img-profile{display: inline-block !important;background-color: #fff;border-radius: 6px;margin-top: -50%;padding: 1px;vertical-align: bottom;border: 2px solid #fff;-moz-box-sizing: border-box;box-sizing: border-box;color: #fff;z-index: 2;}
.row-float{margin-top: -40px;}
.explore a {color: green; font-size: 13px;font-weight: 600}
.twitter a {color:#4099FF}
.img-box{box-shadow: 0 3px 6px rgba(0,0,0,.16),0 3px 6px rgba(0,0,0,.23);border-radius: 2px;border: 0;}

p {font-size: 14px;}

.single {
    padding: 3em 0 0;
}

@media only screen and (max-width: 599px) {
p,h4 {margin : 20px;}
}


</style>
</head>
<body>
<!--header-->
<?php include_once $data->homedir.'view/homepage/header.php'; ?>
<!--banner-->

<div style="background: #f3f3f3 !important;" class="single">
<div class="container">
	<div class="col-md-9">
		<div class="row">
                    <div class="col-xs-12 col-sm-3 col-md-3">
                        <a href="#">
                            <img src="<?= $data->base_url ?>assets/homepage/images/no.png" class="img-responsive img-box img-thumbnail">
                        </a>
                    </div>
                    <div class="col-xs-12 col-sm-9 col-md-9">
                        <div class="list-group">
                            <div class="list-group-item">
                                <div class="row-picture">
                                    <a href="#" title="alt">
                                        <img class="circle img-thumbnail img-box" src="<?= $data->base_url ?>assets/homepage/images/user.jpg" alt="alt">
                                    </a>
                                </div>
                                <div class="row-content">
                                    <div class="list-group-item-heading">
                                        <a href="#" title="alt">
                                            <small>alt</small>
                                        </a>
                                    </div>
                                    <small>
                                        <i class="glyphicon glyphicon-time"></i> 3 days ago via <span class="twitter"> <i class="fa fa-twitter"></i> <a target="_blank" href="https://twitter.com/alt" alt="alt" title="alt">@alt</a></span>
                                        <br>
                                        <span class="explore"><i class="glyphicon glyphicon-tag"></i> <a href="#">Item</a></span>
                                    </small>
                                </div>
                            </div>
                        </div>
                        <h4><a href=<?php echo "'".$data->base_url."artikel/read/apalah"."'";?>>Lorem ipsum </a></h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quorum sine causa fieri nihil putandum est. </p>
                    </div>
        </div>
                <hr>

	</div>

<?php include_once $data->homedir.'view/homepage/sidebar0.php'; ?>

</div>

</div>

	<!--//content-->
		<!--//footer-->

		<!--//footer-->
<script src="<?= $data->base_url ?>assets/homepage/js/imagezoom.js"></script>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script defer src="<?= $data->base_url ?>assets/homepage/js/jquery.flexslider.js"></script>
<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage/css/flexslider.css" type="text/css" media="screen" />

<script>
// Can also be used with $(document).ready()
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide",
    controlNav: "thumbnails"
  });
});
</script>

	<script src="<?= $data->base_url ?>assets/homepage/js/simpleCart.min.js"> </script>
<!-- slide -->
<script src="<?= $data->base_url ?>assets/homepage/js/bootstrap.min.js"></script>


</body>
</html>
