<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<?php include_once 'baseurl.php'; ?>
<html>
<head>
<title></title>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<!-- Custom Theme files -->
<!--theme-style-->
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--theme-style-->
<link href="css/style4.css" rel="stylesheet" type="text/css" media="all" />
<link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
<!--//theme-style-->
<script src="js/jquery.min.js"></script>
<!--- start-rate---->
<script src="js/jstarbox.js"></script>
	<link rel="stylesheet" href="css/jstarbox.css" type="text/css" media="screen" charset="utf-8" />
		<script type="text/javascript">
			jQuery(function() {
			jQuery('.starbox').each(function() {
				var starbox = jQuery(this);
					starbox.starbox({
					average: starbox.attr('data-start-value'),
					changeable: starbox.hasClass('unchangeable') ? false : starbox.hasClass('clickonce') ? 'once' : true,
					ghosting: starbox.hasClass('ghosting'),
					autoUpdateAverage: starbox.hasClass('autoupdate'),
					buttons: starbox.hasClass('smooth') ? false : starbox.attr('data-button-count') || 5,
					stars: starbox.attr('data-star-count') || 5
					}).bind('starbox-value-changed', function(event, value) {
					if(starbox.hasClass('random')) {
					var val = Math.random();
					starbox.next().text(' '+val);
					return val;
					}
				})
			});
		});
		</script>
<!---//End-rate---->
<link href="css/form.css" rel="stylesheet" type="text/css" media="all" />
<style>
.process-step .btn:focus{outline:none}
.process{display:table;width:100%;position:relative}
.process-row{display:table-row}
.process-step button[disabled]{opacity:1 !important;filter: alpha(opacity=100) !important}
.process-row:before{top:40px;bottom:0;position:absolute;content:" ";width:100%;height:1px;background-color:#ccc;z-order:0}
.process-step{display:table-cell;text-align:center;position:relative}
.process-step p{margin-top:4px}
.btn-circle{width:80px;height:80px;text-align:center;font-size:12px;border-radius:50%}

</style>
</head>
<body>
<!--header-->
<?php include_once 'header.php'; ?>
<!--banner-->

<!-- Page Content -->
<div class="single">
	<div id="blog"  class="container">
		<div class="row">
			<div class="col-sm-8">
				<div class="container">


				</div>
			</div>
		</div>
	</div>


	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<div class="container">
	 <div class="row">
	  <div class="process">
	   <div class="process-row nav nav-tabs">
	    <div class="process-step">
	     <button type="button" class="btn btn-info btn-circle" data-toggle="tab" href="#menu1"><i class="fa fa-info fa-3x"></i></button>
	     <p><small>Fill<br />information</small></p>
	    </div>

			<div class="process-step">
	     <button type="button" class="btn btn-default btn-circle" data-toggle="tab" href="#menu2"><i class="fa fa-file-text-o fa-3x"></i></button>
	     <p><small>Fill<br />description</small></p>
	    </div>


	    <div class="process-step">
	     <button type="button" class="btn btn-default btn-circle disabled" data-toggle="tab" href="#menu5"><i class="fa fa-check fa-3x"></i></button>
	     <p><small>Save<br />result</small></p>
	    </div>
	   </div>
	  </div>

	  <div class="tab-content">
	   <div id="menu1" class="tab-pane fade active in">


			 <div class="container">
			 		<div class="login">
			 			<form>
			 			<div class="col-md-6 login-do">

							<h3 style="margin-bottom:20px;    display: inline-block;
    vertical-align: middle;">Alamat pengiriman Anda</h3>
							<a style="font-size: 13px;
    vertical-align: middle;
    float: right;" href="#">Login untuk proses yang lebih cepat</a>



		 <div class="form-group">
		 <label for="email">Email:</label>
		 <input type="email" class="form-control" id="email" placeholder="Masukkan alamat email Anda:" name="email">
	   </div>

		 <div class="form-group">
		 <label for="name">Nama:</label>
		 <input type="name" class="form-control" id="name" placeholder="Nama Lengkap" name="name">
	   </div>

		 <div class="form-group">
		 <label for="phone">Nomor Handphone:</label>
		 <input type="phone" class="form-control" id="phone" placeholder="Nomor Handphone" name="name">
		 </div>

		 <div class="form-group">
  <label for="address">Alamat:</label>
  <textarea placeholder="Alamat" class="form-control" rows="5" id="address"></textarea>
</div>

<div class="form-group">
  <label for="prv">Provinsi :</label>
  <select class="form-control" rel="location_0" data-field="input" id="prv">
		<option value="22" selected="selected" data-translated="Daerah Khusus Ibukota Jakarta">DKI Jakarta</option>
		<option value="29" data-translated="">Bali</option>
		<option value="32" data-translated="">Bangka Belitung</option>
		<option value="36" data-translated="Banten">Banten</option>
		<option value="40" data-translated="">Bengkulu</option>
		<option value="19" data-translated="">Di Yogyakarta</option>
		<option value="17">Gorontalo</option>
		<option value="26" data-translated="">Jambi</option>
		<option value="35" data-translated="Jawa Barat">Jawa Barat</option>
		<option value="46" data-translated="">Jawa Tengah</option>
		<option value="23" data-translated="Jawa Timur">Jawa Timur</option>
		<option value="27" data-translated="">Kalimantan Barat</option>
		<option value="20" data-translated="">Kalimantan Selatan</option>
		<option value="25" data-translated="">Kalimantan Tengah</option>
		<option value="24" data-translated="">Kalimantan Timur</option>
		<option value="17198" data-translated="">Kalimantan Utara</option>
		<option value="37" data-translated="">Kepulauan Riau</option>
		<option value="18" data-translated="">Lampung</option>
		<option value="28" data-translated="">Maluku</option>
		<option value="38" data-translated="">Maluku Utara</option>
		<option value="3023" data-translated="">Nanggroe Aceh Darussalam (Nad)</option>
		<option value="2896" data-translated="">Nusa Tenggara Barat (Ntb)</option>
		<option value="2590" data-translated="">Nusa Tenggara Timur (Ntt)</option>
		<option value="42" data-translated="">Papua</option>
		<option value="48" data-translated="">Papua Barat</option>
		<option value="21" data-translated="">Riau</option>
		<option value="50" data-translated="">Sulawesi Barat</option>
		<option value="45" data-translated="">Sulawesi Selatan</option>
		<option value="30" data-translated="">Sulawesi Tengah</option>
		<option value="47" data-translated="">Sulawesi Tenggara</option>
		<option value="49" data-translated="">Sulawesi Utara</option>
		<option value="31" data-translated="">Sumatera Barat</option>
		<option value="34" data-translated="">Sumatera Selatan</option>
		<option value="39" data-translated="Sumatera Utara">Sumatera Utara</option>
  </select>
</div>


<div class="form-group">
  <label for="city">Kota:</label>
  <select class="form-control" id="city">
		<option value="">Pilih</option>
		<option value="6604" data-translated="">Kab. Kepulauan Seribu</option>
		<option value="6607" data-translated="Kota Jakarta Utara">Kota Jakarta Utara</option>
		<option value="6614" data-translated="Kota Jakarta Timur">Kota Jakarta Timur</option>
		<option value="6625" data-translated="Kota Jakarta Selatan">Kota Jakarta Selatan</option>
		<option value="6636" data-translated="Kota Jakarta Pusat">Kota Jakarta Pusat</option>
		<option value="6645" data-translated="Kota Jakarta Barat">Kota Jakarta Barat</option>
  </select>
</div>

<div class="col-md-6">
        <div class="form-group">
          <label>Pilih Jasa Kirim</label>
          <select name="in[dv]" class="form-control" id="dv">
            <option value="jne">JNE</option>
            <option value="pos">POS Indonesia</option>
            <option value="tiki">TIKI</option>
          </select>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label>Layanan:</label>
          <select name="in[ds]" class="form-control" id="ds">
          </select>
        </div>
      </div>

			 				<label class="hvr-skew-backward">
			 					<input type="submit" value="login">
			 				</label>
			 			</div>




			 			<div class="col-md-6 login-right">
							 <div class="panel panel-info">
							 	<div class="panel-heading">
							 		<div class="panel-title">
							 			<div class="row">
							 				<div class="col-xs-6">
							 					<h5><span class="glyphicon glyphicon-shopping-cart"></span> Rincian Pesanan (1 items)</h5>
							 				</div>
							 				<div class="col-xs-6">
							 					<button type="button" class="btn btn-primary btn-sm btn-block">
							 						<span class="glyphicon glyphicon-share-alt"></span> Lanjutkan Belanja
							 					</button>
							 				</div>
							 			</div>
							 		</div>
							 	</div>
							 	<div class="panel-body">
							 		<div class="row">
							 			<div class="col-xs-2"><img class="img-responsive" src="http://placehold.it/100x70">
							 			</div>
							 			<div class="col-xs-4">
							 				<h4 class="product-name"><strong>Product name</strong></h4><h4><small>Product description</small></h4>
							 			</div>
							 			<div class="col-xs-6">
							 				<div class="col-xs-6 text-right">
							 					<h6><strong>16000 <span class="text-muted">x</span></strong></h6>
							 				</div>
							 				<div class="col-xs-4">
							 					<input type="text" class="form-control input-sm" value="1">
							 				</div>
							 				<div class="col-xs-2">
							 					<button type="button" class="btn btn-link btn-xs">
							 						<span class="glyphicon glyphicon-trash"> </span>
							 					</button>
							 				</div>
							 			</div>
							 		</div>
							 		<hr>
							 		<div class="row">
							 			<div class="col-xs-2"><img class="img-responsive" src="http://placehold.it/100x70">
							 			</div>
							 			<div class="col-xs-4">
							 				<h4 class="product-name"><strong>Product name</strong></h4><h4><small>Product description</small></h4>
							 			</div>
							 			<div class="col-xs-6">
							 				<div class="col-xs-6 text-right">
							 					<h6><strong>34000 <span class="text-muted">x</span></strong></h6>
							 				</div>
							 				<div class="col-xs-4">
							 					<input type="text" class="form-control input-sm" value="1">
							 				</div>
							 				<div class="col-xs-2">
							 					<button type="button" class="btn btn-link btn-xs">
							 						<span class="glyphicon glyphicon-trash"> </span>
							 					</button>
							 				</div>
							 			</div>
							 		</div>
									<hr>
									<div class="row">
							 			<div class="text-center">
							 				<div class="col-xs-9">
							 					<h6 class="text-right">Added items?</h6>
							 				</div>
							 				<div class="col-xs-3">
							 					<button type="button" class="btn btn-default btn-sm btn-block">
							 						Update cart
							 					</button>
							 				</div>
							 			</div>
							 		</div>
							 		<hr>
							 		<div class="row">
							 			<div class="text-center">
							 				<div class="col-xs-9">
							 					<h6 class="text-right">Ongkos kirim</h6>
							 				</div>
							 				<div class="col-xs-3">
							 					<button type="button" class="btn btn-default btn-sm btn-block">
							 						Rp 5000
							 					</button>
							 				</div>
							 			</div>
							 		</div>
							 	</div>
							 	<div class="panel-footer">
							 		<div class="row text-center">
							 			<div class="col-xs-6">
							 				<!-- <h4 class="text-right">Total <strong>$50.00</strong></h4> -->
							 			</div>
							 			<div class="col-xs-6">
							 				<!-- <button type="button" class="btn btn-success btn-block">
							 					Checkout
							 				</button> -->
											<h4 class="text-right">Total <strong>Rp 55000</strong></h4>
							 			</div>
							 		</div>
							 	</div>
							 </div>
							<button type="button" class="btn btn-info next-step">Next <i class="fa fa-chevron-right"></i></button>
			 			</div>
			 			<div class="clearfix"> </div>
			 			</form>
			 		</div>
			 </div>


	   </div>
	   <div id="menu2" class="tab-pane fade">
			<div style="margin-top:60px" class="container">
				<div class="row">
					<div class="col-md-12">

					</div>
				</div>
			</div>
	    <ul class="list-unstyled list-inline pull-right">
	     <li><button type="button" class="btn btn-default prev-step"><i class="fa fa-chevron-left"></i> Back</button></li>
	     <li><button type="button" class="btn btn-info next-step">Next <i class="fa fa-chevron-right"></i></button></li>
	    </ul>
	   </div>
	  </div>
	 </div>
	</div>
</div>


<script>
$(function(){
 $('.btn-circle').on('click',function(){
   $('.btn-circle.btn-info').removeClass('btn-info').addClass('btn-default');
   $(this).addClass('btn-info').removeClass('btn-default').blur();
 });

 $('.next-step, .prev-step').on('click', function (e){
   var $activeTab = $('.tab-pane.active');

   $('.btn-circle.btn-info').removeClass('btn-info').addClass('btn-default');

   if ( $(e.target).hasClass('next-step') )
   {
      var nextTab = $activeTab.next('.tab-pane').attr('id');
      $('[href="#'+ nextTab +'"]').addClass('btn-info').removeClass('btn-default');
      $('[href="#'+ nextTab +'"]').tab('show');
   }
   else
   {
      var prevTab = $activeTab.prev('.tab-pane').attr('id');
      $('[href="#'+ prevTab +'"]').addClass('btn-info').removeClass('btn-default');
      $('[href="#'+ prevTab +'"]').tab('show');
   }
 });
});
</script>
<script src="js/bootstrap.min.js"></script>


</body>
</html>
