<!DOCTYPE html>
<html>
<head>
    <title>Rumah Iska -- <?php echo $data->judul ?></title>
    <?php include_once $data->homedir.'/view/administrasi/elements/header.php'; ?>
    <?php MetaTag() ?>
    <!-- <?php Links($data->base_url) ?> -->
    <?php Upload($data->base_url) ?>
    <?php Scripts($data->base_url) ?>
    <!-- <?php Styles() ?> -->

    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
    <link href="http://salvina.id/assets/homepage/css/style.css" rel="stylesheet" type="text/css" media="all" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <link href="http://salvina.id/assets/homepage/css/style4.css" rel="stylesheet" type="text/css" media="all" />
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <script src="http://salvina.id/assets/homepage/js/jquery.min.js"></script>
    <script src="http://salvina.id/assets/homepage/js/jstarbox.js"></script>
    	<link rel="stylesheet" href="http://salvina.id/assets/homepage/css/jstarbox.css" type="text/css" media="screen" charset="utf-8" />
    		<script type="text/javascript">
    			jQuery(function() {
    			jQuery('.starbox').each(function() {
    				var starbox = jQuery(this);
    					starbox.starbox({
    					average: starbox.attr('data-start-value'),
    					changeable: starbox.hasClass('unchangeable') ? false : starbox.hasClass('clickonce') ? 'once' : true,
    					ghosting: starbox.hasClass('ghosting'),
    					autoUpdateAverage: starbox.hasClass('autoupdate'),
    					buttons: starbox.hasClass('smooth') ? false : starbox.attr('data-button-count') || 5,
    					stars: starbox.attr('data-star-count') || 5
    					}).bind('starbox-value-changed', function(event, value) {
    					if(starbox.hasClass('random')) {
    					var val = Math.random();
    					starbox.next().text(' '+val);
    					return val;
    					}
    				})
    			});
    		});
    		</script>
    <style>
    body {
    	font-family: 'Quicksand', sans-serif;
    	    /* background: #f3f3f3 !important; */
    }
    .content-top {
    	padding-top: 20px;
    }
    .col-2 {
        padding-top: 1.5em;
    }


    </style>

</head>
<body>

<?php include_once $data->homedir.'view/homepage/header.php'; ?>
<div class="container" style="padding-top:50px;padding-bottom:50px;">
    <div class="row">
        <div class="col-md-12">
            <a href="<?= $data->base_url ?>distributor/login" class="btn btn-info">Login Distributor</a>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">

            <div class="row">
                <div class="col-md-12">

                    <?= $data->konten ?>

                </div>
            </div>
        </div>
    </div>



</div>
	<?php include_once $data->homedir.'view/homepage/footer.php'; ?>
</body>
</html>
