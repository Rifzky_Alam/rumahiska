<!DOCTYPE html>
<html>
<head>
      <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="<?= $data->base_url.'assets/bootstrap4/css/bootstrap.min.css' ?>">
	<title>Member | Pembayaran</title>
</head>
<body>

	<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
	  <!-- Brand -->
	  <a class="navbar-brand" href="#">SALVINA.ID</a>

	  <!-- Links -->
	  <ul class="navbar-nav">
	    <li class="nav-item">
	      <a class="nav-link" href="#"></a>
	    </li>
	    <li class="nav-item">
	      <a class="nav-link" href="#"></a>
	    </li>

	    <!-- Dropdown -->
	    <li class="nav-item dropdown">
	      <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
	        Menu
	      </a>
	      <div class="dropdown-menu">
	        <a class="dropdown-item" href="<?= $data->base_url ?>distributor/dasbor">Dasbor</a>
	        <!--<a class="dropdown-item" href="#">Status Transaksi</a>-->
	        <!--<a class="dropdown-item" href="#">Profil</a>-->
	      </div>
	    </li>
	  </ul>
	</nav>



	<div class="container" style="padding-top: 40px;">
		<form action="" method="post" accept-charset="utf-8">
		<div class="row">
			<div class="col-md-12">
				<div class="page-header">
					<h2>Pembayaran</h2>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-4">
				<div class="form-group">
					<label>Alamat</label>
					<select class="form-control" name="in[alamat]" id="cbalamat">
						<?php if (count($data->listalamat)=='0'): ?>
							<option value="">--Tidak Ada Alamat Tersedia, harap utk tambah alamat anda.--</option>
						<?php else: ?>
							<?php $num = 1; ?>
							<?php foreach ($data->listalamat as $key): ?>
								<option value="<?= $key['adr_id'] ?>"><?= 'Alamat-'.$num ?></option>	
								<?php $num++; ?>
							<?php endforeach ?>
						<?php endif ?>
						
					</select>
				</div>

					<section>
						<p id="alamatdesc">
							
						</p>
						<a href="<?= $data->base_url.'distributor/tambahalamat' ?>" title="">Klik untuk Tambah Alamat Baru</a>
						<input type="hidden" id="provhid" value="">
						<input type="hidden" id="kothid" value="">
						<input type="hidden" id="sbd" value="">
					</section>
					<br>

				<div class="form-group">
					<label>Kurir</label>
					<select id="cbkurir" name="in[svc]" class="form-control">
						
			            <option value="jne" selected="">JNE</option>
			            <option value="sicepat">SiCepat</option>
			            <option value="tiki">TIKI</option>
			            <option value="pos">POS Indonesia</option>
			            <option value="jnt">J&amp;T Express (J&amp;T)</option>
			            <option value="pcp">Priority Cargo and Package (PCP)</option>
			            <!-- <option value="esl">Eka Sari Lorena (ESL)</option> -->
			            <!-- <option value="rpx">RPX Holding (RPX)</option> -->
			            <option value="pandu">Pandu Logistics (PANDU)</option>
			            <option value="wahana">Wahana Prestasi Logistik (WAHANA)</option>
			            <!-- <option value="pahala">Pahala Kencana Express (PAHALA)</option> -->
			            <option value="cahaya">Cahaya Logistik (CAHAYA)</option>
			            <option value="sap">SAP Express (SAP)</option>
			            <option value="jet">JET Express (JET)</option>
			            <!-- <option value="indah">Indah Logistic (INDAH)</option> -->
			            <!-- <option value="slis">Solusi Ekspres (SLIS)</option> -->
			            <option value="dse">21 Express (DSE)</option>
			            <!-- <option value="star">Star Cargo (STAR)</option> -->
			            <option value="first">First Logistics (FIRST)</option>
			            <!-- <option value="ncs">Nusantara Card Semesta (NCS)</option> -->
			            
					</select>
				</div>
				<div class="form-group">
					<label>Layanan Kurir</label>
					<select id="ds" name="in[ds]" class="form-control">
						
					</select>
				</div>
			</div>
			<div class="col-md-6 offset-md-2">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>Nama Barang</th>
							<th>Berat</th>
							<th>Qty</th>
							<th>Harga</th>
							<th>Jumlah</th>
						</tr>
					</thead>
					<tbody>
						<?php Rows($data->listbarang) ?>
					</tbody>
				</table>
				<input type="hidden" id="ogkr" name="in[ongkir]">
				<button id="tombol" class="btn btn-warning">Proses dan masukkan ke transaksi saya</button>
				<p>Dengan memproses belanjaan anda, kami akan mengirimkan email detail pembayaran dan nota pembayaran/invoice. Anda dapat mencetak nota pembayaran/invoice di histori transaksi anda (laman dasbor)</p>
			</div>
			</form>
		</div>
	</div>
	<script src="<?= $data->base_url.'assets/common/jquery3/jquery-3.3.1.min.js' ?>"></script>
	<script src="<?= $data->base_url.'assets/bootstrap4/js/bootstrap.min.js' ?>"></script>
	<script>
		$(document).ready(function(){
			// alert($('#cbalamat').val());
			getalamatdescription($('#cbalamat').val());
		});

		$('#cbalamat').change(function(){
			// alert($(this).val());
			getalamatdescription($(this).val());

		});

		function getalamatdescription(value){
			$.ajax({
	            type: "GET",
	            url: "<?= $data->base_url ?>member/alamatapi",
	            data: {
	              'hash':value
	            },
	            cache: false,
	            success: function(data){
	            	// console.log(data);
	            var kecamatan;
	            var kota;
	            var provinsi;
	            var alamat;
	            var kodepos;
	            
	            if(typeof data.data.adr_alamat==="undefined"){
	                alamat = '-';
	            }else{
	                alamat = data.data.adr_alamat;
	            }
	            
	            if(typeof data.data.namakec==="undefined"){
	                kecamatan = '-';
	            }else{
	                kecamatan = data.data.namakec;
	            }
	            
	            if(typeof data.data.namakota==="undefined"){
	                kota = '-';
	            }else{
	                kota = data.data.namakota;
	            }
	            
	            if(typeof data.data.namaprov==="undefined"){
	                provinsi = '-';
	            }else{
	                provinsi = data.data.namaprov;
	            }
	            
	            if(typeof data.data.adr_kode_pos==="undefined"){
	                kodepos = '-';
	            }else{
	                kodepos = data.data.adr_kode_pos;
	            }

	            if(typeof data.data.adr_notes==="undefined"){
	                notes = '';
	            }else{
	                notes = '<br/>Catatan: '+ '<br/>'+data.data.adr_notes;
	            }
	            
	              document.getElementById("alamatdesc").innerHTML = alamat + "<br/>"+kecamatan+ "<br/>"+kota+ "<br/>"+provinsi+ "<br/>"+"Kode Pos: "+kodepos+notes;
	              document.getElementById("provhid").value = data.data.adr_prov;
	              document.getElementById("kothid").value = data.data.adr_kota;
	              document.getElementById("sbd").value = data.data.adr_kec;
	              LoadService($('#cbkurir').val());
	            }
          }); //end ajax
		}

		function LoadService(kurir) {
      		$('.optsvc').remove();
	      	$.ajax({
	            type: "GET",
	            url: "<?= $data->base_url ?>library/rajaongkir/calculateprice",
	            data: {
	              'js':kurir,
	              'it':$('#sbd').val(),
	              'bb':$('#sbtb').val()
	            },
	            cache: false,
	            success: function(data){
	              var myjson = JSON.parse(data);
	              var result = myjson.rajaongkir.results[0].costs
	              for (x in result) {
	                if (result[x].service=='REG') {
	                  document.getElementById("ds").innerHTML += "<option class='optsvc' selected value='" + result[x].service + "'>" + result[x].description + '</option>';
	                } else {
	                  document.getElementById("ds").innerHTML += "<option class='optsvc' value='" + result[x].service + "'>" + result[x].description + '</option>';
	                }
	              }
	              GetPayment(result);
	            }
	          }); //end ajax
    	}

    	function GetPayment(dataset){
      // $('#tombol').prop('disabled', true);
	      Number.prototype.format = function(n, x) {
	          var re = '(\\d)(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
	          return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$1,');
	      };
        document.getElementById("tombol").disabled = true;
        for (z in dataset) {
          if ($('#ds').val()==dataset[z].service) {
            document.getElementById("ogkr").value = dataset[z].cost[0].value;
            // GetNumber(dataset[z].cost[0].value,'bo','IDR');
            $('#bo').html(dataset[z].cost[0].value.format() + " IDR");
            var hah = parseInt($('#sbt').val());
            var tes = hah + parseInt(dataset[z].cost[0].value);
            $('#tb').html(tes.format() + " IDR");
            // GetNumber(tes,'tb','IDR');
          }
        }
        	document.getElementById("tombol").disabled = false;
      }
      
        $('#cbkurir').change(function() {
            LoadService($(this).val());
        });
      
      $('#ds').change(function() {
      $.ajax({
            type: "GET",
            url: "http://salvina.id/library/rajaongkir/calculateprice",
            data: {
              'js':$('#cbkurir').val(),
              'it':$('#sbd').val(),
              'bb':$('#sbtb').val()
            },
            cache: false,
            success: function(data){
              var myjson = JSON.parse(data);
              var result = myjson.rajaongkir.results[0].costs
              GetPayment(result);
            }
          }); //end ajax
    });
	</script>
</body>
</html>

<?php
function Rows($data){
  if (count($data)=='0') {
    echo "<td colspan='3'>Tidak ada data barang yang anda pesan di sistem kami.</td>";
  }else {
    $total = 0;
    $totalberat = 0;
    foreach ($data as $key => $value) {
      echo '<tr>';
      echo '<td>'.$value['kat']. ' : '.$value['jns'].' - '. $value['nb'].'<a href="removeitem?itm='.$value['ib'].'" title="Hapus item"><span class="glyphicon glyphicon-remove"></span></a></td>';
      $subBerat = $value['bb'] * $value['qty'];
      $totalberat += $subBerat;
      echo '<td id="wgh">'.$value['bb'].' gram</td>';
      echo '<td>'.$value['qty'].'</td>';
      $subtotal = $value['price']*$value['qty'];
      echo '<td>'.number_format($value['price']).' IDR</td>';
      echo '<td>'.number_format($subtotal).' IDR</td>';
      $total += $subtotal;
    }
      echo '<tr>';
      echo '<td colspan="4" id="st">Sub Total:</td><td>'.number_format($total).' IDR</td>';
      echo '<input type="hidden" id="sbt" value="'.$total.'">';
      echo '<input type="hidden" id="sbtb" value="'.$totalberat.'">';
      echo '</tr>';

      $ongkir = 0;
      echo '<tr>';
      echo '<td colspan="4">Ongkos Kirim:</td><td id="bo">Menghitung ...</td>';
      echo '</tr>';

      echo '<tr>';
      echo '<td colspan="4">Total Biaya:</td><td><b id="tb">Menghitung ...</b></td>';
      echo '</tr>';

  }
}

?>