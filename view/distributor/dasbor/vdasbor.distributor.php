<?php $pengumuman = $data->pengumuman[0] ?>
<!DOCTYPE html>
<html>
<head>
    <title>Salvina -- <?php echo $data->judul ?></title>
    <?php include_once $data->homedir.'/view/administrasi/elements/header.php'; ?>
    <?php MetaTag() ?>
    <?php Links($data->base_url) ?>
    <?php Upload($data->base_url) ?>
    <?php Scripts($data->base_url) ?>
    <?php Styles() ?>

</head>
<body>
<?php include_once $data->homedir.'view/order/vheader.order.php'; ?>
<div class="container" style="padding-top:50px;padding-bottom:50px;">
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div class="row">
                <div class="jumbotron">
                    <h2>Halo, <?= $data->nama_agen ?></h2>
                    <h3><?= @$pengumuman['subjek'] ?></h3>
                    <br>
                    <?= @$pengumuman['conten'] ?>
                </div>
            </div>
            <div class="row">
                    <div class="col-md-12" style="text-align:right">
                        <a href="<?= $data->base_url ?>distributor/logout" class="btn btn-danger">Logout</a>
                    </div>
                </div>
            <div class="row">
                    <div class="col-md-12">
                        <h2>Riwayat Transaksi</h2>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Tanggal</th>
                                    <th>Item</th>
                                    <th>Total</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                              <?php if (count($data->historitrans)=='0'): ?>
                                <tr>
                                    <td colspan="5" style="text-align:center;">Tidak ada data tersedia.</td>
                                </tr>
                              <?php else: ?>
                                <?php //print_r($data->historitrans) ?>
                                <?php foreach ($data->historitrans as $key): ?>
                                  <?php //print_r($key) ?>
                                  <tr>
                                      <td style="text-align:center;vertical-align:middle;">
                                          <?= $key['tgl_trans'] ?>
                                      </td>
                                      <?php $items = explode(' # ', $key['item']) ?>
                                      <td style="vertical-align:middle;">
                                          <ul>
                                              <?php foreach ($items as $val): ?>
                                                <li><?= $val ?></li>  
                                              <?php endforeach ?>
                                          </ul>
                                      </td>
                                      <td style="vertical-align:middle;">
                                          <?= number_format($key['total_harga_item']) ?> IDR
                                      </td>
                                      <td style="vertical-align:middle;">
                                          <a href="<?= $data->base_url.'order/status-transaksi/?tr='.$data->securitycode($key['tr_id'],'e') ?>" title="lihat disini" class="btn btn-info">Lihat</a>
                                      </td>
                                      <td style="vertical-align:middle;">
                                          <a href="<?= $data->base_url.'order/invoice?tr='.$key['tr_id'] ?>" target="_blank" class="btn btn-info">
                                              Detail
                                          </a>
                                          ||
                                          <a href="<?= $data->base_url.'order/invoice/pdf?tr='.$key['tr_id'] ?>" class="btn btn-danger">
                                              Bukti/Invoice
                                          </a>
                                      </td>    
                                  </tr>
                                <?php endforeach ?>
                              <?php endif ?>
                            </tbody>
                        </table>
                    </div>
                </div>
        </div>
    </div>
    
</div>
<script src='<?php echo $data->base_url ?>assets/upload/js/fileinput.js' type="text/javascript"></script>
<script src='<?php echo $data->base_url ?>assets/upload/js/fileinput_locale_fr.js' type="text/javascript"></script>
</body>
</html>
