<!DOCTYPE html>
<html>
<head>
    <title>Salvina -- <?php echo $data->judul ?></title>
    <?php include_once $data->homedir.'/view/administrasi/elements/header.php'; ?>
    <?php MetaTag() ?>
    <?php Links($data->base_url) ?>
    <?php Upload($data->base_url) ?>
    <?php Scripts($data->base_url) ?>
    <?php Styles() ?>

</head>
<body>
<?php include_once $data->homedir.'view/order/vheader.order.php'; ?>
<div class="container" style="padding-top:50px;padding-bottom:50px;">
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div class="row">
                <div class="page-header">
                    <h2>Tambah Alamat</h2>
                </div>
            </div>

            <div class="row">
              <h4>Alamat Terdaftar</h4>
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th style="text-align:center;vertical-align:middle;">Alamat</th>
                    <th style="text-align:center;vertical-align:middle;">Kecamatan</th>
                    <th style="text-align:center;vertical-align:middle;">Kota/Kab</th>
                    <th style="text-align:center;vertical-align:middle;">Provinsi</th>
                    <th style="text-align:center;vertical-align:middle;">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if (count($data->listalamat)=='0'): ?>
                    <tr>
                      <td style="text-align:center;" colspan="5">Tidak ada data alamat terdaftar untuk pengiriman</td>
                    </tr>
                  <?php else: ?>
                    <?php foreach ($data->listalamat as $key): ?>
                      <tr>
                        <td style="text-align:center;vertical-align:middle;"><?= $key['alamat'] ?></td>
                        <td style="text-align:center;vertical-align:middle;"><?= $key['kec'] ?></td>
                        <td style="text-align:center;vertical-align:middle;"><?= $key['kota'] ?></td>
                        <td style="text-align:center;vertical-align:middle;"><?= $key['prov'] ?></td>
                        <td style="text-align:center;vertical-align:middle;">
                          <a href="<?= $data->base_url.'distributor/hapusalamat?val='.$key['idalamat'] ?>" class="btn btn-danger" title="Hapus">
                            Hapus
                          </a>
                        </td>
                      </tr>
                    <?php endforeach ?>    
                  <?php endif ?>
                  
                  
                </tbody>
              </table>
            </div>

            <div class="row">
                <div class="col-md-12">
                  <h4>Data Alamat Baru</h4>
                    <form action="" method="post">
                        <div class="form-group">
							<label>Provinsi</label>
							<select name="in[prov]" id="prv" class="form-control">
								
							</select>
						</div>

						<div class="form-group">
							<label>Kota/Kabupaten</label>
							<select name="in[kota]" id="cty" class="form-control">
								<option value="">--Pilih Kota/Kab--</option>
							</select>
						</div>

						<div class="form-group">
							<label>Kecamatan</label>
							<select name="in[kec]" id="sbd" class="form-control">
								<option value="">--Pilih Kecamatan--</option>
							</select>
						</div>

						<div class="form-group">
							<label>Alamat Pengiriman</label>
							<textarea name="in[alamat]" class="form-control" placeholder="No Rumah, RT/RW dan Kelurahan"></textarea>
						</div>

						<div class="form-group">
							<label>Kode Pos</label>
							<input type="text" name="in[kodepos]" class="form-control">
						</div>
						
						<div class="form-group">
							<label>Keterangan/Notes</label>
							<textarea name="in[note]" class="form-control"></textarea>
						</div>
						
						
						<input type="hidden" name="in[namaprov]" id="namaprov" >
						<input type="hidden" name="in[namakota]" id="namakota" >
						<input type="hidden" name="in[namakec]" id="namakec" >
                        <button style="width:100%;" class="btn btn-lg btn-primary">Submit</button>
                        </form>
                </div>
            </div>
        </div>
    </div>
    
</div>
<script src='<?php echo $data->base_url ?>assets/upload/js/fileinput.js' type="text/javascript"></script>
<script src='<?php echo $data->base_url ?>assets/upload/js/fileinput_locale_fr.js' type="text/javascript"></script>
<script>
    $(document).ready(function(){
        LoadProv('9');
	        $("form").bind("keypress", function (e) {
	            if (e.keyCode == 13) {
	                $("#btnSearch").attr('value');
	                //add more buttons here
	                return false;
	            }
	        });
    });

    $('#prv').change(function() {
      document.getElementById("namaprov").value = $("#prv option:selected").text();
      LoadCity();
    });

    $('#cty').change(function() {
      document.getElementById("namakota").value = $("#cty option:selected").text();
      LoadSubdistrict();
    });

    $('#sbd').change(function() {
      document.getElementById("namakec").value = $("#sbd option:selected").text();
      LoadService($('#dv').val());
    });

    function LoadProv(id){
      $('.optprv').remove();
      $.ajax({
            type: "GET",
            url: "http://salvina.id/library/rajaongkir/store",
            data: {
              'q':'province'
            },
            cache: false,
            success: function(data){
              // console.log(data);
              
              var myjson = JSON.parse(data);
              var result = myjson.rajaongkir.results
              for (x in result) {
                if (result[x].province_id==id) {
                  document.getElementById("prv").innerHTML += "<option class='optprv' selected value='" + result[x].province_id + "'>" + result[x].province + '</option>';
                } else {
                  document.getElementById("prv").innerHTML += "<option class='optprv' value='" + result[x].province_id + "'>" + result[x].province + '</option>';
                }
              }
              document.getElementById("namaprov").value = $("#prv option:selected").text();
              LoadCity();
            }
          }); //end ajax
    }

    function LoadCity() {
      $('.optcty').remove();
      $.ajax({
            type: "GET",
            url: "http://salvina.id/library/rajaongkir/store",
            data: {
              'kota':$('#prv').val()
            },
            cache: false,
            success: function(data){
              var myjson = JSON.parse(data);
              var result = myjson.rajaongkir.results
              for (x in result) {
                if (result[x].city_id=='79') {
                  document.getElementById("cty").innerHTML += "<option class='optcty' selected value='" + result[x].city_id + "'>" + result[x].type + " "+ result[x].city_name + '</option>';
                } else {
                  document.getElementById("cty").innerHTML += "<option class='optcty' value='" + result[x].city_id + "'>" + result[x].type + " "+ result[x].city_name + '</option>';
                }
              }
              document.getElementById("namakota").value = $("#cty option:selected").text();
              LoadSubdistrict();
            }
          }); //end ajax
    }

    function LoadSubdistrict(){
      $('.optsbd').remove();
      $.ajax({
            type: "GET",
            url: "http://salvina.id/library/rajaongkir/store",
            data: {
              'sbd':$('#cty').val()
            },
            cache: false,
            success: function(data){
              var myjson = JSON.parse(data);
              var result = myjson.rajaongkir.results
              for (x in result) {
                if (result[x].subdistrict_id=='1066') {
                  document.getElementById("sbd").innerHTML += "<option class='optsbd' selected value='" + result[x].subdistrict_id + "'>" + result[x].subdistrict_name + '</option>';
                } else {
                  document.getElementById("sbd").innerHTML += "<option class='optsbd' value='" + result[x].subdistrict_id + "'>" + result[x].subdistrict_name + '</option>';
                }
              }
              document.getElementById("namakec").value = $("#sbd option:selected").text();
            }
          }); //end ajax
    }
</script>
</body>
</html>