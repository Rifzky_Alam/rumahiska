<!DOCTYPE html>
<html>
<head>
    <title>Rumah Iska -- <?php echo $data->judul ?></title>
    <?php include_once $data->homedir.'/view/administrasi/elements/header.php'; ?>
    <?php MetaTag() ?>
    <?php Links($data->base_url) ?>
    <?php Upload($data->base_url) ?>
    <?php Scripts($data->base_url) ?>
    <?php Styles() ?>

</head>
<body>
<?php include_once $data->homedir.'view/order/vheader.order.php'; ?>
<div class="container" style="padding-top:50px;padding-bottom:50px;">
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header">
                        <h3>Registrasi Agen RumahIska</h3>
                    </div>
                </div>
            </div>
            <form action="" method="post" accept-charset="utf-8">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Username</label>
                        <input type="text" name="in[username]" class="form-control" placeholder="Username untuk login" required>
                    </div>

                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" name="in[password]" class="form-control" placeholder="Password untuk login" required>
                    </div>

                    <div class="form-group">
                        <label>Nama Lengkap</label>
                        <input type="text" name="in[nama]" class="form-control" placeholder="Nama lengkap anda" required>
                    </div>

                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" name="in[email]" class="form-control" placeholder="Email anda" required>
                    </div>

                    <div class="form-group">
                        <label>Telepon</label>
                        <input type="text" name="in[telepon]" class="form-control" placeholder="Telepon anda yang aktif" required>
                    </div>

                    <button class="btn btn-lg btn-primary" style="width:100%">Submit</button>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>
