<!DOCTYPE html>
<html>
<head>
  <title><?= $data->company ?> || Login</title>
  <link rel="stylesheet" type="text/css" href="<?= $data->base_url.'assets/common/bootstrap/css/bootstrap.min.css' ?>">
  <style type="text/css" media="screen">
    @import "bourbon";

    body {
      background: #eee !important;  
    }

    .wrapper {  
      margin-top: 80px;
      margin-bottom: 80px;
    }

    .form-signin {
      max-width: 380px;
      padding: 15px 35px 45px;
      margin: 0 auto;
      background-color: #fff;
      border: 1px solid rgba(0,0,0,0.1);  

      .form-signin-heading,
      .checkbox {
        margin-bottom: 30px;
      }

      .checkbox {
        font-weight: normal;
      }

      .form-control {
        position: relative;
        font-size: 16px;
        height: auto;
        padding: 10px;
        @include box-sizing(border-box);

        &:focus {
          z-index: 2;
        }
      }

      input[type="text"] {
        margin-bottom: -1px;
        border-bottom-left-radius: 0;
        border-bottom-right-radius: 0;
      }

      input[type="password"] {
        margin-bottom: 20px;
        border-top-left-radius: 0;
        border-top-right-radius: 0;
      }
    }

  </style>

</head>
<body>
<div class="container">
  <div class="wrapper">
    <form class="form-signin" action="" method="post">       
      <h2 class="form-signin-heading">Please login</h2>
      <input type="text" class="form-control" name="in[username]" placeholder="Username" minlength="4" required autofocus="" />
      <br>
      <input type="password" class="form-control" name="in[password]" placeholder="Password" required/>      <br>
      <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>   
    </form>
  </div>
</div>
</body>
</html>