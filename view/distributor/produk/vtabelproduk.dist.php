<?php $pengumuman = $data->pengumuman[0] ?>
<!DOCTYPE html>
<html>
<head>
    <title>Salvina -- <?php echo $data->judul ?></title>
    <?php include_once $data->homedir.'/view/administrasi/elements/header.php'; ?>
    <?php MetaTag() ?>
    <?php Links($data->base_url) ?>
    <?php Upload($data->base_url) ?>
    <?php Scripts($data->base_url) ?>
    <?php Styles() ?>

</head>
<body>
<?php include_once $data->homedir.'view/order/vheader.order.php'; ?>
<div class="container">

    <div class="row">
        <div class="page-header">
            <h2>Produk</h2>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <a href="#" title="Search" class="btn btn-primary">Cari Data</a>
        </div>
    </div><br>


    <div class="row">
        <div class="col-md-12">
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail" >
                    <h4 class="text-center"><span class="label label-info">Jilbab</span></h4>
                    <img src="http://placehold.it/650x450&text=Galaxy S5" class="img-responsive">
                    <div class="caption">
                        <div class="row">
                            <div class="col-md-6 col-xs-6">
                                <h4>Katun Vintage</h4>
                            </div>
                            <div class="col-md-6 col-xs-6 price">
                                <h4>
                                <label>2,125,000 IDR</label></h4>
                            </div>
                        </div>
                        <p>Warna: Yellow, Stok: 10</p>
                        <div class="row">
                            <div class="col-md-6">
                                <a class="btn btn-primary btn-product"><span class="glyphicon glyphicon-shopping-cart"></span> Order</a> 
                            </div>
                        </div>

                        <p> </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail" >
                    <h4 class="text-center"><span class="label label-info">Segi Empat</span></h4>
                    <img src="http://placehold.it/650x450&text=iPhone 6" class="img-responsive">
                    <div class="caption">
                        <div class="row">
                            <div class="col-md-6 col-xs-6">
                                <h4>Rasya</h4>
                            </div>
                            <div class="col-md-6 col-xs-6 price">
                                <h4>
                                <label>165,000 IDR</label></h4>
                            </div>
                        </div>
                        <p>Warna: Maroon, Stok: 12</p>
                        <div class="row">
                            <div class="col-md-6">
                                <a class="btn btn-primary btn-product"><span class="glyphicon glyphicon-shopping-cart"></span> Order</a> 
                            </div>
                        </div>

                        <p> </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail" >
                    <h4 class="text-center"><span class="label label-info">Pashmina Instan</span></h4>
                    <img src="http://placehold.it/650x450&text=Lumia 1520" class="img-responsive">
                    <div class="caption">
                        <div class="row">
                            <div class="col-md-6 col-xs-6">
                                <h4>Instant Elsa</h4>
                            </div>
                            <div class="col-md-6 col-xs-6 price">
                                <h4>
                                <label>65,000 IDR</label></h4>
                            </div>
                        </div>
                        <p>Warna: Navy, Stok: 27</p>
                        <div class="row">
                            <div class="col-md-6">
                                <a class="btn btn-primary btn-product"><span class="glyphicon glyphicon-shopping-cart"></span> Order</a> 
                            </div>
                        </div>

                        <p> </p>
                    </div>
                </div>
            </div>
            
        </div> 

    </div>
</div>
<script src='<?php echo $data->base_url ?>assets/upload/js/fileinput.js' type="text/javascript"></script>
<script src='<?php echo $data->base_url ?>assets/upload/js/fileinput_locale_fr.js' type="text/javascript"></script>
</body>
</html>
