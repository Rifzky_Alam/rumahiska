<!DOCTYPE html>
<html>
<head>
    <title>Rumah Iska -- <?php echo $data->judul ?></title>
    <?php include_once $data->homedir.'/view/administrasi/elements/header.php'; ?>
    <?php MetaTag() ?>
    <?php Links($data->base_url) ?>
    <?php Upload($data->base_url) ?>
    <?php Scripts($data->base_url) ?>
    <?php Styles() ?>

</head>
<body>
<?php include_once $data->homedir.'view/order/vheader.order.php'; ?>
<div class="container" style="padding-top:50px;padding-bottom:50px;">
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="jumbotron">
                        <h3>Registrasi Berhasil</h3>
                        <p>
                            Anda telah terdaftar sebagai agen di rumahiska.com. Harap mengecek email anda untuk melakukan verifikasi, apabila email belum terkirim harap untuk mengecek folder spam anda atau kirim kembali email anda dengan <a href="#">klik disini.</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
