<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
<title><?= $data->judul ?></title>
<script src="<?= $data->base_url ?>assets/homepage/js/jquery.min.js"></script>
<!-- <link href="<?= $data->base_url ?>assets/homepage/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" /> -->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
<!-- Custom Theme files -->
<!--theme-style-->
<link href="<?= $data->base_url ?>assets/homepage/css/style.css" rel="stylesheet" type="text/css" media="all" />
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--theme-style-->
<link href="<?= $data->base_url ?>assets/homepage/css/style4.css" rel="stylesheet" type="text/css" media="all" />
<link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
<!--//theme-style-->
<!--- start-rate---->
<script src="<?= $data->base_url ?>assets/homepage/js/jstarbox.js"></script>
	<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage/css/jstarbox.css" type="text/css" media="screen" charset="utf-8" />
		<script type="text/javascript">
			jQuery(function() {
			jQuery('.starbox').each(function() {
				var starbox = jQuery(this);
					starbox.starbox({
					average: starbox.attr('data-start-value'),
					changeable: starbox.hasClass('unchangeable') ? false : starbox.hasClass('clickonce') ? 'once' : true,
					ghosting: starbox.hasClass('ghosting'),
					autoUpdateAverage: starbox.hasClass('autoupdate'),
					buttons: starbox.hasClass('smooth') ? false : starbox.attr('data-button-count') || 5,
					stars: starbox.attr('data-star-count') || 5
					}).bind('starbox-value-changed', function(event, value) {
					if(starbox.hasClass('random')) {
					var val = Math.random();
					starbox.next().text(' '+val);
					return val;
					}
				})
			});
		});
		</script>
<!---//End-rate---->
<link href="<?= $data->base_url ?>assets/homepage/css/form.css" rel="stylesheet" type="text/css" media="all" />
<style>
.mid-pop {
    border: 0px solid #EFECEC;
}
</style>

<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '365006690755478');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=365006690755478&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->


</head>
<body>
<!--header-->
<?php include_once $data->homedir.'view/homepage/header.php'; ?>
<!--banner-->

	<!--content-->
		<div style="background: #f3f3f3 !important;" class="product">
			<div class="container">
			<?php include_once $data->homedir.'view/homepage/sidebar.php'; ?>
			<div class="col-md-10">
				<div class="mid-popular">
					<?php if (count($data->listitems)=='0'): ?>
						<script>alert('Maaf, untuk saat ini barang di kategori tersebut belum tersedia :(');</script>
						<script>location.replace("<?= $data->base_url.'produk' ?>");</script>
					<?php else: ?>
						<?php foreach ($data->listitems as $key): ?>
							<div class="col-md-4 item-grid1 simpleCart_shelfItem">
							<div class=" mid-pop">
								<div class="pro-img crop crop-square">
									<img src="<?= DefaultProductPicPath($key->bg_name,$data->base_url) ?>" class="img-responsive" alt="">
									<div class="zoom-icon ">
									<a class="picture" href="<?= DefaultProductPicPath($key->bg_name,$data->base_url) ?>" rel="title" class="b-link-stripe b-animate-go  thickbox"><i class="glyphicon glyphicon-search icon "></i></a>
										</div>
									</div>
								<div class="mid-1">
								<div class="women">
								<div class="women-top">
									<span><?= $key->kb_ket ?></span>
									<h6><a href=<?php echo "'".$data->base_url."produk/neworder/".$key->aidi."'";?>><?= $key->jenis_barang.' '.$key->detail_jenis ?></a></h6>
									</div>
									<div class="clearfix"></div>
									</div>
									<div class="mid-2">
										<p><em class="item_price">IDR <?= number_format($key->th_harga) ?></em></p>
										<div class="clearfix"></div>
										<a href="<?= $data->base_url.'produk/neworder/'.$key->aidi ?>" class="btn btn-success" style="float: right">Order</a>
									</div>


								</div>
							</div>
							</div>
						<?php endforeach ?>
					<?php endif ?>
						

					<div class="clearfix"></div>
				</div>
			</div>

			<div class="clearfix"></div>
			</div>
				<!--products-->

			<!--//products-->
		<!--brand-->
		<div class="container">
			<?php include_once $data->homedir.'view/homepage/brand.php'; ?>
			</div>
			<!--//brand-->


		</div>
	<!--//content-->
		<!--//footer-->
	<?php include_once $data->homedir.'view/homepage/footer.php'; ?>

		<!--//footer-->
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<?php include_once $data->homedir.'view/homepage/vcarts.homepage.php'; ?>

	<script src="<?= $data->base_url ?>assets/homepage/js/simpleCart.min.js"> </script>
<!-- slide -->
<script src="<?= $data->base_url ?>assets/homepage/js/bootstrap.min.js"></script>
 <!--light-box-files -->
		<script src="<?= $data->base_url ?>assets/homepage/js/jquery.chocolat.js"></script>
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage/css/chocolat.css" type="text/css" media="screen" charset="utf-8">
		<!--light-box-files -->
		<script type="text/javascript" charset="utf-8">
		$(function() {
			$('a.picture').Chocolat();
		});
		</script>
</body>
</html>
