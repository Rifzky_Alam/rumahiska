

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= $data->title ?></title>
  <link rel="stylesheet" href="<?= $data->base_url ?>assets/common/bootstrap/css/bootstrap.min.css">
</head>
<body>
<nav class="navbar navbar-default" role="navigation" style="margin-bottom:0px;">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?= $data->base_url ?>">RumahIska</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"><a href="<?= $data->base_url ?>">Home</a></li>
        <li><a href="<?= $data->base_url.'produk' ?>">Produk</a></li>
       <li><a href="<?= $data->base_url.'artikel/list' ?>">Artikel</a></li>
        <li><a href="<?= $data->base_url.'company/kontak' ?>">Kontak</a></li>
       
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

<div class="jumbotron jumbotron-sm">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-lg-12">
                <h1 class="h1">Keranjang Belanja</h1>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th style="text-align:center;" colspan="2">Nama Barang</th>
                        <th style="text-align:center;">Harga</th>
                        <th style="text-align:center;">Jumlah</th>
                        <th style="text-align:center;">Subtotal</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (count($data->carts)=='0'): ?>
                    <tr>
                        <td colspan="5">Belum Ada Barang</td>
                    </tr>
                    <?php else: ?>
                        <?php 
                            $total = 0;
                            $totalberat = 0;
                        ?>
                        <?php foreach ($data->carts as $key => $value) { ?>
                        <tr>
                            <td>
                                <center>
                                <img src="<?= DefaultProductPicPath($value['foto'],$data->base_url) ?>" class="img img-responsive" style="width:60px;height:80px;">
                                </center>
                            </td>
                            <td style="vertical-align:middle;">
                                <?= $value['kat'].' '.$value['jns'].' '.$value['nb'] ?>
                                <a href="<?= $data->base_url.'produk/deleteitem/'.$key ?>" title="Delete"><span class="glyphicon glyphicon-trash"></span></a>        
                            </td>
                            <td style="vertical-align:middle;"><?= number_format($value['price']) ?></td>
                            <td style="vertical-align:middle;">
                                <center>
                                    <a href="#">
                                        <i class="glyphicon glyphicon-minus" style="padding-right:5px;"></i>
                                    </a> 
                                    <span id="iqty">
                                        <?= $value['qty'] ?>
                                    </span> 
                                    <a href="#">
                                        <i class="glyphicon glyphicon-plus" style="padding-left:5px;"></i>
                                    </a>
                                </center>
                            </td>
                            <td style="vertical-align:middle;"><?= number_format($value['qty'] * $value['price']) ?></td>
                        </tr>
                        <?php } ?>
                    <?php endif ?>
                        
                    <!---->
                </tbody>
            </table>
            <a href="<?= $data->base_url.'produk' ?>" class="btn btn-lg btn-warning"><< Kembali Belanja</a>
            <a href="<?= $data->base_url.'order/payment' ?>" class="btn btn-lg btn-primary">Pembayaran >></a>
        </div>
    </div>
</div>
<script src="<?= $data->base_url ?>assets/common/bootstrap/js/jquery.js"></script>
<script src="<?= $data->base_url ?>assets/common/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>