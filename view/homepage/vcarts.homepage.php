<!-- Modal -->
  <div class='modal fade' id='modal-carts' role='dialog'>
    <div class='modal-dialog'>
    
      <!-- Modal content-->
      <div class='modal-content'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'>&times;</button>
          <h3 class="modal-title">Keranjang belanja anda</h3>

        </div>
        <div class='modal-body'>
        <div style='padding-left:15px;padding-right:15px;padding-top:0px; height: 300px;overflow-y:auto;' class='row'>
          
          <div class="col-md-12">
			<table class="table table-bordered">
			<thead id="tablehead">
				<tr>
					<th>No</th>
					<th>Items</th>
          <th>Jumlah</th>
          <th>Hapus</th>
				</tr>
			</thead>
			<tbody id="tablebody">
					
						<?php if (count($data->items)!='0'): ?>
							<?php $num = 1; ?>
							<?php foreach ($data->items as $key => $value) { ?>
								<tr class='items'>
									<td><?= $num ?></td>
									<td><?= $value['kat']. ' : '.$value['jns'].' - '. $value['nb'] ?></td>
                  <td><?= $value['qty'] ?></td>
                  <td><a href="<?= $data->me_url.'?del='.$key ?>" title="Hapus barang">Hapus</a></td>
								</tr>
								<?php $num++; ?>
							<?php } ?>
						<?php endif ?>
				</tbody>
			</table> 

			<button type='button' class='btn btn-lg btn-primary' data-dismiss='modal'>Teruskan berbelanja</button>
			<a href="<?= $data->package ?>/payment" title="Teruskan berbelanja" class="btn btn-lg btn-success">
				Lanjutkan ke pembayaran >>
			</a>
          </div>
             
          

        </div>
          

        </div>
        <div class='modal-footer'>
            <div class="row">
                <div class="col-md-6" style="text-align:left">
                    <span>Rumahiska 2017</span>
                </div>
                <div class="col-md-6">
                    
                </div>
            </div>
          
        </div>
      </div>
      
    </div>
  </div>
  <!-- end modal -->