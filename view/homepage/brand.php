<div class="brand">
  <div  class="container">
  <p style="text-align:center;font-size:12px">Rumahiska.com adalah situs belanja fesyen muslimah di Indonesia. Rumah Iska adalah produsen jilbab harian untuk muslimah aktif, dengan brand "Salvina" dan tagline "Beauty in Simplicity". Kami menyediakan produk fesyen berkualitas dengan harga terjangkau yang bervariasi dari pakaian muslimah, gamis/abaya, jilbab, basic, aksesori, sepatu, dan tas.
    Komitmen kami adalah memberikan pengalaman belanja online yang menyenangkan, mudah, dan terpercaya melalui koleksi baru dan penawaran spesial setiap harinya.</p>
</div>
</div>
