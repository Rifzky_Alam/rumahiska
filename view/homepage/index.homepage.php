<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
<title><?= $data->judul ?></title>
<script src="<?= $data->base_url ?>assets/homepage/js/jquery.min.js"></script>
<!-- <link href="<?= $data->base_url ?>assets/homepage/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" /> -->
<link rel="stylesheet" href="<?= $data->base_url ?>assets/bootstrap331/css/bootstrap.min.css">
<!-- Custom Theme files -->
<!--theme-style-->
<link href="<?= $data->base_url ?>assets/homepage/css/style.css" rel="stylesheet" type="text/css" media="all" />
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--theme-style-->
<link href="<?= $data->base_url ?>assets/homepage/css/style4.css" rel="stylesheet" type="text/css" media="all" />
<link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
<!--//theme-style-->

<!--- start-rate---->
<script src="<?= $data->base_url ?>assets/homepage/js/jstarbox.js"></script>
	<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage/css/jstarbox.css" type="text/css" media="screen" charset="utf-8" />
		<script type="text/javascript">
			jQuery(function() {
			jQuery('.starbox').each(function() {
				var starbox = jQuery(this);
					starbox.starbox({
					average: starbox.attr('data-start-value'),
					changeable: starbox.hasClass('unchangeable') ? false : starbox.hasClass('clickonce') ? 'once' : true,
					ghosting: starbox.hasClass('ghosting'),
					autoUpdateAverage: starbox.hasClass('autoupdate'),
					buttons: starbox.hasClass('smooth') ? false : starbox.attr('data-button-count') || 5,
					stars: starbox.attr('data-star-count') || 5
					}).bind('starbox-value-changed', function(event, value) {
					if(starbox.hasClass('random')) {
					var val = Math.random();
					starbox.next().text(' '+val);
					return val;
					}
				})
			});
		});
		</script>
<!---//End-rate---->

<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '365006690755478');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=365006690755478&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<style>
body {
	font-family: 'Quicksand', sans-serif;
	    /* background: #f3f3f3 !important; */
}
.content-top {
	padding-top: 20px;
}
.col-2 {
    padding-top: 1.5em;
}


</style>
</head>
<body>
<!--header-->
<?php include_once $data->homedir.'view/homepage/header.php'; ?>

<!--banner-->
<!-- <div class="banner">
<div class="container">
<section class="rw-wrapper">
				<h1 class="rw-sentence">
					<span>Lorem &amp; ipsum</span>
					<div class="rw-words rw-words-1">
						<span>dolor sit amet (1)</span>
						<span>dolor sit amet (2)</span>
						<span>dolor sit amet (3)</span>
					</div>
					<div class="rw-words rw-words-2">
						<span>consectetur adipiscing elit (1)</span>
						<span>consectetur adipiscing elit (2)</span>
						<span>consectetur adipiscing elit (3)</span>

					</div>
				</h1>
			</section>
			</div>
</div> -->


<!-- <header id="page-top">
	  <img width="100%" src="<?= $data->base_url ?>uploads/images/homepage/banner/banner.jpg" alt="">
</header> -->


<div style="width:100%" >
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->

    <ol class="carousel-indicators">
      <?php $num = 0; ?>
      <?php foreach ($data->banners as $key): ?>
      	<?php if ($num=='0'): ?>
      		<li data-target="#myCarousel" data-slide-to="<?= $num ?>" class="active"></li>
      		<?php else: ?>
      		<li data-target="#myCarousel" data-slide-to="<?= $num ?>"></li>
      	<?php endif ?>
      	<?php $num++; ?>
      <?php endforeach ?>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
    <?php $num = 0; ?>
    <?php foreach ($data->banners as $key): ?>
    	<?php if ($num=='0'): ?>
    	<div class="item active">
        	<img src="<?= $data->base_url.'media/home/'.$key['fd_filename'] ?>" alt="salvina-banner" style="width:100%;">
      	</div>
    	<?php else: ?>
    	<div class="item">
        	<img src="<?= $data->base_url.'media/home/'.$key['fd_filename'] ?>" alt="salvina-banner" style="width:100%;">
      	</div>
    	<?php endif ?>
    	<?php $num++; ?>
    <?php endforeach ?>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>







<!--
<div class="container" style="max-width:1000px">
	<h3 style="margin-top:40px;font-size: 1em;color: #000000;line-height: 1.8em; font-family:'Ubuntu-Regular';text-align: center;">PENAWARAN TERBAIK</h3>
	<label style="margin-bottom:20px" class="line"></label>
	<div class="col-md-6 col-md1">
		<div class="col-3">
		 <a href="<?php echo $data->base_url.'/produk/detail/djaw243sfnewf03412efd';?>" class="b-link-stroke b-animate-go thickbox">
		<img src="<?= $data->base_url ?>assets/homepage/images/10300032_1510396719290135_7816138436312735370_n.jpg" class="img-responsive" alt=""/>
		<div class="b-wrapper1 long-img">
			<p class="b-animate b-from-right    b-delay03 ">INFO SELENGKAPNYA</p><label class="b-animate b-from-right    b-delay03 "></label>
		</div></a>
		</div>
	</div>
	<div class="col-md-6 col-md">
		<div class="col-1">
		 <a href=<?php echo "'".$data->base_url."/produk/detail/djaw243sfnewf03412efd"."'";?> class="b-link-stroke b-animate-go  thickbox">
		<img src="<?= $data->base_url ?>assets/homepage/images/17951777_598961746966805_2633833949475257977_n.jpg" class="img-responsive" alt=""/><div class="b-wrapper1 long-img"><p class="b-animate b-from-right    b-delay03 ">INFO SELENGKAPNYA</p><label class="b-animate b-from-right    b-delay03 "></label></div></a>
		</div>
	</div>
</div>
-->

<div class="container" style="max-width:1200px">
			<h3 style="margin-top:40px;font-size: 1em;color: #000000;line-height: 1.8em; font-family:'Ubuntu-Regular';text-align: center;">
				PRODUK UNGGULAN
			</h3>
			<label style="" class="line"></label>
			<div class="mid-popular">

		<?php if (count($data->prfv)=='0'): ?>
			<div class="jumbotron">
				<h3>Data Belum Tersedia</h3>
				<p>Kami belum menaruh data untuk kategori ini.</p>
			</div>
		<?php else: ?>
			<?php foreach ($data->prfv as $key): ?>
				<!-- unggulan -->
				<div class="col-md-3 item-grid simpleCart_shelfItem">
					<div class=" mid-pop">
						<div class="pro-img crop crop-square">
							<img src="<?= DefaultProductPicPath($key->bg_name,$data->base_url) ?>" class="img-responsive" alt="<?= $key->jenis_barang ?>">
							<div class="zoom-icon ">
								<a class="picture" href="<?= DefaultProductPicPath($key->bg_name,$data->base_url) ?>" rel="title" class="b-link-stripe b-animate-go  thickbox"><i class="glyphicon glyphicon-search icon "></i></a>
							</div>
						</div>
						<div class="mid-1">
							<div class="women">
								<div class="women-top">
									<h6 style="">
										<a href="<?= $data->base_url.'produk/neworder/'.$key->jb_id ?>">
										<?= $key->kb_ket.' '.$key->jenis_barang ?>
										</a>
									</h6>
								</div>
									<div class="clearfix"></div>
								<div class="mid-2">
									<p><em class="item_price"><?= number_format($key->th_harga) ?> IDR</em></p>
									<div class="clearfix"></div>
									<a href="<?= $data->base_url.'produk/neworder/'.$key->jb_id ?>" class="btn btn-success" style="float: right">Order</a>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				</div>
			<?php endforeach ?>
		<?php endif ?>

		<div class="clearfix"></div>

	</div>

	<div class="clearfix"></div>
</div>



<?php if (count($data->prnew)=='0'): ?>

<?php else: ?>
	<div class="container" style="max-width:1200px">
		<h3 style="margin-top:40px;font-size: 1em;color: #000000;line-height: 1.8em; font-family:'Ubuntu-Regular';text-align: center;">PRODUK TERBARU</h3>
		<label style="" class="line"></label>
		<div class="mid-popular">
			<?php foreach ($data->prnew as $key): ?>
				<div class="col-md-3 item-grid simpleCart_shelfItem">
					<div class=" mid-pop">
						<div class="pro-img crop crop-square">
							<img src="<?= DefaultProductPicPath($key->bg_name,$data->base_url) ?>" class="img-responsive" alt="<?= $key->jenis_barang ?>">
						<div class="zoom-icon ">
							<a class="picture" href="<?= DefaultProductPicPath($key->bg_name,$data->base_url) ?>" rel="title" class="b-link-stripe b-animate-go  thickbox">
								<i class="glyphicon glyphicon-search icon "></i>
							</a>
						</div>
						</div>
						<div class="mid-1">
							<div class="women">
								<div class="women-top">
									<h6>
										<a href="<?= $data->base_url.'produk/neworder/'.$key->jb_id ?>">
											<?= $key->jenis_barang ?>
										</a>
									</h6>
								</div>
									<div class="clearfix"></div>
								<div class="mid-2">
									<p><em class="item_price"><?= number_format($key->th_harga) ?> IDR</em></p>
									<div class="clearfix"></div>
									<a href="<?= $data->base_url.'produk/neworder/'.$key->jb_id ?>" class="btn btn-success" style="float: right">Order</a>
								</div>

								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				</div>
			<?php endforeach ?>
			<div class="clearfix"></div>

		</div>
	</div>
<?php endif ?>



	<!--content-->
		<div class="content">
			<?php include_once $data->homedir.'view/homepage/brand.php'; ?>
		</div>
	<!--//content-->
	<!--//footer-->
	<?php include_once $data->homedir.'view/homepage/footer.php'; ?>
		<!--//footer-->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="<?= $data->base_url ?>assets/homepage/js/simpleCart.min.js"> </script>
<!-- slide -->
<script src="<?= $data->base_url ?>assets/homepage/js/bootstrap.min.js"></script>
<!--light-box-files -->
		<script src="<?= $data->base_url ?>assets/homepage/js/jquery.chocolat.js"></script>
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage/css/chocolat.css" type="text/css" media="screen" charset="utf-8">
		<!--light-box-files -->
		<script type="text/javascript" charset="utf-8">
		$(function() {
			$('a.picture').Chocolat();
		});
		</script>

<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5c5550816cb1ff3c14cad2a3/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
</body>
</html>
