

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= $data->title ?></title>
  <link rel="stylesheet" href="<?= $data->base_url ?>assets/common/bootstrap/css/bootstrap.min.css">
</head>
<body>
<nav class="navbar navbar-default" role="navigation" style="margin-bottom:0px;">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?= $data->base_url ?>"><?= $data->company ?></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"><a href="<?= $data->base_url ?>">Home</a></li>
        <li><a href="<?= $data->base_url.'produk' ?>">Produk</a></li>
       <li><a href="<?= $data->base_url.'artikel/list' ?>">Artikel</a></li>
        <li><a href="<?= $data->base_url.'company/kontak' ?>">Kontak</a></li>
       
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

<div class="jumbotron jumbotron-sm">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-lg-12">
                <h1 class="h1">
                    Kontak Kami <small>dan umpan balik</small></h1>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="well well-sm">
                <form action="" method="post">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">
                                Nama</label>
                            <input type="text" class="form-control" id="name" name="in[nama]" placeholder="Enter name" required="required" />
                        </div>
                        <div class="form-group">
                            <label for="email">
                                Email</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span>
                                </span>
                                <input type="email" name="in[email]" class="form-control" id="email" placeholder="Enter email" required="required" /></div>
                        </div>
                        <div class="form-group">
                            <label for="subject">
                                Subjek</label>
                            <select id="subject" name="in[subjek]" class="form-control" required="required">
                                <option value="" selected="">Pilih Salah Satu:</option>
                                <option value="1">Layanan Customer</option>
                                <option value="2">Saran</option>
                                <option value="3">Dukungan Produk</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">
                                Pesan</label>
                            <textarea name="in[pesan]" id="message" class="form-control" rows="9" cols="25" required="required"
                                placeholder="Message"></textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary pull-right" id="btnContactUs">
                            Kirim Pesan</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
        <div class="col-md-4">
            <form>
            <legend><span class="glyphicon glyphicon-globe"></span> Kantor Kami</legend>
            <address>
                <strong><?= $data->company ?></strong><br>
                <?= $data->addrcompany ?>

                <br>
                <abbr title="Phone">
                    Telp:</abbr>
                <a href="tel:\\<?= $data->companyphone ?>" title="<?= $data->companyphone ?>"><?= $data->companyphone ?></a>
            </address>
            <address>
                <strong><?= $data->companycontactname ?></strong><br>
                <a href="mailto:<?= $data->companyemail ?>"><?= $data->companyemail ?></a>
            </address>
            </form>
        </div>
    </div>
</div>
<script src="<?= $data->base_url ?>assets/common/bootstrap/js/jquery.js"></script>
<script src="<?= $data->base_url ?>assets/common/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>