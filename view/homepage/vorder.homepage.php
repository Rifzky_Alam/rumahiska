<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<?php include_once 'baseurl.php'; ?>
<html>
<head>
<title><?= $data->judul ?></title>
<script src="<?= $data->base_url ?>assets/homepage/js/jquery.min.js"></script>
<!-- <link href="<?= $data->base_url ?>assets/homepage/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" /> -->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
<!-- Custom Theme files -->
<!--theme-style-->
<link href="<?= $data->base_url ?>assets/homepage/css/style.css" rel="stylesheet" type="text/css" media="all" />
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--theme-style-->
<link href="<?= $data->base_url ?>assets/homepage/css/style4.css" rel="stylesheet" type="text/css" media="all" />
<link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
<!--//theme-style-->

<!--- start-rate---->
<script src="<?= $data->base_url ?>assets/homepage/js/jstarbox.js"></script>
	<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage/css/jstarbox.css" type="text/css" media="screen" charset="utf-8" />
		<script type="text/javascript">
			jQuery(function() {
			jQuery('.starbox').each(function() {
				var starbox = jQuery(this);
					starbox.starbox({
					average: starbox.attr('data-start-value'),
					changeable: starbox.hasClass('unchangeable') ? false : starbox.hasClass('clickonce') ? 'once' : true,
					ghosting: starbox.hasClass('ghosting'),
					autoUpdateAverage: starbox.hasClass('autoupdate'),
					buttons: starbox.hasClass('smooth') ? false : starbox.attr('data-button-count') || 5,
					stars: starbox.attr('data-star-count') || 5
					}).bind('starbox-value-changed', function(event, value) {
					if(starbox.hasClass('random')) {
					var val = Math.random();
					starbox.next().text(' '+val);
					return val;
					}
				})
			});
		});
		</script>

		<style>
		.hide-bullets {
list-style:none;
margin-left: -0px;
margin-top:20px;
}
		</style>
<!---//End-rate---->
<link href="<?= $data->base_url ?>assets/homepage/css/form.css" rel="stylesheet" type="text/css" media="all" />
</head>
<body>
<!--header-->
<?php include_once $data->homedir.'view/homepage/header.php'; ?>
<!--banner-->
<div style="background: #f3f3f3 !important;" class="single">

<div class="container">
<div class="col-md-12">
<div class="col-xs-12 col-md-4" id="carousel-bounding-box">
	<?php if (count(@$data->galeri)=='0'): ?>

		<div class="carousel slide" id="myCarousel" >
			<div class="carousel-inner">
				<div class="active item" data-slide-number="0">
					<img src="<?= DefaultProductPicPath('',$data->base_url) ?>">
				</div>
			</div>
			<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left"></span>
			</a>
			<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right"></span>
			</a>
		</div>

		<div class="row hidden-xs" id="slider-thumbs">
			<ul class="hide-bullets">
				<li class="col-sm-3">
					<a class="thumbnail" id="carousel-selector-0">
						<img src="<?= DefaultProductPicPath('',$data->base_url) ?>">
					</a>
				</li>
			</ul>
		</div>

	<?php else: ?>
		<?php $num = 0; ?>

		<div class="carousel slide" id="myCarousel" >
			<div class="carousel-inner">
				<?php foreach ($data->galeri as $key): ?>
					<?php if ($num==0): ?>
						<div class="active item" data-slide-number="<?= $num ?>">
							<img src="<?= DefaultProductPicPath($key->bg_name,$data->base_url) ?>">
						</div>
					<?php else: ?>
						<div class="item" data-slide-number="<?= $num ?>">
							<img src="<?= DefaultProductPicPath($key->bg_name,$data->base_url) ?>">
						</div>
					<?php endif ?>
						<?php $num++; ?>
				<?php endforeach ?>
			</div>
			<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left"></span>
			</a>
			<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right"></span>
			</a>
		</div>



			<div class="row hidden-xs" id="slider-thumbs">
				<ul class="hide-bullets">
					<?php $num = 0; ?>
					<?php foreach (@$data->galeri as $key): ?>
					<li class="col-sm-3">
						<a class="thumbnail" <?= 'id="carousel-selector-'.$num.'"' ?>>
							<img src="<?= DefaultProductPicPath($key->bg_name,$data->base_url) ?>">
						</a>
					</li>
					<?php $num++; ?>
					<?php endforeach ?>
				</ul>
			</div>

	<?php endif ?>

<!-- <img class="myImg" onclick="reply_click(this)" src="images/ris/19114009_629964410533205_7693411940020148899_n.jpg" alt="" width="100%"> -->
</div>


	<!-- <div class="col-md-3 grid">
		<img class="myImg"  onclick="reply_click(this)"  src="images/ris/19149058_629964413866538_3803655667581665984_n.jpg" alt="Trolltunga, Norway" width="100%">
	</div> -->


	<div id="myModal" class="modal">
	  <span class="close">&times;</span>
	  <img class="modal-content" id="img01">
	</div>


<div class="col-xs-12 col-md-5 single-top-in">
		<div class="span_2_of_a1 simpleCart_shelfItem">
				<h3>
					<?= GetDefaultValTidakAda(@$data->item->kb_ket).' '.GetDefaultValNull(@$data->item->jenis_barang).' '.GetDefaultValNull(@$data->item->detail_jenis) ?>
				</h3>
			   <div class="price_single" style="padding-top:15px;">
				  <span id="iprc" class="reducedfrom item_price"><?= number_format(GetDefaultValZero(@$data->item->brg_harga)).' IDR' ?></span>
				</div>
				<!--
				<p class="quick_desc"> Jilbab motif berbahan double hycone ini lembut dan enak banget di pakai, dengan hiasan pinggir Rawis, bisa bikin kamu tambah manis lho... sangat cocok untuk keseharianmu </p>
				-->
				<br>
				<section>
					<h4>Stok Barang</h4>
					<p><?= GetDefaultValHypen(@$data->item->jumlah_barang).' '.GetDefaultValHypen(@$data->item->brg_satuan) ?></p>
				</section>
				<br>
				<form action="" method="post" accept-charset="utf-8">
				<div class="form-group col-md-10">


					<div class="form-group">
		  <label for="sel1">Tipe/Warna/Ukuran:</label>
		  <select class="form-control" name="in[id_detail]" id="sel1">
		    
		  </select>
		</div>


					<label>Jumlah Pesanan</label>
					<input type="number" name="in[qty]" class="form-control" value="0" placeholder="Hanya Angka">
				</div>
			    <button class="add-to item_add hvr-skew-backward">Add to cart</button>
			    </form>
			<div class="clearfix"> </div>
			</div>


			<div style="margin-top:100px" class="span_2_of_a1 simpleCart_shelfItem">
				<p>Produk Terkait</p>
				<ul class="hide-bullets">
					<?php if (count($data->listprodukterkait)=='0'): ?>

					<?php else: ?>
						<?php foreach ($data->listprodukterkait as $key): ?>
							<li class="col-sm-3">
								<a class="thumbnail" href="<?= $data->base_url.'produk/order/'.$key['id_jb'].'/'.$key['id'] ?>" title="<?= $key['kb_ket'].' '.$key['jenis_barang'].' '.$key['detail_jenis'] ?>">
									<img src="<?= DefaultProductPicPath($key['bg_name'],$data->base_url) ?>">
								</a>
							</li>
						<?php endforeach ?>
					<?php endif ?>
				</ul>
			</div>

	</div>




	<div class="col-md-3 product-bottom">
	<!--categories-->
		<div class=" rsidebar span_1_of_left">

				<h4 class="cate">Categories</h4>
<ul class="menu-drop">
								<?php for ($i = 0; $i < count($data->sidebardata); $i++) { ?>
									<li class="item1"><a href="#"><?php echo $data->sidebardata[$i]->kb_ket ?> </a>
										<ul class="cute">
											<?php $iditems = explode(' # ', $data->sidebardata[$i]->id_jenis) ?>
											<?php $items = explode(' # ', $data->sidebardata[$i]->jenis_barang) ?>
											<?php for ($j = 0; $j < count($iditems); $j++) { ?>
												<li class="subitem1">
													<a href="<?= $data->base_url.'produk/jenis/'.$iditems[$j] ?>"><?= $items[$j] ?></a>
												</li>
											<? } ?>
										</ul>
									</li>
								<? } ?>
							</ul>
			</div>
		<!--initiate accordion-->
				<script type="text/javascript">
					$(function() {
							var menu_ul = $('.menu-drop > li > ul'),
										 menu_a  = $('.menu-drop > li > a');
							menu_ul.hide();
							menu_a.click(function(e) {
									e.preventDefault();
									if(!$(this).hasClass('active')) {
											menu_a.removeClass('active');
											menu_ul.filter(':visible').slideUp('normal');
											$(this).addClass('active').next().stop(true,true).slideDown('normal');
									} else {
											$(this).removeClass('active');
											$(this).next().stop(true,true).slideUp('normal');
									}
							});

					});
				</script>
	<!--//menu-->

	</div>


</div>
	</div>

			<!--brand-->
		<?php include_once $data->homedir.'view/homepage/brand.php'; ?>
			<!--//brand-->
		</div>

	<!--//content-->
		<!--//footer-->
	<?php include_once $data->homedir.'view/homepage/footer.php'; ?>
		<!--//footer-->
	<?php include_once $data->homedir.'view/homepage/vcarts.homepage.php'; ?>

<script src="<?= $data->base_url ?>assets/homepage/js/imagezoom.js"></script>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script defer src="<?= $data->base_url ?>assets/homepage/js/jquery.flexslider.js"></script>
<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage/css/flexslider.css" type="text/css" media="screen" />

<script>

jQuery(document).ready(function($) {

			$('#myCarousel').carousel({
							// interval: 5000
							interval: false
			});

			$('#carousel-text').html($('#slide-content-0').html());

			//Handles the carousel thumbnails
		 $('[id^=carousel-selector-]').click( function(){
					var id = this.id.substr(this.id.lastIndexOf("-") + 1);
					var id = parseInt(id);
					$('#myCarousel').carousel(id);
			});


			// When the carousel slides, auto update the text
			$('#myCarousel').on('slid.bs.carousel', function (e) {
							//  var id = $('.item.active').data('slide-number');
							// $('#carousel-text').html($('#slide-content-'+id).html());
			});
			// alert('oke');
			$.ajax({
          		type: "GET",
          		url: "<?= $data->base_url.'library/rajaongkir/' ?>store",
          		data: {
            		'prov': '6'
          		},
          		cache: false,
          		success: function(data){

          }
      }); //end ajax

			$.ajax({
            type: "GET",
            url: "<?= $data->base_url ?>uat/service",
            data: {
              'b':<?= "'".$data->jenis_barang."'" ?>
            },
            cache: false,
            success: function(data){
              var myjson = JSON.parse(data);
              for (x in myjson) {
                // console.log(myjson[x].id);
                // console.log(myjson);
                document.getElementById("sel1").innerHTML += "<option value='"+myjson[x].id_brg+"' class='ok'>"+myjson[x].kategori+" "+myjson[x].jenis + " " + myjson[x].dtl_brg +" | "+ myjson[x].jumlah + " " + myjson[x].satuan +"</option>";
              }
              console.log($('#sel1').val());
              Tes($('#sel1').val());
            }
          }); //end ajax




});

$('#sel1').change(function(){
	// alert($(this).val());
	console.log($(this).val());
	Tes($(this).val());
});

function Tes(idbarang) {
	Number.prototype.format = function(n, x) {
        var re = '(\\d)(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    	return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$1,');
    };

	$.ajax({
		type: "GET",
        url: "<?= $data->base_url ?>uat/service",
        data: {
        	'prc':idbarang
		},
        cache: false,
        success: function(data){
        	var myjson = JSON.parse(data);
        	// console.log(myjson);
        	var harga = parseInt(myjson.harga);
        	$('#iprc').html(harga.format() +' IDR');
   		}
    }); //end ajax
}

</script>


<script>
var modal = document.getElementById('myModal');
var modalImg = document.getElementById("img01");

function reply_click(obj) {
	modal.style.display = "block";
	modalImg.src = obj.src;
}


// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
		modal.style.display = "none";
}

</script>

	<script src="<?= $data->base_url ?>assets/homepage/js/simpleCart.min.js"> </script>
<!-- slide -->
<script src="<?= $data->base_url ?>assets/homepage/js/bootstrap.min.js"></script>


</body>
</html>
