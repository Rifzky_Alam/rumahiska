<div  class="footer">
<div class="footer-middle">
			<div class="container">
				<div class="col-md-3 footer-middle-in">
					<a href=<?php echo "'".$data->base_url."'";?>><img width="60px" src="images/_logo2.png" alt=""></a>
					<p>Rumah Iska adalah produsen jilbab harian untuk muslimah aktif, dengan brand "Salvina" dan tagline "Beauty in Simplicity". </p>
				</div>

				<div class="col-md-3 footer-middle-in">
					<h6>Information</h6>
					<ul class=" in">
						<li><a href="<?= $data->base_url ?>produk">Products</a></li>
						<li><a href="<?= $data->base_url ?>company/kontak">Contact Us</a></li>
						<li><a href="<?= $data->base_url ?>artikel/list">Article</a></li>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div class="col-md-3 footer-middle-in">
					<h6 style="text-align:center">Pembayaran</h6>
					<!-- <p style="text-align:center"><img width="250px" src="<?= $data->base_url ?>assets/homepage/images/logo-bank.png" /></p> -->
				</div>
				<div class="col-md-3 footer-middle-in">
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
		<div class="footer-bottom">
			<div class="container">
				<ul class="footer-bottom-top">
					<!-- <li><a href="#"><img src="images/f1.png" class="img-responsive" alt=""></a></li> -->
				</ul>
				<p class="footer-class" style="font-size:10px">&copy; 2018 Rumah Iska. All Rights Reserved | Design by  <a href="http://w3layouts.com/" target="_blank">W3layouts</a> </p>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
