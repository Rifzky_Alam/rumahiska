<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
<title><?= $data->judul ?></title>
<script src="<?= $data->base_url ?>assets/homepage/js/jquery.min.js"></script>
<!-- <link href="<?= $data->base_url ?>assets/homepage/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" /> -->
<link rel="stylesheet" href="<?= $data->base_url ?>assets/bootstrap331/css/bootstrap.min.css">
<!-- Custom Theme files -->
<!--theme-style-->
<link href="<?= $data->base_url ?>assets/homepage/css/style.css" rel="stylesheet" type="text/css" media="all" />
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--theme-style-->
<link href="<?= $data->base_url ?>assets/homepage/css/style4.css" rel="stylesheet" type="text/css" media="all" />
<link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
<!--//theme-style-->

<!--- start-rate---->
<script src="<?= $data->base_url ?>assets/homepage/js/jstarbox.js"></script>
	<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage/css/jstarbox.css" type="text/css" media="screen" charset="utf-8" />
		<script type="text/javascript">
			jQuery(function() {
			jQuery('.starbox').each(function() {
				var starbox = jQuery(this);
					starbox.starbox({
					average: starbox.attr('data-start-value'),
					changeable: starbox.hasClass('unchangeable') ? false : starbox.hasClass('clickonce') ? 'once' : true,
					ghosting: starbox.hasClass('ghosting'),
					autoUpdateAverage: starbox.hasClass('autoupdate'),
					buttons: starbox.hasClass('smooth') ? false : starbox.attr('data-button-count') || 5,
					stars: starbox.attr('data-star-count') || 5
					}).bind('starbox-value-changed', function(event, value) {
					if(starbox.hasClass('random')) {
					var val = Math.random();
					starbox.next().text(' '+val);
					return val;
					}
				})
			});
		});
		</script>
<!---//End-rate---->

<style>
body {
	font-family: 'Quicksand', sans-serif;
	    /* background: #f3f3f3 !important; */
}
.content-top {
	padding-top: 20px;
}
.col-2 {
    padding-top: 1.5em;
}


</style>
</head>
<body>
<!--header-->
<?php include_once $data->homedir.'view/homepage/header.php'; ?>

<!--banner-->
<!-- <div class="banner">
<div class="container">
<section class="rw-wrapper">
				<h1 class="rw-sentence">
					<span>Lorem &amp; ipsum</span>
					<div class="rw-words rw-words-1">
						<span>dolor sit amet (1)</span>
						<span>dolor sit amet (2)</span>
						<span>dolor sit amet (3)</span>
					</div>
					<div class="rw-words rw-words-2">
						<span>consectetur adipiscing elit (1)</span>
						<span>consectetur adipiscing elit (2)</span>
						<span>consectetur adipiscing elit (3)</span>

					</div>
				</h1>
			</section>
			</div>
</div> -->


<!-- <header id="page-top">
	  <img width="100%" src="<?= $data->base_url ?>uploads/images/homepage/banner/banner.jpg" alt="">
</header> -->


<div style="width:100%" >
  
</div>




<div class="jumbotron jumbotron-sm">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-lg-12">
                <h1 class="h1">
                    Kontak Kami <small>dan umpan balik</small></h1>
            </div>
        </div>
    </div>
</div>



<div class="container" style="max-width:1200px;padding-top40px;">
	<div class="row">
        <div class="col-md-8">
            <div class="well well-sm">
                <form action="" method="post">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">
                                Nama</label>
                            <input type="text" class="form-control" id="name" name="in[nama]" placeholder="Enter name" required="required" />
                        </div>
                        <div class="form-group">
                            <label for="email">
                                Email</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span>
                                </span>
                                <input type="email" name="in[email]" class="form-control" id="email" placeholder="Enter email" required="required" /></div>
                        </div>
                        <div class="form-group">
                            <label for="subject">
                                Subjek</label>
                            <select id="subject" name="in[subjek]" class="form-control" required="required">
                                <option value="" selected="">Pilih Salah Satu:</option>
                                <option value="1">Layanan Customer</option>
                                <option value="2">Saran</option>
                                <option value="3">Dukungan Produk</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">
                                Pesan</label>
                            <textarea name="in[pesan]" id="message" class="form-control" rows="9" cols="25" required="required"
                                placeholder="Message"></textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary pull-right" id="btnContactUs">
                            Kirim Pesan</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
        <div class="col-md-4">
            <form>
            <legend><span class="glyphicon glyphicon-globe"></span> Kantor Kami</legend>
            <address>
                <strong><?= $data->company ?></strong><br>
                <?= $data->addrcompany ?>

                <br>
                <abbr title="Phone">
                    Telp:</abbr>
                <a href="tel:\\<?= $data->companyphone ?>" title="<?= $data->companyphone ?>"><?= $data->companyphone ?></a>
            </address>
            <address>
                <strong><?= $data->companycontactname ?></strong><br>
                <a href="mailto:<?= $data->companyemail ?>"><?= $data->companyemail ?></a>
            </address>
            </form>
        </div>
    </div>

	<div class="clearfix"></div>
</div>





	<!--content-->
		<div class="content">
			<?php include_once $data->homedir.'view/homepage/brand.php'; ?>
		</div>
	<!--//content-->
	<!--//footer-->
	<?php include_once $data->homedir.'view/homepage/footer.php'; ?>
		<!--//footer-->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="<?= $data->base_url ?>assets/homepage/js/simpleCart.min.js"> </script>
<!-- slide -->
<script src="<?= $data->base_url ?>assets/homepage/js/bootstrap.min.js"></script>
<!--light-box-files -->
		<script src="<?= $data->base_url ?>assets/homepage/js/jquery.chocolat.js"></script>
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage/css/chocolat.css" type="text/css" media="screen" charset="utf-8">
		<!--light-box-files -->
		<script type="text/javascript" charset="utf-8">
		$(function() {
			$('a.picture').Chocolat();
		});
		</script>

<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5c5550816cb1ff3c14cad2a3/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
</body>
</html>
