<!DOCTYPE html>
<html lang="en">
  <head>
    
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?= $data->title ?></title>
    <link rel="stylesheet" href="<?= $data->base_url ?>assets/common/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style type="text/css" media="screen">
      
/*****************globals*************/
body {
  font-family: 'open sans';
  overflow-x: hidden; }

img {
  max-width: 100%; }

.preview {
  display: -webkit-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
  -webkit-flex-direction: column;
      -ms-flex-direction: column;
          flex-direction: column; }
  @media screen and (max-width: 996px) {
    .preview {
      margin-bottom: 20px; } }

.preview-pic {
  -webkit-box-flex: 1;
  -webkit-flex-grow: 1;
      -ms-flex-positive: 1;
          flex-grow: 1; }

.preview-thumbnail.nav-tabs {
  border: none;
  margin-top: 15px; }
  .preview-thumbnail.nav-tabs li {
    width: 18%;
    margin-right: 2.5%; }
    .preview-thumbnail.nav-tabs li img {
      max-width: 100%;
      display: block; }
    .preview-thumbnail.nav-tabs li a {
      padding: 0;
      margin: 0; }
    .preview-thumbnail.nav-tabs li:last-of-type {
      margin-right: 0; }

.tab-content {
  overflow: hidden; }
  .tab-content img {
    width: 100%;
    -webkit-animation-name: opacity;
            animation-name: opacity;
    -webkit-animation-duration: .3s;
            animation-duration: .3s; }

.card {
  margin-top: 50px;
  background: #eee;
  padding: 3em;
  line-height: 1.5em; }

@media screen and (min-width: 997px) {
  .wrapper {
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex; } }

.details {
  display: -webkit-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
  -webkit-flex-direction: column;
      -ms-flex-direction: column;
          flex-direction: column; }

.colors {
  -webkit-box-flex: 1;
  -webkit-flex-grow: 1;
      -ms-flex-positive: 1;
          flex-grow: 1; }

.product-title, .price, .sizes, .colors {
  text-transform: UPPERCASE;
  font-weight: bold; }

.checked, .price span {
  color: #ff9f1a; }

.product-title, .rating, .product-description, .price, .vote, .sizes {
  margin-bottom: 15px; }

.product-title {
  margin-top: 0; }

.size {
  margin-right: 10px; }
  .size:first-of-type {
    margin-left: 5px; }

.color {
  display: inline-block;
  vertical-align: middle;
  margin-right: 10px;
  height: 2em;
  width: 2em;
  border-radius: 2px; }
  .color:first-of-type {
    margin-left: 20px; }

.add-to-cart, .like {
  background: #ff9f1a;
  padding: 1.2em 1.5em;
  border: none;
  text-transform: UPPERCASE;
  font-weight: bold;
  color: #fff;
  -webkit-transition: background .3s ease;
          transition: background .3s ease; }
  .add-to-cart:hover, .like:hover {
    background: #b36800;
    color: #fff; }

.not-available {
  text-align: center;
  line-height: 2em; }
  .not-available:before {
    font-family: fontawesome;
    content: "\f00d";
    color: #fff; }

.orange {
  background: #ff9f1a; }

.green {
  background: #85ad00; }

.blue {
  background: #0076ad; }

.tooltip-inner {
  padding: 1.3em; }

@-webkit-keyframes opacity {
  0% {
    opacity: 0;
    -webkit-transform: scale(3);
            transform: scale(3); }
  100% {
    opacity: 1;
    -webkit-transform: scale(1);
            transform: scale(1); } }

@keyframes opacity {
  0% {
    opacity: 0;
    -webkit-transform: scale(3);
            transform: scale(3); }
  100% {
    opacity: 1;
    -webkit-transform: scale(1);
            transform: scale(1); } }

/*# sourceMappingURL=style.css.map */
    </style>


  </head>

  <body>
  
  <div class="container">
    <div class="card">
      <div class="container-fliud">
        <div class="wrapper row">
          <div class="preview col-md-6">
            
            <div class="preview-pic tab-content">
              <div class="tab-pane active" id="pic-1"><img src="http://s3.amazonaws.com/bzzagent-bzzscapes-prod/mms-jpg--jpeg-image--400x300-pixels--lrg.png" /></div>
              <div class="tab-pane" id="pic-2"><img src="http://assets.hardwarezone.com/img/2013/08/apple_logo_450.jpg" /></div>
              <div class="tab-pane" id="pic-3"><img src="http://placekitten.com/400/252" /></div>
              <div class="tab-pane" id="pic-4"><img src="http://placekitten.com/400/252" /></div>
              <div class="tab-pane" id="pic-5"><img src="http://placekitten.com/400/252" /></div>
            </div>
            <ul class="preview-thumbnail nav nav-tabs">
              <li class="active">
                <a data-target="#pic-1" data-toggle="tab">
                  <img style="width:90px;height:56px;" src="http://s3.amazonaws.com/bzzagent-bzzscapes-prod/mms-jpg--jpeg-image--400x300-pixels--lrg.png" />
                </a>
              </li>
              <li>
                <a data-target="#pic-2" data-toggle="tab">
                  <img style="width:90px;height:56px;" src="http://assets.hardwarezone.com/img/2013/08/apple_logo_450.jpg" />
                </a>
              </li>
              <li>
                <a data-target="#pic-3" data-toggle="tab">
                  <img style="width:90px;height:56px;" src="http://placekitten.com/200/126" />
                </a>
              </li>
              <li>
                <a data-target="#pic-4" data-toggle="tab">
                  <img style="width:90px;height:56px;" src="http://placekitten.com/200/126" />
                </a>
              </li>
              <li>
                <a data-target="#pic-5" data-toggle="tab">
                  <img style="width:90px;height:56px;" src="http://placekitten.com/200/126" />
                </a>
              </li>
            </ul>
            
          </div>
          <div class="details col-md-6">
            <h3 class="product-title"><?= $data->product_title ?></h3>
            <div class="rating">
              <div class="stars">
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star"></span>
                <span class="fa fa-star"></span>
              </div>
              <span class="review-no">41 reviews</span>
            </div>
            <p class="product-description"><?= $data->product_desc ?></p>
            <h4 class="price">Harga Barang: <span><?= $data->product_price ?></span></h4>
            <!--<p class="vote"><strong>91%</strong> of buyers enjoyed this product! <strong>(87 votes)</strong></p>-->
            <h5 class="sizes">Stok Barang:
              <span class="size" data-toggle="tooltip" title="<?= $data->product_amount ?>"><?= $data->product_amount.' '.$data->product_qty_type  ?></span>
            </h5>
            <h5 class="colors">colors:
              <span class="color orange not-available" data-toggle="tooltip" title="Not In store"></span>
              <span class="color green"></span>
              <span class="color blue"></span>
            </h5>
            <form action="" method="post">
            <div class="form-group" style="padding-bottom: 15px;">
              <label>Jumlah Pesanan</label>
              <input type="number" name="qty" value="1"> <span><?= $data->product_qty_type ?></span>
            </div>


            <div class="action">
              <button class="add-to-cart btn btn-default">Masukkan keranjang saya</button>
            </form>
              <button class="like btn btn-default" type="button"><span class="fa fa-heart-o"></span></button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script src="<?= $data->base_url ?>assets/common/bootstrap/js/jquery.js"></script>
<script src="<?= $data->base_url ?>assets/common/bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>
