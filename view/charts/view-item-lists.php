<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Tes Cart</title>
  <link rel="stylesheet" href="<?= $data->base_url ?>assets/common/bootstrap/css/bootstrap.min.css">
  </head>
<body>

<div class="container">

  <div class="row">
    <div class="col-md-12">
      <div class="page-header">
        <h3><?= $data->subtitle ?></h3>
      </div>
    </div>
  </div>

  <div class="row">
    <a data-toggle="modal" href="#modal-carts">Show modal</a>
  </div>

	<div class="row">
		<div class="col-md-12">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Kategori</th>
            <th>Jenis Barang</th>
            <th>Detail Jenis</th>
            <th>Barang Harga</th>
            <th>Barang Satuan</th>
            <th>Jumlah Barang</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
          <?php Rows($data->listbarang) ?>
        </tbody>
      </table>
		</div>
	</div>

</div>

<!-- Modal -->
  <div class='modal fade' id='modal-carts' role='dialog'>
    <div class='modal-dialog'>
    
      <!-- Modal content-->
      <div class='modal-content'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'>&times;</button>
          <h3 class="modal-title">Keranjang belanja anda</h3>

        </div>
        <div class='modal-body'>
        <div style='padding-left:15px;padding-right:15px;padding-top:0px; height: 300px;overflow-y:auto;' class='row'>
          
          <div class="col-md-12">
			<table class="table table-bordered">
			<thead id="tablehead">
				<tr>
					<th>No</th>
					<th>Items</th>
          <th>Jumlah</th>
          <th>Hapus</th>
				</tr>
			</thead>
			<tbody id="tablebody">
					
						<?php if (count($data->items)!='0'): ?>
							<?php $num = 1; ?>
							<?php foreach ($data->items as $key => $value) { ?>
								<tr class='items'>
									<td><?= $num ?></td>
									<td><?= $value['kat']. ' : '.$value['jns'].' - '. $value['nb'] ?></td>
                  <td><?= $value['qty'] ?></td>
                  <td><a href="<?= $data->me_url.'?del='.$key ?>" title="Hapus barang">Hapus</a></td>
								</tr>
								<?php $num++; ?>
							<?php } ?>
						<?php endif ?>
				</tbody>
			</table> 

			<button type='button' class='btn btn-lg btn-primary' data-dismiss='modal'>Teruskan berbelanja</button>
			<a href="<?= $data->package ?>/payment" title="Teruskan berbelanja" class="btn btn-lg btn-success">
				Lanjutkan ke pembayaran >>
			</a>
          </div>
             
          

        </div>
          

        </div>
        <div class='modal-footer'>
            <div class="row">
                <div class="col-md-6" style="text-align:left">
                    <span>Rumahiska 2017</span>
                </div>
                <div class="col-md-6">
                    
                </div>
            </div>
          
        </div>
      </div>
      
    </div>
  </div>
  <!-- end modal -->

<script src="<?= $data->base_url ?>assets/common/bootstrap/js/jquery.js"></script>
<script src="<?= $data->base_url ?>assets/common/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript">
	$('#btnsubmit').click(function(){

		$('.items').remove();

		$.ajax({
  			type: "GET",
  			url: "store",
  			data: {
    			'item':$('#txtitem').val()
    		},
  			cache: false,
  			success: function(data){
     			$('#tablebody').html(data);
     			$('#modal-carts').modal('show');
  			}
		}); //end ajax

	});

	$('#tablebody').on('click','.coba',function(){
		// alert(this.id);
		var idku = this.id;
		$('.items').remove();
		$.ajax({
  			type: "GET",
  			url: "store",
  			data: {
    			'del':idku
    		},
  			cache: false,
  			success: function(data){
     			$('#tablebody').html(data);
  			}
		}); //end ajax
	});
</script>
</body>
</html>

<?php 
function Rows($data){
  if (count($data)=='0') {
    echo "<td colspan='7'>Tidak ada data dalam sistem kami</td>";
  }else {

    foreach ($data as $key) {
      echo '<tr>';  
      echo '<td>'.$key->kb_ket.'</td>';
      echo '<td>'.$key->jenis_barang.'</td>';
      echo '<td>'.$key->detail_jenis.'</td>';
      echo '<td>'.$key->brg_harga.'</td>';
      echo '<td>'.$key->brg_satuan.'</td>';
      echo '<td>'.$key->jumlah_barang.'</td>';
      echo '<td><a href="detail?itm='.$key->brg_id.'" id="'.$key->brg_id.'" class="coba">Order</a></td>';
      echo '</tr>';
    }
      
  }
}

?>
