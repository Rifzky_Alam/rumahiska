<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?= $data->title ?></title>
  <!-- <link rel="stylesheet" href="<?= $data->base_url ?>assets/common/bootstrap/css/bootstrap.min.css"> -->
  <script src="http://salvina.id/assets/homepage/js/jquery.min.js"></script>
  <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
  <link href="http://salvina.id/assets/homepage/css/style.css" rel="stylesheet" type="text/css" media="all" />
  <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
  <link href="http://salvina.id/assets/homepage/css/style4.css" rel="stylesheet" type="text/css" media="all" />
  <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
  
  <script src="http://salvina.id/assets/homepage/js/jstarbox.js"></script>
    <link rel="stylesheet" href="http://salvina.id/assets/homepage/css/jstarbox.css" type="text/css" media="screen" charset="utf-8" />
      <script type="text/javascript">
        jQuery(function() {
        jQuery('.starbox').each(function() {
          var starbox = jQuery(this);
            starbox.starbox({
            average: starbox.attr('data-start-value'),
            changeable: starbox.hasClass('unchangeable') ? false : starbox.hasClass('clickonce') ? 'once' : true,
            ghosting: starbox.hasClass('ghosting'),
            autoUpdateAverage: starbox.hasClass('autoupdate'),
            buttons: starbox.hasClass('smooth') ? false : starbox.attr('data-button-count') || 5,
            stars: starbox.attr('data-star-count') || 5
            }).bind('starbox-value-changed', function(event, value) {
            if(starbox.hasClass('random')) {
            var val = Math.random();
            starbox.next().text(' '+val);
            return val;
            }
          })
        });
      });
      </script>
  <style>
  body {
    font-family: 'Quicksand', sans-serif;
        /* background: #f3f3f3 !important; */
  }
  .content-top {
    padding-top: 20px;
  }
  .col-2 {
      padding-top: 1.5em;
  }


  </style>

<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '365006690755478');
  fbq('track', 'PageView');
  fbq('track', 'AddToCart');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=365006690755478&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

</head>
<body>

  <?php include_once $data->homedir.'view/homepage/header.php'; ?>
<div class="container" style="padding-bottom:40px;">

  <div class="row">
    <div class="col-md-12">
      <div class="page-header">
        <p style="margin-bottom:10px; display: inline-block; vertical-align: middle;font-size:24px" ><?= $data->subtitle ?></p>
    <!-- <a style="font-size: 13px;vertical-align: middle;float: right;" href="#">Login untuk proses yang lebih cepat</a> -->

      </div>
    </div>
  </div>

	<div class="row">

    <div id="hasil">

    </div>

		<div class="col-md-6">
      <h3><a href="<?= $data->base_url.'member/registrasi' ?>" title="">Daftar jadi member salvina.id</a></h3>
      <form action="" method="post">
      <div class="form-group">
        <label>Email</label>
        <input type="email" id="femail" name="in[email]" placeholder="Masukkan alamat email Anda:" class="form-control" required>
      </div>
      <div class="form-group">
        <label>Nama Lengkap</label>
        <input type="text" name="in[nama]" placeholder="Nama Lengkap" class="form-control" required>
      </div>
      <div class="form-group">
        <label>Telepon</label>
        <input type="text" name="in[telepon]" placeholder="Nomor Handphone" class="form-control" required>
      </div>
      <div class="form-group">
        <label>Alamat Rumah</label>
        <textarea name="in[alamat]" class="form-control" required placeholder="No Rumah, RT/RW, Perum/Desa, kodepos"></textarea>
      </div>
      <div class="form-group">
        <label>Provinsi</label>
        <select name="in[provinsi]" class="form-control" id="prv">

        </select>
      </div>

      <div class="form-group">
        <label>Kota</label>
        <select name="in[city]" class="form-control" id="cty">

        </select>
      </div>

      <div class="form-group">
        <label>Kecamatan</label>
        <select name="in[kec]" class="form-control" id="sbd">

        </select>
      </div>

      <div class="col-md-6">
        <div class="form-group">
          <label>Pilih Jasa Kurir</label>
          <select name="in[dv]" class="form-control" id="dv">

            <option value="jne" selected>JNE</option>
            <option value="sicepat">SiCepat</option>
            <option value="tiki">TIKI</option>
            <option value="pos">POS Indonesia</option>
            <option value="jnt">J&amp;T Express (J&amp;T)</option>
            <option value="pcp">Priority Cargo and Package (PCP)</option>
            <!-- <option value="esl">Eka Sari Lorena (ESL)</option> -->
            <!-- <option value="rpx">RPX Holding (RPX)</option> -->
            <option value="pandu">Pandu Logistics (PANDU)</option>
            <option value="wahana">Wahana Prestasi Logistik (WAHANA)</option>
            <!-- <option value="pahala">Pahala Kencana Express (PAHALA)</option> -->
            <option value="cahaya">Cahaya Logistik (CAHAYA)</option>
            <option value="sap">SAP Express (SAP)</option>
            <option value="jet">JET Express (JET)</option>
            <!-- <option value="indah">Indah Logistic (INDAH)</option> -->
            <!-- <option value="slis">Solusi Ekspres (SLIS)</option> -->
            <option value="dse">21 Express (DSE)</option>
            <option value="first">First Logistics (FIRST)</option>
            <!-- <option value="ncs">Nusantara Card Semesta (NCS)</option> -->
            <!-- <option value="star">Star Cargo (STAR)</option> -->
          </select>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label>Layanan:</label>
          <select name="in[ds]" class="form-control" id="ds">

          </select>
        </div>
      </div>
      <input type="hidden" id="ogkr" name="in[ongkir]">
      <button id="tombol" disabled class="btn btn-lg btn-primary">Konfirmasi</button>

		</div>



    <div class="col-md-6">
      <h3 style="margin-bottom:15px">Rincian Pesanan</h3>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Nama Item</th>
            <th>Berat</th>
            <th>Qty</th>
            <th>Harga</th>
            <th>Jumlah</th>
          </tr>
        </thead>
        <tbody>
          <?php Rows($data->listbarang) ?>
        </tbody>
      </table>
      <section>
        <h4>Pembayaran</h4>
        <p>Pembayaran dapat dilakukan secara transfer dan di kirim ke no rek yang kami kirimkan ke email anda, terimakasih.</p>
      </section>
    </div>
    </form>

	</div>
</div>

<script src="<?= $data->base_url ?>assets/common/bootstrap/js/jquery.js"></script>
<script src="<?= $data->base_url ?>assets/common/bootstrap/js/bootstrap.min.js"></script>
<script>
    $(document).ready(function(){
      LoadProv('9');
    });

    $('#prv').change(function() {
      LoadCity();
    });

    $('#cty').change(function() {
      LoadSubdistrict();
    });

    $('#sbd').change(function() {
      LoadService($('#dv').val());
    });

    $('#dv').change(function() {
      LoadService($(this).val());
    });

    $('#ds').change(function() {
      $.ajax({
            type: "GET",
            url: "<?= $data->base_url ?>library/rajaongkir/calculateprice",
            data: {
              'js':$('#dv').val(),
              'it':$('#sbd').val(),
              'bb':$('#sbtb').val()
            },
            cache: false,
            success: function(data){
              var myjson = JSON.parse(data);
              var result = myjson.rajaongkir.results[0].costs
              GetPayment(result);
            }
          }); //end ajax
    });

    function LoadProv(id){
      $('.optprv').remove();
      $.ajax({
            type: "GET",
            url: "<?= $data->base_url ?>library/rajaongkir/store",
            data: {
              'q':'province'
            },
            cache: false,
            success: function(data){
              var myjson = JSON.parse(data);
              var result = myjson.rajaongkir.results
              for (x in result) {
                if (result[x].province_id==id) {
                  document.getElementById("prv").innerHTML += "<option class='optprv' selected value='" + result[x].province_id + "'>" + result[x].province + '</option>';
                } else {
                  document.getElementById("prv").innerHTML += "<option class='optprv' value='" + result[x].province_id + "'>" + result[x].province + '</option>';
                }
              }
              LoadCity();
            }
          }); //end ajax
    }

    function LoadCity() {
      $('.optcty').remove();
      $.ajax({
            type: "GET",
            url: "<?= $data->base_url ?>library/rajaongkir/store",
            data: {
              'kota':$('#prv').val()
            },
            cache: false,
            success: function(data){
              var myjson = JSON.parse(data);
              var result = myjson.rajaongkir.results
              for (x in result) {
                if (result[x].city_id=='79') {
                  document.getElementById("cty").innerHTML += "<option class='optcty' selected value='" + result[x].city_id + "'>" + result[x].type + " "+ result[x].city_name + '</option>';
                } else {
                  document.getElementById("cty").innerHTML += "<option class='optcty' value='" + result[x].city_id + "'>" + result[x].type + " "+ result[x].city_name + '</option>';
                }
              }
              LoadSubdistrict();
            }
          }); //end ajax
    }

    function LoadSubdistrict(){
      $('.optsbd').remove();
      $.ajax({
            type: "GET",
            url: "<?= $data->base_url ?>library/rajaongkir/store",
            data: {
              'sbd':$('#cty').val()
            },
            cache: false,
            success: function(data){
              var myjson = JSON.parse(data);
              var result = myjson.rajaongkir.results
              for (x in result) {
                if (result[x].subdistrict_id=='1066') {
                  document.getElementById("sbd").innerHTML += "<option class='optsbd' selected value='" + result[x].subdistrict_id + "'>" + result[x].subdistrict_name + '</option>';
                } else {
                  document.getElementById("sbd").innerHTML += "<option class='optsbd' value='" + result[x].subdistrict_id + "'>" + result[x].subdistrict_name + '</option>';
                }
              }
            LoadService($('#dv').val());
            }
          }); //end ajax
    }

    function LoadService(kurir) {
      $('.optsvc').remove();
      $.ajax({
            type: "GET",
            url: "<?= $data->base_url ?>library/rajaongkir/calculateprice",
            data: {
              'js':kurir,
              'it':$('#sbd').val(),
              'bb':$('#sbtb').val()
            },
            cache: false,
            success: function(data){
              var myjson = JSON.parse(data);
              var result = myjson.rajaongkir.results[0].costs
              for (x in result) {
                if (result[x].service=='REG') {
                  document.getElementById("ds").innerHTML += "<option class='optsvc' selected value='" + result[x].service + "'>" + result[x].description + '</option>';
                } else {
                  document.getElementById("ds").innerHTML += "<option class='optsvc' value='" + result[x].service + "'>" + result[x].description + '</option>';
                }
              }
              GetPayment(result);
            }
          }); //end ajax
    }

    function GetPayment(dataset){
      // $('#tombol').prop('disabled', true);
      Number.prototype.format = function(n, x) {
          var re = '(\\d)(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
          return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$1,');
      };
        document.getElementById("tombol").disabled = true;
        for (z in dataset) {
          if ($('#ds').val()==dataset[z].service) {
            document.getElementById("ogkr").value = dataset[z].cost[0].value;
            // GetNumber(dataset[z].cost[0].value,'bo','IDR');
            $('#bo').html(dataset[z].cost[0].value.format() + " IDR");
            var hah = parseInt($('#sbt').val());
            var tes = hah + parseInt(dataset[z].cost[0].value);
            $('#tb').html(tes.format() + " IDR");
            // GetNumber(tes,'tb','IDR');
          }
        }
        document.getElementById("tombol").disabled = false;
      }

  </script>
  	<?php include_once $data->homedir.'view/homepage/footer.php'; ?>
</body>
</html>

<?php
function Rows($data){
  if (count($data)=='0') {
    echo "<td colspan='3'>Tidak ada data barang yang anda pesan di sistem kami.</td>";
  }else {
    $total = 0;
    $totalberat = 0;
    foreach ($data as $key => $value) {
      echo '<tr>';
      echo '<td>'.$value['kat']. ' : '.$value['jns'].' - '. $value['nb'].'<a href="removeitem?itm='.$value['ib'].'" title="Hapus item"><span class="glyphicon glyphicon-remove"></span></a></td>';
      $subBerat = $value['bb'] * $value['qty'];
      $totalberat += $subBerat;
      echo '<td id="wgh">'.$value['bb'].' gram</td>';
      echo '<td>'.$value['qty'].'</td>';
      $subtotal = $value['price']*$value['qty'];
      echo '<td>'.number_format($value['price']).' IDR</td>';
      echo '<td>'.number_format($subtotal).' IDR</td>';
      $total += $subtotal;
    }
      echo '<tr>';
      echo '<td colspan="4" id="st">Sub Total:</td><td>'.number_format($total).' IDR</td>';
      echo '<input type="hidden" id="sbt" value="'.$total.'">';
      echo '<input type="hidden" id="sbtb" value="'.$totalberat.'">';
      echo '</tr>';

      $ongkir = 0;
      echo '<tr>';
      echo '<td colspan="4">Ongkos Kirim:</td><td id="bo">Menghitung ...</td>';
      echo '</tr>';

      echo '<tr>';
      echo '<td colspan="4">Total Biaya:</td><td><b id="tb">Menghitung ...</b></td>';
      echo '</tr>';

  }
}

?>
