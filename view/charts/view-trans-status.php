<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?= $data->title ?></title>
  <link rel="stylesheet" href="<?= $data->base_url ?>assets/common/bootstrap/css/bootstrap.min.css">
  </head>
<body>
	<div class="container">
		<div class="row">
			<div class="page-header">
				<h2>Status Transaksi</h2>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<table class="table table-bordered">
					<thead>
						
					</thead>
					<tbody>
						<tr>
							<td>Nama Pelanggan</td>
							<td>Rifzky Alam</td>
						</tr>
						<tr>
							<td>Alamat Pengiriman</td>
							<td>Rifzky Alam</td>
						</tr>
						<tr>
							<td>Barang</td>
							<td>
								<ul>
									<li>Jilbab Rasya Navy</li>
									<li>Jilbab Shafa Maroon</li>
								</ul>
							</td>
						</tr>
						<tr>
							<td>Status Transaksi</td>
							<td>Menunggu Konfirmasi Admin</td>
						</tr>
						<tr>
							<td>Jasa Pengiriman</td>
							<td>TIKI - DELIVERY ON TIME</td>
						</tr>
						<tr>
							<td>No Resi</td>
							<td>N9323423923K31</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>

</html>