<!DOCTYPE html>
<html>
<head>
    <title>Rumah Iska -- <?php echo $data->judul ?></title>
    <?php include_once $data->homedir.'/view/administrasi/elements/header.php'; ?>
    <?php MetaTag() ?>
    <?php Links($data->base_url) ?>
    <?php Upload($data->base_url) ?>
    <?php Scripts($data->base_url) ?>
    <?php Styles() ?>

</head>
<body>
<?php include_once $data->homedir.'view/order/vheader.order.php'; ?>
<div class="container" style="padding-top:50px;padding-bottom:50px;">
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div class="row">
                <div class="jumbotron">
                    <h3>Halo, <?= $data->nama_cust ?></h3>
                    <p>Terimakasih telah memesan barang di <?= $data->company ?>,<br>Silahkan untuk mengunggah/upload bukti transfer anda disini. Kami akan konfirmasi pembayaran anda dalam 1x24jam.<br><br>Salam,<br><br><b><u>Admin <?= $data->company ?></u></b></p>
                </div>
            </div>

            <div class="row">
                <form action="" method="post" enctype="multipart/form-data">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Bukti Transaksi</label>
                        <input id="file-3" type="file" name="bukti" class="file" />
                    </div>

                    <button class="btn btn-lg btn-primary" style="width:100%">Submit</button>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src='<?php echo $data->base_url ?>assets/upload/js/fileinput.js' type="text/javascript"></script>
<script src='<?php echo $data->base_url ?>assets/upload/js/fileinput_locale_fr.js' type="text/javascript"></script>
</body>
</html>
