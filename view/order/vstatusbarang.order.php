<!DOCTYPE html>
<html>
<head>
    <title>Rumah Iska -- <?php echo $data->judul ?></title>
    <?php include_once $data->homedir.'/view/administrasi/elements/header.php'; ?>
    <?php MetaTag() ?>
    <?php Links($data->base_url) ?>
    <?php Upload($data->base_url) ?>
    <?php Scripts($data->base_url) ?>
    <?php Styles() ?>

</head>
<body>
<?php include_once $data->homedir.'view/order/vheader.order.php'; ?>
<div class="container" style="padding-top:50px;padding-bottom:50px;">
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div class="row">
                <div class="jumbotron">
                    <h3>Halo, <?= $data->namacust ?></h3>
                    <p>Terimakasih telah memesan barang di <?= $data->company ?>,<br>Berikut kami infokan status transaksi anda di bawah ini :).<br><br>Salam,<br><br><b><u>Admin <?= $data->company ?></u></b></p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-6 col-lg-6">
            <section>
                <h3>Konfirmasi</h3>
                <ul>
                    <?php if ($data->status==1||$data->status==0): ?>
                        <li>Tanda bukti transaksi <i class="glyphicon glyphicon-remove"></i></li>
                        <li>Proses paket barang <i class="glyphicon glyphicon-remove"></i></li>
                    <?php elseif($data->status==2): ?>
                        <li>Tanda bukti transaksi <i class="glyphicon glyphicon-ok"></i></li>
                        <li>Proses paket barang <i class="glyphicon glyphicon-remove"></i></li>
                    <?php elseif($data->status==3): ?> 
                        <li>Tanda bukti transaksi <i class="glyphicon glyphicon-ok"></i></li>
                        <li>Proses paket barang <i class="glyphicon glyphicon-ok"></i></li>   
                    <?php endif ?>
                        
                </ul>
            </section>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-6">
            <section>
                <h3>Pengiriman (Delivery)</h3>
                <ul>
                    <?php if ($data->status<3): ?>
                        <li>Penitipan paket ke jasa ongkir <i class="glyphicon glyphicon-remove"></i></li>
                    <?php elseif($data->status<2): ?>
                        <li>Penitipan paket ke jasa ongkir <i class="glyphicon glyphicon-remove"></i></li>
                    <?php elseif($data->status==3): ?>
                        <li>Penitipan paket ke jasa ongkir <i class="glyphicon glyphicon-ok"></i></li>
                    <?php endif ?>
                    
                </ul>
                <br>
                <p><b>Jasa Ongkir: </b><?= $data->ongkir ?></p>
                <p><b>Resi Pengiriman: </b><?= $data->resi ?></p>
            </section>
        </div>
    </div>
</div>
<script src='<?php echo $data->base_url ?>assets/upload/js/fileinput.js' type="text/javascript"></script>
<script src='<?php echo $data->base_url ?>assets/upload/js/fileinput_locale_fr.js' type="text/javascript"></script>
</body>
</html>
