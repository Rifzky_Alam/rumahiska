<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="<?= $data->base_url ?>assets/common/bootstrap/css/bootstrap.min.css">
	<title>Invoice Transaksi - salvina.id</title>
<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '365006690755478');
  fbq('track', 'PageView');
  fbq('track', 'Purchase');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=365006690755478&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
</head>
<body>

	<div class="container" style="padding-bottom:40px;">
		<div class="row">
			<div class="page-header">
				<h2>Invoice Transaksi</h2>
			</div>
		</div>

		<!-- <div class="row"> -->
			<!-- <div class="col-md-12"> -->
				
			<!-- </div> -->
		<!-- </div> -->
	
		<div class="row">
			<div class="col-md-12">
				<form action="/order/invoice/template" method="post" accept-charset="utf-8">
					<a href="<?= $data->base_url.'order/invoice/pdf?tr='.$data->id_transaksi ?>" class="btn btn-lg btn-warning" title="Cetak dengan ukuran kertas A5" target="_blank">Cetak Invoice</a>	
					<input type="hidden" name="token" value="<?= $data->securitycode($data->id_transaksi,'e') ?>">
					<button class="btn btn-lg btn-info" title="Klik bila anda tidak mendapatkan pesan apapun di email anda.">Kirim Ulang Invoice ke Email Saya</button>
					<a href="#" id="whatsappz" target="_blank" class="btn btn-lg btn-success">Konfirmasi Whatsapp</a>
					<?php if(isset($_SESSION['ri_agen'])): ?>
					<a href="<?= $data->base_url.'distributor/dasbor' ?>" id="whatsappz" target="_blank" class="btn btn-lg btn-danger">Kembali Ke Dasbor</a>
					<?php endif; ?>
					<?php if(isset($_SESSION['smember'])): ?>
					<a href="<?= $data->base_url.'member/dasbor' ?>" id="whatsappz" target="_blank" class="btn btn-lg btn-danger">Kembali ke dasbor</a>
					<?php endif; ?>
				</form>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<section>
					<h3>SALVINA.ID</h3>
					<p>
						<?= $data->addrcompany ?> <br>
						<?= $data->companyemail ?> <br>
						<?= $data->companyphone ?>
					</p>
				</section>
				
			</div>
			<div class="col-md-6">
				<br><br>
				<div class="row">
					<div class="col-md-6">
						No Invoice
					</div>
					<div class="col-md-6">
						#<?= $data->labeltrans ?>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						Tanggal
					</div>
					<div class="col-md-6">
						<?= date('d-m-Y H:m:s', $data->transtanggaltimestamp); ?>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						Expired
					</div>
					<div class="col-md-6">
						<?= $data->tanggalexpired ?>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<section>
					<h3>Kepada</h3>
					<p>
						<?= $data->namacust ?> <br>
						<?= $data->alamat ?> <br>
						<span id="kecamatan"></span> <br>
						<span id="kota"></span> <span id="provinsi"></span> <br>
						<?php if($data->notealamat=='-'): ?>
						
						<?php else: ?>
						<span><?= $data->notealamat ?></span>
						<?php endif ?>
						<span></span>
						<?= $data->emailcust ?> <br>
						<a href="tel://<?= $data->telepon ?>" title="Nomor Kontak"><?= $data->telepon ?></a>
					</p>
				</section>
				<br>
				<table class="table table-bordered">
					<thead>
						<tr>
							<th style="text-align:center;">Nama Produk</th>
							<th style="text-align:center;">Harga</th>
							<th style="text-align:center;">Qty</th>
							<th style="text-align:center;">Jumlah</th>
						</tr>
					</thead>
					<tbody>

						<?php if (count($data->listbarang)=='0'): ?>
							<tr>
								<td style="text-align:center;">Tidak ada data barang dalam transaksi ini.</td>
							</tr>
						<?php else: ?>
							<?php $total = 0; ?>
							<?php foreach ($data->listbarang as $key): ?>
								<tr>
									<td><?= $key->jenis_barang.' - '. $key->detail_jenis ?></td>
									<td style="text-align: right;"><?= number_format($key->th_harga) ?> IDR</td>
									<td><?= $key->qty ?></td>
									<td style="text-align: right;"><?= number_format($key->th_harga * $key->qty) ?> IDR</td>
									<?php $total += $key->th_harga * $key->qty ?>
								</tr>
							<?php endforeach ?>
						<?php endif ?>
						<tr>
							<td colspan="3"><b>Total</b></td>
							<td style="text-align:right;"><b><?= number_format($total).' IDR' ?></b></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<h4>Pembayaran</h4>
				<ul>
					<?php foreach ($data->rekening as $key): ?>
						<li><?= $key ?></li>
					<?php endforeach ?>
				</ul>
			</div>
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-6">
						<b>Subtotal</b>
					</div>
					<div class="col-md-6" style="text-align:right;">
						<?= number_format($total).' IDR' ?>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-6">
						<b>Ongkir</b>
					</div>
					<div class="col-md-6" style="text-align:right;">
						<?= number_format($data->ongkir).' IDR' ?>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-6">
						<b>Potongan</b>
					</div>
					<div class="col-md-6" style="text-align:right;">
						<?= '- '.number_format($data->potongan).' IDR' ?>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-6">
						<b>Total</b>
					</div>
					<div class="col-md-6" style="text-align:right;">
						<b><?= number_format($total + $data->ongkir - $data->potongan).' IDR' ?></b>
					</div>
				</div>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-6">
				<a href="<?= $data->base_url ?>order/konfirmasi/?tr=<?= $data->securitycode($data->id_transaksi,'e') ?>" title="Upload Bukti Pembayaran" class="btn btn-lg btn-primary" target="_blank">Upload Bukti Pembayaran</a>
			</div>
		</div>

	</div>

<script src="<?= $data->base_url ?>assets/common/bootstrap/js/jquery.js"></script>
<script src="<?= $data->base_url ?>assets/common/bootstrap/js/bootstrap.min.js"></script>
<script>
  $(document).ready(function(){
  	LinkWhatsapp(<?= $data->companyphone ?>);
    LoadProv(<?= $data->prov ?>);
    LoadCity(<?= $data->kota ?>);
    LoadKecamatan(<?= $data->kecamatan ?>);
  });

  function LinkWhatsapp(nomornya){
  	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
 		// alert('mobile detected!');
 		<?php $phonee = str_replace('+', '', $data->companyphone) ?>
 		$('#whatsappz').prop('href', "https://wa.me/<?= $phonee ?>?text=Assalamualaikum");
	}else{
		$('#whatsappz').prop('href', "https://web.whatsapp.com/send?phone=<?= $data->companyphone ?>&text=Assalamualaikum, saya <?= $data->namacust ?> ingin konfirmasi transaksi saya di website salvina.id (<?= $data->id_transaksi ?>)");
		// alert('Web detected!');
	}
  }

  function LoadProv(idprov) {
    $.ajax({
      type: "GET",
        url: "<?= $data->base_url ?>library/rajaongkir/pcheck",
        data: {
          'id':idprov
        },
        cache: false,
        success: function(data){
          var myjson = JSON.parse(data);
          var result = myjson.rajaongkir.results
          document.getElementById("provinsi").innerHTML = result.province
          
        }
      }); //end ajax
  }

  function LoadCity(city_id) {
    $.ajax({
      type: "GET",
        url: "<?= $data->base_url ?>library/rajaongkir/ctcheck",
        data: {
          'id':city_id
        },
        cache: false,
        success: function(data){
          var myjson = JSON.parse(data);
          var result = myjson.rajaongkir.results
          document.getElementById("kota").innerHTML = result.type + " " + result.city_name
          
        }
      }); //end ajax
  }

  function LoadKecamatan(kec_id) {
    $.ajax({
      type: "GET",
        url: "<?= $data->base_url ?>library/rajaongkir/sdcheck",
        data: {
          'id':kec_id
        },
        cache: false,
        success: function(data){
          var myjson = JSON.parse(data);
          var result = myjson.rajaongkir.results
          document.getElementById("kecamatan").innerHTML = result.subdistrict_name
          
        }
      }); //end ajax
  }

</script>
</body>
</html>