<!DOCTYPE HTML>

<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<meta charset="utf-8">

<!-- Description, Keywords and Author -->
<meta name="keywords" content="Jilbab,Hijab,Muslimah,Kerudung">
    <meta name="author" content="Rifzky Alam">
<meta name="description" content="Rumah Iska akan hadir kembali dengan sistem yang baru.">
<meta name="author" content="">
<title><?php echo $data->judul ?></title>
<!--<link rel="shortcut icon" href="<?php echo $data->baseurl.'view/home/' ?>images/favicon.ico" type="image/x-icon">-->

<!-- style -->

<link href="<?php echo $data->baseurl.'view/home/' ?>css/style.css" rel="stylesheet">

<!-- style -->

<!-- bootstrap -->

<link href="<?php echo $data->baseurl.'view/home/' ?>css/bootstrap.min.css" rel="stylesheet">

<!-- responsive -->

<link href="<?php echo $data->baseurl.'view/home/' ?>css/responsive.css" rel="stylesheet">

<!-- font-awesome -->

<link href="<?php echo $data->baseurl.'view/home/' ?>css/font-awesome.min.css" rel="stylesheet">

<!-- font-awesome -->

</head>

<body>

<!-- main-wrapper-iamge -->

<main id="main" role="main-wrapper-iamge">
  <div class="over-bg-color"> 
    
    <!-- container -->
    
    <div class="container"> 
      
      <!-- tab-content -->
      
      <div class="tab-content text-center"> 
        
        <!-- Countdown -->
        
        <section id="home" class="tab-pane fade in active">
          <article role="countdown" class="countdown-pan">
            <div id="countdown" class="text-center"></div>
            <p>Kami Sedang Memperbaharui Sistem Kami</p>
          </article>
        </section>
        
        <!-- Countdown --> 
        
        <!-- introduction 
        
        <section id="menu1" class="tab-pane fade">
          <article role="introduction" class="introduction-pan">
            <header class="page-title">
              <h2>Tentang Kami</h2>
            </header>
            <p>Kami adalah produsen dari hijab muslimah salvina dan </p>
            
            services 
            
            <ul role="services">
              <li> <i class="fa fa-diamond" aria-hidden="true"></i>
                <h6>Branding Consuting</h6>
                <p>We are a team of talented people<br/>
                  with big ideas and creative.</p>
              </li>
              <li> <i class="fa fa-camera-retro" aria-hidden="true"></i>
                <h6>Fashion Photography</h6>
                <p>We are a team of talented people<br/>
                  with big ideas and creative.</p>
              </li>
              <li> <i class="fa fa-bullhorn" aria-hidden="true"></i>
                <h6>Digital Marketing</h6>
                <p>We are a team of talented people<br/>
                  with big ideas and creative.</p>
              </li>
            </ul>
            
             
            
          </article>
        </section>
        
         --> 
        
        <!-- Subscribe -->
        
        <section id="menu2" class="tab-pane fade">
          <article role="subscribe" class="subscribe-pan">
            <header class="page-title">
              <h2>Subscribe to Us</h2>
            </header>
            <div class="ntify_form">
              <form method="post" action="" name="subscribeform" id="subscribeform">
                <input name="email" type="email" id="subemail" placeholder="Enter Your Email...">
                <label>
                  <input name="" type="submit" class="button-icon">
                  <i class="fa fa-paper-plane" aria-hidden="true"></i> </label>
              </form>
              
              <!-- subscribe message -->
              
              <div id="mesaj"></div>
              
              <!-- subscribe message --> 
              
            </div>
            <p>Please enter your email below and we'll let you know once<br/>
              we're up and running.</p>
          </article>
        </section>
        
        <!-- Subscribe --> 
        
        <!-- Contact -->
        
        <section id="menu3" class="tab-pane fade">
          <article role="contact" class="contact-pan">
            <header class="page-title">
              <h2>Stay in touch with us</h2>
            </header>
            <h3><a href="mailto:rikzadaftaronline@gmail.com">Contact @ rumahiska.com </a></h3>
            <ul>
              <li><i class="fa fa-map-marker" aria-hidden="true"></i> Rikza Adhia.</li>
              <li><i class="fa fa-phone" aria-hidden="true"></i> <a href="tel://08121867072">(0812-1867-072)</a></li>
            </ul>
          </article>
        </section>
      </div>
      
      <!-- tab-content --> 
      
    </div>
    
    <!-- container --> 
    
    <!-- header -->
    
    <header role="header">
      <hgroup> 
        
        <!-- logog -->
        
        <h1> <a href="#" title="Pixicon">salvina</a> </h1>
        
        <!-- logog --> 
        
        <!-- nav -->
        
        <nav role="nav" id="header-nav" class="nav navy">
          <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#home" title="Countdown">Countdown</a></li>
            <!--<li><a data-toggle="tab" href="#menu1" title="Introduction">introduction</a></li>-->
            <!--<li><a data-toggle="tab" href="#menu2" title="Subscribe">Subscribe</a></li>-->
            <li><a data-toggle="tab" href="#menu3" title="Contact">Contact</a></li>
          </ul>
          <div role="socil-icons" class="mobile-social">
            <!--<li><a href="#" target="_blank" title="twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>-->
            <li><a href="https://www.facebook.com/profile.php?id=100009594645423" target="_blank" title="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
          </div>
        </nav>
        
        <!-- nav --> 
        
        <!-- Socil Icon -->
        
        <ul role="socil-icons" class="desk-social">
          <!--<li><a href="#" target="_blank" title="twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>-->
          <li><a href="https://www.facebook.com/profile.php?id=100009594645423" target="_blank" title="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
          <!--<li><a href="#" target="_blank" title="google-plus"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>-->
          <!--<li><a href="#" target="_blank" title="pinterest"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>-->
        </ul>
        
        <!-- Socil Icon --> 
        
      </hgroup>
      <footer class="desk">
        <p>Designed and Created by: <i class="fa fa-heart" aria-hidden="true"></i> Rifzky Alam</p>
      </footer>
    </header>
    
    <!-- header -->
    
    <footer class="mobile">
      <p>Designed By <i class="fa fa-heart" aria-hidden="true"></i> Rifzky Alam</p>
    </footer>
  </div>
</main>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 

<script src="<?php echo $data->baseurl.'view/home/' ?>js/jquery.min.js" type="text/javascript"></script> 

<!-- custom --> 

<script src="<?php echo $data->baseurl.'view/home/' ?>js/custom.js" type="text/javascript"></script> 
<script src="<?php echo $data->baseurl.'view/home/' ?>js/nav-custom.js" type="text/javascript"></script> 

<!-- Include all compiled plugins (below), or include individual files as needed --> 

<script src="<?php echo $data->baseurl.'view/home/' ?>js/bootstrap.min.js" type="text/javascript"></script> 

<!-- jquery.countdown --> 

<script src="<?php echo $data->baseurl.'view/home/' ?>js/countdown-js.js" type="text/javascript"></script> 
<script src="<?php echo $data->baseurl.'view/home/' ?>js/html5shiv.js" type="text/javascript"></script>
</body>
</html>