<!DOCTYPE html>
<html>
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="title" content=' - RumahIska' />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta property="og:url" content="" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content=""/> 
    <meta property="og:image" content="" />
    <meta property="og:site_name" content="Rumah Iska" />
    <meta property="og:description" content="" />
    <meta property="og:image:type" content="image/jpeg" />
    <meta property="og:image:width" content="610" />
    <meta property="og:image:height" content="409" />

    <title><?php echo $data->title ?></title>
    <link rel="stylesheet" type="text/css" href="<?php echo $data->base_url ?>assets/common/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $data->base_url ?>assets/blogs/singlepostnav.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $data->base_url ?>assets/blogs/singlepost.css">
</head>
<body>
<nav class="navbar navbar-inverse sidebar" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">RumahIska</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="#">Home<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-home"></span>
                    </a>
                </li>
                <li >
                    <a href="#">Kontak<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-user"></span>
                    </a>
                </li>
                <li >
                    <a href="#">Tentang Kami<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-envelope"></span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="main">
    <div class="container">
        <div class="row">
                 <div class="col-sm-2 paddingTop20">
                <nav class="nav-sidebar">
                    <ul class="nav">
                        <li class="active"><a href="javascript:;"><span class="glyphicon glyphicon-star"></span> Artikel</a></li>
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#new" class="collapsed">Artikel Terbaru</a>
                            <ul id="new" class="collapse" style="list-style-type:none;">
                                <li><a href="#">Coba</a></li>
                                <li><a href="#">Apalah</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:;"  data-toggle="collapse" data-target="#popular" class="collapsed">Populer</a>
                            <ul id="popular" class="collapse" style="list-style-type:none;">
                                <li><a href="#">Jajal</a></li>
                                <li><a href="#">okelah</a></li>
                            </ul>
                        </li>
                        
                    </ul>
                </nav>
            </div>  
        
         <div class="col-md-10 blogShort">
                        <center><h1><?php echo $data->title ?></h1></center>
                         
                         <img src="gambar?v=<?php echo $data->gambar ?>" alt="post img" class="img-responsive postImg img-thumbnail margin10" style="width:945px;height:400px;">
                         <article>
                            <?php echo $data->content ?>
                         </article>
                    
                     </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo $data->base_url ?>assets/common/bootstrap/js/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="<?php echo $data->base_url ?>assets/common/bootstrap/js/bootstrap.js"></script>
<script type="text/javascript" src="<?php echo $data->base_url ?>assets/blogs/singlepostnav.js"></script>
</body>
</html>