<!DOCTYPE html>
<html>
<head>
    <title>Administrasi -- <?php echo $data->judul ?></title>
    <?php include_once HomeDir().'/view/administrasi/elements/header.php'; ?>
    <?php MetaTag() ?>
    <?php Links($data->base_url) ?>
    <?php Scripts($data->base_url) ?>
    <?php Styles() ?>
    
</head>
<body>
<?php 

?>

<?php include_once HomeDir().'/view/administrasi/elements/top-nav.php'; ?>
<div class="container-fluid" style="padding-top:50px">
    <div class="row">
        <!--sidebar is here !!!!-->
        <?php include_once HomeDir().'/view/administrasi/elements/sidebar.php'; ?>
        <!--sidebar ends here !!!!-->
        <div class="col-sm-9 col-md-9">
            <div class="well">
                <h1>Rumah Iska ~ Administrasi-Home</h1>
            </div>
            <div class="row">
            	<div class="col-md-6">
            		<div class="panel panel-warning">
      					<div class="panel-heading">Artikel</div>
      					<div class="panel-body"><?= $data->jumlahartikel ?></div>
    				</div>            	
    			</div>
            	<div class="col-md-6">
            		<div class="panel panel-default">
                        <div class="panel-heading">Jumlah Transaksi</div>
                        <div class="panel-body"><?= $data->jumlahtransaksi ?></div>
                    </div>
            	</div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header">
                        <h3>Feedbacks.</h3>    
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Subjek</th>
                                <th>Pesan</th>
                                <th>Waktu Submit</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (count($data->feedbacks)=='0'): ?>
                                <tr>
                                    <td colspan="5" style="text-align:center;">Tidak ada data dalam database kami</td>
                                </tr>
                                <?php else: ?>
                                    <?php foreach ($data->feedbacks as $key): ?>
                                        <tr>
                                            <td><?= $key['fc_nama'] ?></td>
                                            <td><?= $key['fc_email'] ?></td>
                                            <td><?= $key['fc_subjek'] ?></td>
                                            <td><?= $key['fc_pesan'] ?></td>
                                            <td><?= $key['fc_timestamp'] ?></td>
                                        </tr>
                                    <?php endforeach ?>
                            <?php endif ?>
                        </tbody>
                    </table>
                </div>    
            </div>


        </div>
    </div>
</div>

</body>
</html>