<?php function MetaTag(){ ?>
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php } ?>

<?php function Links($value){ ?>
	<link rel="stylesheet" type="text/css" href='<?php echo $value ?>assets/common/bootstrap/css/bootstrap.css'>
<?php } ?>

<?php function Login($value){ ?>
	<link rel="stylesheet" type="text/css" href="<?php echo $value.'assets/administrasi/login.css' ?>">
<?php } ?>

<?php function Upload($value){ ?>
<link href=<?php echo "'".$value."assets/upload/css/fileinput.css'"; ?> media="all" rel="stylesheet" type="text/css" />
<?php } ?>

<?php function DatePicker($value){ ?>
	<link rel="stylesheet" type="text/css" href=<?php echo "'".$value."js/jquery-datepicker/jquery-ui.min.css'"; ?>>	
<?php } ?>

<?php function TimePicker(){ ?>
	<link rel="stylesheet" type="text/css" href='<?php echo $value ?>assets/timepicker/css/timepicki.css'>
<?php } ?>

<?php function Scripts($value){ ?>
	<script type="text/javascript" src='<?php echo $value ?>assets/common/bootstrap/js/jquery.js'></script>
	<script type="text/javascript" src='<?php echo $value ?>assets/common/bootstrap/js/bootstrap.min.js'></script>		
<?php } ?>

<?php function MapScript($value){ ?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCUn6Q6sg0JVJBlgiz4tL2kF1zmVoC0ScE&libraries=places&sensor=true"></script>
<?php } ?>


<?php function Styles(){ ?>
	<style type="text/css">
	.glyphicon { margin-right:10px; }
	.panel-bodyz { padding:0px; }
	.panel-bodyz table tr td { padding-left: 15px }
	.panel-bodyz .table {margin-bottom: 0px; }
	</style>
<?php } ?>