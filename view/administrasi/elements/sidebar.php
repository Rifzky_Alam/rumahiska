
<div class="col-sm-3 col-md-3">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="glyphicon glyphicon-folder-close">
                            </span>Produk</a>
                        </h4>
                    </div>

                        <div id="collapseOne" class="panel-collapse collapse in">    
                    
                        <div class="panel-bodyz">
                            <table class="table">
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-dashboard"></span><a href="<?php echo $data->base_url.'administrasi/' ?>">Dashboard</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-compressed"></span><a href="<?php echo $data->base_url.'administrasi/produk/kategori-barang' ?>">Kategori</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-calendar text-success"></span><a href="<?php echo $data->base_url.'administrasi/produk/jenis-barang' ?>">Model/Jenis Barang</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-barcode text-success"></span><a href="<?php echo $data->base_url.'administrasi/produk/barang' ?>">Barang</a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"><span class="glyphicon glyphicon-user">
                            </span><?php echo $data->nama_admin ?></a>
                        </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse">
                        <div class="panel-bodyz">
                            <table class="table">
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-edit text-primary"></span><a href="#">Ubah Password</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-log-out"></span><a href="<?php echo $data->base_url.'administrasi/logout' ?>">Log-Out</a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"><span class="glyphicon glyphicon-shopping-cart">
                            </span>Transaksi</a>
                        </h4>
                    </div>

                    <div id="collapseFour" class="panel-collapse collapse">
                        <div class="panel-bodyz">
                            <table class="table">
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-th-list"></span>
                                        <a href="<?php echo $data->base_url.'administrasi/transaksi/' ?>">Data Transaksi</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-thumbs-up"></span>
                                        <a href="<?php echo $data->base_url.'administrasi/transaksi/customer' ?>">Customer</a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive"><span class="glyphicon glyphicon-pencil">
                            </span>Artikel</a>
                        </h4>
                    </div>

                    <div id="collapseFive" class="panel-collapse collapse">
                        <div class="panel-bodyz">
                            <table class="table">
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-th-list"></span>
                                        <a href="<?php echo $data->base_url.'administrasi/artikel/' ?>">Data Artikel</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-plus"></span>
                                        <a href="<?php echo $data->base_url.'administrasi/artikel/input-artikel' ?>">Input Artikel</a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix"><span class="glyphicon glyphicon-globe">
                            </span>Agen</a>
                        </h4>
                    </div>

                    <div id="collapseSix" class="panel-collapse collapse">
                        <div class="panel-bodyz">
                            <table class="table">
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-th-list"></span>
                                        <a href="<?php echo $data->base_url.'administrasi/agen/list' ?>">Data Agen</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-envelope"></span>
                                        <a href="<?php echo $data->base_url.'administrasi/agen/notif' ?>">Notifikasi</a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseSeven"><span class="glyphicon glyphicon-globe">
                            </span>Homepage</a>
                        </h4>
                    </div>

                    <div id="collapseSeven" class="panel-collapse collapse">
                        <div class="panel-bodyz">
                            <table class="table">
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-th-list"></span>
                                        <a href="<?php echo $data->base_url.'administrasi/homepage/banner' ?>">Banner</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-th-list"></span>
                                        <a href="<?php echo $data->base_url.'administrasi/agen/peluang' ?>">Laman Peluang Agen</a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>