<!DOCTYPE html>
<html>
<head>
    <title>Rumah Iska -- <?php echo $data->judul ?></title>
    <?php include_once $data->homedir.'/view/administrasi/elements/header.php'; ?>
    <?php MetaTag() ?>
    <?php Links($data->base_url) ?>
    <?php Scripts($data->base_url) ?>
    <?php Styles() ?>

</head>
<body>
    

<?php include_once $data->homedir.'/view/administrasi/elements/top-nav.php'; ?>
<div class="container-fluid" style="padding-top:50px;padding-bottom:50px;">
    <div class="row">
        <!--sidebar is here !!!!-->
        <?php include_once $data->homedir.'/view/administrasi/elements/sidebar.php'; ?>
        <!--sidebar ends here !!!!-->
        <div class="col-sm-9 col-md-9">
            <div class="well">
                <h1>Rumah Iska ~ Administrasi-<?php echo $data->subtitle ?></h1>
            </div>

            <div class="row">
                <form action="" method="post" enctype="multipart/form-data">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Kode Akses Register Agen</label>
                        <input type="text" minlength="12" name="in[kodeakses]" class="form-control" placeholder="Input kode kurang dari 12 karakter, hindari spesial karakter" maxlength="12" required>
                    </div>

                    <button class="btn btn-lg btn-primary" style="width:100%">Submit</button>
                </form>
                </div>
            </div>
            
        </div>
    </div>
</div>
</body>
</html>
