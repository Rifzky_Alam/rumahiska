<!DOCTYPE html>
<html>
<head>
    <title>Rumah Iska -- <?php echo $data->judul ?></title>
    <?php include_once $data->homedir.'/view/administrasi/elements/header.php'; ?>
    <?php MetaTag() ?>
    <?php Links($data->base_url) ?>
    <?php Scripts($data->base_url) ?>
    <?php Styles() ?>

</head>
<body>
    

<?php include_once $data->homedir.'/view/administrasi/elements/top-nav.php'; ?>
<div class="container-fluid" style="padding-top:50px;padding-bottom:50px;">
    <div class="row">
        <!--sidebar is here !!!!-->
        <?php include_once $data->homedir.'/view/administrasi/elements/sidebar.php'; ?>
        <!--sidebar ends here !!!!-->
        <div class="col-sm-9 col-md-9">
            <div class="well">
                <h1>Rumah Iska ~ Administrasi-<?php echo $data->subtitle ?></h1>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <a href="<?= $data->base_url.'administrasi/agen/new-agen' ?>" >Input Agen Baru</a>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="jumbotron">
                        <h3>Data Agen Berhasi disimpan</h3>
                        <p>Harap berikan link <a href="<?= $data->link ?>" title="Copy Location Link Ini"><?= $data->link ?></a> kepada agen anda untuk registrasi ke dalam sistem <?= $data->company ?></p>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>
</body>
</html>
