<!DOCTYPE html>
<html>
<head>
    <title>Rumah Iska -- <?php echo $data->judul ?></title>
    <?php include_once $data->homedir.'/view/administrasi/elements/header.php'; ?>
    <?php MetaTag() ?>
    <?php Links($data->base_url) ?>
    <?php Scripts($data->base_url) ?>
    <?php Styles() ?>

</head>
<body>
    

<?php include_once $data->homedir.'/view/administrasi/elements/top-nav.php'; ?>
<div class="container-fluid" style="padding-top:50px;padding-bottom:50px;">
    <div class="row">
        <!--sidebar is here !!!!-->
        <?php include_once $data->homedir.'/view/administrasi/elements/sidebar.php'; ?>
        <!--sidebar ends here !!!!-->
        <div class="col-sm-9 col-md-9">
            <div class="well">
                <h1>Rumah Iska ~ Administrasi-<?php echo $data->subtitle ?></h1>
            </div>

            <div class="row">
                <form action="" method="post">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Isi Laman</label>
                        <textarea class="form-control" name="in[apalah]" id="isi"><?= $data->konten ?></textarea>
                    </div>

                    <button class="btn btn-lg btn-primary" style="width:100%">Submit</button>
                </form>
                </div>
            </div>
            
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo $data->base_url ?>library/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
    CKEDITOR.replace('isi',{
        extraPlugins: 'imageuploader'
    });
</script>
</body>
</html>
