<!DOCTYPE html>
<html>
<head>
    <title>Rumah Iska -- <?php echo $data->judul ?></title>
    <?php include_once $data->homedir.'/view/administrasi/elements/header.php'; ?>
    <?php MetaTag() ?>
    <?php Links($data->base_url) ?>
    <?php Scripts($data->base_url) ?>
    <?php Styles() ?>

</head>
<body>
    

<?php include_once $data->homedir.'/view/administrasi/elements/top-nav.php'; ?>
<div class="container-fluid" style="padding-top:50px;padding-bottom:50px;">
    <div class="row">
        <!--sidebar is here !!!!-->
        <?php include_once $data->homedir.'/view/administrasi/elements/sidebar.php'; ?>
        <!--sidebar ends here !!!!-->
        <div class="col-sm-9 col-md-9">
            <div class="well">
                <h1>Rumah Iska ~ Administrasi-<?php echo $data->subtitle ?></h1>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <a href="<?= $data->base_url.'administrasi/agen/new-agen' ?>" >Input Agen Baru</a>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Kode</th>
                                <th>Username</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Telepon</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (count($data->lists)=='0'): ?>
                                    <tr>
                                        <td colspan="5" style="text-align:center;">
                                            Tidak Ada Data Agen Dalam Database Kami
                                        </td>
                                    </tr>
                                <?php else: ?>
                                <?php foreach ($data->lists as $key): ?>
                                    <tr>
                                        <td><?= $key['rc_hash'] ?></td>
                                        <td><?= $key['rc_username'] ?></td>
                                        <td><?= $key['rc_nama'] ?></td>
                                        <td><?= $key['rc_email'] ?></td>
                                        <td><?= $key['rc_telepon'] ?></td>
                                    </tr>
                                <?php endforeach ?>
                            <?php endif ?>
                        </tbody>
                    </table>
                </div>
            </div>


        </div>
    </div>
</div>
</body>
</html>
