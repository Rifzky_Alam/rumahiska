<!DOCTYPE html>
<html>
<head>
    <title>Rumah Iska -- <?php echo $data->judul ?></title>
    <?php include_once $data->homedir.'/view/administrasi/elements/header.php'; ?>
    <?php MetaTag() ?>
    <?php Links($data->base_url) ?>
    <?php Scripts($data->base_url) ?>
    <?php Styles() ?>

</head>
<body>
<?php

?>

<?php include_once $data->homedir.'/view/administrasi/elements/top-nav.php'; ?>
<div class="container-fluid" style="padding-top:50px;padding-bottom:50px;">
    <div class="row">
        <!--sidebar is here !!!!-->
        <?php include_once $data->homedir.'/view/administrasi/elements/sidebar.php'; ?>
        <!--sidebar ends here !!!!-->
        <div class="col-sm-9 col-md-9">
            <div class="well">
                <h1>Rumah Iska ~ Administrasi-<?php echo $data->subtitle ?></h1>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <form action="" method="get" accept-charset="utf-8">
                    <div class="form-group">
                        <input type="text" class="form-control" name="nama" placeholder="Cari berdasarkan nama customer dan tekan enter">                                
                    </div>                            
                    </form>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Email</th>
                                <th>Nama</th>
                                <th>Telepon</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (count($data->listcust)==''): ?>
                                <tr>
                                    <td colspan="4" style="text-align:center;">Tidak Ada Data Dalam Database Kami</td>
                                </tr>
                            <?php else: ?>
                                <?php foreach ($data->listcust as $key): ?>
                                    <tr>
                                        <td><?= $key->rc_email ?></td>
                                        <td><?= $key->rc_nama ?></td>
                                        <td><?= $key->rc_telepon ?></td>
                                        <td>
                                            <a href="<?= $data->base_url.'administrasi/transaksi/customer/edit/'.$key->rc_email ?>">Edit</a>
                                            ||
                                            <a href="<?= $data->base_url.'administrasi/transaksi/customer/newalamat/'.$key->rc_email ?>">Tambah Tanggal</a>
                                        </td>
                                    </tr>      
                                <?php endforeach ?>
                            <?php endif ?>
                            
                        </tbody>
                    </table>
                </div>
            </div>


        </div>
    </div>
</div>

</body>
</html>
