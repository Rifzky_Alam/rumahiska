<!DOCTYPE html>
<html>
<head>
    <title>Rumah Iska -- <?php echo $data->judul ?></title>
    <?php $data->View('administrasi/elements/header.php',$data); ?>
    <?php MetaTag() ?>
    <?php Links($data->base_url) ?>
    <?php Scripts($data->base_url) ?>
    <?php Styles() ?>

</head>
<body>
<?php

?>

<?php $data->View('administrasi/elements/top-nav.php',$data); ?>
<div class="container-fluid" style="padding-top:50px;padding-bottom:50px;">
    <div class="row">
        <!--sidebar is here !!!!-->
        <?php $data->View('administrasi/elements/sidebar.php',$data); ?>
        <!--sidebar ends here !!!!-->
        <div class="col-sm-9 col-md-9">
            <div class="well">
                <h1>Rumah Iska ~ Administrasi-<?php echo $data->judul ?></h1>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <a href="<?php echo $data->base_url.$data->package ?>/transaksi/?act=new" style="text-align:left;">Input Baru</a>
                    ||
                    <a id="cari-data" href="#modal-cari" data-toggle="modal">Search <i class="glyphicon glyphicon-search"></i></a>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th style="text-align:center;">No Transaksi</th>
                                <th style="text-align:center;">Tanggal</th>
                                <th style="text-align:center;">Nama Cust</th>
                                <th style="text-align:center;">Email</th>
                                <th style="text-align:center;">Telepon</th>
                                <th style="text-align:center;">Items</th>
                                <th style="text-align:center;">Total</th>
                                <th style="text-align:center;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php Rows($data->me,$data->lists) ?>
                        </tbody>
                    </table>
                </div>
            </div>


        </div>
    </div>
</div>


<!-- Modal -->
  <div class='modal fade' id='modal-cari' role='dialog'>
    <div class='modal-dialog'>

      <!-- Modal content-->
      <div class='modal-content'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'>&times;</button>
          <h3 class="modal-title">Pencarian Data</h3>

        </div>
        <div class='modal-body'>
        <div style='padding-left:15px;padding-right:15px;padding-top:0px; height: 300px;' class='row'>

          <div class="col-md-12">

          </div>



        </div>


        </div>
        <div class='modal-footer'>
            <div class="row">
                <div class="col-md-6" style="text-align:left">
                    <span>Rumah iska 2017</span>
                </div>
                <div class="col-md-6">
                    <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                </div>
            </div>

        </div>
      </div>

    </div>
  </div>
  <!-- end modal -->
<!-- Modal -->
  <div class='modal fade' id='modal-menu' role='dialog'>
    <div class='modal-dialog'>

      <!-- Modal content-->
      <div class='modal-content'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'>&times;</button>
          <h3 class="modal-title">Menu Transaksi</h3>

        </div>
        <div class='modal-body'>
        <div style='padding-left:15px;padding-right:15px;padding-top:0px; height:300px;overflow-y:auto;' class='row'>

          <div class="col-md-12">
            <a href="#" target="_blank" id="btndt" style="width:100%" class="btn btn-lg btn-default">Detail Transaksi</a>
            <a href="#" target="_blank" id="btndc" style="width:100%" class="btn btn-lg btn-warning">Data Customer</a>
            <a href="#" target="_blank" id="btndb" style="width:100%" class="btn btn-lg btn-success">Data Barang</a>
            <a href="#" target="_blank" id="btndk" style="width:100%" class="btn btn-lg btn-danger">Data Kurir</a>
            <a href="#" target="_blank" id="btnur" style="width:100%" class="btn btn-lg btn-warning">Update Resi</a>
            <a href="#" target="_blank" id="btnpf" style="width:100%" class="btn btn-lg btn-primary">Preview Faktur</a>
            <a href="#" target="_blank" id="btnkf" style="width:100%" class="btn btn-lg btn-info">Kirim Faktur</a>
            <a href="#" target="_blank" id="btnubt" style="width:100%" class="btn btn-lg btn-default">Upload Bukti Transaksi</a>
          </div>
        </div>


        </div>
        <div class='modal-footer'>
            <div class="row">
                <div class="col-md-6" style="text-align:left">
                    <span>Rumah iska 2017</span>
                </div>
                <div class="col-md-6">
                    <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                </div>
            </div>

        </div>
      </div>

    </div>
  </div>
  <!-- end modal -->
  <!-- Modal -->
  <div class='modal fade' id='modal-status' role='dialog'>
    <div class='modal-dialog'>

      <!-- Modal content-->
      <div class='modal-content'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'>&times;</button>
          <h3 class="modal-title">Menu Transaksi</h3>

        </div>
        <div class='modal-body'>
        <div style='padding-left:15px;padding-right:15px;padding-top:0px; height:300px;overflow-y:auto;' class='row'>

          <div class="col-md-12">
            <a href="" id="stc" style="width:100%" class="btn btn-lg btn-danger">
              Cancel
            </a>
            <a href="" id="stk" style="width:100%" class="btn btn-lg btn-warning">
              Faktur Terkirim
            </a>
            <a href="" id="stp" style="width:100%" class="btn btn-lg btn-success">
              Bukti Transfer Terunggah
            </a>
            <a href="" id="sts" style="width:100%" class="btn btn-lg btn-success">
              Barang Terkirim
            </a>
          </div>
        </div>


        </div>
        <div class='modal-footer'>
            <div class="row">
                <div class="col-md-6" style="text-align:left">
                    <span>Rumah iska 2017</span>
                </div>
                <div class="col-md-6">
                    <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                </div>
            </div>

        </div>
      </div>

    </div>
  </div>
  <!-- end modal -->

<script>
  $('.mymenu').click(function (){
    // alert('bisa');
    $('#btndt').prop('href', "<?= $data->base_url ?>administrasi/transaksi/detail?tr=" + this.id);
    $('#btndc').prop('href', "<?= $data->base_url ?>administrasi/transaksi/?edt=" + this.id);
    $('#btndb').prop('href', "<?= $data->base_url ?>administrasi/transaksi/carts/add/" + this.id);
    $('#btndk').prop('href', "<?= $data->base_url ?>administrasi/transaksi/kurir/view/" + this.id);
    $('#btnur').prop('href', "<?= $data->base_url ?>administrasi/transaksi/resi?tr=" + this.id);
    $('#btnpf').prop('href', "<?= $data->base_url ?>administrasi/transaksi/faktur?tr=" + this.id);
    $('#btnkf').prop('href', "<?= $data->base_url ?>administrasi/transaksi/faktur?send=" + this.id);
    $('#btnubt').prop('href', "<?= $data->base_url ?>administrasi/transaksi/bukti/upload/" + this.id);
  });
  $('.mystatus').click(function (){
    // alert('bisa');
    $('#stc').prop('href', "<?= $data->base_url ?>administrasi/transaksi/status/cancel/" + idstatrep(this.id));
    $('#stk').prop('href', "<?= $data->base_url ?>administrasi/transaksi/status/kirimfaktur/" + idstatrep(this.id));
    $('#stp').prop('href', "<?= $data->base_url ?>administrasi/transaksi/status/uploadbukti/" + idstatrep(this.id));
    $('#sts').prop('href', "<?= $data->base_url ?>administrasi/transaksi/status/pengiriman/" + idstatrep(this.id));
  });

  function idstatrep(id) {
    id = id.replace('st','');
    return id;
  }
</script>
</body>
</html>

<?php function Rows($url,$data){ ?>
    <?php foreach ($data as $key) { ?>
      <?php if ($key->tr_status=='0'): ?>
        <tr class="danger">
      <?php elseif($key->tr_status=='1'): ?>
        <tr class="warning">
      <?php elseif($key->tr_status=='2'): ?>
        <tr class="info">
      <?php elseif($key->tr_status=='3'): ?>
        <tr class="success">
      <?php endif ?>
      
          <td style="text-align:center;"><?php echo $key->tr_id ?></td>
          <td style="text-align:center;"><?php echo $key->tr_tanggal ?></td>
          <td style="text-align:center;"><?php echo $key->rc_nama ?></td>
          <td style="text-align:center;"><?php echo $key->rc_email ?></td>
          <td style="text-align:center;"><?php echo $key->rc_telepon ?></td>
          <td style="text-align:center;"><?php echo $key->items ?></td>
          <td style="text-align:center;"><?php echo $key->total ?></td>
          <td>
              <a id="<?php echo $key->tr_id ?>" href="#modal-menu" class="mymenu" data-toggle="modal">Menu</a>
              ||
              <a id="<?= 'st'.$key->tr_id ?>" href="#modal-status" class="mystatus" data-toggle="modal">Status</a>
              <!--||
              <a href="faktur?send=<?= $key->tr_id ?>">(Kirim Faktur)</a>
              ||
              <a href="resi?tr=<?= $key->tr_id ?>">(input/ubah resi)</a>
            -->
          </td>

      </tr>
    <?php } ?>
<?php } ?>

<?php function KategoriBarang($data){ ?>
    <?php foreach ($data as $key) { ?>
        <option value="<?php echo $key->kb_id ?>"><?php echo $key->kb_ket ?></option>
    <?php } ?>
<?php } ?>

<?php function JenisBarang($data){ ?>
    <?php foreach ($data as $key) { ?>
        <option value="<?php echo $key->id ?>"><?php echo $key->jenis_barang ?></option>
    <?php } ?>
<?php } ?>
