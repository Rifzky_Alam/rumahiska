<!DOCTYPE html>
<html>
<head>
    <title><?php echo $data->company.' -- '.$data->judul ?></title>
    <?php $data->View('administrasi/elements/header.php',$data); ?>
    <?php MetaTag() ?>
    <meta charset="utf-8">
    <?php Links($data->base_url) ?>
    <?php Scripts($data->base_url) ?>
    <?php Styles() ?>

</head>
<body>
<?php

?>

<?php $data->View('administrasi/elements/top-nav.php',$data); ?>
<div class="container-fluid" style="padding-top:50px;padding-bottom:50px;">
    <div class="row">


        <div class="col-sm-12 col-md-12">
            <div class="well">
                <h1><?= $data->company ?> ~ Administrasi-<?php echo $data->judul ?></h1>
            </div>

            

            <div class="row">
                <div class="col-md-8">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th style="text-align:center;" colspan="2">Detail Transaksi</th>
                            </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>Nama Pelanggan <a href="#" title=""><i class="glyphicon glyphicon-edit"></i></a></td>
                            <td><?= $data->namacust ?></td>
                          </tr>
                          <tr>
                            <td>Email Pelanggan</td>
                            <td><?= $data->emailcust ?></td>
                          </tr>
                          <tr>
                            <td>Telepon Pelanggan</td>
                            <td><?= $data->telepon ?></td>
                          </tr>
                          <tr>
                            <td>Alamat Kirim Pelanggan</td>
                            <td><?= $data->alamat ?></td>
                          </tr>
                          <tr>
                            <td>Kecamatan</td>
                            <td id="kecamatan"><?= $data->kecamatan ?></td>
                          </tr>
                          <tr>
                            <td>Kota</td>
                            <td id="kota"><?= $data->kota ?></td>
                          </tr>
                          <tr>
                            <td>Provinsi</td>
                            <td id="provinsi"><?= $data->prov ?></td>
                          </tr>
                          <tr>
                            <td>Jasa Kurir <a href="#" title=""><i class="glyphicon glyphicon-edit"></i></a></td>
                            <td><?= $data->jasakurir ?></td>
                          </tr>
                          <tr>
                            <td>No Resi</td>
                            <td><?= $data->no_resi ?></td>
                          </tr>
                          <tr>
                            <td>Layanan Jasa (kurir)</td>
                            <td><?= $data->service ?></td>
                          </tr>
                          <tr>
                            <td>Biaya Jasa Kurir</td>
                            <td><?= $data->ongkir ?></td>
                          </tr>
                        </tbody>
                    </table>
                    <a href="<?= $data->base_url ?>administrasi/transaksi/" class="btn btn-lg btn-warning" title="Kembali">
                      << Kembali
                    </a>
                    <a href="<?= $data->base_url.'order/invoice?tr='.$data->id_transaksi ?>" class="btn btn-lg btn-info" title="Kembali" target="_blank">
                      Lihat Detail Invoice
                    </a>
                </div>

                <div class="col-md-4">
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th>Nama Item</th>
                        <th>Berat</th>
                        <th>Harga</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php if (count($data->listbarang)=='0'): ?>
                        <tr>
                          <td colspan="4" style="text-align:center;">Belum Ada Data</td>
                        </tr>  
                        <?php else: ?>
                          <?php foreach ($data->listbarang as $key): ?>
                            <tr>
                              <td><?= $key->jenis_barang.' - '. $key->detail_jenis ?></td>
                              <td><?= $key->brg_berat ?></td>
                              <td><?= $key->th_harga ?></td>
                              <td><a href="carts.php/delete/<?= $key->cart_id ?>" class="btn btn-danger" title="">Del</a></td>
                            </tr>
                          <?php endforeach ?>
                      <?php endif ?>
                      
                    </tbody>
                  </table>

                  <section>
                    <h4>Bukti Transfer <a href="<?= $data->base_url.'administrasi/transaksi/bukti.php/upload/'.$data->id_transaksi ?>" title=""><i class="glyphicon glyphicon-edit"></i></a></h4>
                    <a href="<?= $data->base_url.'uploads/images/transaksi/'.$data->buktiupload ?>" title="">
                      <img src="<?= $data->base_url.'uploads/images/transaksi/'.$data->buktiupload ?>" alt="Bukti Transfer" class="img img-responsive">
                    </a>
                  </section>
                </div>

            </div>
        </div>
    </div>
</div>


<!-- Modal -->
  <div class='modal fade' id='modal-cari' role='dialog'>
    <div class='modal-dialog'>

      <!-- Modal content-->
      <div class='modal-content'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'>&times;</button>
          <h3 class="modal-title">Pencarian Data</h3>

        </div>
        <div class='modal-body'>
        <div style='padding-left:15px;padding-right:15px;padding-top:0px; height: 300px;' class='row'>

          <div class="col-md-12">

          </div>



        </div>


        </div>
        <div class='modal-footer'>
            <div class="row">
                <div class="col-md-6" style="text-align:left">
                    <span>Rumah iska 2017</span>
                </div>
                <div class="col-md-6">
                    <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                </div>
            </div>

        </div>
      </div>

    </div>
  </div>
  <!-- end modal -->

<script src="<?= $data->base_url ?>assets/common/bootstrap/js/jquery.js"></script>
<script src="<?= $data->base_url ?>assets/common/bootstrap/js/bootstrap.min.js"></script>

<script>
  $(document).ready(function(){
    LoadProv(<?= $data->prov ?>);
    LoadCity(<?= $data->kota ?>);
    LoadKecamatan(<?= $data->kecamatan ?>)
  });

  function LoadProv(idprov) {
    $.ajax({
      type: "GET",
        url: "<?= $data->base_url ?>library/rajaongkir/pcheck",
        data: {
          'id':idprov
        },
        cache: false,
        success: function(data){
          var myjson = JSON.parse(data);
          var result = myjson.rajaongkir.results
          document.getElementById("provinsi").innerHTML = result.province
          
        }
      }); //end ajax
  }

  function LoadCity(city_id) {
    $.ajax({
      type: "GET",
        url: "<?= $data->base_url ?>library/rajaongkir/ctcheck",
        data: {
          'id':city_id
        },
        cache: false,
        success: function(data){
          var myjson = JSON.parse(data);
          var result = myjson.rajaongkir.results
          document.getElementById("kota").innerHTML = result.type + " " + result.city_name
          
        }
      }); //end ajax
  }

  function LoadKecamatan(kec_id) {
    $.ajax({
      type: "GET",
        url: "<?= $data->base_url ?>library/rajaongkir/sdcheck",
        data: {
          'id':kec_id
        },
        cache: false,
        success: function(data){
          var myjson = JSON.parse(data);
          var result = myjson.rajaongkir.results
          document.getElementById("kecamatan").innerHTML = result.subdistrict_name
          
        }
      }); //end ajax
  }

</script>

</body>
</html>

<?php function Rows($url,$data){ ?>
    <?php foreach ($data as $key) { ?>
      <tr>
          <td style="text-align:center;"><?php echo $key->tr_id ?></td>
          <td style="text-align:center;"><?php echo $key->tr_tanggal ?></td>
          <td style="text-align:center;"><?php echo $key->rc_nama ?></td>
          <td style="text-align:center;"><?php echo $key->rc_email ?></td>
          <td style="text-align:center;"><?php echo $key->rc_telepon ?></td>
          <td style="text-align:center;"><?php echo $key->items ?></td>
          <td style="text-align:center;"><?php echo $key->total ?></td>
          <td>
              <a href="barang?edt=<?php echo $key->tr_id ?>">Edit</a>
              ||
              <a href="barang-gallery?idb=<?= $key->tr_id ?>">(Kirim Faktur)</a>
              ||
              <a href="barang-gallery?idb=<?= $key->tr_id ?>">(input/ubah resi)</a>
          </td>

      </tr>
    <?php } ?>
<?php } ?>

<?php function KategoriBarang($data){ ?>
    <?php foreach ($data as $key) { ?>
        <option value="<?php echo $key->kb_id ?>"><?php echo $key->kb_ket ?></option>
    <?php } ?>
<?php } ?>

<?php function JenisBarang($data){ ?>
    <?php foreach ($data as $key) { ?>
        <option value="<?php echo $key->id ?>"><?php echo $key->jenis_barang ?></option>
    <?php } ?>
<?php } ?>
