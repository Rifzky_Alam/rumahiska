<!DOCTYPE html>
<html>
<head>
    <title>Rumah Iska -- <?php echo $data->judul ?></title>
    <?php include_once $data->homedir.'/view/administrasi/elements/header.php'; ?>
    <?php MetaTag() ?>
    <?php Links($data->base_url) ?>
    <?php Scripts($data->base_url) ?>
    <?php Styles() ?>

</head>
<body>
<?php

?>

<?php include_once $data->homedir.'/view/administrasi/elements/top-nav.php'; ?>
<div class="container-fluid" style="padding-top:50px;padding-bottom:50px;">
    <div class="row">
        <!--sidebar is here !!!!-->
        <?php include_once $data->homedir.'/view/administrasi/elements/sidebar.php'; ?>
        <!--sidebar ends here !!!!-->
        <div class="col-sm-9 col-md-9">
            <div class="well">
                <h1>Rumah Iska ~ Administrasi-<?php echo $data->subtitle ?></h1>
            </div>

            <div class="row">
                <form action="" method="post" accept-charset="utf-8">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Tanggal Kirim</label>
                        <input class="form-control" type="text" name="in[tglkirim]" placeholder="Format: yyyy-mm-dd (2018-01-31)" required></input>
                    </div>
                    <div class="form-group">
                        <label>No Resi</label>
                        <input class="form-control" value="-" type="text" name="in[resi]" placeholder="No Resi Pengiriman Barang" required></input>
                    </div>

                    <button class="btn btn-lg btn-primary" style="width:100%">Submit</button>
                </form>
                </div>
            </div>


        </div>
    </div>
</div>

</body>
</html>
