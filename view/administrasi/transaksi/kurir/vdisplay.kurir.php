<!DOCTYPE html>
<html>
<head>
    <title>Rumah Iska -- <?php echo $data->judul ?></title>
    <?php include_once $data->homedir.'/view/administrasi/elements/header.php'; ?>
    <?php MetaTag() ?>
    <?php Links($data->base_url) ?>
    <?php Scripts($data->base_url) ?>
    <?php Styles() ?>

</head>
<body>
<?php

?>

<?php include_once $data->homedir.'/view/administrasi/elements/top-nav.php'; ?>
<div class="container-fluid" style="padding-top:50px;padding-bottom:50px;">
    <div class="row">
        <!--sidebar is here !!!!-->
        <?php include_once $data->homedir.'/view/administrasi/elements/sidebar.php'; ?>
        <!--sidebar ends here !!!!-->
        <div class="col-sm-9 col-md-9">
            <div class="well">
                <h1>Rumah Iska ~ Administrasi-<?php echo $data->subtitle ?></h1>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Jasa Kurir</th>
                                <th>Layanan</th>
                                <th>Alamat Pengiriman</th>
                                <th>Provinsi</th>
                                <th>Kota</th>
                                <th>Biaya</th>
                                <th>Resi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (count($data->listkurir)=='0'): ?>
                            <tr>
                                <td colspan="7">Tidak Ada Data Kurir di transaksi ini, harap untuk memasukkannya terlebih dahulu</td>
                            </tr>
                            <?php else: ?>
                                <?php foreach ($data->listkurir as $key): ?>
                                    <tr>
                                        <td></td>
                                    </tr>
                                <?php endforeach ?>
                            <?php endif ?>
                        </tbody>
                    </table>
                </div>
            </div>

            <form action="" method="post" accept-charset="utf-8">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Jasa Kurir</label>
                        <input type="hidden" id="sbtb" value="<?= $data->beratbarang ?>">
                        <input type="hidden" id="sbt" value="<?= $data->totalhargabarang ?>">
                        <select name="in[jasa]" id="dv" class="form-control">
                            <option value="jne">JNE</option>
                            <option value="pos">POS Indonesia</option>
                            <option value="tiki">TIKI</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Layanan</label>
                        <select name="in[service]" id="ds" class="form-control"></select>
                    </div>

                    <div class="form-group">
                        <h6>Alamat</h6>
                        <?php if (count($data->listalamat)=='0'): ?>
                            <p>Tidak ada data alamat untuk transaksi ini.</p>
                        <?php else: ?>
                            <?php foreach ($data->listalamat as $key): ?>
                                <div class="radio">
                                    <label><input type="radio" name="in[alamat]" value="<?= $key->adr_id ?>"><?= $key->adr_alamat ?></label>
                                </div>
                            <?php endforeach ?>
                        <?php endif ?>
                    </div>

                    <div class="form-group">
                        <label>Kota</label>
                        <select id="cty" name="in[kota]" class="form-control">
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Provinsi</label>
                        <select id="prv" name="in[prov]" class="form-control">
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Ongkir</label>
                        <input type="text" name="in[ongkir]" id="ogkr" placeholder="hanya angka" class="form-control">
                    </div>
                    <button class="btn btn-lg btn-primary" style="width:100%" id="tombol">Submit</button>
                </div>
            </div>
            </form>

        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
      $.ajax({
          type: "GET",
          url: "<?= $data->base_url.'library/rajaongkir/' ?>store",
          data: {
            'q':'province'
          },
          cache: false,
          success: function(data){
            // $('#hasil').html(data);
            var myjson = JSON.parse(data);
            var result = myjson.rajaongkir.results
            for (x in result) {
              if (result[x].province_id=='6') {
                document.getElementById("prv").innerHTML += "<option selected value='" + result[x].province_id + "'>" + result[x].province + '</option>';
              } else {
                document.getElementById("prv").innerHTML += "<option value='" + result[x].province_id + "'>" + result[x].province + '</option>';
              }
                            }
          }
      }); //end ajax
      $('.optcity').remove();
      $.ajax({
          type: "GET",
          url: "<?= $data->base_url.'library/rajaongkir/' ?>store",
          data: {
            'prov': '6'
          },
          cache: false,
          success: function(data){
            // $('#hasil').html(data);
            
            var myjsonn = JSON.parse(data);
            var resultt = myjsonn.rajaongkir.results
            for (z in resultt) {
              document.getElementById("cty").innerHTML += "<option class='optcity' value='" + resultt[z].city_id + "'>" + resultt[z].type + ' ' +resultt[z].city_name + '</option>';
            }
            
            $('.optsrv').remove();
            GetService();

          }
      }); //end ajax

    });



    function TentukanHarga() {
      
      var wgh = $('#sbtb').val();//$('#wgh').html();
      var vdr = $('#dv').val();
      var dst = $('#cty').val();

      $.ajax({
          type: "GET",
          url: "<?= $data->base_url.'library/rajaongkir/' ?>calculateprice",
          data: {
            'js': vdr,
            'it': dst,
            'bb': wgh
          },
          cache: false,
          success: function(data){
            // $('#hasil').html(data);
            
            var myjsonz = JSON.parse(data);
            var resultz = myjsonz.rajaongkir.results[0].costs
            GetPayment(resultz);
          }
      }); //end ajax
    }

    $('#ds').change(function(){
      // $('.optsrv').remove();
      TentukanHarga();
    });

    $('#cty').change(function(){
      // TentukanHarga();
      GetService();
    });

    $('#prv').change(function(){
      $('.optcity').remove();
      $.ajax({
          type: "GET",
          url: "<?= $data->base_url.'library/rajaongkir/' ?>store",
          data: {
            'prov': $('#prv').val()
          },
          cache: false,
          success: function(data){
            // $('#hasil').html(data);
            
            var myjsonn = JSON.parse(data);
            var resultt = myjsonn.rajaongkir.results
            for (z in resultt) {
              document.getElementById("cty").innerHTML += "<option class='optcity' value='" + resultt[z].city_id + "'>" + resultt[z].type + ' ' +resultt[z].city_name + '</option>';
            }
            $('.optsrv').remove();
            GetService();
            // TentukanHarga();
            

          // console.log(resultt);
          }
      }); //end ajax
    });

    $('#dv').change(function(){
      $('.optsrv').remove();
      GetService();
    });

    function GetNumber(number,id,currency) {
      $('#'+id).html('Menghitung ..');
      $.ajax({
          type: "GET",
          url: "priceformat",
          data: {
            'n': number,
            'f': currency
          },
          cache: false,
          success: function(data){
            // $('#hasil').html(data);
            
            $('#'+id).html(data);

          }
      }); //end ajax
    }


    function GetPayment(dataset){
      document.getElementById("tombol").disabled = true;
      for (z in dataset) {
        if ($('#ds').val()==dataset[z].service) {
          document.getElementById("ogkr").value = dataset[z].cost[0].value;
          GetNumber(dataset[z].cost[0].value,'bo','IDR');
          var hah = parseInt($('#sbt').val());
          var tes = hah + parseInt(dataset[z].cost[0].value);
          GetNumber(tes,'tb','IDR');
        }
      }
      document.getElementById("tombol").disabled = false;
    }
      
    function GetService() {
      var wgh = $('#sbtb').val();
      var vdr = $('#dv').val();
      var dst = $('#cty').val();
      $.ajax({
          type: "GET",
          url: "<?= $data->base_url.'library/rajaongkir/' ?>calculateprice",
          data: {
            'js': vdr,
            'it': dst,
            'bb': wgh
          },
          cache: false,
          success: function(data){
            // $('#hasil').html(data);
            console.log(data);
            var myjsonz = JSON.parse(data);
            var resultz = myjsonz.rajaongkir.results[0].costs
            for (z in resultz) {
              document.getElementById("ds").innerHTML += "<option class='optsrv' value='" + resultz[z].service + "'>" + resultz[z].description + '</option>';
            }
            GetPayment(resultz);
          }
      }); //end ajax
    }

  </script>

</body>
</html>
