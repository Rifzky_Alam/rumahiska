<!DOCTYPE html>
<html>
<head>
    <title>Rumah Iska -- <?php echo $data->judul ?></title>
    <?php include_once $data->homedir.'/view/administrasi/elements/header.php'; ?>
    <?php MetaTag() ?>
    <?php Links($data->base_url) ?>
    <?php Scripts($data->base_url) ?>
    <?php Styles() ?>

</head>
<body>
<?php

?>

<?php include_once $data->homedir.'/view/administrasi/elements/top-nav.php'; ?>
<div class="container-fluid" style="padding-top:50px;padding-bottom:50px;">
    <div class="row">
        <!--sidebar is here !!!!-->
        <?php include_once $data->homedir.'/view/administrasi/elements/sidebar.php'; ?>
        <!--sidebar ends here !!!!-->
        <div class="col-sm-9 col-md-9">
            <div class="well">
                <h1>Rumah Iska ~ Administrasi-<?php echo $data->subtitle ?></h1>
            </div>

            <div class="row">
                <form action="" method="post" enctype="multipart/form-data">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Nama Customer</label>
                        <select name="in[email]" class="form-control">
                            <?php if (count($data->listcust)=='0'): ?>
                            <option value="#">Belum ada data customer</option>
                                <?php else: ?>
                            <?php foreach ($data->listcust as $key): ?>
                                <?php if ($key->rc_email==$data->customer): ?>
                                    <option value="<?= $key->rc_email ?>" selected><?= $key->rc_nama ?></option>
                                    <?php else: ?>
                                    <option value="<?= $key->rc_email ?>"><?= $key->rc_nama ?></option>    
                                <?php endif ?>
                                
                            <?php endforeach ?>
                            <?php endif ?>
                            
                        </select>
                    </div>

                    <button class="btn btn-lg btn-primary" style="width:100%">Submit</button>
                </form>
                </div>
            </div>


        </div>
    </div>
</div>
</body>
</html>
