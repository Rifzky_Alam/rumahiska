<?php 
function Email($data){
	return '

    <table id="m_-5588023840110578919m_3185550004040353724backgroundTable" style="background:#e1e1e1" width="100%" cellspacing="0" cellpadding="0" border="0">
        <tbody><tr>
            <td class="m_-5588023840110578919m_3185550004040353724body" style="background:#e1e1e1" width="100%" valign="top" align="center">
                <table cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td width="640">
                            
                        </td>
                    </tr>
                    <tr>
                        <td class="m_-5588023840110578919m_3185550004040353724main" style="padding:0 10px" width="640" align="center">
                            <table cellspacing="0" cellpadding="0">
    <tbody><tr>
        <td width="640" align="left">
            <table width="100%" cellspacing="0" cellpadding="0">
                <tbody><tr>
                    <td class="m_-5588023840110578919m_3185550004040353724header m_-5588023840110578919m_3185550004040353724header--left" style="padding:20px 10px" align="left">
                        <a href="'.$data->base_url.'" target="_blank">
                            <img class="m_-5588023840110578919m_3185550004040353724header__logo CToWUd" src="'.$data->base_url.'view/_rumahiska/data/bitmapz.png" alt="'.$data->company.'" style="display:block;border:0" width="185" height="110"></a>
                    </td>
                </tr>
            </tbody></table>
        </td>
    </tr>
</tbody></table>
                            <table class="m_-5588023840110578919m_3185550004040353724featured-story m_-5588023840110578919m_3185550004040353724featured-story--top" cellspacing="0" cellpadding="0">
    <tbody><tr>
        <td style="padding-bottom:20px">
            <table cellspacing="0" cellpadding="0">
                <tbody><tr>
                    <td class="m_-5588023840110578919m_3185550004040353724featured-story__inner" style="background:#fff">
                        <table cellspacing="0" cellpadding="0">
                            <tbody><tr>
                                <td class="m_-5588023840110578919m_3185550004040353724featured-story__content-inner" style="padding:32px 30px 45px">
                                    <table cellspacing="0" cellpadding="0">
                                        <tbody><tr>
<td class="m_-5588023840110578919m_3185550004040353724featured-story__heading m_-5588023840110578919m_3185550004040353724featured-story--top__heading" style="background:#fff" width="640" align="left">
                                                <table cellspacing="0" cellpadding="0">
                                                    <tbody><tr>
                                                        <td style="font-family:Geneva,Tahoma,Verdana,sans-serif;font-size:22px;color:#464646" width="600" align="left">
                                                            <a href="'.$data->base_url.'" style="text-decoration:none;color:#464646">Konfirmasi Pembayaran</a>
                                                        </td>
                                                    </tr>
                                                </tbody></table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="m_-5588023840110578919m_3185550004040353724featured-story__copy" style="background:#fff" width="640" align="left">
                                                <table cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                    <tr>
                                                        <td style="font-family:Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;font-size:16px;line-height:22px;color:#555555;padding-top:16px" align="left">
                                                        '.HeaderText($data->customer).'
                                                        <h3>Rincian Item</h3>
                                                        <table border="1" width="100%">
                                                            '.TableItem($data->carts,$data->ongkir).'
                                                        </table>
                                                        '.DeliveryDesc($data->tglkirim,$data->resi,$data->securitycode($data->idtrans,'e'),$data->base_url).'
                                                        '.Payment(['BANK BCA Syariah (Kode:536) 0261000395 A/N Ayu Noor Laila Sari','BANK BNI Syariah 0322131041 A/N Ayu Noor Laila','BANK Mandiri 900000 360 2027 A/N Ayu Noor Laila Sari'],$data->securitycode($data->idtrans,'e'),$data->base_url).'
                                                        <br><br>
                                                        Salam<br><br><br><br>


                                                        <b><u>'.$data->petugas.'</u></b><br>
                                                        <b>Admin '.$data->company.'</b>
                                                        </td>
                                                    </tr>
                                                </tbody></table>
                                            </td>
                                        </tr>
                                    </tbody></table>
                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
            </tbody></table>
        </td>
    </tr>
</tbody></table>
                            <table cellspacing="0" cellpadding="0">
<tbody><tr>
                        <td class="m_-5588023840110578919m_3185550004040353724footer" style="padding-top:10px" width="640" align="center">
                            <table cellspacing="0" cellpadding="0">
                                <tbody><tr>
                                    <td style="font-family:Geneva,Tahoma,Verdana,sans-serif;font-size:14px;line-height:18px;color:#738597;padding:0 20px 30px" align="center">
                                                        
We sent this message to you because you are associated with '.$data->company.' Website System.<br><br>

The system designed and developed by <a href="mailto:rifzky.mail@gmail.com?subject=feedback">Rifzky Alam</a>

                        </td>
                                </tr>
</tbody></table>
                        
                        </td>
                    </tr>
                    <tr>
                        <td class="m_-5588023840110578919m_3185550004040353724footer" style="padding-top:10px" width="640" align="center">
                            <table cellspacing="0" cellpadding="0">
                                <tbody><tr>
                                    <td style="font-family:Geneva,Tahoma,Verdana,sans-serif;font-size:14px;line-height:18px;color:#738597;padding:0 20px 40px" align="center">
                                        <a href="'.$data->base_url.'" target="_blank"><img src="'.$data->base_url.'view/_rumahiska/data/bitmapz.png" alt="'.$data->company.'" style="display:block;border:0" class="CToWUd" width="120" height="50"></a>
                        

                                        <br>'.$data->company.'<br><a href="https://maps.google.com/?q=Rumah+Iska" target="_blank">'.$data->addrcompany.'</a> 
                                        <br>
                                        '.$data->companydesc.'
                                    </td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                </tbody></table>
            </td>
        </tr>
    </tbody></table>

    
    <img src="https://ci3.googleusercontent.com/proxy/hKZyLEBTjnS-PycQdIixFD9IvcAGaZ8cU1JqGAVx73I4iKNB9FKc2JNpS2pb4xsS-oadFhmbDC2laUKuBBl28tniF5rJVTfGkXSKMzSitVdrZtka0GUum0LAe3FhnTfb0RwHr2NEN7KZkqgWzQqTX7MuBL3yLlbOIB4yjvY4NU9xJVabZxzrta8p1t_ITNxJ84m_U6CwVulJAnDms1pvNshsqsTgdJO2s8rLbTUpF6IckPl184AsNuw=s0-d-e1-ft#https://click.e.mozilla.org/open.aspx?ffcb10-fe89107470670d787d-fdf716707367057b70127771-fe9915707361037e75-ff6e157075-fe3417757664017b761472-ff2a11767d66&amp;d=40053" class="CToWUd" width="1" height="1">

</td></tr></tbody></table>
	';
}

function HeaderText($cust){
    return 'Assalamualaikum '.$cust.', <br>
            Terimakasih telah melakukan pesanan barang di salvina.id. 
            Berikut kami informasikan detail transaksi anda.';
}

function TableItem($item,$ongkir){
    $hasil = '';
    $subtotal = 0;
    if (count($item)=='0') {
        $hasil.='<tr><td colspan="4">Tidak Ada Data Pesanan Dalam Database Kami</td></tr>';
    } else {
        foreach ($item as $key) {
            $hasil .= '<tr><td>'.$key->jenis_barang .' '. $key->detail_jenis .'</td><td style="text-align:right;">'.$key->brg_berat.' gram</td><td style="text-align:center;">'.$key->qty.'</td><td style="text-align:right;">'.number_format($key->th_harga).' IDR</td></tr>';
            $subtotal+=$key->th_harga*$key->qty;
        }
    }

    return '<thead>
                <tr>
                <th>Nama Item</th>
                <th>Berat</th>
                <th>Qty</th>
                <th>Harga</th>
                </tr>
            </thead>
            <tbody>
                '.$hasil.'
                <tr>
                    <td colspan="3">Sub Total</td>
                    <td style="text-align:right;">'.number_format($subtotal).' IDR</td>
                </tr>
                <tr>
                    <td colspan="3">Jasa Kurir</td>
                    <td style="text-align:right;">'.number_format($ongkir).' IDR</td>
                </tr>
                <tr>
                    <td colspan="3">Total</td>
                    <td style="text-align:right;"><b>'.number_format($subtotal + $ongkir).' IDR</b></td>
                </tr>
            </tbody>';
}

function Payment($rekening='',$idtrans,$baseurl){
    return '<h3>Pembayaran</h3>
    Pembayaran di lakukan dengan transfer ke salah satu dari rekening berikut ini. <br>( 1x24 jam )<br> '.DaftarRekening($rekening).'<br> 
    <h3>Konfirmasi Pembayaran</h3>
    <a href="'.$baseurl.'order/konfirmasi/?tr='.$idtrans.'" style="font-size:20px;font-weight:bold;color:#ffffff;text-decoration:none;border-radius:3px;background-color:#ec7979;border-top:12px solid #ec7979;border-bottom:12px solid #ec7979;border-right:20px solid #ec7979;border-left:20px solid #ec7979;display:inline-block" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://explorer.tokopedia.com/v1/emailclick?em%3Drifzky.mail%2540gmail.com%26user_id%3DS%2527%255Cxd4%255Cxcf%255CxdfF%255Cxf63%2522%255Cx12%255Cxd1%255Cxd3%255CxbcD%255Cxb0%255Cxc8%255Cx99%255Cxf1.%255Cxbd1%255Cx17%255Cxeb%255Cx08Z%255Cx9c%255Cxbb%255Cxe8%255Cx8e%255Cx10%255Cxc8%255Cxe8%255Cxa4%255Cxdc%2527%250Ap0%250A.%26d%3DS%2527%2528%255Cxd9e%255Cxf2%255Cx15%255CxfbH%2521%255Cxf7%255D%2525%255Cx94%255Cxc4%252C%255Cxe1%255Cx9a%255Cxc4%255Cr%255Cxf9%255Cxb3I%2527%250Ap0%250A.%26ts%3D1536375055%26cid%3DS%2527%255Cxa0%252F%255Cxecu%255Cx0e%2523Dzm%255Cxac%255Cx1d%255Cxd3%255CxfeOm%253B%255Cxfb%255Cx15%255Cx92%255Cx9fk%255Cx03%255Cxef%252FV%255Cxa6sa%255Cxe5%255Cxc4%255Cxdd%255Cx90%255Cx0bb%255Cxef%255CxadL%255Cxae%255Cxb0%255Cx92%255Cxf9%255Cxa1%255Cxa6%255Cx84%255Cxb9H%255Cx1cT%255B%255Cx8f%255Cxbf%255Cxb6%2527%250Ap0%250A.%26ut%3Dl%26moeclickid%3D5b93333e7da87848b22e8659_F_T_EM_AB_0_P_0_L_0ecli39%26app_id%3DS%25274D%255Cx07%255Cxd7%2529%255Cx18%255Cxde%255Cxbf%255Cxe3%255Cxc9%255Cxcd%257B%255Cx85%255Cx8e%255Cxcb%255Cxda%255Cx06%255Cx1a%255Cxa0%255Cx9a%255Cxe0%255Cxb4%255Cxfa%257D%255Cxd4%255Cxb0S0%255Cx809%255Cx1e%255Cxf7%2527%250Ap0%250A.%26pl%3DA%26c_t%3Dge%26rlink%3Dhttps://tokopedia.link/promoNative?$deep_link%3Dtrue%2526utm_source%3D7teOvA%2526utm_medium%3Demail%2526utm_campaign%3Dco_OFS_BR_982018_Compilation%2526utm_content%3DMAINCTA&amp;source=gmail&amp;ust=1536559189325000&amp;usg=AFQjCNGY4Eyoidg713nOVcEck1K5hywUMg">UPLOAD BUKTI PEMBAYARAN</a>
    
    <br><br> Bila ada hal-hal yang ingin ditanyakan jangan sungkan untuk menghubungi kami terimakasih,';
}

function DaftarRekening($arrRekening){
    $hasil = '<ul>';
    foreach ($arrRekening as $key) {
        $hasil.='<li>'.$key.'</li>';
    }
    $hasil.='</ul>';    
    return $hasil;
}

function DeliveryDesc($tglkirim,$resi,$idtrans,$baseurl){
    return '<h3>Pengiriman</h3>
    Pengiriman dilakukan segera setelah biaya total transaksi dibayarkan.<br><br>
    Adapun jika anda ingin melihat status transaksi anda dalam sistem kami silahkan untuk klik tombol di bawah ini <br><br> <a href="'.$baseurl.'order/status-transaksi/?tr='.$idtrans.'" style="font-size:20px;font-weight:bold;color:#ffffff;text-decoration:none;border-radius:3px;background-color:#ec7979;border-top:12px solid #ec7979;border-bottom:12px solid #ec7979;border-right:20px solid #ec7979;border-left:20px solid #ec7979;display:inline-block" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://explorer.tokopedia.com/v1/emailclick?em%3Drifzky.mail%2540gmail.com%26user_id%3DS%2527%255Cxd4%255Cxcf%255CxdfF%255Cxf63%2522%255Cx12%255Cxd1%255Cxd3%255CxbcD%255Cxb0%255Cxc8%255Cx99%255Cxf1.%255Cxbd1%255Cx17%255Cxeb%255Cx08Z%255Cx9c%255Cxbb%255Cxe8%255Cx8e%255Cx10%255Cxc8%255Cxe8%255Cxa4%255Cxdc%2527%250Ap0%250A.%26d%3DS%2527%2528%255Cxd9e%255Cxf2%255Cx15%255CxfbH%2521%255Cxf7%255D%2525%255Cx94%255Cxc4%252C%255Cxe1%255Cx9a%255Cxc4%255Cr%255Cxf9%255Cxb3I%2527%250Ap0%250A.%26ts%3D1536375055%26cid%3DS%2527%255Cxa0%252F%255Cxecu%255Cx0e%2523Dzm%255Cxac%255Cx1d%255Cxd3%255CxfeOm%253B%255Cxfb%255Cx15%255Cx92%255Cx9fk%255Cx03%255Cxef%252FV%255Cxa6sa%255Cxe5%255Cxc4%255Cxdd%255Cx90%255Cx0bb%255Cxef%255CxadL%255Cxae%255Cxb0%255Cx92%255Cxf9%255Cxa1%255Cxa6%255Cx84%255Cxb9H%255Cx1cT%255B%255Cx8f%255Cxbf%255Cxb6%2527%250Ap0%250A.%26ut%3Dl%26moeclickid%3D5b93333e7da87848b22e8659_F_T_EM_AB_0_P_0_L_0ecli39%26app_id%3DS%25274D%255Cx07%255Cxd7%2529%255Cx18%255Cxde%255Cxbf%255Cxe3%255Cxc9%255Cxcd%257B%255Cx85%255Cx8e%255Cxcb%255Cxda%255Cx06%255Cx1a%255Cxa0%255Cx9a%255Cxe0%255Cxb4%255Cxfa%257D%255Cxd4%255Cxb0S0%255Cx809%255Cx1e%255Cxf7%2527%250Ap0%250A.%26pl%3DA%26c_t%3Dge%26rlink%3Dhttps://tokopedia.link/promoNative?$deep_link%3Dtrue%2526utm_source%3D7teOvA%2526utm_medium%3Demail%2526utm_campaign%3Dco_OFS_BR_982018_Compilation%2526utm_content%3DMAINCTA&amp;source=gmail&amp;ust=1536559189325000&amp;usg=AFQjCNGY4Eyoidg713nOVcEck1K5hywUMg">CEK STATUS TRANSAKSI</a>';

}

function iResi($value=''){
    if ($value=='-'||$value=='') {
        return '';
    } else {
        return ' dengan no resi <b>'.$value.'</b>';
    }
}

function FooterText($admin){
    return '<h3>Pengiriman</h3>
    Pengiriman dilakukan pada hari Jumat 22 Desember 2017 dengan no resi 12739423ASD213.<br>
    adapun jika anda ingin melihat status transaksi anda dalam sistem kami silahkan untuk mengunjungi link berikut ini.';
}


?>