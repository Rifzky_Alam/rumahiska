<!DOCTYPE html>
<html>
<head>
    <title>Rumah Iska -- <?php echo $data->judul ?></title>
    <?php include_once $data->homedir.'/view/administrasi/elements/header.php'; ?>
    <?php MetaTag() ?>
    <?php Links($data->base_url) ?>
    <?php Scripts($data->base_url) ?>
    <?php Styles() ?>

</head>
<body>
<?php

?>

<?php include_once $data->homedir.'/view/administrasi/elements/top-nav.php'; ?>
<div class="container-fluid" style="padding-top:50px;padding-bottom:50px;">
    <div class="row">
        <!--sidebar is here !!!!-->
        <?php include_once $data->homedir.'/view/administrasi/elements/sidebar.php'; ?>
        <!--sidebar ends here !!!!-->
        <div class="col-sm-9 col-md-9">
            <div class="well">
                <h1>Rumah Iska ~ Administrasi-<?php echo $data->judul ?></h1>
            </div>
            
            <div class="row">
              <div class="col-md-12">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th>Nama Barang</th>
                      <th>Jumlah Pesanan</th>
                      <th>Harga Barang</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if (count($data->listbarang)=='0'): ?>
                      <tr>
                        <td colspan="4" style="text-align:center;">Tidak Ada Barang dalam transaksi ini</td>
                      </tr>
                    <?php else: ?>
                      <?php foreach ($data->listbarang as $key): ?>
                        <tr>
                          <td><?= $key->jenis_barang.'-'.$key->detail_jenis ?></td>
                          <td><?= $key->qty ?></td>
                          <td><?= $key->th_harga ?></td>
                          <td><a href="<?= $data->base_url.$data->package.'carts.php/delete/'.$key->cart_id ?>" title="Hapus Data" class="btn btn-danger">Delete</a></td>
                        </tr>
                      <?php endforeach ?>
                    <?php endif ?>
                  </tbody>
                </table>
              </div>
            </div> 

            <form action="" method="post">
            <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                      <label>Jenis Barang</label>
                      <select class="form-control" id="jnsbrg" name="in[jenisbarang]">
                        <?php foreach ($data->jenisbarang as $key) { ?>
                            <option value="<?php echo $key->id ?>"><?php echo $key->jenis_barang ?></option>
                        <?php } ?>
                      </select>
                  </div>

                  <div class="form-group">
                      <label>Detail Barang</label>
                      <select class="form-control" id="dj" name="in[detailbarang]" required>
                        <option value="">--Tidak Ada Detail Jenis Barang--</option>
                      </select>
                  </div>

                  <div class="form-group">
                      <label>Jumlah Pesanan</label>
                      <input type="text" name="in[qty]" placeholder="Jumlah Barang (ketik dalam bentuk digit angka)" class="form-control" required>
                  </div>
                    <button class="btn btn-lg btn-primary" style="width:100%">Submit</button>
                </div>
            </div>
            </form>

        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        getdetailbarang($('#jnsbrg').val());
    });

    $('#jnsbrg').change(function(){
        getdetailbarang($('#jnsbrg').val());
    });

    function getdetailbarang(idjenis){
        $.ajax({
          type: "GET",
          url: "<?= $data->base_url ?>administrasi/transaksi/carts.php/databarang",
          data: {
            'jns': idjenis
          },
          cache: false,
          success: function(data){
            // console.log(data);
            $('#dj').html(data);
          }
      }); //end ajax
    }

</script>
</body>
</html>