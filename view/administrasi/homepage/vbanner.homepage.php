<!DOCTYPE html>
<html>
<head>
    <title><?php echo $data->judul ?></title>
    <?php include_once $data->homedir.'/view/administrasi/elements/header.php'; ?>
    <?php MetaTag() ?>
    <?php Links($data->base_url) ?>
    <?php Upload($data->base_url) ?>
    <?php Scripts($data->base_url) ?>
    <?php Styles() ?>

</head>
<body>
<?php

?>

<?php include_once $data->homedir.'/view/administrasi/elements/top-nav.php'; ?>
<div class="container-fluid" style="padding-top:50px;padding-bottom:50px;">
    <div class="row">
        <!--sidebar is here !!!!-->
        <?php include_once $data->homedir.'/view/administrasi/elements/sidebar.php'; ?>
        <!--sidebar ends here !!!!-->
        <div class="col-sm-9 col-md-9">
            <div class="well">
                <h1><?= $data->company ?> ~ Administrasi-<?php echo $data->subtitle ?></h1>
            </div>

            <div class="row">
                <form action="" method="post" enctype="multipart/form-data">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Banner No:</label>
                        <select class="form-control" name="bannerno">
                            <option value="0">1</option>
                            <option value="1">2</option>
                            <option value="2">3</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Banner Homepage File</label>
                        <input id="file-3" type="file" name="banner" class="file" />
                    </div>

                    <button class="btn btn-lg btn-primary" style="width:100%">Submit</button>
                </form>
                </div>
            </div>


        </div>
    </div>
</div>
<script src='<?php echo $data->base_url ?>assets/upload/js/fileinput.js' type="text/javascript"></script>
<script src='<?php echo $data->base_url ?>assets/upload/js/fileinput_locale_fr.js' type="text/javascript"></script>
</body>
</html>
