<!DOCTYPE html>
<html>
<head>
    <title>Administrasi -- <?php echo $data->judul ?></title>
    <?php include_once HomeDir().'/view/administrasi/elements/header.php'; ?>
    <?php MetaTag() ?>
    <?php Links($data->base_url) ?>
    <?php Scripts($data->base_url) ?>
    <?php Styles() ?>
    
</head>
<body>
<?php 

?>

<?php include_once HomeDir().'/view/administrasi/elements/top-nav.php'; ?>
<div class="container-fluid" style="padding-top:50px">
    <div class="row">
        <!--sidebar is here !!!!-->
        <?php include_once HomeDir().'/view/administrasi/elements/sidebar.php'; ?>
        <!--sidebar ends here !!!!-->
        <div class="col-sm-9 col-md-9">
            <div class="well">
                <h1>Rumah Iska ~ Administrasi-Artikel</h1>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <h2>Data Artikel</h2>
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Judul</th>
                                <th>Tags</th>
                                <th>Tanggal</th>
                                <th>Author</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php Rows($artikel,$data->base_url,$data->list) ?>
                        </tbody>
                    </table>                                            
                </div>    
            </div>


        </div>
    </div>
</div>

</body>
</html>


<?php 
function Rows($fungsi,$baseurl,$value){ ?>
   <?php if (count($value)=='0'): ?>
       <tr>
           <td colspan="5">Tidak ada data artikel dalam database rumahiska.com</td>
       </tr>
   <?php else: ?>
        <? foreach ($value as $key) { ?>
            <tr>
                <td><?php echo $key->a_judul ?></td>
                <td><?php echo $key->a_tags ?></td>
                <td><?php echo $key->a_tanggal ?></td>
                <td><?php echo $key->a_author ?></td>
                <td>
                    <a href="edit-artikel?id=<?php echo $key->a_id ?>">Edit</a>
                    ||
                    <a target="_blank" href="<?php echo $baseurl.'blogs/post?o='.$key->a_id ?>">Preview</a>
                </td>
            </tr>
        <? } ?>
   <?php endif ?>
<? } ?>