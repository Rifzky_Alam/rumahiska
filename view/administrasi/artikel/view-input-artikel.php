<!DOCTYPE html>
<html>
<head>
    <title>Administrasi -- <?php echo $data->judul ?></title>
    <?php include_once HomeDir().'/view/administrasi/elements/header.php'; ?>
    <?php MetaTag() ?>
    <?php Links($data->base_url) ?>
    <?php Scripts($data->base_url) ?>
    <?php Upload($data->base_url) ?>
    <?php Styles() ?>
    
</head>
<body>
<?php 

?>

<?php include_once HomeDir().'/view/administrasi/elements/top-nav.php'; ?>
<div class="container-fluid" style="padding-top:50px;padding-bottom:50px;">
    <div class="row">
        <!--sidebar is here !!!!-->
        <?php include_once HomeDir().'/view/administrasi/elements/sidebar.php'; ?>
        <!--sidebar ends here !!!!-->
        <div class="col-sm-9 col-md-9">
            <div class="well">
                <h1>Rumah Iska ~ Administrasi-Artikel</h1>
            </div>
            <form action="" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="col-md-12">
                    <h2>Artikel Baru</h2>
                    <div class="form-group">
                        <label>Judul Artikel</label>
                        <input type="text" name="in[judul]" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Tags</label>
                        <input type="text" name="in[tag]" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Image</label>
                        <input id="file-3" type="file" name="gambar" class="file" />
                    </div>
                    <div class="form-group">
                        <label>Content/Isi</label>
                        <textarea class="form-control" id="isi" name="in[isi]"></textarea>
                    </div>
                </div>    
            </div>
            <button class="btn btn-lg btn-primary" style="width:100%;">Submit</button>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo $data->base_url ?>library/ckeditor/ckeditor.js"></script>
<script src='<?php echo $data->base_url ?>assets/upload/js/fileinput.js' type="text/javascript"></script>
<script src='<?php echo $data->base_url ?>assets/upload/js/fileinput_locale_fr.js' type="text/javascript"></script>
    <script type="text/javascript">
        CKEDITOR.replace('isi',{
            extraPlugins: 'imageuploader'
        });
    </script>
</body>
</html>