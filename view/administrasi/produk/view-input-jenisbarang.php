<!DOCTYPE html>
<html>
<head>
    <title>Rumah Iska -- <?php echo $data->judul ?></title>
    <?php include_once HomeDir().'/view/administrasi/elements/header.php'; ?>
    <?php MetaTag() ?>
    <?php Links($data->base_url) ?>
    <?php Scripts($data->base_url) ?>
    <?php Styles() ?>
    
</head>
<body>
<?php 

?>

<?php include_once HomeDir().'/view/administrasi/elements/top-nav.php'; ?>
<div class="container-fluid" style="padding-top:50px;padding-bottom:50px;">
    <div class="row">
        <!--sidebar is here !!!!-->
        <?php include_once HomeDir().'/view/administrasi/elements/sidebar.php'; ?>
        <!--sidebar ends here !!!!-->
        <div class="col-sm-9 col-md-9">
            <div class="well">
                <h1>Rumah Iska ~ Administrasi-<?php echo $data->judul ?></h1>
            </div>
            <form action="" method="post">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Kategori</label>
                        <select name="in[kategori]" required class="form-control">
                            <?php DataSelect($data->datakategori) ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Jenis/Model Barang</label>
                        <input type="text" name="in[jenis]" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Status Jenis</label>
                        <select name="in[status]" required class="form-control">
                            <?php if (count($data->datastatus)=='0'): ?>
                                <option value="">--Tidak Ada Data--</option>
                            <?php else: ?>
                                <?php foreach ($data->datastatus as $key): ?>
                                    <option value="<?= $key->jbs_id ?>"><?= $key->jbs_ket ?></option>
                                <?php endforeach ?>
                            <?php endif ?>
                        </select>
                    </div>
                    <button class="btn btn-lg btn-primary" style="width:100%">Submit</button>
                </div>    
            </div>
            </form>

        </div>
    </div>
</div>

</body>
</html>


<?php 
function DataSelect($data){

    foreach ($data as $key) {
        echo "<option value='".$key->kb_id."'>".$key->kb_ket."</value>";    
    }
    
}

?>