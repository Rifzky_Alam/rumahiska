<!DOCTYPE html>
<html>
<head>
    <title>Rumah Iska -- <?php echo $data->judul ?></title>
    <?php include_once HomeDir().'/view/administrasi/elements/header.php'; ?>
    <?php MetaTag() ?>
    <?php Links($data->base_url) ?>
    <?php Scripts($data->base_url) ?>
    <?php Styles() ?>
    
</head>
<body>
<?php 

?>

<?php include_once HomeDir().'/view/administrasi/elements/top-nav.php'; ?>
<div class="container-fluid" style="padding-top:50px;padding-bottom:50px;">
    <div class="row">
        <!--sidebar is here !!!!-->
        <?php include_once HomeDir().'/view/administrasi/elements/sidebar.php'; ?>
        <!--sidebar ends here !!!!-->
        <div class="col-sm-9 col-md-9">
            <div class="well">
                <h1>Rumah Iska ~ Administrasi-<?php echo $data->judul ?></h1>
            </div>
            
            <div class="row">
                <div class="col-md-12">
                    <a href="<?php echo $data->base_url ?>administrasi/produk/jenis-barang?act=new" style="text-align:left;">Input Baru</a> 
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th style="text-align:center;">Nama Model/Jenis Barang</th>
                                <th style="text-align:center;">Kategori</th>
                                <th style="text-align:center;">Keterangan</th>
                                <th style="text-align:center;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php Rows($data->me,$data->list) ?>
                        </tbody>
                    </table>                    
                </div>    
            </div>


        </div>
    </div>
</div>

</body>
</html>

<?php function Rows($url,$data){ ?>
    <?php foreach ($data as $key) { ?>
        <tr>
            <td><?php echo $key->jenis_barang ?></td>
            <td style="text-align:center;"><?php echo $key->kb_ket ?></td>
            <td style="text-align:center;"><?= $key->jbs_ket ?></td>
            <td style="text-align:center;">
                <a href="<?php echo $url.'?edt='.$key->id ?>">Edit</a>
                ||
                <a href="<?php echo 'barang?act=new' ?>">Add Item</a>
            </td>
        </tr>        
    <?php } ?>
<?php } ?>