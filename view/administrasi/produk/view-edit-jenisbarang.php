<!DOCTYPE html>
<html>
<head>
    <title>Rumah Iska -- <?php echo $data->judul ?></title>
    <?php include_once HomeDir().'/view/administrasi/elements/header.php'; ?>
    <?php MetaTag() ?>
    <?php Links($data->base_url) ?>
    <?php Scripts($data->base_url) ?>
    <?php Styles() ?>
    
</head>
<body>
<?php 

?>

<?php include_once HomeDir().'/view/administrasi/elements/top-nav.php'; ?>
<div class="container-fluid" style="padding-top:50px;padding-bottom:50px;">
    <div class="row">
        <!--sidebar is here !!!!-->
        <?php include_once HomeDir().'/view/administrasi/elements/sidebar.php'; ?>
        <!--sidebar ends here !!!!-->
        <div class="col-sm-9 col-md-9">
            <div class="well">
                <h1>Rumah Iska ~ Administrasi-<?php echo $data->judul ?></h1>
            </div>
            <form action="" method="post">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Kategori</label>
                        <select name="in[kategori]" required class="form-control">
                            <?php DataSelect($data->kategori,$data->datakategori) ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Jenis/Model Barang</label>
                        <input type="text" name="in[jenis]" class="form-control" required value="<?php echo $data->jenisbarang ?>">
                    </div>

                    <div class="form-group">
                        <label>Status Jenis Barang</label>
                        <select name="in[status]" class="form-control" required>
                            <?php if (count($data->datastatusjb)=='0'): ?>
                                <option value="" selected="selected">Tidak Ada Data</option>
                            <?php else: ?>
                                <?php foreach ($data->datastatusjb as $key): ?>
                                    <?php if ($key->jbs_id==$data->statusjb): ?>
                                    <option value="<?= $key->jbs_id ?>" selected><?= $key->jbs_ket ?></option>    
                                        <?php else: ?>
                                    <option value="<?= $key->jbs_id ?>"><?= $key->jbs_ket ?></option>        
                                    <?php endif ?>
                                <?php endforeach ?>
                            <?php endif ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Setelan Baku Display</label>
                        <select name="in[defitem]" class="form-control">
                            <?php if (count($data->listbarang)=='0'): ?>
                                <option value="" selected="selected">Tidak ada detail item</option>
                            <?php else: ?>
                                <option value="" selected="selected">--Pilih Setelan Baku--</option>
                                <?php foreach ($data->listbarang as $key): ?>
                                    <?php if ($data->defitem==$key->id): ?>
                                        <option value="<?= $key->id ?>" selected="selected"><?= $key->detail_jenis ?></option>
                                    <?php else: ?>
                                        <option value="<?= $key->id ?>"><?= $key->detail_jenis ?></option>
                                    <?php endif ?>
                                    
                                <?php endforeach ?>
                            <?php endif ?>
                        </select>
                    </div>
                    <button class="btn btn-lg btn-primary" style="width:100%">Submit</button>
                </div>    
            </div>
            </form>

        </div>
    </div>
</div>

</body>
</html>


<?php 
function DataSelect($kategori,$data){

    foreach ($data as $key) {
            
        if ($kategori==$key->kb_id) {
            echo "<option value='".$key->kb_id."' selected>".$key->kb_ket."</value>";    
        } else {
            echo "<option value='".$key->kb_id."'>".$key->kb_ket."</value>";
        }
        

    }
    
}

?>