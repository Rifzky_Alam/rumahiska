<!DOCTYPE html>
<html>
<head>
    <title>Rumah Iska -- <?php echo $data->judul ?></title>
    <?php include_once HomeDir().'/view/administrasi/elements/header.php'; ?>
    <?php MetaTag() ?>
    <?php Links($data->base_url) ?>
    <?php Scripts($data->base_url) ?>
    <?php Styles() ?>

</head>
<body>
<?php

?>

<?php include_once HomeDir().'/view/administrasi/elements/top-nav.php'; ?>
<div class="container-fluid" style="padding-top:50px;padding-bottom:50px;">
    <div class="row">
        <!--sidebar is here !!!!-->
        <?php include_once HomeDir().'/view/administrasi/elements/sidebar.php'; ?>
        <!--sidebar ends here !!!!-->
        <div class="col-sm-9 col-md-9">
            <div class="well">
                <h1>Rumah Iska ~ Administrasi-<?php echo $data->judul ?></h1>
            </div>
            <form action="" method="post">
            <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                      <label>Jenis Barang</label>
                      <select class="form-control" name="in[jenisbarang]">
                        <?php JenisBarang($data->jenisbarang) ?>
                      </select>
                  </div>

                    <div class="form-group">
                        <label>Nama Barang</label>
                        <input class="form-control" type="text" name="in[namabarang]" placeholder="Nama Barang" required></input>
                    </div>

                    <div class="form-group">
                        <label>Berat (Gram)</label>
                        <input class="form-control" value="0" type="number" name="in[berat]" placeholder="hanya angka"/>
                    </div>

                    <div class="form-group">
                        <label>Jumlah Barang</label>
                        <input type="text" name="in[jumlahbarang]" type="number" class="form-control" placeholder="Jumlah (dalam bentuk angka)">
                    </div>

                    <div class="form-group">
                        <label>Satuan</label>
                        <select id='satuan' name="in[satuan]" class='form-control'>
                            <?php SatuanBarang($data->barangsatuan); ?>
                        </select>
                    </div>

                    <button class="btn btn-lg btn-primary" style="width:100%">Submit</button>
                </div>
            </div>
            </form>

        </div>
    </div>
</div>

</body>
</html>

<?php function JenisBarang($data){ ?>
    <?php foreach ($data as $key) { ?>
        <option value="<?php echo $key->id ?>"><?php echo $key->jenis_barang ?></option>
    <?php } ?>
<?php } ?>
<?php function SatuanBarang($data){ ?>
    <?php foreach ($data as $key) { ?>
        <option value="<?php echo $key->bs_id ?>"><?php echo $key->bs_ket ?></option>
    <?php } ?>
<?php } ?>
