<!DOCTYPE html>
<html>
<head>
    <title>Rumah Iska -- <?php echo $data->judul ?></title>
    <?php include_once HomeDir().'/view/administrasi/elements/header.php'; ?>
    <?php MetaTag() ?>
    <?php Links($data->base_url) ?>
    <?php Scripts($data->base_url) ?>
    <?php Styles() ?>

</head>
<body>
<?php

?>

<?php include_once HomeDir().'/view/administrasi/elements/top-nav.php'; ?>
<div class="container-fluid" style="padding-top:50px;padding-bottom:50px;">
    <div class="row">
        <!--sidebar is here !!!!-->
        <?php include_once HomeDir().'/view/administrasi/elements/sidebar.php'; ?>
        <!--sidebar ends here !!!!-->
        <div class="col-sm-9 col-md-9">
            <div class="well">
                <h1>Rumah Iska ~ Administrasi-<?php echo $data->judul ?></h1>
            </div>

            <div class="row">
                <div class="col-md-12">
                  <form action="" method="post">
                  <div class="form-group">
                      <label>Jenis Barang</label>
                      <select class="form-control" name="in[jenisbarang]">
                        <?php JenisBarang($data->idjenis,$data->listjenisbarang) ?>
                      </select>
                  </div>

                    <div class="form-group">
                        <label>Nama Barang</label>
                        <input value="<?php echo $data->namabarang ?>" class="form-control" type="text" name="in[namabarang]" placeholder="Nama Barang" required></input>
                    </div>

                    <div class="form-group">
                        <label><a href="<?= $data->me_url.'?prc='.$data->id_barang ?>" title="">Harga Non Member</a></label>
                        <select name="in[harga]" class="form-control">
                          <?php DaftarHarga($data->harga,$data->listharga) ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label><a href="<?= $data->me_url.'?prc='.$data->id_barang ?>" title="">Harga Agen</a></label>
                        <select name="in[hargaagen]" class="form-control">
                          <?php DaftarHarga($data->hargaagen,$data->listharga) ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Berat Barang (Gram)</label>
                        <input value="<?php echo $data->berat ?>" type="text" name="in[berat]" class="form-control" placeholder="berat barang (dalam bentuk angka)">
                    </div>

                    <div class="form-group">
                        <label>Jumlah Barang</label>
                        <input value="<?php echo $data->jumlah ?>" type="text" name="in[jumlah]" class="form-control" placeholder="Jumlah (dalam bentuk angka)">
                    </div>

                    <div class="form-group">
                        <label>Satuan</label>
                        <select id='satuan' name="in[satuan]" class='form-control'>
                            <?php SatuanBarang($data->satuan,$data->listsatuan) ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Status</label>
                        <select id='status' name="in[status]" class='form-control'>
                            <?php StatusBarang($data->status,$data->liststatus) ?>
                        </select>
                    </div>
                    <button class="btn btn-lg btn-primary" style="width:100%">Submit</button>
                    </form>
                </div>
            </div>


        </div>
    </div>
</div>

</body>
</html>

<?php function JenisBarang($jenis,$data){ ?>
    <?php foreach ($data as $key) { ?>
      <?php if ($key->id==$jenis): ?>
        <option value="<?php echo $key->id ?>" selected><?php echo $key->jenis_barang ?></option>
      <?php else: ?>
        <option value="<?php echo $key->id ?>"><?php echo $key->jenis_barang ?></option>
      <?php endif; ?>

    <?php } ?>
<?php } ?>
<?php function SatuanBarang($satuan,$data){ ?>
    <?php foreach ($data as $key) { ?>
      <?php if ($satuan==$key->bs_id): ?>
          <option value="<?php echo $key->bs_id ?>" selected><?php echo $key->bs_ket ?></option>
        <?php else: ?>
          <option value="<?php echo $key->bs_id ?>"><?php echo $key->bs_ket ?></option>
      <?php endif; ?>

    <?php } ?>
<?php } ?>
<?php function StatusBarang($status,$data){ ?>
    <?php foreach ($data as $key) { ?>
      <?php if ($status==$key->bsk_id): ?>
          <option value="<?php echo $key->bsk_id ?>" selected><?php echo $key->bsk_ket ?></option>
        <?php else: ?>
          <option value="<?php echo $key->bsk_id ?>"><?php echo $key->bsk_ket ?></option>
      <?php endif; ?>

    <?php } ?>
<?php } ?>

<?php function DaftarHarga($default,$data){ ?>
    <?php if (count($data)=='0'): ?>
        <option value="">--Belum ada data, harap input harga dulu--</option>
      <?php else: ?>
        <option value="">--Pilih Harga--</option>
      <?php foreach ($data as $key): ?>
        <?php if ($default==$key->th_id): ?>
          <option value="<?= $key->th_id ?>" selected="selected"><?= $key->th_harga ?></option>
          <?php else: ?>
          <option value="<?= $key->th_id ?>"><?= $key->th_harga ?></option>  
        <?php endif ?>
      <?php endforeach ?>
    <?php endif ?>
<?php } ?>
