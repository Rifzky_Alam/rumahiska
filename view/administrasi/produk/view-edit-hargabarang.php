<!DOCTYPE html>
<html>
<head>
    <title>Rumah Iska -- <?php echo $data->judul ?></title>
    <?php include_once HomeDir().'/view/administrasi/elements/header.php'; ?>
    <?php MetaTag() ?>
    <?php Links($data->base_url) ?>
    <?php Scripts($data->base_url) ?>
    <?php Styles() ?>

</head>
<body>
<?php

?>

<?php include_once HomeDir().'/view/administrasi/elements/top-nav.php'; ?>
<div class="container-fluid" style="padding-top:50px;padding-bottom:50px;">
    <div class="row">
        <!--sidebar is here !!!!-->
        <?php include_once HomeDir().'/view/administrasi/elements/sidebar.php'; ?>
        <!--sidebar ends here !!!!-->
        <div class="col-sm-9 col-md-9">
            <div class="well">
                <h1>Rumah Iska ~ Administrasi-<?php echo $data->judul ?></h1>
            </div>

            <div class="row">
              <div class="col-md-12">
                <table class="table table-bordered">
                  <caption style="text-align:center;"><h4>Tabel Histori Harga</h4></caption>
                  <thead>
                    <tr>
                      <th style="text-align:center;">Harga</th>
                      <th style="text-align:center;">Deskripsi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php Rows($data->listharga) ?>
                  </tbody>
                </table>
              </div>
              
            </div>
            <hr>
            <div class="row">
                <div class="col-md-12">
                  <form action="" method="post">
                  
                    <div class="form-group">
                        <label>Harga Barang Baru</label>
                        <input class="form-control" type="number" name="in[harga]" value="0" placeholder="Hanya Angka" required></input>
                    </div>

                    <div class="form-group">
                        <label>Deskripsi Harga</label>
                        <textarea name="in[desc]" class="form-control"></textarea>
                    </div>

                    <button class="btn btn-lg btn-primary" style="width:100%">Submit</button>
                    </form>
                </div>
            </div>


        </div>
    </div>
</div>

</body>
</html>

<?php function Rows($data){ ?>
    <?php if (count($data)=='0'): ?>
        <tr>
          <td colspan="2" style="text-align:center;">Tidak ada histori harga barang ini.</td>
        </tr>
      <?php else: ?>
      <?php foreach ($data as $key): ?>
        <tr>
          <td style="text-align:center;"><?= number_format($key->th_harga) ?></td>
          <td style="text-align:center;"><?= @$key->th_desc ?></td>
        </tr>
      <?php endforeach ?>
    <?php endif ?>
<?php } ?>
