<?php function ModalDeleteGambar(){ ?>
<!-- Modal -->
  <div class='modal fade' id='modal-delete' role='dialog'>
    <div class='modal-dialog'>
    
      <!-- Modal content-->
      <div class='modal-content'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'>&times;</button>
          <h3 class="modal-title">Kamu yakin ingin hapus foto ini?</h3>

        </div>
        <div class='modal-body'>
        <div style='padding-left:15px;padding-right:15px;padding-top:0px; height: 300px;overflow-y:auto;' class='row'>
          
          <div class="col-md-12">
            <form action="" method="post">
            <center>
                <img src="" id="mdlimg" alt="modalFoto" style="width:175px;height:200px">
            </center>
            <br><br>

            <input type="text" name="delimg" id="txtdelimg" style="display:none;">
            <div class="row">
                <button class="btn btn-lg btn-danger" style="width:100%">Hapus</button>
            </div> 
            </form>
          </div>
             
          

        </div>
          

        </div>
        <div class='modal-footer'>
            <div class="row">
                <div class="col-md-6" style="text-align:left">
                    <span>Rumahiska 2017</span>
                </div>
                <div class="col-md-6">
                    <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                </div>
            </div>
          
        </div>
      </div>
      
    </div>
  </div>
  <!-- end modal -->
<?php } ?>

<?php function ModalEditGambarKeterangan(){ ?>
<!-- Modal -->
  <div class='modal fade' id='modal-editket' role='dialog'>
    <div class='modal-dialog'>
    
      <!-- Modal content-->
      <div class='modal-content'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'>&times;</button>
          <h3 class="modal-title">Edit Keterangan Gambar.</h3>

        </div>
        <div class='modal-body'>
        <div style='padding-left:15px;padding-right:15px;padding-top:0px; height: 300px;overflow-y:auto;' class='row'>
          
          <div class="col-md-12">
            <form action="" method="post">
              <div class="form-group">
                <label for="edtket">Keterangan</label>
                <input type="text" name="edtimg[idimg]" id="idedtimg" style="display:none;">
                <textarea class="form-control" id="edtketimg" name="edtimg[ketimg]"></textarea>
              </div>

            <div class="row">
                <button class="btn btn-lg btn-danger" style="width:100%">Edit</button>
            </div> 
            </form>
          </div>
             
          

        </div>
          

        </div>
        <div class='modal-footer'>
            <div class="row">
                <div class="col-md-6" style="text-align:left">
                    <span>Rumahiska 2017</span>
                </div>
                <div class="col-md-6">
                    <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                </div>
            </div>
          
        </div>
      </div>
      
    </div>
  </div>
  <!-- end modal -->
<?php } ?>