<!DOCTYPE html>
<html>
<head>
    <title><?php echo $data->company.' | '.$data->judul ?></title>
    <?php include_once HomeDir().'/view/administrasi/elements/header.php'; ?>
    <?php MetaTag() ?>
    <?php Links($data->base_url) ?>
    <?php Scripts($data->base_url) ?>
    <?php Styles() ?>

</head>
<body>
<?php

?>

<?php include_once HomeDir().'/view/administrasi/elements/top-nav.php'; ?>
<div class="container-fluid" style="padding-top:50px;padding-bottom:50px;">
    <div class="row">
        <!--sidebar is here !!!!-->
        <?php include_once HomeDir().'/view/administrasi/elements/sidebar.php'; ?>
        <!--sidebar ends here !!!!-->
        <div class="col-sm-9 col-md-9">
            <div class="well">
                <h1><?= $data->company ?> ~ Administrasi-<?php echo $data->judul ?></h1>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <a href="<?php echo $data->base_url.$data->package ?>/produk/barang?act=new" style="text-align:left;">Input Baru</a>
                    ||
                    <a id="cari-data" href="#modal-cari" data-toggle="modal">Search <i class="glyphicon glyphicon-search"></i></a>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th style="text-align:center;">Kategori</th>
                                <th style="text-align:center;">Model/Jenis Barang</th>
                                <th style="text-align:center;">Nama Barang</th>
                                <th style="text-align:center;">Harga</th>
                                <th style="text-align:center;">Satuan</th>
                                <th style="text-align:center;">Jumlah</th>
                                <th style="text-align:center;">Photos</th>
                                <th style="text-align:center;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php Rows($data->me,$data->list) ?>
                        </tbody>
                    </table>
                </div>
            </div>


        </div>
    </div>
</div>


<!-- Modal -->
  <div class='modal fade' id='modal-cari' role='dialog'>
    <div class='modal-dialog'>

      <!-- Modal content-->
      <div class='modal-content'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'>&times;</button>
          <h3 class="modal-title">Pencarian Data</h3>

        </div>
        <div class='modal-body'>
        <div style='padding-left:15px;padding-right:15px;padding-top:0px; height: 300px;' class='row'>

          <div class="col-md-12">


                <form action="" method="GET">
                  <div class="row">
                      <div class="form-group">
                        <label>Kategori Barang</label>
                          <select class='form-control' name='src[ktg]'>
                              <option value="">-- Pilih Kategori barang --</option>
                              <?php KategoriBarang($data->kategori) ?>
                          </select>
                      </div>
                  </div>

                    <div class="row">

                        <div class="form-group">
                        	<label>Jenis Barang</label>
                            <select class='form-control' name='src[jns]'>
                                <option value="">-- Pilih jenis barang --</option>
                                <?php JenisBarang($data->jenisbarang) ?>
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <label for="namaBarang">Nama / Warna Barang</label>
                            <input type="text" name="src[nb]" id="namaBarang" class="form-control" />
                        </div>
                    </div>

                    <div class="row">
                        <button class="btn btn-success" style="width:100%">Cari</button>
                    </div>

                </form>
          </div>



        </div>


        </div>
        <div class='modal-footer'>
            <div class="row">
                <div class="col-md-6" style="text-align:left">
                    <span><?= $data->company ?> 2018</span>
                </div>
                <div class="col-md-6">
                    <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                </div>
            </div>

        </div>
      </div>

    </div>
  </div>
  <!-- end modal -->

</body>
</html>

<?php function Rows($url,$data){ ?>
    <?php foreach ($data as $key) { ?>
      <tr>
          <td style="text-align:center;"><?php echo $key->kb_ket ?></td>
          <td style="text-align:center;"><?php echo $key->jenis_barang ?></td>
          <td style="text-align:center;"><?php echo $key->detail_jenis ?></td>
          <td style="text-align:center;"><?php echo number_format($key->brg_harga) ?> IDR</td>
          <td style="text-align:center;"><?php echo $key->brg_satuan ?></td>
          <td style="text-align:center;"><?php echo $key->jumlah_barang ?></td>
          <td style="text-align:center;">0</td>
          <td>
              <a href="barang?edt=<?php echo $key->brg_id ?>">Edit</a>
              ||
              <a href="barang-gallery?idb=<?= $key->brg_id ?>">Gallery</a>
          </td>

      </tr>
    <?php } ?>
<?php } ?>

<?php function KategoriBarang($data){ ?>
    <?php foreach ($data as $key) { ?>
        <option value="<?php echo $key->kb_id ?>"><?php echo $key->kb_ket ?></option>
    <?php } ?>
<?php } ?>

<?php function JenisBarang($data){ ?>
    <?php foreach ($data as $key) { ?>
        <option value="<?php echo $key->id ?>"><?php echo $key->jenis_barang ?></option>
    <?php } ?>
<?php } ?>
