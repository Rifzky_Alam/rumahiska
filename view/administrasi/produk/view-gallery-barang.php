<!DOCTYPE html>
<html>
<head>
    <?php include_once HomeDir().'/view/administrasi/elements/header.php'; ?>
    <?php MetaTag() ?>
    <?php Links($data->base_url) ?>
    <?php Scripts($data->base_url) ?>
    <?php Styles() ?>
    <style type="text/css" media="screen">
        .hide-bullets {
            list-style:none;
            margin-left: -40px;
            margin-top:20px;
        }

        .thumbnail {
            padding: 0;
        }

        .carousel-inner>.item>img, .carousel-inner>.item>a>img {
            width: 100%;
        }
    </style>
</head>
<body>
<?php include_once HomeDir().'/view/administrasi/elements/top-nav.php'; ?>
<div class="container" style="padding-bottom:40px;">
    <h2>Gallery Produk </h2>

    <div class="row">
        <div class="col-md-6">
            Upload gambar dengan minimal lebar kesamping 350 pixel dan panjang keatas minimal 400 pixel, hindari pemakaian special character (!,@,#,$,%,^,&,*,(,)) pada namafile anda.<br>Contoh: <del>r!fz|&lty@|_@M.jpg</del> <i class="glyphicon glyphicon-remove"></i> || RifzkyAlam.jpg <i class="glyphicon glyphicon-ok"></i>
            <br><br>
            <form action="" accept-charset="utf-8" enctype="multipart/form-data" method="post">
                <div class="form-group">
                    <input type="file" name="gambar">
                </div>
                <div class="form-group">
                    <label for="ket">Keterangan foto</label>
                    <textarea name="in[ket]" class="form-control" placeholder="Tulis keterangan foto, cth: hijab tampak depan, samping dll."></textarea>
                </div>
                <button style="width:50%" class="btn btn-lg btn-primary">Upload</button>
            </form>
        </div>
        <div class="col-md-6">
            <table class="table table-bordered">
                <caption>Data Produk/Barang</caption>
                <thead>
                    <tr>
                        <th>Nama Barang</th>
                        <th>Jenis</th>
                        <th>Kategori</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><?= $data->namabarang ?></td>
                        <td><?= $data->jenisbarang ?></td>
                        <td><?= $data->kategori ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>  
    <?php Foto($data->pathfolder,$data->foto) ?>
    <div class="row">
        <div class="col-md-12">
            <a href="barang?src[ktg]=&src[jns]=<?= $data->idjenis ?>&src[nb]=" class="btn btn-lg btn-warning" title="Kembali ke data barang"><< Kembali ke data barang</a>
        </div>
    </div>
</div>
    <?php ModalDeleteGambar() ?>
    <?php ModalEditGambarKeterangan() ?>

<script type="text/javascript">
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover(); 
});

$('.gbr').on('click', function() {
    var defimg = <?= $data->defaultgbr ?>;
    $('#prev').prop('src', this.src);
    $('#mdlimg').prop('src', this.src);
    $('#txtdelimg').val(this.id);
    $('#txtmkdef').val(this.id);
    $('#idedtimg').val(this.id);
    $('#img-ket').html(this.title);
    $('#edtketimg').html(this.title);
    if (defimg==this.id) {
        $('#makedefault').attr("disabled", true);
        $('#makedefault').html('Gambar default barang');
    } else {
        $('#makedefault').attr("disabled", false);
        $('#makedefault').html('Save as default img!');
    }


});

</script>               
</body>
</html>

<?php function Foto($pathfolder,$data){ ?>
<?php if (count($data)=='0'): ?>
    <div id="main_area" style="margin-top:15px;">
        <div class="row">
            <div class="col-sm-6" id="slider-thumbs">
                <div class="jumbotron">
                    <h3>Tidak ada data gambar untuk item barang ini ...</h3>
                    <p></p>
                </div>
            </div>
        </div>
    </div>
    <?php else: ?>
        <div id="main_area">
        <div class="row">
            <div class="col-sm-6" id="slider-thumbs">
                <ul class="hide-bullets">
        <?php foreach ($data as $key): ?>
            
                    <li class="col-sm-3">
                        <a title="Klik untuk detail" class="thumbnail" id="carousel-selector-0">
                            <img id="<?= $key->bg_id ?>" class="gbr" title="<?= $key->bg_ket ?>" src="<?= $pathfolder.$key->bg_name ?>">
                        </a>
                    </li>

        <?php endforeach ?>
                </ul>
            </div>

            <div class="col-sm-6">
                <div class="col-xs-12">
                <center>
                    <h3>Preview Gambar</h3>
                    <img src="<?= $pathfolder ?>" id="prev" alt="gambar barang" style="width:350px;height:400px;">
                    <br><br>
                    <div class="row">
                        <section>
                            <h5>Keterangan <a data-toggle="modal" href="#modal-editket" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></h5>
                            <p id="img-ket"></p>
                        </section>
                    </div>
                    <div class="row">
                        <form action="" method="post">
                            <input type="text" name="mkdef" id="txtmkdef" style="display:none;"><br>
                            <button class="btn btn-lg btn-primary" id="makedefault">Tetapkan gambar utama</button>
                            <a data-toggle="modal" href="#modal-delete" class="btn btn-lg btn-danger" id="makedefault">Hapus</a>
                        </form>
                        
                        
                    </div>
                </center>
                </div>
            </div>

        </div>
        </div>
<?php endif ?>

<? } ?>


