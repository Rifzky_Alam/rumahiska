<!DOCTYPE html>
<html>
<head>
    <title>Rumah Iska -- <?php echo $data->judul ?></title>
    <?php include_once HomeDir().'/view/administrasi/elements/header.php'; ?>
    <?php MetaTag() ?>
    <?php Links($data->base_url) ?>
    <?php Scripts($data->base_url) ?>
    <?php Styles() ?>

</head>
<body>
<?php

?>

<?php include_once HomeDir().'/view/administrasi/elements/top-nav.php'; ?>
<div class="container-fluid" style="padding-top:50px;padding-bottom:50px;">
    <div class="row">
        <!--sidebar is here !!!!-->
        <?php include_once HomeDir().'/view/administrasi/elements/sidebar.php'; ?>
        <!--sidebar ends here !!!!-->
        <div class="col-sm-9 col-md-9">
            <div class="well">
                <h1>Rumah Iska ~ Administrasi-<?php echo $data->judul ?></h1>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Nama Barang</label>
                        <input class="form-control" type="text" name="in[namabarang]" placeholder="Nama Barang" required></input>
                    </div>

                    <div class="form-group">
                        <label>Harga</label>
                        <input class="form-control" name="in[alamat]"/>
                    </div>

                    <div class="form-group">
                        <label>Jumlah Barang</label>
                        <input type="text" name="in[kota]" class="form-control" placeholder="Jumlah (dalam bentuk angka)">
                    </div>

                    <div class="form-group">
                        <label>Satuan</label>
                        <select id='provinsi' name="in[provinsi]" class='form-control'>
                            <option value=''>--Pilih Satuan Barang--</option>
                        </select>
                    </div>

                    <button class="btn btn-lg btn-primary" style="width:100%">Submit</button>
                </div>
            </div>


        </div>
    </div>
</div>

</body>
</html>
