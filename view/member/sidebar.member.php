<!-- Sidebar -->
        <nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation">
            <ul class="nav sidebar-nav">
                <li class="sidebar-brand">
                    <a href="#">
                       SALVINA.ID
                    </a>
                </li>
                <li>
                    <a href="<?= $data->base_url.'member/dasbor' ?>">Beranda</a>
                </li>
                <li>
                    <a href="<?= $data->base_url.'member/historitransaksi' ?>">Riwayat Transaksi</a>
                </li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Profil <span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu">
                    <li class="dropdown-header">-----------------</li>
                    <li><a href="<?= $data->base_url.'member/tambahalamat' ?>">Tambah Alamat</a></li>
                    <li><a href="<?= $data->base_url.'member/ubahpassword' ?>">Ubah Password</a></li>
                  </ul>
                </li>
                <li>
                    <a href="<?= $data->base_url ?>member/logout">Keluar / Log Out</a>
                </li>
                <li>
                    <a href="<?= $data->base_url ?>company/kontak">Contact Us</a>
                </li>
            </ul>
        </nav>
        <!-- /#sidebar-wrapper