<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="<?= 'http://'.$_SERVER['HTTP_HOST'].'/rikza/assets/bootstrap4/css/bootstrap.min.css' ?>">
	<title>Member | Pembayaran</title>
</head>
<body>

	<?php include_once 'vtopnav.member.php'; ?>

	<div class="container" style="padding-top: 40px;">

		<div class="row">
			<div class="col-md-12">
				<div class="page-header">
					<h2>Data Transaksi</h2>
				</div>
			</div>
		</div>

		<div class="row" style="overflow: auto;">
			<div class="col-md-12">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th class="text-center">No Transaksi</th>
							<th class="text-center">Tanggal</th>
							<th class="text-center">Item</th>
							<th class="text-center">Total</th>
							<th class="text-center">Status</th>
							<th class="text-center">Aksi</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<!-- <td colspan="4" style="text-align:center;">Tidak ada data.</td> -->
							<td style="vertical-align:middle;">TR--001</td>
							<td style="vertical-align:middle;"><?= date('Y-m-d') ?></td>
							<td style="vertical-align:middle;">
								<ul>
									<li>Jilbab Rasya Maroon</li>
									<li>Jilbab Hayra</li>
								</ul>
							</td>
							<td style="vertical-align:middle;"><?= number_format(200000) ?> IDR</td>
							<td style="vertical-align:middle;">Belum upload bukti</td>
							<td style="vertical-align:middle;">
								<a href="#" title="Upload bukti" class="btn btn-warning">Upload bukti</a>
								<a href="#" title="Tanya support" class="btn btn-success">Chat Admin</a>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<script src="<?= 'http://'.$_SERVER['HTTP_HOST'].'/rikza/assets/common/jquery3/jquery-3.3.1.min.js' ?>"></script>
	<script src="<?= 'http://'.$_SERVER['HTTP_HOST'].'/rikza/assets/bootstrap4/js/bootstrap.min.js' ?>"></script>
</body>
</html>