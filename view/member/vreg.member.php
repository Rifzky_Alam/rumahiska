<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="<?= $data->base_url.'assets/common/bootstrap/css/bootstrap.min.css' ?>">
	<title>Member | Registrasi</title>
</head>
<body>
	<div class="container" style="padding-top: 40px;">
		<?php if ($data->validation=='0'): ?>
			<script>alert('Kombinasi Password Tidak Sama');</script>
		<?php endif ?>
		<div class="row">
			<div class="col-md-12">
				<div class="page-header">
					<h2>Registrasi</h2>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<form action="" method="post" accept-charset="utf-8">
				<div class="form-group">
					<label>Nama Lengkap</label>
					<input type="text" name="in[nama]" class="form-control" placeholder="Nama Lengkap" value="<?= $data->namalengkap ?>" required>
				</div>

				<div class="form-group">
					<label>No Telepon</label>
					<input value="<?= $data->telepon ?>" name="in[telepon]" type="text" class="form-control" value="" placeholder="No Telepon Aktif" required>
				</div>

				<div class="form-group">
					<label>Email</label>
					<input value="<?= $data->email ?>" name="in[email]" type="email" class="form-control" value="" placeholder="email aktif" required>
				</div>

				<div class="form-group">
					<label>Password</label>
					<input type="password" name="in[password]" class="form-control" placeholder="Password" required>
				</div>

				<div class="form-group">
					<label>Masukkan Password Sekali Lagi</label>
					<input type="password" name="in[passwordconfirm]" class="form-control" placeholder="Konfirmasi Password" required>
				</div>
				<button class="btn btn-lg btn-info" style="width:100%">Submit</button>
				</form>
			</div>
		</div>
	</div>
	<script src="<?= $data->base_url.'rikza/assets/common/bootstrap/js/jquery.js' ?>"></script>
	<script src="<?= $data->base_url.'rikza/assets/common/bootstrap/js/bootstrap.min.js' ?>"></script>
</body>
</html>