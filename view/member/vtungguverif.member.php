<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="<?= $data->base_url.'assets/common/bootstrap/css/bootstrap.min.css' ?>">
	<title>Member | Verifikasi Email</title>
</head>
<body>
	<div class="container" style="padding-top: 40px;">

		<div class="row">
			<div class="col-md-12">
				<div class="page-header">
					<h2>Verifikasi Email</h2>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="jumbotron">
					<p>
						Kami telah mengirimkan email untuk verifikasi akun anda, silahkan untuk membuka email dan masukan kode validasi yang tertera di email anda di kolom di bawah ini.
						<br><br>
						Apabila anda tidak menerima email verifikasi, kirim ulang dengan klik <a href="<?= $data->base_url.'member/sendemail?v='.$data->id ?>" class="btn btn-primary">tombol ini</a>
					</p>
				</div>
				<form action="" method="POST" accept-charset="utf-8">
					<div class="form-group">
						<label for="validasitxt">Kode Validasi</label>
						<input id="validasitxt" type="text" name="in[kode]" class="form-control" placeholder="Kode Validasi">				
					</div>
					<button class="btn btn-primary">Submit</button>
				</form>
			</div>
		</div>
	</div>
	<script src="<?= $data->base_url.'assets/common/bootstrap/js/jquery.js' ?>"></script>
	<script src="<?= $data->base_url.'assets/common/bootstrap/js/bootstrap.min.js' ?>"></script>
</body>
</html>