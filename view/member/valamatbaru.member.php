<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Member | Tambah Alamat</title>
</head>
<body>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<link rel="stylesheet" type="text/css" href="<?= $data->base_url ?>assets/member/sdasbor.css">
<script src="<?= $data->base_url ?>assets/common/jquery3/jquery-3.3.1.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

<!------ Include the above in your HEAD tag ---------->

    <div id="wrapper">
        <div class="overlay"></div>
    
        <?php include_once $data->homedir.'view/member/sidebar.member.php' ?>

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <button type="button" class="hamburger is-closed" data-toggle="offcanvas">
                <span class="hamb-top"></span>
                <span class="hamb-middle"></span>
                <span class="hamb-bottom"></span>
            </button>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="page-header">
                            <h2>Tambah Alamat/AlamatPengiriman Baru | Salvina </h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <form action="" method="post">
                        <div class="form-group">
							<label>Provinsi</label>
							<select name="in[prov]" id="prv" class="form-control">
								
							</select>
						</div>

						<div class="form-group">
							<label>Kota/Kabupaten</label>
							<select name="in[kota]" id="cty" class="form-control">
								<option value="">--Pilih Kota/Kab--</option>
							</select>
						</div>

						<div class="form-group">
							<label>Kecamatan</label>
							<select name="in[kec]" id="sbd" class="form-control">
								<option value="">--Pilih Kecamatan--</option>
							</select>
						</div>

						<div class="form-group">
							<label>Alamat Pengiriman</label>
							<textarea name="in[alamat]" class="form-control" placeholder="No Rumah, RT/RW dan Kelurahan"></textarea>
						</div>

						<div class="form-group">
							<label>Kode Pos</label>
							<input type="text" name="in[kodepos]" class="form-control">
						</div>
						<input type="hidden" name="in[namaprov]" id="namaprov" >
						<input type="hidden" name="in[namakota]" id="namakota" >
						<input type="hidden" name="in[namakec]" id="namakec" >
                        <button class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->
    <script>
        $(document).ready(function () {
          var trigger = $('.hamburger'),
              overlay = $('.overlay'),
             isClosed = false;

            trigger.click(function () {
              hamburger_cross();      
            });

            function hamburger_cross() {

              if (isClosed == true) {          
                overlay.hide();
                trigger.removeClass('is-open');
                trigger.addClass('is-closed');
                isClosed = false;
              } else {   
                overlay.show();
                trigger.removeClass('is-closed');
                trigger.addClass('is-open');
                isClosed = true;
              }
          }
          
          $('[data-toggle="offcanvas"]').click(function () {
                $('#wrapper').toggleClass('toggled');
          }); 
          LoadProv('9');
	        $("form").bind("keypress", function (e) {
	            if (e.keyCode == 13) {
	                $("#btnSearch").attr('value');
	                //add more buttons here
	                return false;
	            }
	        }); 
        });
   	$('#prv').change(function() {
      document.getElementById("namaprov").value = $("#prv option:selected").text();
      LoadCity();
    });

    $('#cty').change(function() {
      document.getElementById("namakota").value = $("#cty option:selected").text();
      LoadSubdistrict();
    });

    $('#sbd').change(function() {
      document.getElementById("namakec").value = $("#sbd option:selected").text();
      LoadService($('#dv').val());
    });

    function LoadProv(id){
      $('.optprv').remove();
      $.ajax({
            type: "GET",
            url: "http://salvina.id/library/rajaongkir/store",
            data: {
              'q':'province'
            },
            cache: false,
            success: function(data){
              // console.log(data);
              
              var myjson = JSON.parse(data);
              var result = myjson.rajaongkir.results
              for (x in result) {
                if (result[x].province_id==id) {
                  document.getElementById("prv").innerHTML += "<option class='optprv' selected value='" + result[x].province_id + "'>" + result[x].province + '</option>';
                } else {
                  document.getElementById("prv").innerHTML += "<option class='optprv' value='" + result[x].province_id + "'>" + result[x].province + '</option>';
                }
              }
              document.getElementById("namaprov").value = $("#prv option:selected").text();
              LoadCity();
            }
          }); //end ajax
    }

    function LoadCity() {
      $('.optcty').remove();
      $.ajax({
            type: "GET",
            url: "http://salvina.id/library/rajaongkir/store",
            data: {
              'kota':$('#prv').val()
            },
            cache: false,
            success: function(data){
              var myjson = JSON.parse(data);
              var result = myjson.rajaongkir.results
              for (x in result) {
                if (result[x].city_id=='79') {
                  document.getElementById("cty").innerHTML += "<option class='optcty' selected value='" + result[x].city_id + "'>" + result[x].type + " "+ result[x].city_name + '</option>';
                } else {
                  document.getElementById("cty").innerHTML += "<option class='optcty' value='" + result[x].city_id + "'>" + result[x].type + " "+ result[x].city_name + '</option>';
                }
              }
              document.getElementById("namakota").value = $("#cty option:selected").text();
              LoadSubdistrict();
            }
          }); //end ajax
    }

    function LoadSubdistrict(){
      $('.optsbd').remove();
      $.ajax({
            type: "GET",
            url: "http://salvina.id/library/rajaongkir/store",
            data: {
              'sbd':$('#cty').val()
            },
            cache: false,
            success: function(data){
              var myjson = JSON.parse(data);
              var result = myjson.rajaongkir.results
              for (x in result) {
                if (result[x].subdistrict_id=='1066') {
                  document.getElementById("sbd").innerHTML += "<option class='optsbd' selected value='" + result[x].subdistrict_id + "'>" + result[x].subdistrict_name + '</option>';
                } else {
                  document.getElementById("sbd").innerHTML += "<option class='optsbd' value='" + result[x].subdistrict_id + "'>" + result[x].subdistrict_name + '</option>';
                }
              }
              document.getElementById("namakec").value = $("#sbd option:selected").text();
            }
          }); //end ajax
    }
    </script>
    
</body>
</html>