<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="<?= 'http://'.$_SERVER['HTTP_HOST'].'/rikza/assets/common/bootstrap/css/bootstrap.min.css' ?>">
	<title>Member | Status Transaksi</title>
</head>
<body>
	<div class="container" style="padding-top: 40px;">

		<div class="row">
			<div class="col-md-12">
				<div class="page-header">
					<h2>Status Transaksi</h2>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<table class="table table-bordered">
					<tbody>
						<tr class="success">
							<td>Upload bukti transaksi</td>
							<td style="text-align:center;">Sudah</td>
						</tr>
						<tr class="success">
							<td>Paket Barang</td>
							<td style="text-align:center;">Sudah</td>
						</tr>
						<tr class="danger">
							<td>Barang dikirim</td>
							<td style="text-align:center;">Belum</td>
						</tr>
						<tr class="danger">
							<td>Barang diterima</td>
							<td style="text-align:center;">Belum</td>
						</tr>
					</tbody>
				</table>
				<p>
					<b>Notes:</b><br>
					<ul>
						<li>Warna merah : belum</li>
						<li>Warna hijau : Sudah</li>
						<li>Warna kuning : dalam proses</li>
					</ul>
				</p>
			</div>
		</div>
	</div>
	<script src="<?= 'http://'.$_SERVER['HTTP_HOST'].'/rikza/assets/common/bootstrap/js/jquery.js' ?>"></script>
	<script src="<?= 'http://'.$_SERVER['HTTP_HOST'].'/rikza/assets/common/bootstrap/js/bootstrap.min.js' ?>"></script>
</body>
</html>