<html>
  <head>

  <link href="<?= $data->base_url ?>assets/bootstrap4/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <link rel="stylesheet" type="text/css" href="<?= $data->base_url ?>assets/member/slogin.css">
  <script src="<?= $data->base_url ?>assets/bootstrap4/js/bootstrap.min.js"></script>
  <script src="<?= $data->base_url ?>assets/common/jquery3/jquery-3.3.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
  </head>
<body id="LoginForm">
<div class="container">
<div class="login-form">
<div class="main-div">
    <div class="panel">
   <h2>Member Login</h2>
   <p>Please enter your email and password</p>
   </div>

      <form action="" method="post" accept-charset="utf-8">
        <div class="form-group">
            <input type="email" name="in[email]" class="form-control" id="inputEmail" placeholder="Email Address">
        </div>

        <div class="form-group">
            <input type="password" name="in[password]" class="form-control" id="inputPassword" placeholder="Password">
        </div>
        
        <button type="submit" class="btn btn-primary">Login</button>

      </form>
      <a href="<?= $data->base_url.'member/registrasi' ?>" title="Register here.">Daftar/Register</a>
    </div>
</div></div>


</body>
</html>
