<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="<?= $data->base_url.'assets/common/bootstrap/css/bootstrap.min.css' ?>">
	<title>Member | Registrasi</title>
</head>
<body>

	<div class="container" style="padding-top: 40px;">
		<?php if ($data->validation=='0'): ?>
			<script>alert('Kombinasi Password Tidak Sama');</script>
		<?php endif ?>
		<div class="row">
			<div class="col-md-12">
				<div class="page-header">
					<h2>Registrasi</h2>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">

				<section>
					<h3>Hai <?= $data->namacust ?>, anda sudah pernah terdaftar sebagai pembeli di salvina.id.</h3>
					<p>Untuk melanjutkan transaksi anda, silahkan masukkan password untuk masuk ke layanan member salvina.id :)</p>
				</section>

				<form action="/member/registerexistingmember" method="post" accept-charset="utf-8">
				<input type="hidden" name="in[v]" value="<?= $data->hash ?>">
				<div class="form-group">
					<label>Password</label>
					<input type="password" name="in[password]" class="form-control" placeholder="Password" required>
				</div>

				<div class="form-group">
					<label>Masukkan Password Sekali Lagi</label>
					<input type="password" name="in[passwordconfirm]" class="form-control" placeholder="Konfirmasi Password" required>
				</div>
				<button class="btn btn-lg btn-info" style="width:100%">Submit</button>
				</form>
			</div>
		</div>
	</div>
	<script src="<?= $data->base_url.'assets/common/bootstrap/js/jquery.js' ?>"></script>
	<script src="<?= $data->base_url.'assets/common/bootstrap/js/bootstrap.min.js' ?>"></script>
</body>
</html>