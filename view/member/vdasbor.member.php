<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Member | Dasbor</title>
</head>
<body>
<!-- <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css"> -->
<link href="<?= $data->base_url ?>assets/common/bootstrap/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<link rel="stylesheet" type="text/css" href="<?= $data->base_url ?>assets/member/sdasbor.css">
<script src="<?= $data->base_url ?>assets/common/jquery3/jquery-3.3.1.min.js"></script>
<!-- <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script> -->
 <script src="<?= $data->base_url ?>assets/common/bootstrap/js/bootstrap.min.js"></script> 

<!------ Include the above in your HEAD tag ---------->

    <div id="wrapper">
        <div class="overlay"></div>
    
        <?php include_once $data->homedir.'view/member/sidebar.member.php' ?>

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <button type="button" class="hamburger is-closed" data-toggle="offcanvas">
                <span class="hamb-top"></span>
                <span class="hamb-middle"></span>
                <span class="hamb-bottom"></span>
            </button>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="page-header">
                            <h1>Beranda | Salvina | Member</h1>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <h3>Troli/Keranjang Saya</h3>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Item</th>
                                    <th>Total</th>
                                    <th>Voucher</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                              <?php if (count($data->listbarang)=='0'): ?>
                                <tr>
                                    <td colspan="4" style="text-align:center;">Tidak ada data tersedia.</td>
                                </tr>
                              <?php else: ?>

                                <tr>
                                  
                                    <td style="vertical-align:middle;">
                                      <ul>
                                      <?php foreach ($data->listbarang as $key => $value): ?>
                                        <?= '<li>'.$value['kat']. ' : '.$value['jns'].' - '. $value['nb'].'<a href="removeitem?itm='.$value['ib'].'" title="Hapus item"><span class="glyphicon glyphicon-remove"></span></a></li>' ?>
                                      <?php endforeach ?>
                                      </ul>
                                    </td>
                                    <td style="vertical-align:middle;">
                                        <?php $total = 0; ?>
                                        <?php foreach ($data->listbarang as $key => $value): ?>
                                          <?php $subtotal += $value['price']*$value['qty'];$total+=$subtotal ?>
                                        <?php endforeach ?>
                                        <?= number_format($total).' IDR' ?>
                                    </td>
                                    <td style="vertical-align:middle;"></td>
                                    <td style="vertical-align:middle;text-align:center;">
                                        <a href="#modal-cari" data-toggle="modal" id="vcr"  class="btn btn-success">Voucher</a>
                                        ||
                                        <a href="<?= $data->base_url.'member/pembayaran' ?>" class="btn btn-info">
                                            Detail
                                        </a>
                                        
                                    </td>
                                </tr>
                              <?php endif ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                

                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <h2>Riwayat Transaksi</h2>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Tanggal</th>
                                    <th>Item</th>
                                    <th>Total</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                              <?php if (count($data->historitrans)=='0'): ?>
                                <tr>
                                    <td colspan="5" style="text-align:center;">Tidak ada data tersedia.</td>
                                </tr>
                              <?php else: ?>
                                <?php //print_r($data->historitrans) ?>
                                <?php foreach ($data->historitrans as $key): ?>
                                  <?php //print_r($key) ?>
                                  <tr>
                                      <td style="text-align:center;vertical-align:middle;">
                                          <?= $key['tgl_trans'] ?>
                                      </td>
                                      <?php $items = explode(' # ', $key['item']) ?>
                                      <td style="vertical-align:middle;">
                                          <ul>
                                              <?php foreach ($items as $val): ?>
                                                <li><?= $val ?></li>  
                                              <?php endforeach ?>
                                          </ul>
                                      </td>
                                      <td style="vertical-align:middle;">
                                          <?= number_format($key['total_harga_item']) ?> IDR
                                      </td>
                                      <td style="vertical-align:middle;">
                                          <a href="<?= $data->base_url.'order/status-transaksi/?tr='.$data->securitycode($key['tr_id'],'e') ?>" title="lihat disini" class="btn btn-info">Lihat</a>
                                      </td>
                                      <td style="vertical-align:middle;">
                                          <a href="<?= $data->base_url.'order/invoice?tr='.$key['tr_id'] ?>" target="_blank" class="btn btn-info">
                                              Detail
                                          </a>
                                          ||
                                          <a href="<?= $data->base_url.'order/invoice/pdf?tr='.$key['tr_id'] ?>" class="btn btn-danger">
                                              Bukti/Invoice
                                          </a>
                                      </td>    
                                  </tr>
                                <?php endforeach ?>
                              <?php endif ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- Modal -->
  <div class='modal fade' id='modal-cari' role='dialog'>
    <div class='modal-dialog'>

      <!-- Modal content-->
      <div class='modal-content'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'>&times;</button>
          <h3 class="modal-title">Input Voucher</h3>

        </div>
        <div class='modal-body'>
        <div style='padding-left:15px;padding-right:15px;padding-top:0px; height: 300px;' class='row'>

          <div class="col-md-12">
            <form action="" method="post">
            <div class="form-group">
                <label>Kode Voucher</label>
                <input type="text" class="form-control" name="vcr[kode]" />
            </div>
            <button class="btn btn-primary">Submit</button>
            </form>
          </div>
        </div>


        </div>
        <div class='modal-footer'>
            <div class="row">
                <div class="col-md-6" style="text-align:left">
                    <span>Rumah iska 2017</span>
                </div>
                <div class="col-md-6">
                    <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                </div>
            </div>

        </div>
      </div>

    </div>
  </div>
  <!-- end modal -->
    <!-- /#wrapper -->
    <script>
        $(document).ready(function () {
          var trigger = $('.hamburger'),
              overlay = $('.overlay'),
             isClosed = false;

            trigger.click(function () {
              hamburger_cross();      
            });

            function hamburger_cross() {

              if (isClosed == true) {          
                overlay.hide();
                trigger.removeClass('is-open');
                trigger.addClass('is-closed');
                isClosed = false;
              } else {   
                overlay.show();
                trigger.removeClass('is-closed');
                trigger.addClass('is-open');
                isClosed = true;
              }
          }
          
          $('[data-toggle="offcanvas"]').click(function () {
                $('#wrapper').toggleClass('toggled');
          });  
        });
    </script>
</body>
</html>

<?php
function Rows($data){
  if (count($data)=='0') {
    echo "<td colspan='3'>Tidak ada data barang yang anda pesan di sistem kami.</td>";
  }else {
    $total = 0;
    $totalberat = 0;
    foreach ($data as $key => $value) {
      echo '<tr>';
      echo '<td>'.$value['kat']. ' : '.$value['jns'].' - '. $value['nb'].'<a href="removeitem?itm='.$value['ib'].'" title="Hapus item"><span class="glyphicon glyphicon-remove"></span></a></td>';
      $subBerat = $value['bb'] * $value['qty'];
      $totalberat += $subBerat;
      echo '<td id="wgh">'.$value['bb'].' gram</td>';
      echo '<td>'.$value['qty'].'</td>';
      $subtotal = $value['price']*$value['qty'];
      echo '<td>'.number_format($value['price']).' IDR</td>';
      echo '<td>'.number_format($subtotal).' IDR</td>';
      $total += $subtotal;
    }
      echo '<tr>';
      echo '<td colspan="4" id="st">Sub Total:</td><td>'.number_format($total).' IDR</td>';
      echo '<input type="hidden" id="sbt" value="'.$total.'">';
      echo '<input type="hidden" id="sbtb" value="'.$totalberat.'">';
      echo '</tr>';

      $ongkir = 0;
      echo '<tr>';
      echo '<td colspan="4">Ongkos Kirim:</td><td id="bo">Menghitung ...</td>';
      echo '</tr>';

      echo '<tr>';
      echo '<td colspan="4">Total Biaya:</td><td><b id="tb">Menghitung ...</b></td>';
      echo '</tr>';

  }
}

?>