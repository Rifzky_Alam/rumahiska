<?php 
session_start();
include_once '/home/salvinai/public_html/classes/views/class-global.php';
// include_once '/Applications/XAMPP/xamppfiles/htdocs/rikza/classes/views/class-global.php';
$data = new DataView();
$data->Model('Artikel');
$artikel = new Artikel();

// carts
if (isset($_SESSION['ri_items'])) {
	$data->items = $_SESSION['ri_items'];
	$data->countmycart = count($_SESSION['ri_items']);
} else {
	$data->items = array();
	$data->countmycart=0;
}
// end carts

if (isset($_SERVER['PATH_INFO'])) {
	$_SERVER['PATH_INFO'] = str_replace('.php', '', $_SERVER['PATH_INFO']);
	@$url_segment = explode('/', trim($_SERVER['PATH_INFO'],'/'));
	$key = array_shift($url_segment);

	switch ($key) {
		case 'read':
			$data->CheckSegment($url_segment[0]);
			$artikel->setID($url_segment[0]);
			$data->artikel = $artikel->FetchByID();
			// print_r($data->artikel);
			$data->judul='RumahIska || Artikel';
			$data->View('blogs/vpost.artikel.php',$data);
			break;
		case 'list':
			$data->judul='RumahIska || Artikel';
			$data->Model('Artikel');
			$artikel = new Artikel();
			$data->listartikel = $artikel->FetchArtikelData();

			// print_r($data->listartikel);

			$data->View('blogs/vlist.artikel.php',$data);
			break;
		case '/':
			
			break;
		case '':
			header('location:'.$data->base_url.basename(__FILE__,'.php'));
			break;
	}
} else {
	$data->judul='Bisa lah ya';
	$data->View('homepage/vproduct.homepage.php',$data);
}

?>