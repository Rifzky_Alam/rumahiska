<?php 
session_start();
include_once '/home/salvinai/public_html/classes/views/class-global.php';

// include_once '/Applications/XAMPP/xamppfiles/htdocs/rikza/classes/views/class-global.php';
$data = new DataView();
$data->Model('Barang');
$barang = new Barang();
$data->Lib('otherfunction/Route');
$data->Lib('otherfunction/Picturefunc');
$data->Lib('otherfunction/ProdukFunc');

// carts
if (isset($_SESSION['ri_items'])) {
	$data->items = $_SESSION['ri_items'];
	$data->countmycart = count($_SESSION['ri_items']);
} else {
	$data->items = array();
	$data->countmycart=0;
}
// end carts

if (isset($_SERVER['PATH_INFO'])) {
	$_SERVER['PATH_INFO'] = str_replace('.php', '', $_SERVER['PATH_INFO']);
	@$url_segment = explode('/', trim($_SERVER['PATH_INFO'],'/'));
	$key = array_shift($url_segment);

	switch ($key) {
		case 'detail':
			$data->CheckSegment($url_segment[0]);
			$data->judul=$data->company.' || Produk Kami';
			$barang->setModeljenis($url_segment[0]);
			$data->listitems = $barang->FetchDetailBarangByJenisBarang();
			$data->View('homepage/vsingle.homepage.php',$data);
			break;
		case 'coba':
			echo 'bisa';
			break;
		case 'jenis':
			$data->CheckSegment($url_segment[0]);
			$data->judul=$data->company.' || '.$barang->getNamaJenisBarang($url_segment[0]);
			$data->sidebardata = $barang->FetchSidebarClient();
			$barang->setModeljenis($url_segment[0]);
			// echo $url_segment[0];
			$data->listitems = $barang->FetchDetailBarangByJenisBarang();
			$data->View('homepage/vproduct.homepage.php',$data);
			break;
		case 'neworder':
			$data->CheckSegment($url_segment[0]);
			include_once '/home/salvinai/public_html/library/controller/order.controller.php';
			Order::ClientOrder($url_segment[0]);
			break;
		case 'order':
			$data->CheckSegment($url_segment[0]);
			$data->CheckSegment($url_segment[1]);
			$data->jenis_barang = $url_segment[0];
			$data->sidebardata = $barang->FetchSidebarClient(); //rifzky 13 mei 2018
			$barang->setID($url_segment[1]);
			$data->Model('Querybuilder');
			$db = new QueryBuilder();
			$cb = 	$db->FetchJoinWhere(['db.id AS id','jb.id AS id_jb','kb_ket','jenis_barang','detail_jenis','th_harga','bg_name'],
										'jenis_barang AS jb',
										array(
											$db->getJoin('INNER','kategori_barang AS kb','kb.kb_id','jb.kb_id'),
											$db->getJoin('INNER','data_barang AS db','db.id_jenis','jb.id'),
											$db->getJoin('LEFT','tabel_harga AS th','th.th_id','db.brg_harga'),
											$db->getJoin('LEFT','barang_gambar AS bg','bg.bg_id','db.brg_pic')
										),
										array(
											$db->GetCond('jb.id','=',$url_segment[0])
										)
					);
			$data->listprodukterkait = $cb;
			// print_r(json_encode($cb));
			if(isset($_SESSION['ri_agen'])&&!empty($_SESSION['ri_agen'])){
	           $data->item = $barang->SelectForChartAgen();     
	        }else{
	            $data->item = $barang->SelectForChart(); 
	        }
			
			// print_r($data->item);
			$data->galeri = $barang->FetchDataGambar();

			// begin post
			if (isset($_POST['qty'])) {
				if ($_POST['qty']>$data->item->jumlah_barang) {
					echo "<script>alert('Jumlah Barang tidak bisa melebihi jumlah stok kami :(');</script>";
				}elseif ($_POST['qty']=='0') {
					echo "<script>alert('Jumlah Barang tidak bisa nol :(');</script>";
				} else {
					if (!isset($_SESSION['ri_items'])) {
						$_SESSION['ri_items'] = [
							$url_segment[1] => array(
								'ib' => $url_segment[1],
												'kat' => $data->item->kb_ket,
												'jns'=>$data->item->jenis_barang,
												'nb' => $data->item->detail_jenis,
												'bb' => $data->item->brg_berat,
												'qty' => $_POST['qty'],
												'priceid' => $data->item->th_id,
												'price' => $data->item->brg_harga,
												'foto' => $data->item->bg_name
							)
						];
						
					}else{
						if (isset($_SESSION['ri_items'][$url_segment[1]])) {
							$_SESSION['ri_items'][$url_segment[1]] = [
								'ib' => $url_segment[1],
								'kat' => $data->item->kb_ket,
								'jns'=>$data->item->jenis_barang,
								'nb' => $data->item->detail_jenis,
								'bb' => $data->item->brg_berat,
								'qty' => $_POST['qty'],
								'priceid' => $data->item->th_id,
								'price' => $data->item->brg_harga,
								'foto' => $data->item->bg_name
							];
							// echo "<script>alert('barang ada!');</script>";
						} else {
							$_SESSION['ri_items'][$url_segment[1]] = [
								'ib' => $url_segment[1],
								'kat' => $data->item->kb_ket,
								'jns'=>$data->item->jenis_barang,
								'nb' => $data->item->detail_jenis,
								'bb' => $data->item->brg_berat,
								'qty' => $_POST['qty'],
								'priceid' => $data->item->th_id,
								'price' => $data->item->brg_harga,
								'foto' => $data->item->bg_name
							];
							// echo "<script>alert('barang baru!');</script>";
						}
						
						
					}
					echo "<script>location.replace('".$data->base_url."');</script>";
				}	
			}
			// end post


			$data->judul=$data->company.' || Produk Kami';
			$data->View('homepage/vsingle.homepage.php',$data);
			break;
		case 'carts':
			// print_r($_SESSION['ri_items']);
			$data->judul = $data->company.' || Troli';
			$data->carts = $_SESSION['ri_items'];
			$data->View('homepage/vtablecarts.homepage.php',$data);
			break;
		case 'deleteitem':
			$data->CheckSegment($url_segment[0]);
			unset($_SESSION['ri_items'][$url_segment[0]]);
			header('Location:'.$data->base_url.'produk/carts');
			break;
		case '/':
			
			break;
		case '':
			header('location:'.$data->base_url.basename(__FILE__,'.php'));
			break;
	}
} else {
	$data->judul='Salvina || Produk Kami';
	$data->sidebardata = $barang->FetchSidebarClient();
	// $barang->setModeljenis('38e72ca41bc7e61e693a38fd03caeb11');
	if(isset($_SESSION['ri_agen'])&&!empty($_SESSION['ri_agen'])){
	    $data->listitems = $barang->DisplayDataBarang();
	}else{
	    $data->listitems = $barang->DisplayDataBarang();    
	}
	
	// print_r($data->listitems);
	$data->View('homepage/vproduct.homepage.php',$data);
}
