<?php 
session_start();
include_once '/home/salvinai/public_html/classes/views/class-global.php';
// include_once '/Applications/XAMPP/xamppfiles/htdocs/rikza/classes/views/class-global.php';
$data = new DataView();
$data->Lib('otherfunction/Picturefunc');
$data->Model('Barang');
$data->Model('File');
$db = new Barang();
$file = new File();
$data->baseurl=$data->base_url;
$data->judul=$data->company.' || Beranda';

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// carts
if (isset($_SESSION['ri_items'])) {
	$data->items = $_SESSION['ri_items'];
	$data->countmycart = count($_SESSION['ri_items']);
} else {
	$data->items = array();
	$data->countmycart=0;
}

$data->banners = $file->Banners();
// end carts

// data promo
$data->prpromo = $db->DisplayProduk('3'); 
// end data promo

// data produk unggulan
$data->prfv = $db->DisplayProduk('2');
// end data produk unggulan

// data produk terbaru
$data->prnew = $db->DisplayProduk('1');
// print_r($data->prnew);
// end data produk terbaru

$data->View('homepage/index.homepage.php',$data);

// include_once 'view/home/index.php'; 

?>