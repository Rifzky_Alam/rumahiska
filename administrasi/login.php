<?php 
session_start();
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
include_once '../baseurl.php';
// echo $_SESSION['page'];
if (isset($_SESSION['admin'])&&!empty($_SESSION['admin'])) {
	echo "<script>location.replace('".getBaseUrl()."administrasi/');</script>";
}

if (isset($_POST['in'])) {
	include_once HomeDir().'model/Admin.php';
	$admin = new Admin();
	$admin->setUsername($_POST['in']['username']);
	$admin->setPassword($_POST['in']['password']);
	// print_r($_POST['in']);
	$datalogin = $admin->Login();

	
	if (@$datalogin->username!='') { //start validation
		//set session admin
		$_SESSION['admin'] = array(
			'username' => $datalogin->username,
			'nama' => $datalogin->nama_user,
			'status' => $datalogin->status 
		);

		if (isset($_SESSION['page'])&&!empty($_SESSION['page'])) {
			echo "<script>alert('Selamat datang, ".$datalogin->nama_user." :)');</script>";
			echo "<script>location.replace('".$_SESSION['page']."');</script>";
		}else{
			echo "<script>alert('Selamat datang, ".$datalogin->nama_user." :)');</script>";
			echo "<script>location.replace('".getBaseUrl()."administrasi/');</script>";
		}
	}else{
		echo "<script>alert('Anda tidak terdaftar dalam sistem kami :(');</script>";
	}//end validation


}

$data = array(
	'base_url' => getBaseUrl(),
	'judul' => 'Login',
	'package' => 'administrasi' 
);
// 
$data = (object) $data;
include_once '../view/administrasi/view-login.php'; 



?>