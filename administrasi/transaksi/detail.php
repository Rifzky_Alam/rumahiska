<?php 
session_start();
// print_r($_SERVER);

include_once $_SERVER['DOCUMENT_ROOT'].'/classes/views/class-global.php';
// include_once '/Applications/XAMPP/xamppfiles/htdocs/rikza/classes/views/class-global.php';
$data = new DataView();
if (isset($_GET['tr'])) {
	$_SESSION['page'] = $data->base_url.'administrasi/produk/'.basename(__FILE__,'.php');
	$data->Model('Transaksi');
	$data->Lib('session/sessions');
	$sesi = new MySession();
	$transaksi = new Transaksi();
	$transaksi->setId($_GET['tr']);

	$detailcust = $transaksi->DetailTransaction();
	// print_r($detailcust);
	$data->id_transaksi = $_GET['tr'];
	$data->buktiupload = $detailcust->tr_bukti_upload;
	$data->namacust=$detailcust->rc_nama;
	$data->emailcust=$detailcust->rc_email;
	$data->telepon=$detailcust->rc_telepon;
	$data->alamat=$detailcust->adr_alamat;
	$data->jasakurir=$detailcust->rk_kode_jasa;
	$data->no_resi=$detailcust->rk_resi;
	$data->service=$detailcust->rjk_kd_svc;
	$data->ongkir=$detailcust->rk_ongkir;
	$data->kecamatan = $detailcust->rk_kec;
	$data->kota = $detailcust->rk_kota;
	$data->prov=$detailcust->rk_prov;
	$data->listbarang = $transaksi->GetCartsOfCust();

	$data->judul="RumahIska - Data Transaksi";
	$data->subtitle="Data Transaksi";
	$data->nama_admin=$_SESSION['admin']['nama'];
	$data->me=$data->base_url.'administrasi/transaksi/';
	$data->package="administrasi";
	// print_r($data->listbarang);
	$data->View('administrasi/transaksi/vdetailtrans.php',$data);
}
	