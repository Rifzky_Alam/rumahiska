<?php 
session_start();
include_once '/home/salvinai/public_html/classes/views/class-global.php';
//include_once '/Applications/XAMPP/xamppfiles/htdocs/rikza/classes/views/class-global.php';
$data = new DataView();
$data->Lib('session/sessions');
$sesi = new MySession();
$sesi->OnlyAdmin($data->base_url.'administrasi/logout');
$data->package="administrasi/transaksi/";
$data->base_name=basename(__FILE__,'.php');
$data->nama_admin=$_SESSION['admin']['nama'];
if (isset($_SERVER['PATH_INFO'])) {
	$url_segment = explode('/', trim($_SERVER['PATH_INFO'],'/'));
	

	if (isset($url_segment)) {
		$key = array_shift($url_segment);
		switch ($key) {
			case 'new':
				// $data->CheckSegment2($url_segment[0],$data->base_url.$data->package.'customer');

				if (isset($_POST['in'])) {
					$data->Model('Customer');
					$cust = new Customer();
					$cust->setEmail($_POST['in']['email']);
					$cust->setPassword($_POST['in']['password']);
					$cust->setNama($_POST['in']['nama']);
					$cust->setTelepon($_POST['in']['telepon']);
					$cust->setAlamat($_POST['in']['alamat']);

					if ($cust->NewCustomer()) {
						echo "<script>alert('Data berhasil disimpan.');</script>";
						echo "<script>location.replace('".$data->base_url.$data->package."customer');</script>";
					} else {
						echo "<script>alert('Data berhasil disimpan.');</script>";
					}

				}

				$data->judul="RumahIska - Input Customer";
				$data->subtitle="Input Customer";
				$data->me = $data->base_url.'administrasi/transaksi/customer/new/';
				$data->View('administrasi/transaksi/customer/vinput.customer.php',$data);
				break;
			case 'search':
				if (isset($_GET['nama'])) {
					$data->Model('Customer');
					$cust = new Customer();
					$data->judul="RumahIska - Data Pencarian";
					$data->subtitle="Cari Customer";
					$data->listcust = $cust->SearchCust(['atr' => 'rc_nama','value' => $_GET['nama']]);
					$data->View('administrasi/transaksi/customer/vdisplay.customer.php',$data);
				}
				break;
			case 'edit':
				$data->CheckSegment($url_segment[0]);
				$data->Model('Customer');
				$cust = new Customer();
				

				if (isset($_POST['in'])) {

					$cust->setEmail($url_segment[0]);
					$cust->setData($_POST['in']['email']);
					$cust->setPassword($_POST['in']['password']);
					$cust->setNama($_POST['in']['nama']);
					$cust->setTelepon($_POST['in']['telepon']);
					$cust->setAlamat($_POST['in']['alamat']);
					// print_r($_POST['in']);
					if ($cust->EditData()) {
						echo "<script>alert('Data berhasil diubah.');</script>";
						echo "<script>location.replace('".$data->base_url."administrasi/transaksi/customer');</script>";
					} else {
						echo "<script>alert('Data gagal diubah.');</script>";
					}
				}
				$cust->setEmail($url_segment[0]);
				$cust->Fill();
				// print_r($cust);
				$data->customer=$cust;
				$data->listalamat=$cust->FetchAlamat();
				$data->judul="RumahIska - Edit Customer";
				$data->subtitle="Edit Customer";
				$data->me = $data->base_url.'administrasi/transaksi/customer/new/';
				$data->View('administrasi/transaksi/customer/vedit.customer.php',$data);
				break;
			case 'newalamat':
				$data->CheckSegment($url_segment[0]);
				$data->Model('Customer');
				$cust = new Customer();
				$cust->setEmail($url_segment[0]);

				if (isset($_POST['in'])) {
					$cust->setAlamat($_POST['in']['alamat']);
					if ($cust->NewAddress()) {
						echo "<script>alert('Data berhasil disimpan.');</script>";
						echo "<script>location.replace('".$data->base_url."administrasi/transaksi/customer');</script>";
					} else {
						echo "<script>alert('Data gagal disimpan.');</script>";
					}
				}

				$cust->Fill();
				$data->judul="Tambah Alamat";
				$data->subtitle="Tambah Alamat ".$cust->getNama();
				$data->View('administrasi/transaksi/customer/vnewaddr.customer.php',$data);
				break;
			case '':
				echo "<script>location.replace('".$data->base_url.$data->package."customer');</script>";
				break;
		}
	}	
} else {
	$data->judul="RumahIska - Data Customer";
	$data->subtitle="Data Customer";
	$data->nama_admin=$_SESSION['admin']['nama'];
	$data->me = $data->base_url.'administrasi/transaksi/customer/new/';
	$data->package="administrasi";
	$data->View('administrasi/transaksi/customer/vsearch.customer.php',$data);
}

?>