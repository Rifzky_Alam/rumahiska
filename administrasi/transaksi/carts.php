<?php 
session_start();
include_once $_SERVER['DOCUMENT_ROOT'].'/classes/views/class-global.php';
// include_once '/Applications/XAMPP/xamppfiles/htdocs/rikza/classes/views/class-global.php';
$data = new DataView();
$data->Lib('session/sessions');
$sesi = new MySession();
$sesi->OnlyAdmin($data->base_url.'administrasi/logout');
$data->package="administrasi/transaksi/";
$data->nama_admin=$_SESSION['admin']['nama'];
$data->base_name=basename(__FILE__,'.php');
if (isset($_SERVER['PATH_INFO'])) {
	$url_segment = explode('/', trim($_SERVER['PATH_INFO'],'/'));
	

	if (isset($url_segment)) {
		$key = array_shift($url_segment);
		switch ($key) {
			case 'add':
				$data->CheckSegment($url_segment[0]);
				$data->Model('Barang');
				$data->Model('Transaksi');
				$barang = new Barang();
				$transaksi = new Transaksi();

				if (isset($_POST['in'])) {
					// print_r($_POST['in']);
					$transaksi->setId($url_segment[0]);
					$transaksi->setKeranjang($_POST['in']['detailbarang']);
					$transaksi->setData($_POST['in']['qty']);
					if ($transaksi->NewAdminCarts()) {
						echo "<script>alert('Data berhasil disimpan.');</script>";
						echo "<script>location.replace('".$data->base_url.$data->package."');</script>";
					} else {
						echo "<script>alert('Data gagal disimpan.');</script>";
					}
				}
				$transaksi->setId($url_segment[0]);
				$data->listbarang = $transaksi->GetCartsOfCust();
				$data->jenisbarang=$barang->FetchJenisBarang();
				$data->judul="RumahIska - Tambah Barang Transaksi";
				$data->subtitle="Tambah Barang";
				$data->View('administrasi/transaksi/carts/vinput.carts.php',$data);
				break;
			case 'delete':
				$data->CheckSegment($url_segment[0]);
				$data->Model('Transaksi');
				$transaksi = new Transaksi();
				$transaksi->setData($url_segment[0]);
				if ($transaksi->DeleteCartItem()) {
					echo "<script>alert('Data berhasil dihapus.');</script>";
					echo "<script>location.replace('".$data->base_url.$data->package."');</script>";
				} else {
					echo "<script>alert('Data gagal dihapus.');</script>";
				}

				break;
			case 'edit':
				
				break;
			case 'databarang':
				if (isset($_GET['jns'])) {
					$data->Model('Barang');
					$barang = new Barang();
					$listbarang = $barang->BarangSearch(['atr'=>'id_jenis','cond' => '=','value' => $_GET['jns']]);
					if (count($listbarang)=='0') {
						echo "<option value=''>Tidak Ada Data Barang</option>";
					}else{
						foreach ($listbarang as $key) {
							echo "<option value='".$key->id."'>".$key->detail_jenis."</option>";
						}
					}

				}
				break;

			case '':
				echo "<script>location.replace('".$data->base_url."administrasi/transaksi/carts');</script>";
				break;
		}
	}	
}
?>