<?php 
session_start();
include_once $_SERVER['DOCUMENT_ROOT'].'/classes/views/class-global.php';
// include_once '/Applications/XAMPP/xamppfiles/htdocs/rikza/classes/views/class-global.php';
$data = new DataView();
$_SESSION['page'] = $data->base_url.'administrasi/produk/'.basename(__FILE__,'.php');
$data->Lib('session/sessions');
$sesi = new MySession();
$sesi->OnlyAdmin($data->base_url.'administrasi/logout');
$data->Model('Transaksi');
$data->Model('Mail');
$data->Model('Querybuilder');

$db = new QueryBuilder();
$email = new RumahIskaMail();
$trans = new Transaksi();
$sesi = new MySession();

if (isset($_GET['send'])) {
	$data->judul="RumahIska - Data Transaksi";
	$data->subtitle="Data Transaksi";
	$data->nama_admin=$_SESSION['admin']['nama'];
	$data->me=$data->base_url.'administrasi/transaksi/';
	$data->package="administrasi";
	$trans->setId($_GET['send']);
	#region
	// data for email template
	$data->Lib('otherfunction/TanggalFunc');
	$dataemail = $trans->FetchForEmail();
	$data->carts=$trans->FetchCarts();
	$data->customer=$dataemail->rc_nama;
	$data->resi=$dataemail->rk_resi;
	$data->ongkir=$dataemail->rk_ongkir;
	$data->idtrans=$dataemail->tr_id;
	$data->tglkirim=GetTanggal($dataemail->rk_tgl_kirim);
	$data->petugas = $_SESSION['admin']['nama'];
	#end region

	// print_r($data->carts);
	include_once $data->homedir.'view/administrasi/transaksi/email/vkontenemail.php';
	$data->email_to = $dataemail->rc_email;
	$data->mailsubject = 'Invoice Transaksi '.$dataemail->rc_nama;
	$data->mailbody = Email($data);
	if ($email->KirimFaktur($data)) {
		//set status transaksi to "faktur terkirim"
		$db->UpdateData('ri_transaksi',['tr_status'=>'1'],array($db->GetCond('tr_id','=',$_GET['send'])));
		
		echo "<script>alert('Faktur Berhasil Dikirim');</script>";
		echo "<script>location.replace('".$data->base_url."administrasi/transaksi/');</script>";
	} else {
		echo "<script>alert('Faktur gagal Dikirim');</script>";
		echo "<script>location.replace('".$data->base_url."administrasi/transaksi/');</script>";
	}
	
}elseif (isset($_GET['tr'])) {
	$data->judul="RumahIska - Data Transaksi";
	$data->subtitle="Data Transaksi";
	$data->nama_admin=$_SESSION['admin']['nama'];
	$data->me=$data->base_url.'administrasi/transaksi/';
	$data->package="administrasi";
	$trans->setId($_GET['tr']);
	#region
	// data for email template
	$data->Lib('otherfunction/TanggalFunc');
	$dataemail = $trans->FetchForEmail();
	$data->carts=$trans->FetchCarts();
	$data->customer=$dataemail->rc_nama;
	$data->resi=$dataemail->rk_resi;
	$data->ongkir=$dataemail->rk_ongkir;
	$data->idtrans=$dataemail->tr_id;
	$data->tglkirim=GetTanggal($dataemail->rk_tgl_kirim);
	$data->petugas = $_SESSION['admin']['nama'];
	#end region

	// print_r($data->carts);
	include_once $data->homedir.'view/administrasi/transaksi/email/vkontenemail.php';
	echo Email($data);
}
	
// $data->View('administrasi/transaksi/email/vkontenemail.php',$data);