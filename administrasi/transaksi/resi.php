<?php 

if (isset($_GET['tr'])) {
	session_start();
	include_once $_SERVER['DOCUMENT_ROOT'].'/classes/views/class-global.php';
	// include_once '/Applications/XAMPP/xamppfiles/htdocs/rikza/classes/views/class-global.php';
	$data = new DataView();
	$_SESSION['page'] = $data->base_url.'administrasi/produk/'.basename(__FILE__,'.php');
	$data->Lib('session/sessions');
	$sesi = new MySession();
	$sesi->OnlyAdmin($data->base_url.'administrasi/logout');
	
	$data->judul="RumahIska - Data Transaksi";
	$data->subtitle="Data Transaksi";
	$data->nama_admin=$_SESSION['admin']['nama'];
	$data->me=$data->base_url.'administrasi/transaksi/';
	
	if (isset($_POST['in'])) {
		$data->Model('Kurir');
		$kurir = new Kurir();
		$kurir->setIdtrans($_GET['tr']);
		$kurir->setResi($_POST['in']['resi']);
		$kurir->setTglkirim($_POST['in']['tglkirim']);
		if ($kurir->UpdateResi()) {
			echo "<script>alert('No Resi Berhasil disimpan.');</script>";
			echo "<script>location.replace('".$data->me."');</script>";
		} else {
			echo "<script>alert('Data gagal disimpan.');</script>";
		}
	}

	
	$data->package="administrasi";
	$data->View('administrasi/transaksi/vresikirim.php',$data);
}

?>