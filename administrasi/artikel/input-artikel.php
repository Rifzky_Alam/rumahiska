<?php 
session_start();
include_once '../../baseurl.php';
include_once HomeDir().'library/session/sessions.php';
include_once HomeDir().'model/Artikel.php';
$sesi = new MySession();
$sesi->OnlyAdmin(getBaseUrl().'administrasi/logout');
$artikel = new Artikel();

if (isset($_POST['in'])&&isset($_FILES['gambar'])) {
	

	$artikel->setJudul($_POST['in']['judul']);
	$artikel->setIsi($_POST['in']['isi']);
	$artikel->setTags($_POST['in']['tag']);
	$artikel->setTanggal(date('Y-m-d'));
	$artikel->setGambar($_FILES['gambar']);
	$artikel->setPenulis($_SESSION['admin']['username']);
	
	if ($artikel->IsGambarValid()) {
		if ($artikel->UploadImage()) {
			// echo $artikel->JsMessage('File berhasil di upload');
			if ($artikel->NewData()) {
				// echo $artikel->JsMessage('Data Berhasil disimpan..');
				echo "<script>alert('Data Berhasil Disimpan...');</script>";
				echo "<script>location.replace('".getBaseUrl()."administrasi/artikel/');</script>";
			} else {
				echo "<script>alert('Data gagal Disimpan...');</script>";
			}
		} else {
			echo "<script>alert('File gagal Di upload!');</script>";
		}
				
	}else{
		echo "<script>alert('Hanya File .jpg/.png/jpeg yang boleh di upload');</script>";
	}
	/*
	if ($artikel->NewData()) {
		echo "<script>alert('Data berhasil disimpan')</script>";
		echo "<script>alert('".getBaseUrl()."/administrasi/artikel/')</script>";
	} else {
		echo "<script>alert('Data gagal disimpan')</script>";
	}*/
	// print_r($artikel);
}

$data = array(
	'base_url' => getBaseUrl(),
	'judul' => 'RumahIska',
	'package' => 'administrasi',
	'nama_admin' => $_SESSION['admin']['nama'] 
);
$data = (object) $data;
include_once HomeDir().'/view/administrasi/artikel/view-input-artikel.php'; 

?>