<?php 
session_start();
include_once '../../baseurl.php';
include_once HomeDir().'library/session/sessions.php';
include_once HomeDir().'model/Artikel.php';
$sesi = new MySession();
$sesi->OnlyAdmin(getBaseUrl().'administrasi/logout');
$artikel = new Artikel();
if (isset($_GET['id'])&&!empty($_GET['id'])) {

	$artikel->setID($_GET['id']);
	$dataartikel = $artikel->FetchByID();

	//get post data


	if (isset($_POST['in'])) {
		$artikel->setID($_GET['id']);
		$artikel->setJudul($_POST['in']['judul']);
		$artikel->setIsi($_POST['in']['isi']);
		$artikel->setTags($_POST['in']['tag']);
		$artikel->setTanggal(date('Y-m-d'));
		
		$artikel->setPenulis($_SESSION['admin']['username']);
		
		if (isset($_FILES['gambar'])&&!empty($_FILES['gambar']['name'])) {
			// echo "Gambar telah di upload";
			$artikel->setGambar($_FILES['gambar']);

			if ($artikel->IsGambarValid()){
				//after the image valid we upload the image
				@unlink('/home/rumahisk/public_html/uploads/images/artikel/'.$dataartikel->a_image);
				if ($artikel->UploadImageFromEdit()) {
					
					//editting Data
					if ($artikel->EditWithImage()) {
						echo "<script>alert('Data berhasil diubah!');</script>";
						echo "<script>location.replace('http://rumahiska.com/administrasi/artikel/');</script>";	
					} else {
						echo "<script>alert('Data gagal di ubah!');</script>";
					}
					

				} else { //
					echo "<script>alert('Gambar gagal di upload!');</script>";	
				}
				

			}else{
				 echo "<script>alert('Hanya File .jpg || .png yang boleh di upload!');</script>";
			}



		} else { //edit without image...
			if ($artikel->EditWithoutImage()) {
				echo "<script>alert('Data berhasil diubah!');</script>";
			} else {
				echo "<script>alert('Data gagal di ubah!');</script>";
			}
			
		}
		
	}

	// end post data

	//declare variables and casting to view
	
	$data = array('base_url'=>getBaseUrl(),
		'judul'=>'Edit Artikel/RumahIska',
		'package'=>'administrasi',
		'nama_admin'=>$_SESSION['admin']['nama']
	);
	$data['title'] = 'Edit Artikel';
	$data['a_title'] = $dataartikel->a_judul;
	$data['tags'] = $dataartikel->a_tags;
	$data['content'] = $dataartikel->a_isi;
	$data['image'] = @$dataartikel->a_image;
	$data = (object) $data;
	include_once HomeDir().'/view/administrasi/artikel/view-edit-artikel.php'; 

}

?>