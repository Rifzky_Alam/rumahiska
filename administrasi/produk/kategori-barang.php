<?php 
session_start();
// include_once '/Applications/XAMPP/xamppfiles/htdocs/rikza/baseurl.php';
include_once '/home/salvinai/public_html/baseurl.php';
$_SESSION['page'] = getBaseUrl().'administrasi/produk/'.basename(__FILE__,'.php');
include_once HomeDir().'model/Barang.php';
include_once HomeDir().'library/session/sessions.php';

$sesi = new MySession();
$sesi->OnlyAdmin(getBaseUrl().'administrasi/logout');
$barang = new Barang();
if (isset($_GET['act'])&&$_GET['act']=='new') {
	// echo "Add barang!";

	if (isset($_POST['in'])) {
		// print_r($_POST['in']);
		$barang->setKategori($_POST['in']['kategori']);
		if ($barang->NewKategori()) {
			echo "<script>alert('data berhasil disimpan!');</script>";
			echo "<script>location.replace('".basename(__FILE__,'.php')."');</script>";
		} else {
			echo "<script>alert('data gagal disimpan!');</script>";
		}
		
	}



	$data = array(
		'base_url' => getBaseUrl(),
		'judul' => 'Input Kategori Barang',
		'package' => 'administrasi',
		'nama_admin' => $_SESSION['admin']['nama']
	);
	$data = (object) $data;
	include_once HomeDir().'/view/administrasi/produk/view-input-kategori.php'; 
}elseif (isset($_GET['edt'])&&!empty($_GET['edt'])) {
	$barang->setKategori($_GET['edt']);
	$datakategori = $barang->FetchKategoriByID();


	if (isset($_POST['in'])) {
		// print_r($_POST['in']);
		$barang->setData($_POST['in']['kategori']);
		if ($barang->EditKategori()) {
			echo "<script>alert('data berhasil disimpan!');</script>";
			echo "<script>location.replace('".basename(__FILE__,'.php')."');</script>";
		} else {
			echo "<script>alert('data gagal disimpan!');</script>";
		}
		
	}


	$data = array(
		'base_url' => getBaseUrl(),
		'judul' => 'Edit Kategori Barang',
		'package' => 'administrasi',
		'me'=>basename(__FILE__,'.php'),
		'nama_admin' => $_SESSION['admin']['nama'],
		'kategori' => $datakategori->kb_ket
	);

	$data = (object) $data;
	include_once HomeDir().'/view/administrasi/produk/view-edit-kategori.php'; 

} else {
	$data = array(
		'base_url' => getBaseUrl(),
		'judul' => 'Data Kategori Barang',
		'package' => 'administrasi',
		'me'=>basename(__FILE__,'.php'),
		'nama_admin' => @$_SESSION['admin']['nama'],
		'list' => $barang->FetchAllKategori()
	);
	$data = (object) $data;
	include_once HomeDir().'/view/administrasi/produk/view-kategori-barang.php'; 
}


?>