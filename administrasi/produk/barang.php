<?php
session_start();
// include_once '/Applications/XAMPP/xamppfiles/htdocs/rikza/baseurl.php';
include_once '../../baseurl.php';
$_SESSION['page'] = getBaseUrl().'administrasi/produk/'.basename(__FILE__,'.php');
include_once HomeDir().'library/session/sessions.php';
$sesi = new MySession();
$sesi->OnlyAdmin(getBaseUrl().'administrasi/logout');

include_once HomeDir().'model/Barang.php';
$barang = new Barang();

if (isset($_GET['act'])&&$_GET['act']=='new') {

	if (isset($_POST['in'])) {
		// print_r($_POST['in']);
		$barang->setModeljenis($_POST['in']['jenisbarang']);
		$barang->setBerat($_POST['in']['berat']);
		$barang->setNamabarang($_POST['in']['namabarang']);
		$barang->setJumlah($_POST['in']['jumlahbarang']);
		$barang->setSatuan($_POST['in']['satuan']);

		if ($barang->NewDataBarang()) {
			echo "<script>alert('Data Berhasil disimpan!')</script>";
			echo "<script>location.replace('".basename(__FILE__,'.php')."');</script>";
		} else {
			echo "<script>alert('Data Gagal disimpan!')</script>";

		}



	}

	$data = array(
		'base_url' => getBaseUrl(),
		'judul' => 'Input Barang',
		'package' => 'administrasi',
		'nama_admin' => @$_SESSION['admin']['nama'],
		'jenisbarang' =>$barang->FetchJenisBarang(),
		'barangsatuan' => $barang->FetchBarangSatuan()
	);
	// print_r($data['barangsatuan']);
	$data = (object) $data;
	include_once HomeDir().'/view/administrasi/produk/view-input-barang2.php';

}elseif (isset($_GET['jb'])&&!empty($_GET['jb'])) {
	$data = array(
		'base_url' => getBaseUrl(),
		'judul' => 'Input Barang',
		'package' => 'administrasi',
		'nama_admin' => @$_SESSION['admin']['nama']
	);
	$data = (object) $data;
	include_once HomeDir().'/view/administrasi/produk/view-input-barang.php';

}elseif (isset($_GET['src'])&&!empty($_GET['src'])) {

	$search = array(
		'barang' => $_GET['src']['nb'],
		'jenis' => $_GET['src']['jns'],
		'kategori' => $_GET['src']['ktg']
	);

	$barang->setData((object) $search);

	include_once '/home/salvinai/public_html/classes/views/class-global.php';
	$data = new DataView();
	$data->judul = 'Data Stok Barang';
	$data->package = 'administrasi';
	$data->nama_admin = @$_SESSION['admin']['nama'];
	$data->list = $barang->SearchBarang();
	$data->me = $data->base_url.'administrasi/produk/'.basename(__FILE__,'.php');
	$data->kategori = $barang->FetchAllKategori();
	$data->jenisbarang = $barang->FetchJenisBarang();

	include_once HomeDir().'/view/administrasi/produk/view-data-barang.php';

}elseif (isset($_GET['edt'])&&!empty($_GET['edt'])) {
	$barang->setID($_GET['edt']);
	$databarang = $barang->FetchBarangByID();
	// print_r($databarang);
	if (isset($_POST['in'])) {
		// print_r($_POST['in']);
		$barang->setModeljenis($_POST['in']['jenisbarang']);
		$barang->setHarga($_POST['in']['harga']);
		$barang->setHargaagen($_POST['in']['hargaagen']);
		$barang->setBerat($_POST['in']['berat']);
		$barang->setNamabarang($_POST['in']['namabarang']);
		$barang->setJumlah($_POST['in']['jumlah']);
		$barang->setSatuan($_POST['in']['satuan']);
		$barang->setStatusbarang($_POST['in']['status']);

		if ($barang->EditDataBarang()) {
			echo "<script>alert('Data Berhasil diubah!')</script>";
			echo "<script>location.replace('".basename(__FILE__,'.php')."?src[ktg]=&src[jns]=".$databarang->id_jenis."&src[nb]=');</script>";
		} else {
			echo "<script>alert('Data Gagal diubah!')</script>";
		}

	}

	$data = array(
		'base_url' => getBaseUrl(),
		'judul' => 'Data Stok Barang',
		'package' => 'administrasi',
		'nama_admin' => @$_SESSION['admin']['nama'],
		'me_url' => getBaseUrl().'administrasi/produk/'.basename(__FILE__,'.php'),
		'id_barang' => $_GET['edt'],
		'idjenis' => $databarang->id_jenis,
		'namabarang' => $databarang->detail_jenis,
		'jumlah' => $databarang->jumlah_barang,
		'berat' => $databarang->brg_berat,
		'harga' => $databarang->brg_harga,
		'hargaagen' => $databarang->br_harga_agen,
		'satuan' => $databarang->brg_satuan,
		'status' => $databarang->brg_status,
		'listharga' => $barang->FetchDaftarharga(),
		'jenisbarang' => $barang->FetchJenisBarang(),
		'listsatuan' => $barang->FetchBarangSatuan(),
		'liststatus' => $barang->FetchBarangStatus(),
		'listjenisbarang' => $barang->FetchJenisBarang()
	);
	$data = (object) $data;
	include_once HomeDir().'/view/administrasi/produk/view-edit-barang.php';

}elseif (isset($_GET['prc'])&&!empty($_GET['prc'])) {

	$barang->setID($_GET['prc']);
	$databarang = $barang->FetchBarangByID();

	if (isset($_POST['in'])) {
		// print_r($_POST['in']);
		$input = [
			'barang' => $_GET['prc'],
			'price' => $_POST['in']['harga'],
			'deskripsi' => $_POST['in']['desc']
		];
		$barang->setHarga((object)$input);

		if ($barang->NewPrice()) {
			echo "<script>alert('Data harga berhasil disimpan!');</script>";
			echo "<script>location.replace('barang?edt=".$barang->getID()."');</script>";
		} else {
			echo "<script>alert('Data harga gagal disimpan!');</script>";		
		}

	}



	$data = array(
		'base_url' => getBaseUrl(),
		'judul' => 'Edit Harga Barang',
		'package' => 'administrasi',
		'nama_admin' => @$_SESSION['admin']['nama'],
		'me_url' => getBaseUrl().'administrasi/produk/'.basename(__FILE__,'.php'),
		'id_barang' => $_GET['prc'],
		'idjenis' => $databarang->id_jenis,
		'namabarang' => $databarang->detail_jenis,
		'harga' => $databarang->brg_harga,
		'listharga' => $barang->FetchDaftarharga()
	);

	$data = (object) $data;
	include_once HomeDir().'/view/administrasi/produk/view-edit-hargabarang.php';

}  else {
	include_once '/home/salvinai/public_html/classes/views/class-global.php';
	$data = new DataView();
	$data->judul = 'Data Stok Barang';
	$data->package = 'administrasi';
	$data->nama_admin = @$_SESSION['admin']['nama'];
	$data->list = $barang->DisplayTable();
	$data->me = $data->base_url.'administrasi/produk/'.basename(__FILE__,'.php');
	$data->kategori = $barang->FetchAllKategori();
	$data->jenisbarang = $barang->FetchJenisBarang();
	include_once HomeDir().'/view/administrasi/produk/view-data-barang.php';

}
?>
