<?php 
include_once 'Koneksi.php';

class Artikel extends Koneksi{

	private $id;
	private $judul;
	private $isi;
	private $tags;
	private $tanggal;
	private $gambar;
	private $penulis;
	private $viewer;
	private $folder;


	public function getID(){
		return $this->id;
	}

	public function setID($value){
		$this->id = $value;
	}

	public function getFolder(){
		return $this->folder;	
	}

	public function setFolder($value){
		$this->folder = $value;
	}

	public function getJudul(){
		return $this->judul;	
	}

	public function setJudul($value){
		$this->judul = $value;
	}

	public function getIsi(){
		return $this->isi;
	}

	public function setIsi($value){
		$this->isi = $value;
	}

	public function getTags(){
		return $this->tags;
	}

	public function setTags($value){
		$this->tags = $value;
	}

	public function getTanggal(){
		return $this->tanggal;
	}

	public function setTanggal($value){
		$this->tanggal = $value;
	}

	public function getGambar(){
		return $this->gambar;
	}

	public function setGambar($value){
		$this->gambar = $value;
	}

	public function getPenulis(){
		return $this->penulis;
	}

	public function setPenulis($value){
		$this->penulis = $value;
	}


	public function getViewer(){
		return $this->viewer;
	}

	public function setViewer($value){
		$this->viewer = $value;
	}
	
	function __construct(){
		$this->dbHost = $this->bukaKoneksi();
	}

	public function CreateID(){
		return md5($this->judul);
	}

	public function JsMessage($message){
		return "<script>alert('".$message."');</script>";
	}

	public function IsGambarValid(){
		$filetype = pathinfo($this->gambar['name'],PATHINFO_EXTENSION);
		if ($filetype=='jpg'||$filetype=='jpeg'||$filetype=='png') {
            return true;
        }else{
        	return false;
        } 
	}

	public function UploadImage(){
		$gambar = $this->gambar;
		$filetype = pathinfo($gambar['name'],PATHINFO_EXTENSION);
		$gambar['name'] = $this->CreateID().'.'.$filetype;
		// $folder = '/Applications/XAMPP/xamppfiles/htdocs/rikza/uploads/images/artikel'; //localhost
		$folder = '/home/salvinai/public_html/uploads/images/artikel/';
		
		if (move_uploaded_file($gambar['tmp_name'], $folder.$gambar['name'])) {
			return true;
		} else {
			return false;
		}
	}

	public function UploadImageFromEdit(){
		$gambar = $this->gambar;
		$filetype = pathinfo($gambar['name'],PATHINFO_EXTENSION);
		$gambar['name'] = $this->id.'.'.$filetype;
		// $folder = '/Applications/XAMPP/xamppfiles/htdocs/rikza/uploads/images/artikel'; //localhost
		$folder = '/home/salvinai/public_html/uploads/images/artikel/';
		
		if (move_uploaded_file($gambar['tmp_name'], $folder.$gambar['name'])) {
			return true;
		} else {
			return false;
		}
	}

	public function EditWithImage(){
		$query = "UPDATE `artikel` SET `a_judul`=:judul,`a_tags`=:tags,`a_isi`=:isi,`a_image`=:gambar,`a_author`=:petugas 
		WHERE `a_id`=:id;";
		$statement=$this->dbHost->prepare($query);
		
		@$statement->bindParam(':id',$this->id,PDO::PARAM_STR,75);
		@$statement->bindParam(':judul',$this->judul,PDO::PARAM_STR,500);
		@$statement->bindParam(':tags',$this->tags,PDO::PARAM_STR,125);
		@$statement->bindParam(':isi',$this->isi,PDO::PARAM_STR,8000);
		@$statement->bindParam(':gambar',$this->GetGambarNameForEdit($this->gambar),PDO::PARAM_STR,255);
		@$statement->bindParam(':petugas',$this->penulis,PDO::PARAM_STR,75);

		if ($statement->execute()){
			return true;
		}else{
			return false;
		}

	}

	public function EditWithoutImage(){
		$query = "UPDATE `artikel` SET `a_judul`=:judul,`a_tags`=:tags,`a_isi`=:isi,`a_author`=:petugas 
		WHERE `a_id`=:id;";
		$statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':id',$this->id,PDO::PARAM_STR,75);
		@$statement->bindParam(':judul',$this->judul,PDO::PARAM_STR,500);
		@$statement->bindParam(':tags',$this->tags,PDO::PARAM_STR,125);
		@$statement->bindParam(':isi',$this->isi,PDO::PARAM_STR,8000);
		@$statement->bindParam(':petugas',$this->penulis,PDO::PARAM_STR,75);

		if ($statement->execute()){
			return true;
		}else{
			return false;
		}
	}



	public function GetFileExtension($value){
		$filetype = pathinfo($value['name'],PATHINFO_EXTENSION);
		return $this->CreateID().'.'.$filetype;
	}

	public function GetGambarNameForEdit($value){
		$filetype = pathinfo($value['name'],PATHINFO_EXTENSION);
		return $this->id.'.'.$filetype;
	}

	public function NewData(){
		include_once 'MyXML.php';
		$xml = new MyXML();
		$xml->setDir('/home/salvinai/public_html/'); //localhost
		$xml->setNamafile('sitemap.xml');

		$query="INSERT INTO `artikel` (`a_id`, `a_judul`, `a_tags`, `a_isi`, `a_image`, `a_author`) 
		VALUES (:id, :judul, :tags, :isi, :gambar, :petugas);";
		$statement=$this->dbHost->prepare($query);
		$myid = md5($this->judul);
		@$statement->bindParam(':id',$myid,PDO::PARAM_STR,75);
		@$statement->bindParam(':judul',$this->judul,PDO::PARAM_STR,500);
		@$statement->bindParam(':tags',$this->tags,PDO::PARAM_STR,125);
		@$statement->bindParam(':isi',$this->isi,PDO::PARAM_STR,8000);
		@$statement->bindParam(':gambar',$this->GetFileExtension($this->gambar),PDO::PARAM_STR,255);
		@$statement->bindParam(':petugas',$this->penulis,PDO::PARAM_STR,75);
		
		if ($statement->execute()){
			$xml->SitemapAddArtikel($myid);
			return true;

		}else{
			return false;
		}
	}

	public function TesFungsi($value){
		return $value;
	}

	public function FetchByID(){
		$query = "SELECT `a_id`, `a_judul`, `a_tags`, `a_isi`, `a_tanggal`, `a_image`, `a_author`, `a_viewer`, `nama_user` 
		FROM `artikel` 
		INNER JOIN `users` ON `users`.`username`=`artikel`.`a_author`
		WHERE `a_id`=:id;";
		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':id',$this->id, PDO::PARAM_STR,35);

		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);
	}

	public function FetchArtikelData(){
		$query = "SELECT `a_id`, `a_judul`, `a_tags`, `a_isi`, `a_tanggal`, `a_image`, `a_author`, `a_viewer`,`nama_user` 
		FROM `artikel`
		INNER JOIN `users` ON `users`.`username`=`artikel`.`a_author`;";
		$statement = $this->dbHost->prepare($query);
		// $statement->bindParam(':id',$this->transid,PDO::PARAM_STR,35);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

}


?>