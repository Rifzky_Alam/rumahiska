<?php 
include_once 'Koneksi.php';
class File extends Koneksi{
	private $namafile;
	private $folder;
	private $ukuran;
	private $ekstensi;
	private $temp;

	function __construct(){
		$this->dbHost = $this->bukaKoneksi();
	}

	public function getNamafile(){
	    return $this->namafile;
	}

	public function setNamafile($namafile){
	    $this->namafile = $namafile;
	}

	public function getFolder(){
	    return $this->folder;
	}
	 
	public function setFolder($folder){
	    $this->folder = $folder;
	}
	public function getUkuran(){
	     return $this->ukuran;
	}

	public function setUkuran($ukuran){
	     $this->ukuran = $ukuran;
	}

	public function getEkstensi(){
	    return $this->ekstensi;
	}

	public function setEkstensi($ekstensi){
	    $this->ekstensi = $ekstensi;
	}

	public function getTemp(){
	    return $this->temp;
	}

	public function setTemp($temp){
	    $this->temp = $temp;
	}

	public function Banners(){
		$query = "SELECT * FROM `files_data` WHERE `fd_src` LIKE 'banner%' AND `fd_filename`!='-'";
		$statement=$this->dbHost->prepare($query);

		$statement->execute();
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}

	public function uploadFile(){
		if(move_uploaded_file($this->getTemp(), $this->getFolder().$this->getNamafile())){
			return "<script>alert('File berhasil di upload!')</script>";
		}else{
			return "<script>alert('File gagal di upload!')</script>";
		}
	
	}

}



?>