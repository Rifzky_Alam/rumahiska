<?php 
include_once 'Koneksi.php';
class Customer extends Koneksi{
    private $email;
    private $password;
    private $nama;
    private $telepon;
    private $alamat;
    private $data;

    function __construct(){
		$this->dbHost = $this->bukaKoneksi();
	}

    public function getDaftarAlamat($email,$status){
        $query = "SELECT adr_id AS idalamat,adr_alamat AS alamat, dftr_prov.namaprov AS prov, dftr_kota.namakota AS kota, 
        dftr_kec.namakec AS kec,adr_kode_pos AS kodepos,adr_notes AS notes FROM `ri_alamat` 
        LEFT JOIN dftr_prov ON dftr_prov.idprov=ri_alamat.adr_prov
        LEFT JOIN dftr_kota ON dftr_kota.idkota=ri_alamat.adr_kota
        LEFT JOIN dftr_kec ON dftr_kec.idkec=ri_alamat.adr_kec
        WHERE ri_alamat.adr_source=:email
        AND ri_alamat.adr_status=:status";
        $statement=$this->dbHost->prepare($query);
        $statement->bindParam(':email',$email,PDO::PARAM_STR,255);
 		$statement->bindParam(':status',$status,PDO::PARAM_STR,75);
 		
 		$statement->execute();
		return $statement->fetchAll(PDO::FETCH_ASSOC);
    }


	public function NewFeedBack($subjek){
		$query = "INSERT INTO `feedback_cust` (`fc_id`, `fc_email`, `fc_nama`, `fc_subjek`, `fc_pesan`) 
		VALUES (NULL, :email, :nama, :subjek, :pesan);";
		$statement=$this->dbHost->prepare($query);

 		$statement->bindParam(':email',$this->email,PDO::PARAM_STR,255);
 		$statement->bindParam(':nama',$this->nama,PDO::PARAM_STR,75);
 		$statement->bindParam(':subjek',$subjek,PDO::PARAM_STR,25);
 		$statement->bindParam(':pesan',$this->data,PDO::PARAM_STR,2000);

 		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	public function GetWhereCustomer(){
		$query = "SELECT * FROM `ri_customer` WHERE rc_email=:email;";
		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':email',$this->email,PDO::PARAM_STR,35);

		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);
	}

	public function SearchCust($arr){
		$value = "%".$arr['value']."%";
		$query = "SELECT `rc_email`,`rc_nama`,`rc_telepon`,`rc_alamat`  
		FROM `ri_customer` WHERE ".$arr['atr']." LIKE :value;";
		$statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':value',$value,PDO::PARAM_STR,125);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function GetCustomer(){
		$query = "SELECT * FROM `ri_customer`;";
		$statement=$this->dbHost->prepare($query);

		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}


	public function Fill(){
		$query="SELECT `rc_email`, `rc_password`, `rc_nama`, `rc_telepon`, `rc_alamat`,`adr_alamat`
		FROM `ri_customer` 
		INNER JOIN `ri_alamat` ON `ri_alamat`.`adr_source`=`ri_customer`.`rc_email`
		WHERE `rc_email`=:email;";
		$statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':email',$this->email,PDO::PARAM_STR,35);
		$statement->execute();
		$hasil = $statement->fetch(PDO::FETCH_OBJ);

		@$this->email=$hasil->rc_email;
		@$this->password=$hasil->rc_password;
		@$this->nama=$hasil->rc_nama;
		@$this->telepon=$hasil->rc_telepon;
		@$this->alamat=$hasil->rc_alamat;
	}

	public function EditData()	{
		$query = "UPDATE `ri_customer` 
		SET `rc_email`=:uemail,
		`rc_password`=:password,
		`rc_nama`=:nama,
		`rc_telepon`=:telepon,
		`rc_alamat`=:alamat 
		WHERE `rc_email`=:email;
		UPDATE `ri_alamat` SET `adr_source`=:src WHERE `adr_source`=:adrsrc;";
		$statement=$this->dbHost->prepare($query);

 		$statement->bindParam(':email',$this->email,PDO::PARAM_STR,255);
 		$statement->bindParam(':uemail',$this->data,PDO::PARAM_STR,255);
 		$statement->bindParam(':password',$this->password,PDO::PARAM_STR,40);
 		$statement->bindParam(':nama',$this->nama,PDO::PARAM_STR,75);
 		$statement->bindParam(':telepon',$this->telepon,PDO::PARAM_STR,25);
 		$statement->bindParam(':alamat',$this->alamat,PDO::PARAM_STR,32);
 		$statement->bindParam(':src',$this->data,PDO::PARAM_STR,35);
 		$statement->bindParam(':adrsrc',$this->email,PDO::PARAM_STR,35);

 		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	public function NewAddress(){
		$query="INSERT INTO `ri_alamat`(`adr_source`, `adr_alamat`) 
		VALUES (:adrsrc,:alamat);";
		$statement=$this->dbHost->prepare($query);
		
		$statement->bindParam(':adrsrc',$this->email,PDO::PARAM_STR,35);
 		$statement->bindParam(':alamat',$this->alamat,PDO::PARAM_STR,500);

 		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	public function FetchAlamat(){
		$query = "SELECT `adr_id`, `adr_source`, `adr_alamat`, `date_created` 
		FROM `ri_alamat` 
		WHERE `adr_source`=:email;";
		$statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':email',$this->email,PDO::PARAM_STR,35);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function NewCustomer(){
		$query = "INSERT INTO `ri_customer` (`rc_email`, `rc_password`, `rc_nama`, `rc_telepon`) 
		VALUES (:email, :password, :nama, :telepon);
		INSERT INTO `ri_alamat` (`adr_source`, `adr_alamat`) 
		VALUES (:alamatsrc, :alamat);";
		$statement=$this->dbHost->prepare($query);

 		$statement->bindParam(':email',$this->email,PDO::PARAM_STR,255);
 		$statement->bindParam(':password',$this->password,PDO::PARAM_STR,40);
 		$statement->bindParam(':nama',$this->nama,PDO::PARAM_STR,75);
 		$statement->bindParam(':telepon',$this->telepon,PDO::PARAM_STR,25);

 		$statement->bindParam(':alamatsrc',$this->email,PDO::PARAM_STR,35);
 		$statement->bindParam(':alamat',$this->alamat,PDO::PARAM_STR,500);

 		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	
	}
	

    public function getEmail(){
        return $this->email;
    }

	public function setEmail($email){
        $this->email = $email;
    }

    public function getPassword(){
        return $this->password;
    }

    public function setPassword($password){
        $this->password = $password;
    }

    public function getNama(){
        return $this->nama;
    }

    public function setNama($nama){
        $this->nama = $nama;
    }

    public function getAlamat(){
        return $this->alamat;
    }

    public function setAlamat($alamat){
        $this->alamat = $alamat;

    }

    public function getTelepon(){
        return $this->telepon;
    }

    public function setTelepon($telepon){
        $this->telepon = $telepon;
    }

    public function getData(){
        return $this->data;
    }

    public function setData($data){
        $this->data = $data;
    }
}