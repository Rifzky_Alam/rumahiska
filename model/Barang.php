<?php
include_once 'Koneksi.php';
class Barang extends Koneksi{
	private $id;
	private $kategori;
	private $modeljenis;
	private $namabarang;
	private $harga;
	private $hargaagen;
	private $berat;
	private $satuan;
	private $statusbarang;
	private $jumlah;
	private $gambar;
	private $catatan;
	private $data;

	function __construct(){
		$this->dbHost = $this->bukaKoneksi();
	}

	// images
	public function IsGambarValid(){
		$filetype = pathinfo($this->gambar['name'],PATHINFO_EXTENSION);
		if ($filetype=='jpg'||$filetype=='jpeg'||$filetype=='png'||$filetype=='JPG'||$filetype=='PNG') {
            return true;
        }else{
        	return false;
        } 
	}

	public function UploadImage(){
		$gambar = $this->gambar;
		$filetype = pathinfo($gambar['name'],PATHINFO_EXTENSION);
		$gambar['name'] = $this->id.'-'.str_replace(' ', '',$gambar['name']);
		// $folder = '/Applications/XAMPP/xamppfiles/htdocs/rikza/uploads/images/produk/'; //localhost
		$folder = '/home/salvinai/public_html/uploads/images/produk/';
		
		if (move_uploaded_file($gambar['tmp_name'], $folder.$gambar['name'])) {
			return true;
		} else {
			return false;
		}
	}


	public function NewImage(){
		$gbr = $this->gambar;
		$pic = $this->id.'-'.str_replace(' ','',$gbr['name']);
		$query = "INSERT INTO `barang_gambar` (`bg_source`,`bg_name`,`bg_ket`) 
		VALUES(:source,:namafile,:keterangan);";
		$statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':source',$this->id,PDO::PARAM_STR,35);
 		@$statement->bindParam(':namafile',$pic,PDO::PARAM_STR,75);
 		@$statement->bindParam(':keterangan',$this->catatan,PDO::PARAM_STR,75);

 		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	public function EditKeteranganGambar(){
		$query = "UPDATE `barang_gambar` 
		SET `bg_ket` = :keterangan 
		WHERE `barang_gambar`.`bg_id` = :idimg;";
		$statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':idimg',$this->gambar,PDO::PARAM_STR,35);
		@$statement->bindParam(':keterangan',$this->catatan,PDO::PARAM_STR,255);

		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}

	}

	public function SetDefaultImage(){
		$query = "UPDATE `data_barang` 
		SET `brg_pic` = :idgbr 
		WHERE `data_barang`.`id` = :id;";
		$statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':id',$this->id,PDO::PARAM_STR,35);
		@$statement->bindParam(':idgbr',$this->gambar,PDO::PARAM_STR,35);

		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	public function DeleteImage(){
		$query = "DELETE FROM `barang_gambar` WHERE `barang_gambar`.`bg_id`=:id;";
		$statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':id',$this->gambar,PDO::PARAM_STR,35);
		if ($statement->execute()) {
			unlink('/home/salvinai/public_html/uploads/images/produk/'.$this->catatan);
			return true;
		}else{
			return false;
		}
	}	

	public function FetchGambarByID(){
		$query = "SELECT `bg_id`, `bg_source`, `bg_name`,`bg_ket` 
		FROM `barang_gambar` WHERE `bg_id`=:id;";
		$statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':id',$this->gambar,PDO::PARAM_STR,35);		

		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);
	}

	public function FetchBarangByJenisBarangID($idjenis){
		$query ="SELECT `bg_id`, `bg_source`, `bg_name`, `bg_ket` 
		FROM `barang_gambar`
		INNER JOIN `data_barang`
		ON `data_barang`.`id` = `barang_gambar`.`bg_source`
		INNER JOIN `jenis_barang`
		ON `jenis_barang`.`id`= `data_barang`.`id_jenis`
		WHERE `jenis_barang`.`id` = :idjenis";
		$statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':idjenis',$idjenis,PDO::PARAM_STR,35);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz = json_encode($results);
		return json_decode($dataz);
	}

	public function FetchDataGambar(){
		$query = "SELECT `bg_id`, `bg_source`, `bg_name`,`bg_ket` 
		FROM `barang_gambar` WHERE `bg_source`=:id;";
		$statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':id',$this->id,PDO::PARAM_STR,35);

		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz = json_encode($results);
		return json_decode($dataz);
	}

	// end images

	// barang

	// @display produk (url= "/produk")
	public function DisplayDataBarang(){
		$query = "SELECT `jenis_barang`.`id` AS `aidi`, `kb_ket`,`jenis_barang`, `bg_name`, `th_harga`, `jb_status` 
		FROM `jenis_barang` 
		INNER JOIN `kategori_barang`
		ON `kategori_barang`.`kb_id`=`jenis_barang`.`kb_id`
		LEFT JOIN `data_barang`
		ON `data_barang`.`id`=`jenis_barang`.`jb_def_item`
		LEFT JOIN `barang_gambar`
		ON `barang_gambar`.`bg_id`=`data_barang`.`brg_pic`
		LEFT JOIN `tabel_harga`
		ON `tabel_harga`.`th_id`=`data_barang`.`brg_harga` 
		WHERE `data_barang`.`brg_status`>'0'
        AND `jenis_barang`.`jb_status`>'-1'";
		$statement=$this->dbHost->prepare($query);

		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	// @display html title (url="/produk/jenis/${idjenis}")
	public function getNamaJenisBarang($jenisbarang){
		$query = "SELECT `jenis_barang` FROM `jenis_barang` WHERE `jenis_barang`.`id`=:jenisbarang";
		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':jenisbarang',$jenisbarang,PDO::PARAM_STR,8);

		$statement->execute();
		$r = $statement->fetch(PDO::FETCH_ASSOC);
		return $r['jenis_barang'];
	}

	public function DisplayProduk($ket){
		$query = "SELECT `jenis_barang`.`id` AS `jb_id`,`kb_ket`, `jenis_barang`,`detail_jenis`, `th_harga`,`bg_name`, `jb_status` 
		FROM `jenis_barang`
		INNER JOIN `kategori_barang` ON `kategori_barang`.`kb_id`=`jenis_barang`.`kb_id`
		INNER JOIN `data_barang` ON `data_barang`.`id`=`jenis_barang`.`jb_def_item`
		INNER JOIN `tabel_harga` ON `tabel_harga`.`th_id`=`data_barang`.`brg_harga`
		LEFT JOIN `barang_gambar` ON `barang_gambar`.`bg_id`=`data_barang`.`brg_pic`
		WHERE `jenis_barang`.`jb_status` = :ket;";
		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':ket',$ket,PDO::PARAM_STR,8);

		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}


	public function NewPrice(){
		$query = "INSERT INTO `tabel_harga` (`th_id`, `th_src`, `th_harga`, `th_desc`) 
		VALUES (:id, :idbarang, :hargabarang, :descbarang);";
		$statement=$this->dbHost->prepare($query);
		$id = $this->harga->barang.$this->harga->price;
		@$statement->bindParam(':id',$id,PDO::PARAM_STR,35);
 		@$statement->bindParam(':idbarang',$this->harga->barang,PDO::PARAM_STR,75);
		@$statement->bindParam(':hargabarang',$this->harga->price,PDO::PARAM_STR,8);
 		@$statement->bindParam(':descbarang',$this->harga->deskripsi,PDO::PARAM_STR,255);

 		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}

	}

	public function NewDataBarang(){
		$query = "INSERT INTO `data_barang`(`id_jenis`, `detail_jenis`, `brg_berat`, `brg_satuan`, `jumlah_barang`)
		VALUES (:idjenis,:detailjenis,:berat,:satuan,:jumlah);";
		$statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':idjenis',$this->modeljenis,PDO::PARAM_STR,35);
 		@$statement->bindParam(':detailjenis',$this->namabarang,PDO::PARAM_STR,75);
		@$statement->bindParam(':berat',$this->berat,PDO::PARAM_STR,7);
 		@$statement->bindParam(':satuan',$this->satuan,PDO::PARAM_STR,3);
		@$statement->bindParam(':jumlah',$this->jumlah,PDO::PARAM_STR,5);

		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	public function EditDataBarang(){
		$query = "UPDATE `data_barang` 
		SET `id_jenis` = :idjenis, 
		`detail_jenis`=:detailjenis, 
		`brg_harga`=:harga, 
		`br_harga_agen`=:hargaagen,
		`brg_berat`=:berat,
		`brg_satuan`=:satuan, 
		`jumlah_barang`=:jumlah,
		`brg_status`=:status
		WHERE `id`=:id;";
		$statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':idjenis',$this->modeljenis,PDO::PARAM_STR,35);
 		@$statement->bindParam(':detailjenis',$this->namabarang,PDO::PARAM_STR,75);
		@$statement->bindParam(':harga',$this->harga,PDO::PARAM_STR,8);
		@$statement->bindParam(':hargaagen',$this->hargaagen,PDO::PARAM_STR,12);
		@$statement->bindParam(':berat',$this->berat,PDO::PARAM_STR,7);
 		@$statement->bindParam(':satuan',$this->satuan,PDO::PARAM_STR,3);
		@$statement->bindParam(':jumlah',$this->jumlah,PDO::PARAM_STR,5);
		@$statement->bindParam(':status',$this->statusbarang,PDO::PARAM_STR,15);
		
		@$statement->bindParam(':id',$this->id,PDO::PARAM_STR,35);

		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	// @display produk (url="/produk/neworder")
	public function FetchKetJenisBarang($id){
		$query = "SELECT `kb_ket`,`jenis_barang`,`th_harga` AS brg_harga
		FROM `jenis_barang` 
		INNER JOIN `kategori_barang`
		ON `kategori_barang`.`kb_id` = `jenis_barang`.`kb_id`
        LEFT JOIN `data_barang`
        ON `data_barang`.`id`=`jenis_barang`.`jb_def_item`
        LEFT JOIN `tabel_harga`
        ON `tabel_harga`.`th_id`=`data_barang`.`brg_harga`
		WHERE `jenis_barang`.`id` = :jenisbarangid;";
		$statement= $this->dbHost->prepare($query);
		@$statement->bindParam(':jenisbarangid',$id,PDO::PARAM_STR,35);
		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);
	}

	public function FetchBarangByID(){
		$query = "SELECT `data_barang`.`id` AS `id`, `kb_ket`, `id_jenis`, `jenis_barang`, `brg_berat`, `detail_jenis`, `brg_harga`, `brg_pic`, `br_harga_agen`, `brg_satuan`, `jumlah_barang`, `brg_status`,`bs_ket`
		FROM `data_barang` 
		INNER JOIN `jenis_barang` ON `jenis_barang`.`id`=`data_barang`.`id_jenis`
		INNER JOIN `kategori_barang` ON `kategori_barang`.`kb_id`= `jenis_barang`.`kb_id`
		INNER JOIN barang_satuan ON `barang_satuan`.`bs_id` = `data_barang`.`brg_satuan`
		WHERE `data_barang`.`id`= :id;";
		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':id',$this->id,PDO::PARAM_STR,35);

		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);

	}

	public function SelectForChart(){
		$query = "SELECT `db`.`id` AS `brg_id`, `kb_ket`,`jenis_barang`, `detail_jenis`,`th_id`,`th_harga` AS `brg_harga`, `brg_berat`, `barang_satuan`.`bs_ket` AS `brg_satuan`, `bg_name`, `jumlah_barang`
		FROM `data_barang` AS `db`
		INNER JOIN `jenis_barang` ON `jenis_barang`.`id` = `db`.`id_jenis`
		INNER JOIN kategori_barang ON `kategori_barang`.`kb_id` = `jenis_barang`.`kb_id`
		INNER JOIN barang_satuan ON `barang_satuan`.`bs_id` = `db`.`brg_satuan`
		LEFT JOIN `tabel_harga` ON `tabel_harga`.`th_id` = `db`.`brg_harga`
		LEFT JOIN `barang_gambar` ON `barang_gambar`.`bg_id`=`db`.`brg_pic` 
		WHERE `db`.`id` = :id;";
		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':id',$this->id,PDO::PARAM_STR,35);

		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);
	}
	
	public function SelectForChartAgen(){
		$query = "SELECT `db`.`id` AS `brg_id`, `kb_ket`,`jenis_barang`, `detail_jenis`,`th_id`,`th_harga` AS `brg_harga`, `brg_berat`, `barang_satuan`.`bs_ket` AS `brg_satuan`, `bg_name`, `jumlah_barang`
		FROM `data_barang` AS `db`
		INNER JOIN `jenis_barang` ON `jenis_barang`.`id` = `db`.`id_jenis`
		INNER JOIN kategori_barang ON `kategori_barang`.`kb_id` = `jenis_barang`.`kb_id`
		INNER JOIN barang_satuan ON `barang_satuan`.`bs_id` = `db`.`brg_satuan`
		LEFT JOIN `tabel_harga` ON `tabel_harga`.`th_id` = `db`.`br_harga_agen`
		LEFT JOIN `barang_gambar` ON `barang_gambar`.`bg_id`=`db`.`brg_pic` 
		WHERE `db`.`id` = :id;";
		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':id',$this->id,PDO::PARAM_STR,35);

		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);
	}

	public function FetchDaftarharga(){
		$query = "SELECT th_id,th_harga,th_desc FROM `tabel_harga` WHERE `th_src`=:idbarang;";
		$statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':idbarang',$this->id,PDO::PARAM_STR,35);

		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function DisplayTable(){
		$query = "SELECT `db`.`id` AS `brg_id`, `kb_ket`,`jenis_barang`, `detail_jenis`,`th_harga` AS `brg_harga`, `barang_satuan`.`bs_ket` AS `brg_satuan`, `jumlah_barang`
		FROM `data_barang` AS `db`
		INNER JOIN `jenis_barang` ON `jenis_barang`.`id` = `db`.`id_jenis`
		INNER JOIN `kategori_barang` ON `kategori_barang`.`kb_id` = `jenis_barang`.`kb_id`
		INNER JOIN `barang_satuan` ON `barang_satuan`.`bs_id` = `db`.`brg_satuan`
        LEFT JOIN `tabel_harga` ON `tabel_harga`.`th_id` = `db`.`brg_harga`;";
		$statement=$this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function FetchBarangSatuan(){
		$query = "SELECT `bs_id`, `bs_ket` FROM `barang_satuan`";
		$statement=$this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function FetchBarangStatus(){
		$query = "SELECT `bsk_id`, `bsk_ket` FROM `brg_status_ket`;";
		$statement=$this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function BarangSearch($arr){
		$query = "SELECT `id`, `id_jenis`, `detail_jenis`, `brg_berat`, `brg_harga`, `brg_satuan`, `jumlah_barang`, `brg_pic`, `brg_status` 
		FROM `data_barang` 
		WHERE ".$arr['atr']." ".$arr['cond']." :value;";
		// echo $query;
		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':value',$arr['value'],PDO::PARAM_STR,125);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function SearchBarang(){
		$namabarang = "%".$this->data->barang."%";
		$query = "SELECT `db`.`id` AS `brg_id`, `kb_ket`,`jenis_barang`, `detail_jenis`, `th_harga` AS `brg_harga`, `brg_satuan`, `jumlah_barang`
		FROM `data_barang` AS `db`
        INNER JOIN `jenis_barang` AS `jb` ON `jb`.`id` = `db`.`id_jenis`
        INNER JOIN `kategori_barang` AS `kb` ON `kb`.`kb_id` = `jb`.`kb_id`
        LEFT JOIN `tabel_harga` ON `th_id` = `db`.`brg_harga`
		WHERE `detail_jenis` LIKE :namabarang";

		if ($this->data->kategori!='') {
			$query .= " AND `kb`.`kb_id`='".$this->data->kategori."'";
		}

		if ($this->data->jenis!='') {
			$query .= " AND `db`.`id_jenis`='".$this->data->jenis."'";
		}

		$query .= ";";
		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':namabarang',$namabarang,PDO::PARAM_STR,3);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	// end barang


	//kategori

	public function FetchSidebarClient(){
		$query = "SELECT `kategori_barang`.`kb_id` AS `aidi`, `kb_ket`, 
		(SELECT GROUP_CONCAT(`id` SEPARATOR ' # ') FROM `jenis_barang` WHERE `jenis_barang`.`kb_id`=`aidi`) AS `id_jenis`,
		(SELECT GROUP_CONCAT(`jenis_barang` SEPARATOR ' # ') FROM `jenis_barang` WHERE `jenis_barang`.`kb_id`=`aidi`) AS `jenis_barang` 
		FROM `kategori_barang`;";
		$statement=$this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function HitungKategori(){
		$query = "SELECT COUNT(`kb_id`) AS `jumlah` FROM `kategori_barang`;";
		$statement=$this->dbHost->prepare($query);
		// @$statement->bindParam(':id',$this->tanggal, PDO::PARAM_STR,35);


		$statement->execute();
		$hasil = $statement->fetch(PDO::FETCH_OBJ);
		return $hasil->jumlah;
	}


	public function KategoriCreateID($value){
		return 'K'.$value + 1;
	}


	public function NewKategori(){
		$query = "INSERT INTO `kategori_barang` (`kb_id`, `kb_ket`) VALUES (NULL, :kategori);";
		$statement = $this->dbHost->prepare($query);
		$id = $this->HitungKategori();
		// @$statement->bindParam(':id',$id,PDO::PARAM_STR,3);
		@$statement->bindParam(':kategori',$this->kategori,PDO::PARAM_STR,75);
		// echo $id;
		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}

	}

	public function EditKategori(){
		$query = "UPDATE `kategori_barang` SET `kb_ket`=:kategori WHERE `kb_id`=:id;";
		$statement = $this->dbHost->prepare($query);

		@$statement->bindParam(':id',$this->kategori,PDO::PARAM_STR,3);
		@$statement->bindParam(':kategori',$this->data,PDO::PARAM_STR,75);

		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	public function FetchKategoriByID(){
		$query = "SELECT * FROM `kategori_barang` WHERE `kb_id`=:id;";
		$statement = $this->dbHost->prepare($query);

		@$statement->bindParam(':id',$this->kategori,PDO::PARAM_STR,3);
		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);

	}


	public function FetchAllKategori(){
		$query = "SELECT * FROM `kategori_barang`;";
		$statement = $this->dbHost->prepare($query);

		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}
	// end kategori

	// jenis barang

	public function JenisBarangCreateID($value){
		return md5($value);
	}

	public function DisplayToClient($arr){
		$query = "SELECT `jenis_barang`.`id` AS `id`, `kb_ket`, `jenis_barang`, `detail_jenis`, `bg_name`, `jumlah_barang`, `bs_ket`, `jb_def_item`, `jb_status` 
		FROM `jenis_barang`
		INNER JOIN `kategori_barang` ON `kategori_barang`.`kb_id`=`jenis_barang`.`kb_id`
		LEFT JOIN `data_barang` ON `data_barang`.`id`=`jenis_barang`.`jb_def_item`
		LEFT JOIN `barang_gambar` ON `barang_gambar`.`bg_id` = `data_barang`.`brg_pic`
		LEFT JOIN `barang_satuan` ON `barang_satuan`.`bs_id`=`data_barang`.`brg_satuan`
		WHERE `jb_status`='0';";
	}

	public function NewJenisBarang(){
		$query = "INSERT INTO `jenis_barang` (`id`, `jenis_barang`, `kb_id`,`jb_status`)
		VALUES (:id, :jenisbarang, :idkategori,:status);";
		$statement = $this->dbHost->prepare($query);

		@$statement->bindParam(':id',$this->JenisBarangCreateID($this->modeljenis),PDO::PARAM_STR,3);
		@$statement->bindParam(':jenisbarang',$this->modeljenis,PDO::PARAM_STR,75);
		@$statement->bindParam(':idkategori',$this->kategori,PDO::PARAM_STR,3);
		@$statement->bindParam(':status',$this->statusbarang,PDO::PARAM_STR,3);

		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	public function EditJenisBarang(){
		$query = "UPDATE `jenis_barang` SET jenis_barang=:jenisbarang, `kb_id`=:kategori, `jb_def_item`=:defitem,`jb_status`=:status WHERE `id`=:id;";
		$statement = $this->dbHost->prepare($query);

		$statement->bindParam(':id',$this->data,PDO::PARAM_STR,35);
		$statement->bindParam(':jenisbarang',$this->modeljenis,PDO::PARAM_STR,75);
		$statement->bindParam(':defitem',$this->id,PDO::PARAM_STR,75);
		$statement->bindParam(':kategori',$this->kategori,PDO::PARAM_STR,3);
		$statement->bindParam(':status',$this->statusbarang,PDO::PARAM_STR,3);

		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	public function FetchStatusJenisBarang(){
		$query = "SELECT * FROM `jb_status`;";
		$statement = $this->dbHost->prepare($query);

		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function FetchJenisBarangByID(){
		$query = "SELECT `jenis_barang`, `kb_id`, `jb_def_item`,`jb_status` FROM `jenis_barang` WHERE `id`=:id;";
		$statement = $this->dbHost->prepare($query);

		$statement->bindParam(':id',$this->modeljenis,PDO::PARAM_STR,3);
		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);

	}

	public function FetchDetailBarangByJenisBarang(){
		$query = "SELECT `data_barang`.`id` AS `id`, `id_jenis` AS `aidi`, `kb_ket`,`jenis_barang`,`detail_jenis`, `brg_berat`, `th_harga`,`brg_harga`, `bs_ket`, `jumlah_barang`, `bg_name`, `brg_status`
		FROM `data_barang`
		INNER JOIN `jenis_barang` ON `jenis_barang`.`id`=`data_barang`.`id_jenis`
        INNER JOIN `kategori_barang` ON `kategori_barang`.`kb_id`=`jenis_barang`.`kb_id`
        LEFT JOIN `barang_satuan` ON `barang_satuan`.`bs_id`=`data_barang`.`brg_satuan`
        LEFT JOIN `barang_gambar` ON `barang_gambar`.`bg_id` = `data_barang`.`brg_pic`
        LEFT JOIN `tabel_harga` ON `tabel_harga`.`th_id` = `data_barang`.`brg_harga`
        LEFT JOIN `jb_status` ON `jb_status`.`jbs_id`=`jenis_barang`.`jb_status`
		WHERE `id_jenis`=:jenisbarang 
		AND `jb_status`.`jbs_id`> '-1' 
		AND `data_barang`.`brg_status`>'0'";
		$statement = $this->dbHost->prepare($query);

		$statement->bindParam(':jenisbarang',$this->modeljenis,PDO::PARAM_STR,35);

		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);		
	}
	
	public function FetchDetailBarangByJenisBarangDist(){
		$query = "SELECT `data_barang`.`id` AS `id`, `id_jenis`, `kb_ket`,`jenis_barang`,`detail_jenis`, `brg_berat`, `th_harga`,`brg_harga`, `bs_ket`, `jumlah_barang`, `bg_name`, `brg_status`
		FROM `data_barang`
		INNER JOIN `jenis_barang` ON `jenis_barang`.`id`=`data_barang`.`id_jenis`
        INNER JOIN `kategori_barang` ON `kategori_barang`.`kb_id`=`jenis_barang`.`kb_id`
        LEFT JOIN `barang_satuan` ON `barang_satuan`.`bs_id`=`data_barang`.`brg_satuan`
        LEFT JOIN `barang_gambar` ON `barang_gambar`.`bg_id` = `data_barang`.`brg_pic`
        LEFT JOIN `tabel_harga` ON `tabel_harga`.`th_id` = `data_barang`.`br_harga_agen`
		WHERE `id_jenis`=:jenisbarang";
		$statement = $this->dbHost->prepare($query);

		$statement->bindParam(':jenisbarang',$this->modeljenis,PDO::PARAM_STR,35);

		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);		
	}


	public function FetchJenisBarang(){
		$query = "SELECT `id`, `jenis_barang`, `kb_ket`,`jbs_ket`
		FROM `jenis_barang` 
		INNER JOIN `kategori_barang` ON `jenis_barang`.`kb_id`=`kategori_barang`.`kb_id`
		INNER JOIN jb_status ON `jb_status`.`jbs_id`=`jenis_barang`.`jb_status` ;";
		$statement = $this->dbHost->prepare($query);

		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	// end jenis barang

	public function getID(){
		return $this->id;
	}

	public function setID($value){
		$this->id = $value;
	}

	public function getData(){
		return $this->data;
	}

	public function setData($value){
		$this->data = $value;
	}

	public function getCatatan(){
		return $this->catatan;
	}

	public function setCatatan($value){
		$this->catatan = $value;
	}

	public function getGambar(){
		return $this->gambar;
	}

	public function setGambar($value){
		$this->gambar = $value;
	}

	public function getJumlah(){
		return $this->jumlah;
	}

	public function setJumlah($value){
		$this->jumlah = $value;
	}

	public function getSatuan(){
		return $this->satuan;
	}

	public function setSatuan($value){
		$this->satuan = $value;
	}

	public function getHarga(){
		return $this->harga;
	}

	public function setHarga($value){
		$this->harga = $value;
	}

	public function getBerat(){
		return $this->berat;
	}

	public function setBerat($value){
		$this->berat = $value;
	}

	public function getNamabarang(){
		return $this->namabarang;
	}

	public function setNamabarang($value){
		$this->namabarang = $value;
	}

	public function getModeljenis(){
		return $this->modeljenis;
	}

	public function setModeljenis($value){
		$this->modeljenis = $value;
	}

	public function getKategori(){
		return $this->kategori;
	}

	public function setKategori($value){
		$this->kategori = $value;
	}

    public function getStatusbarang(){
        return $this->statusbarang;
    }

    public function setStatusbarang($statusbarang){
        $this->statusbarang = $statusbarang;
    }

    public function getHargaagen(){
        return $this->hargaagen;
    }

    public function setHargaagen($hargaagen){
        $this->hargaagen = $hargaagen;
    }
}


?>
