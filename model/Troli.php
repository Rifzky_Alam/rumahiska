<?php 
include_once 'Koneksi.php';
class Troli extends Koneksi{
	private $dbHost;
	public function __construct(){
        $this->dbHost = $this->bukaKoneksi();
    }
	public function getTroliData($jenis){
		$query = "SELECT `kategori_barang`.`kb_ket` AS `kategori`, `jenis_barang`.`jenis_barang` AS `jenis`,`data_barang`.`id` AS `id_brg`, `data_barang`.`detail_jenis` AS `dtl_brg`, `data_barang`.`jumlah_barang` AS `jumlah`, `barang_satuan`.`bs_ket` AS `satuan` 
		FROM `kategori_barang`
		INNER JOIN `jenis_barang` ON `jenis_barang`.`kb_id`=`kategori_barang`.`kb_id`
		INNER JOIN `data_barang` ON `jenis_barang`.`id`=`data_barang`.`id_jenis`
		INNER JOIN `barang_satuan` ON `barang_satuan`.`bs_id`=`data_barang`.`brg_satuan`
		WHERE `jenis_barang`.`id`='$jenis'
";
		$statement=$this->dbHost->prepare($query);
		$statement->execute();
		// return $query;
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}

	public function getHarga($idbarang){
		$query = "SELECT `tabel_harga`.`th_harga` AS `harga` 
		FROM `data_barang`
		INNER JOIN `tabel_harga`
		ON `tabel_harga`.`th_id` = `data_barang`.`brg_harga`
		WHERE `data_barang`.`id` = :idbarang;";
		$statement=$this->dbHost->prepare($query);

		$statement->bindParam(':idbarang',$idbarang,PDO::PARAM_STR,35);

		$statement->execute();
		// return $query;
		return $statement->fetch(PDO::FETCH_ASSOC);
	}
}