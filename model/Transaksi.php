<?php 
include_once 'Koneksi.php';
class Transaksi extends Koneksi{
    
    protected $id;
    protected $customer;
    protected $tanggal;
    protected $status;
    protected $keranjang;
    protected $data;




    function __construct(){
		$this->dbHost = $this->bukaKoneksi();
	}

    // Transaksi
	function CreateIDTr(){
		$query = "SELECT COUNT(`tr_id`) + 1 AS `jumlah` FROM `ri_transaksi`;";
		$statement=$this->dbHost->prepare($query);

		$statement->execute();
		$hasil = $statement->fetch(PDO::FETCH_OBJ);
		return 'Rit-'.$this->CreateNumber($hasil->jumlah);
	}

	function CreateNumber($value){
		if ($value<10) {
			return '00000'.$value;
		}elseif ($value<100) {
			return '0000'.$value;
		}elseif ($value<1000) {
			return '000'.$value;
		}elseif ($value<10000) {
			return '00'.$value;
		}elseif ($value<100000) {
			return '0'.$value;
		}elseif ($value<1000000) {
			return $value;
		}
	}

	public function GetHistoriTransaksi($emailmember){
		$query = "SELECT `tr_id`,`tr_vcr`,`tr_discount`,`tr_status`,
		( 
			SELECT SUM(`ri_carts`.`qty`*`ri_carts`.`cart_itm_price`) FROM ri_carts 
			WHERE ri_carts.cart_source=tr_id
		) AS `total_harga_item`,
		(
            SELECT GROUP_CONCAT(`data_barang`.`detail_jenis` SEPARATOR ' # ') FROM data_barang
        	INNER JOIN ri_carts ON ri_carts.cart_items=data_barang.id
            WHERE ri_carts.cart_source=tr_id
        ) AS `item`,
        `ri_kurir`.`rk_resi` AS `resi`,`ri_transaksi`.`tr_tanggal` AS `tgl_trans`,
        `ri_kurir`.`rk_kode_jasa` AS `kurir`
		FROM `ri_transaksi`
        INNER JOIN `ri_kurir` ON `ri_kurir`.`rk_id_trans`=`ri_transaksi`.`tr_id`
		WHERE ri_transaksi.tr_id_cust=:email;";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':email',$emailmember,PDO::PARAM_STR,255);
 		$statement->execute();
 		$r = $statement->fetchAll(PDO::FETCH_ASSOC);
 		return $r;
	}

	public function GetDataAlamatbyHash($hash){
		$query = "SELECT `adr_id`, `adr_source`, `adr_alamat`, `adr_kec`,`namakec`, `adr_kota`, 
		`namakota`, `adr_prov`, `namaprov`,`adr_kode_pos`,`adr_notes` 
		FROM `ri_alamat` 
		LEFT JOIN dftr_prov ON dftr_prov.idprov=ri_alamat.adr_prov
		LEFT JOIN dftr_kota ON dftr_kota.idkota=ri_alamat.adr_kota
		LEFT JOIN dftr_kec ON dftr_kec.idkec=ri_alamat.adr_kec
		WHERE `adr_id`=:hash";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':hash',$hash,PDO::PARAM_STR,255);
 		$statement->execute();
 		$r = $statement->fetch(PDO::FETCH_ASSOC);
 		return $r;
	}
	
	public function Cekvoucher($kode){
		$query = "SELECT COUNT(rtv_id) AS jumlah FROM `ri_trans_vcr` WHERE ri_trans_vcr.rtv_kode=:kode";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':kode',$kode,PDO::PARAM_STR,255);
 		$statement->execute();
 		$r = $statement->fetch(PDO::FETCH_ASSOC);
		return $r['jumlah'];
	}
	

	public function UpdateSingleData($arr){
		$query = "UPDATE `ri_transaksi` SET ".$arr['atr']."=:newvalue WHERE ".$arr['key'].$arr['cond'].":keyval;";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':newvalue',$arr['atrval'],PDO::PARAM_STR,255);
 		$statement->bindParam(':keyval',$arr['keyval'],PDO::PARAM_STR,200);
 		
 		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	public function NewTransactionOldCust(){
		$query = "INSERT INTO `ri_alamat`(`adr_source`, `adr_alamat`) 
		VALUES (:addremail,:alamat);
		INSERT INTO `ri_transaksi`(`tr_id`, `tr_id_cust`, `tr_status`) 
		VALUES (:idtransaksi,:tremail,'0');
		INSERT INTO `ri_kurir` (`rk_id`, `rk_id_trans`, `rk_kode_jasa`, `rk_prov`, `rk_kota`, `rk_kec`, `rk_ongkir`) 
		VALUES (NULL, :idtrans, :kodejasa, :prov, :kota, :kec, :ongkir);
		INSERT INTO `ri_carts`(`cart_source`, `cart_items`, `qty`,`cart_itm_price`) VALUES 
		";
		$trid = $this->CreateIDTr();
		$query .= $this->QueryCarts($trid,$this->data->carts);

		$statement=$this->dbHost->prepare($query);

 		$statement->bindParam(':addremail',$this->data->email,PDO::PARAM_STR,255);
 		$statement->bindParam(':alamat',$this->data->alamat,PDO::PARAM_STR,2000);
 		$statement->bindParam(':idtransaksi',$trid,PDO::PARAM_STR,35);
 		$statement->bindParam(':tremail',$this->data->email,PDO::PARAM_STR,255);

 		@$statement->bindParam(':idtrans',$trid,PDO::PARAM_STR,35);
 		@$statement->bindParam(':kodejasa',$this->data->kodejasa,PDO::PARAM_STR,11);
 		@$statement->bindParam(':prov',$this->data->provinsi,PDO::PARAM_STR,40);
 		@$statement->bindParam(':kota',$this->data->kota,PDO::PARAM_STR,40);
 		@$statement->bindParam(':kec',$this->data->kecamatan,PDO::PARAM_STR,40);
 		@$statement->bindParam(':ongkir',$this->data->ongkir,PDO::PARAM_STR,8);
 		// echo $query;
 		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	public function NewAdminCarts(){
		$query="INSERT INTO `ri_carts`(`cart_source`, `cart_items`, `qty`) 
		VALUES (:src,:cartitem,:qty);";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':src',$this->id,PDO::PARAM_STR,35);
 		$statement->bindParam(':cartitem',$this->keranjang,PDO::PARAM_STR,25);
 		$statement->bindParam(':qty',$this->data,PDO::PARAM_STR,5);

 		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	public function DeleteCartItem(){
		$query = "DELETE FROM `ri_carts` WHERE `cart_id` = :cartid;";
		$statement=$this->dbHost->prepare($query);

		$statement->bindParam(':cartid',$this->data,PDO::PARAM_STR,35);

		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	public function NewAdminTransaction(){
		$query = "INSERT INTO `ri_transaksi`(`tr_id`, `tr_id_cust`, `tr_status`) 
		VALUES (:idtransaksi,:tremail,'0');";
		$trid = $this->CreateIDTr();

		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':idtransaksi',$trid,PDO::PARAM_STR,35);
 		$statement->bindParam(':tremail',$this->customer,PDO::PARAM_STR,255);

 		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	public function NewTransaction(){
		$query = "INSERT INTO `ri_customer`(`rc_email`, `rc_password`, `rc_nama`, `rc_telepon`) 
		VALUES (:custemail,:password,:namacust,:teleponcust);
		INSERT INTO `ri_alamat`(`adr_source`, `adr_alamat`) 
		VALUES (:addremail,:alamat);
		INSERT INTO `ri_transaksi`(`tr_id`, `tr_id_cust`, `tr_status`) 
		VALUES (:idtransaksi,:tremail,'0');
		INSERT INTO `ri_kurir` (`rk_id`, `rk_id_trans`, `rk_kode_jasa`, `rk_prov`, `rk_kota`, `rk_kec`, `rk_ongkir`) 
		VALUES (NULL, :idtrans, :kodejasa, :prov, :kota, :kec, :ongkir);
		INSERT INTO `ri_carts`(`cart_source`, `cart_items`, `qty`,`cart_itm_price`) VALUES 
		";
		$trid = $this->CreateIDTr();
		$query .= $this->QueryCarts($trid,$this->data->carts);

		$statement=$this->dbHost->prepare($query);

		$statement->bindParam(':custemail',$this->data->email,PDO::PARAM_STR,255);
 		$statement->bindParam(':password',$this->data->password,PDO::PARAM_STR,35);
 		$statement->bindParam(':namacust',$this->data->nama,PDO::PARAM_STR,75);
 		$statement->bindParam(':teleponcust',$this->data->telepon,PDO::PARAM_STR,25);
 		$statement->bindParam(':addremail',$this->data->email,PDO::PARAM_STR,255);
 		$statement->bindParam(':alamat',$this->data->alamat,PDO::PARAM_STR,2000);
 		$statement->bindParam(':idtransaksi',$trid,PDO::PARAM_STR,35);
 		$statement->bindParam(':tremail',$this->data->email,PDO::PARAM_STR,255);

 		@$statement->bindParam(':idtrans',$trid,PDO::PARAM_STR,35);
 		@$statement->bindParam(':kodejasa',$this->data->kodejasa,PDO::PARAM_STR,11);
 		@$statement->bindParam(':prov',$this->data->provinsi,PDO::PARAM_STR,40);
 		@$statement->bindParam(':kota',$this->data->kota,PDO::PARAM_STR,40);
 		@$statement->bindParam(':kec',$this->data->kecamatan,PDO::PARAM_STR,40);
 		@$statement->bindParam(':ongkir',$this->data->ongkir,PDO::PARAM_STR,8);

 		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	public function GetCartsOfCust(){
		$query = "SELECT `data_barang`.`id` AS `id`, `cart_id`, `id_jenis`, `detail_jenis`, `brg_berat`, `brg_harga`, `brg_satuan`, `jumlah_barang`, `brg_pic`, `brg_status`,
		`qty`,`cart_itm_price` AS `th_harga`,`jenis_barang`,`kb_ket`
		FROM `data_barang`
		INNER JOIN `ri_carts` ON `ri_carts`.`cart_items`=`data_barang`.`id`
		INNER JOIN `ri_transaksi` ON `ri_transaksi`.`tr_id`=`ri_carts`.`cart_source`
		INNER JOIN `jenis_barang` ON `jenis_barang`.`id` = `data_barang`.`id_jenis`
		INNER JOIN `kategori_barang` ON `kategori_barang`.`kb_id` = `jenis_barang`.`kb_id`
		WHERE `ri_transaksi`.`tr_id`=:transid;";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':transid',$this->id,PDO::PARAM_STR,35);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function UpdateCustTransaksi(){
		$query = "UPDATE `ri_transaksi` 
		SET `tr_id_cust` = :idcust 
		WHERE `ri_transaksi`.`tr_id` = :transid;";
		$statement=$this->dbHost->prepare($query);

		$statement->bindParam(':idcust',$this->customer,PDO::PARAM_STR,35);
		$statement->bindParam(':transid',$this->id,PDO::PARAM_STR,35);

		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	public function FetchTransByID(){
		$query = "SELECT * FROM `ri_transaksi` WHERE `ri_transaksi`.`tr_id`=:transid;";
		$statement=$this->dbHost->prepare($query);

		$statement->bindParam(':transid',$this->id,PDO::PARAM_STR,35);

		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);

	}

	public function DetailTransaction(){
		$query = "SELECT * 
		FROM `ri_transaksi`
		INNER JOIN `ri_customer` 
		ON `ri_customer`.`rc_email`=`ri_transaksi`.`tr_id_cust`
		INNER JOIN `ri_kurir`
		ON `ri_kurir`.`rk_id_trans`=`ri_transaksi`.`tr_id`
		INNER JOIN `ri_alamat`
		ON `ri_alamat`.`adr_id`=`ri_kurir`.`rk_alamat`
		WHERE `tr_id`=:transid;";
		$statement=$this->dbHost->prepare($query);

		$statement->bindParam(':transid',$this->id,PDO::PARAM_STR,35);

		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);
	}

	public function FetchCarts(){
		$query="SELECT `jenis_barang`,`detail_jenis`,`th_harga`,`brg_berat`,`cart_source`, `cart_items`, `qty` 
		FROM `ri_carts`
		INNER JOIN `data_barang` ON `data_barang`.`id`=`ri_carts`.`cart_items`
		INNER JOIN `jenis_barang` ON `jenis_barang`.`id`=`data_barang`.`id_jenis`
		INNER JOIN `tabel_harga` ON `data_barang`.`brg_harga` = `tabel_harga`.`th_id`
		INNER JOIN `ri_transaksi` ON `ri_transaksi`.`tr_id`=`ri_carts`.`cart_source`
		WHERE `ri_transaksi`.`tr_id`=:transid;";
		$statement=$this->dbHost->prepare($query);

		$statement->bindParam(':transid',$this->id,PDO::PARAM_STR,35);

		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function FetchForEmail(){
		$query = "SELECT `rc_email`,`rc_nama`,`tr_id`, `tr_id_cust`, `tr_tanggal`, `tr_bukti_upload`, `tr_status`,
		`rk_kode_jasa`, `rjk_kd_svc`, `rk_alamat`, `rk_prov`, `rk_kota`, `rk_kec`, `rk_ongkir`, `rk_resi`, `rk_tgl_kirim`
		FROM `ri_transaksi` 
		INNER JOIN `ri_kurir` ON `ri_kurir`.`rk_id_trans`=`ri_transaksi`.`tr_id`
		INNER JOIN `ri_customer` ON `ri_customer`.`rc_email`=`ri_transaksi`.`tr_id_cust`
		WHERE `tr_id`=:transid;";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':transid',$this->id,PDO::PARAM_STR,35);

		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);
	}

	public function CountItem(){
		$query = "SELECT COUNT(`cart_id`) AS `jumlah` FROM `ri_carts` WHERE `cart_source` = :transid;";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':transid',$this->id,PDO::PARAM_STR,35);

		$statement->execute();
		$hasil = $statement->fetch(PDO::FETCH_OBJ);
		return $hasil->jumlah;
	}

	public function FetchWeightOfItem(){
		$query="SELECT SUM(`data_barang`.`brg_berat` * `ri_carts`.`qty`) AS `berat` FROM `ri_carts` 
		INNER JOIN `data_barang` ON `data_barang`.`id`=`ri_carts`.`cart_items`
		WHERE `cart_source` = :transid;";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':transid',$this->id,PDO::PARAM_STR,35);

		$statement->execute();
		$hasil = $statement->fetch(PDO::FETCH_OBJ);
		return $hasil->berat;
	}

	public function FetchAlamatForKurir(){
		$query = "SELECT `adr_id`, `adr_source`, `adr_alamat` 
		FROM `ri_alamat`
		INNER JOIN `ri_customer` ON `ri_customer`.`rc_email`=`ri_alamat`.`adr_source`
		INNER JOIN `ri_transaksi` ON `ri_transaksi`.`tr_id_cust`=`ri_customer`.`rc_email`
		WHERE `ri_transaksi`.`tr_id`= :transid;";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':transid',$this->id,PDO::PARAM_STR,35);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function ReduceItem($itemid,$totalreduction){
		$query = "UPDATE data_barang SET jumlah_barang = jumlah_barang - $totalreduction WHERE `id` = '$itemid';";
		$statement=$this->dbHost->prepare($query);

		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	public function DisplayTransactions(){
		$query = "SELECT `tr_id`, `tr_id_cust`, `tr_tanggal`, `tr_status`,`rc_email`, `rc_nama`, `rc_telepon`,
		(SELECT GROUP_CONCAT(CONCAT(`jenis_barang`,' ',`detail_jenis`) SEPARATOR ' # ') 
		FROM `ri_carts` 
		INNER JOIN `data_barang` ON `data_barang`.`id` = `ri_carts`.`cart_items`
		INNER JOIN `jenis_barang` ON `jenis_barang`.`id` = `data_barang`.`id_jenis`
		WHERE `ri_carts`.`cart_source` = `tr_id`) AS `items`,
        (
			SELECT SUM(cart_itm_price*qty) FROM ri_carts WHERE cart_source=`tr_id`
		) AS `total`
        FROM `ri_customer`
		INNER JOIN `ri_transaksi` ON `tr_id_cust`=`ri_customer`.`rc_email`;";
		$statement=$this->dbHost->prepare($query);

		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	function QueryCarts($trid,$data){
		$query = '';
		foreach ($data as $key => $value) {
		  $query .= "('".$trid."','".$key."','".$value['qty']."','".$value['priceid']."'),";
		}
		$query.=";";
		$query = str_replace(',;', ';', $query);
		return $query;
	}


	// end Transaksi

    public function getId(){
        return $this->id;
    }

    public function setId($id){
        $this->id = $id; 
    }

    public function getCustomer(){
        return $this->customer;
    }

    public function setCustomer($customer){
        $this->customer = $customer;        
    }

    public function getTanggal(){
        return $this->tanggal;
    }

    public function setTanggal($tanggal){
        $this->tanggal = $tanggal;        
    }

    public function getStatus(){
        return $this->status;
    }

    public function setStatus($status){
        $this->status = $status;        
    }

    public function getKeranjang(){
        return $this->keranjang;
    }

    public function setKeranjang($keranjang){
        $this->keranjang = $keranjang;        
    }

    public function getData(){
        return $this->data;
    }

    public function setData($data){
        $this->data = $data;
    }
}

