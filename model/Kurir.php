<?php 
include_once 'Koneksi.php';
class Kurir extends Koneksi{
    private $idtrans;
    private $kid;
    private $jasa;
    private $layanan;
    private $alamat;
    private $provinsi;
    private $kota;
    private $ongkir;
    private $tglkirim;
    private $resi;

    public function __construct(){
     	$this->dbHost = $this->bukaKoneksi();   
    }

    public function UpdateResi(){
    	$query = "UPDATE `ri_kurir` SET `rk_resi`=:resi, `rk_tgl_kirim`=:tglkirim WHERE `rk_id_trans`=:transid;";
    	$statement=$this->dbHost->prepare($query);

    	$statement->bindParam(':transid',$this->idtrans,PDO::PARAM_STR,35);
    	$statement->bindParam(':resi',$this->resi,PDO::PARAM_STR,255);
        $statement->bindParam(':tglkirim',$this->tglkirim,PDO::PARAM_STR,255);

    	if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
    }

    public function UpdateData(){
        $query = "UPDATE `ri_kurir` 
        SET `rk_kode_jasa`=:jasa,`rjk_kd_svc`=:service,`rk_alamat`=:alamat,`rk_prov`=:provinsi,`rk_kota`=:kota,`rk_ongkir`=:ongkir 
        WHERE `rk_id_trans`=:idtrans;";

        $statement=$this->dbHost->prepare($query);

        $statement->bindParam(':jasa',$this->jasa,PDO::PARAM_STR,35);
        $statement->bindParam(':service',$this->layanan,PDO::PARAM_STR,255);
        $statement->bindParam(':alamat',$this->alamat,PDO::PARAM_STR,15);
        $statement->bindParam(':provinsi',$this->provinsi,PDO::PARAM_STR,25);
        $statement->bindParam(':kota',$this->kota,PDO::PARAM_STR,35);
        $statement->bindParam(':ongkir',$this->ongkir,PDO::PARAM_STR,255);
        $statement->bindParam(':idtrans',$this->idtrans,PDO::PARAM_STR,35);

        if ($statement->execute()) {
            return true;
        }else{
            return false;
        }
    }

    public function FetchKurir(){
        $query = "SELECT `rk_id`, `rk_id_trans`, `rk_kode_jasa`, `rjk_kd_svc`, `ri_alamat`.`adr_alamat` AS `alamat`, `rk_prov`, `rk_kota`, `rk_kec`, `rk_ongkir`, `rk_resi`, `rk_tgl_kirim` 
        FROM `ri_kurir` 
        LEFT JOIN `ri_alamat` ON `ri_alamat`.`adr_id`=`ri_kurir`.`rk_alamat`
        WHERE `rk_id_trans`=:transid;";
        $statement=$this->dbHost->prepare($query);

        $statement->bindParam(':transid',$this->idtrans,PDO::PARAM_STR,35);

        $statement->execute();
        $results=$statement->fetchAll(PDO::FETCH_ASSOC);
        $dataz=json_encode($results);
        return json_decode($dataz);
    }

    public function FetchOneRow($arr){
        $query = "SELECT `rk_id`, `rk_id_trans`, `rk_kode_jasa`, `rjk_kd_svc`, `ri_alamat`.`adr_alamat` AS `alamat`, `rk_prov`, `rk_kota`, `rk_kec`, `rk_ongkir`, `rk_resi`, `rk_tgl_kirim` 
        FROM `ri_kurir` 
        LEFT JOIN `ri_alamat` ON `ri_alamat`.`adr_id`=`ri_kurir`.`rk_alamat` 
        WHERE ".$arr['atr'].$arr['cond'].":value;";
        $statement=$this->dbHost->prepare($query);
        $statement->bindParam(':value',$arr['value'],PDO::PARAM_STR,35);

        $statement->execute();
        return $statement->fetch(PDO::FETCH_OBJ);
    }

    public function NewData(){
        $query = "INSERT INTO `ri_kurir` (`rk_id_trans`, `rk_kode_jasa`, `rjk_kd_svc`, `rk_alamat`, `rk_prov`, `rk_kota`, `rk_ongkir`,`rk_tgl_kirim`) VALUES (:transid, :jasa, :layanan, :alamat, :prov, :kota, :ongkir,CURDATE());";
        $statement=$this->dbHost->prepare($query);

        $statement->bindParam(':transid',$this->idtrans,PDO::PARAM_STR,35);
        $statement->bindParam(':jasa',$this->jasa,PDO::PARAM_STR,55);
        $statement->bindParam(':layanan',$this->layanan,PDO::PARAM_STR,35);
        $statement->bindParam(':alamat',$this->alamat,PDO::PARAM_STR,25);
        $statement->bindParam(':prov',$this->provinsi,PDO::PARAM_STR,75);
        $statement->bindParam(':kota',$this->kota,PDO::PARAM_STR,75);
        $statement->bindParam(':ongkir',$this->ongkir,PDO::PARAM_STR,9);

        if ($statement->execute()) {
            return true;
        }else{
            return false;
        }
    }

    

    public function getKid()
    {
        return $this->kid;
    }

    public function setKid($kid)
    {
        $this->kid = $kid;
    }

    public function getJasa()
    {
        return $this->jasa;
    }

    public function setJasa($jasa)
    {
        $this->jasa = $jasa;
    }

    public function getLayanan()
    {
        return $this->layanan;
    }

    public function setLayanan($layanan)
    {
        $this->layanan = $layanan;
    }

    public function getAlamat()
    {
        return $this->alamat;
    }

    public function setAlamat($alamat)
    {
        $this->alamat = $alamat;
    }

    public function getOngkir()
    {
        return $this->ongkir;
    }

    public function setOngkir($ongkir)
    {
        $this->ongkir = $ongkir;
    }

    public function getResi()
    {
        return $this->resi;
    }

    public function setResi($resi)
    {
        $this->resi = $resi;
    }

    public function getProvinsi()
    {
        return $this->provinsi;
    }

    public function setProvinsi($provinsi)
    {
        $this->provinsi = $provinsi;
    }

    public function getKota()
    {
        return $this->kota;
    }

    public function setKota($kota)
    {
        $this->kota = $kota;
    }

    public function getIdtrans()
    {
        return $this->idtrans;
    }

    public function setIdtrans($idtrans)
    {
        $this->idtrans = $idtrans;
    }

    public function getTglkirim(){
        return $this->tglkirim;
    }

    public function setTglkirim($tglkirim){
        $this->tglkirim = $tglkirim;
    }
}
?>