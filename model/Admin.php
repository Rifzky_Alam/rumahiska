<?php 
include_once 'Koneksi.php';
class Admin extends Koneksi{

	private $username;
	private $password;
	private $namalengkap;
	private $tipe;

	public function getUsername(){
		return $this->username;
	}

	public function setUsername($value){
		$this->username = $value;
	}

	public function getPassword(){
		return $this->password;	
	}

	public function setPassword($value){
		$this->password = $value;
	}

	public function getNamalengkap(){
		return $this->namalengkap;
	}

	public function setNamalengkap($value){
		$this->namalengkap = $value;
	}

	public function getTipe(){
		return $this->tipe;
	}

	public function setTipe($value){
		$this->tipe = $value;
	}
	
	function __construct(){
		$this->dbHost = $this->bukaKoneksi();
	}

	public function Login(){
		$query="SELECT * FROM `users` WHERE `username`=:user AND `password`=:pass";
		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':user',$this->username,PDO::PARAM_STR,75);
		@$statement->bindParam(':pass',md5($this->password),PDO::PARAM_STR,75);
		$statement->execute();

		return $statement->fetch(PDO::FETCH_OBJ);	
	}
}


?>