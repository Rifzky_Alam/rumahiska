<?php 
session_start();
include_once 'library/controller/distributor.controller.php';
if (isset($_SERVER['PATH_INFO'])) {
	$_SERVER['PATH_INFO'] = str_replace('.php', '', $_SERVER['PATH_INFO']);
	@$url_segment = explode('/', trim($_SERVER['PATH_INFO'],'/'));
	$key = array_shift($url_segment);

	switch ($key) {
		case 'login':
			//print_r($_SERVER);
			Distributor::Login();
			break;
		case 'logout':
			Distributor::Logout();
			break;
		case 'new':
			Distributor::Newagent();
			break;
		case 'produk':
			Distributor::Produk();
			break;
		case 'hapusalamat':
			if (isset($_GET['val'])) {
				Distributor::HapusAlamat($_GET['val']);	
			}
			break;
		case 'tambahalamat':
		    Distributor::TambahAlamat();
		    break;
		case 'pembayaran':
		    Distributor::Pembayaran();
		    break;
		case 'registersuccess':
			if (!isset($url_segment[0])) {
				header('Location: http://'.$_SERVER['SERVER_NAME']);
			}
			Distributor::SuccessReg($url_segment[0]);
			break;
		case 'register':
			if (!isset($url_segment[0])) {
				header('Location: http://'.$_SERVER['SERVER_NAME']);
			}
			Distributor::Register($url_segment[0]);
			break;
		case 'validasi':
			if (!isset($url_segment[0])) {
				header('Location: http://'.$_SERVER['SERVER_NAME']);
			}
			Distributor::Validasi($url_segment[0]);
			break;
		case 'peluang':
			Distributor::Peluang();
			break;
		case 'dasbor':
			Distributor::Dasbor();
			break;
		case 'logout':
			session_destroy();
			break;
		case 'coba':
			echo 'bisa';
			break;
		case '':
			// header('Location: '.Distributor::BaseUrl());
			// echo Distributor::BaseUrl();
			header('location:'.'http://'.$_SERVER['SERVER_NAME'].'/distributor/login');
			break;

	}
}else{
    header('location:'.'http://'.$_SERVER['SERVER_NAME'].'/distributor/login');
}

?>