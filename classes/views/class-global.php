<?php 
class DataView{
	public $homedir = '/home/salvinai/public_html/';//
	// public $homedir = '/Applications/XAMPP/xamppfiles/htdocs/rikza/';
    public $base_url = 'http://salvina.id/';
 	public $title;
 	public $subtitle;
 	public $table;
 	public $company='Salvina';
 	public $companyemail='salvina.id@gmail.com';
 	public $addrcompany='Jl Bintara 8 No 22 Rt 4 Rw 3 Bekasi Barat 17134';
 	public $companydesc='Beauty in simplicity';
 	public $companyphone = '+628561550085';
 	public $companycontactname = 'Widia';
 	public $rekening = [
 		'BANK BCA Syariah (kode: 536)0261000395 A/N Ayu noor laila sari',
 		'BANK BNI Syariah 0322131041 A/N Ayu Noor Laila',
 		'BANK Mandiri 900000 360 2027 A/N Ayu Noor Laila Sari'
 	];
 	public $me_url;

 	public function Model($file){
		include_once $this->homedir.'model/'.$file.'.php';
	}

	public function CheckSegment($segment){
		if (empty($segment)) {
			header('Location: '.$this->base_url.'error/404');
		}
	}

	public function securitycode($string,$action){
	  $secret_key = "R!f2ky_Al@m&L0el*3_N@z!f4";
	  $secret_iv = "Lulu_Nazifa_Secret_iv";

	  $output = false;
	  $encrypt_method = "AES-256-CBC";
	  $key = hash('sha256',$secret_key);
	  $iv = substr(hash('sha256', $secret_iv), 0,16);
	  if ($action=='e') {
	    $output = base64_encode(openssl_encrypt($string, $encrypt_method, $key,0,$iv));
	  }elseif ($action=='d') {
	    $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key,0,$iv);
	  }
	  return $output;
	}

	public function CheckSegment2($segment,$redir){
		if (empty($segment)) {
			echo "<script>location.replace('".$redir."');</script>";
		}
	}

	public function JustInclude($path){
		include_once $this->homedir.$path.'.php';	
	}

	public function IncludeFileWithData($path,$data){
		include_once $this->homedir.$path;
	}

	public function View($path,$data){
		include_once $this->homedir.'view/'.$path;
	}

	public function Lib($file){
		include_once $this->homedir.'library/'.$file.'.php';
	}

 	public function __construct(){
 	}
    
}

?>