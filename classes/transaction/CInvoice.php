<?php 

class CInvoice{
    private $ititle;
    private $iheadertext;
    private $icustomer;
    private $ibodytext;
    private $iresi;
    private $ideliverydate;
    private $ibankaccount;
    private $icolumn;
    private $iitems;
    private $ifooter;
    private $istaff;

    public function __construct($array){
        $this->setItitle('Konfirmasi Pembayaran');
        $this->setIheadertext('Terimakasih telah melakukan pesanan barang di rumahiska.com. Berikut kami informasikan detail transaksi anda.');
        $this->setIcustomer($data['customer']);
        $this->setIbodytext('');
        $this->setIresi();
        $this->setIdeliverydate();
        $this->setIbankaccount();
        $this->setIitems();
        $this->setIfooter();
        $this->setIstaff();
    }

    public function TableHead(){
    	return '<tr>
					<th>Nama Item</th>
                	<th>Berat</th>
                	<th>Qty</th>
                	<th>Harga</th>
               	</tr>';
    }

    public function TableBody(){
    	
    }



    public function getItitle(){
        return $this->ititle;
    }

    public function setItitle($ititle){
        $this->ititle = $ititle;
    }

    public function getIheadertext(){
        return $this->iheadertext;
    }

    public function setIheadertext($iheadertext){
        $this->iheadertext = $iheadertext;
    }

    public function getIcustomer(){
        return $this->icustomer;
    }

    public function setIcustomer($icustomer)
    {
        $this->icustomer = $icustomer;
    }

    public function getIbodytext()
    {
        return $this->ibodytext;
    }

    public function setIbodytext($ibodytext){
        $this->ibodytext = $ibodytext;
    }

    public function getIresi(){
        return $this->iresi;
    }

    public function setIresi($iresi){
        $this->iresi = $iresi;
    }

    public function getIdeliverydate(){
        return $this->ideliverydate;
    }

    public function setIdeliverydate($ideliverydate){
        $this->ideliverydate = $ideliverydate;
    }

    public function getIbankaccount(){
        return $this->ibankaccount;
    }

    public function setIbankaccount($ibankaccount){
        $this->ibankaccount = $ibankaccount;
    }

    public function getIitems(){
        return $this->iitems;
    }

    public function setIitems($iitems){
        $this->iitems = $iitems;
    }

    public function getIfooter(){
        return $this->ifooter;
    }

    public function setIfooter($ifooter){
        $this->ifooter = $ifooter;
    }

    public function getIstaff(){
        return $this->istaff;
    }

    public function setIstaff($istaff){
        $this->istaff = $istaff;
    }

    public function getIcolumn(){
        return $this->icolumn;
    }

    public function setIcolumn($icolumn){
        $this->icolumn = $icolumn;
    }
}

?>