<?php 
session_start();
// include_once '/home/rumahisk/public_html/classes/views/class-global.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/classes/views/class-global.php';
$data = new DataView();
$data->Model('Transaksi');
$data->Model('Querybuilder');
$data->Model('Mail');
$surat = new RumahIskaMail();
$trans = new Transaksi();
$db = new QueryBuilder();

if (isset($_SERVER['PATH_INFO'])) {
	@$url_segment = explode('/', trim($_SERVER['PATH_INFO'],'/'));
	$key = array_shift($url_segment);
	
	if (isset($_GET['tr'])&&!empty($_GET['tr'])) {
		// include template of mail
		$data->IncludeFileWithData('view/administrasi/transaksi/email/vemailuploadkonfirmasi.php',$data);

		$idtrans = $data->securitycode($_GET['tr'],'d');
		$trans->setId($idtrans);
		
		// data surat declaration
		$data->idtransaksi = $idtrans;
		$data->email_to = 'rifzky.mail@gmail.com';
		$data->mailsubject = 'Bukti Pembayaran Transaksi '.$idtrans;
		$data->mailbody = NotifikasiUploadFile($data);
		// end declaration

		$datatrans = $trans->DetailTransaction();
	
		if ($datatrans=='') {
			echo "<script>alert('Maaf, data transaksi anda tidak terdaftar :(');</script>";
			echo "<script>location.replace('".$data->base_url."');</script>";	
		}

		$data->judul = 'Konfirmasi Pembayaran';
		$data->nama_cust = $datatrans->rc_nama;
		if (isset($_FILES['bukti'])) {
			$data->Lib('otherfunction/Picturefunc');
			$_FILES['bukti']['name'] = $idtrans . '.' . GetFileExtension($_FILES['bukti']);
			$data->uploadfolder = BuktiTransaksiFolder($data->homedir);
			if ($trans->UpdateSingleData(['atr' => 'tr_bukti_upload','atrval' => $_FILES['bukti']['name'],'key' => 'tr_id','cond' => '=','keyval' => $idtrans])) {
				if (UploadPic($_FILES['bukti'],$_SERVER['DOCUMENT_ROOT'].'/uploads/images/transaksi/')) {
					if ($db->UpdateData('ri_transaksi',['tr_status'=>'2'],array($db->GetCond('tr_id','=',$idtrans)))) {
						// echo "<script>alert('Data anda berhasil disimpan.');</script>";	
						//sending email
						$surat->NotifUploadBukti($data);
						//redirect to homebase
						header('location:'.$data->base_url);
					}
				}else{
					echo "<script>alert('Terdapat kesalahan dalam menyimpan file gambar.');</script>";
				}

			} else {
				echo "<script>alert('Data Gagal disimpan.');</script>";
			}
		}

		$data->View('order/vupload.order.php',$data);




	}

	/*
	$trans->setId($data->securitycode($key,'e'));
	$datatrans = $trans->DetailTransaction();
	
	if ($datatrans=='') {
		echo "<script>alert('Maaf, data transaksi anda tidak terdaftar :(');</script>";
		echo "<script>location.replace('".$data->base_url."');</script>";	
	}

	$data->judul = 'Konfirmasi Pembayaran';
	$data->nama_cust = $datatrans->rc_nama;
	if (isset($_FILES['bukti'])) {
		$data->Lib('otherfunction/Picturefunc');
		$_FILES['bukti']['name'] = $key . '.' . GetFileExtension($_FILES['bukti']);
		$data->uploadfolder = BuktiTransaksiFolder($data->homedir);
		if ($trans->UpdateSingleData(['atr' => 'tr_bukti_upload','atrval' => $_FILES['bukti']['name'],'key' => 'tr_id','cond' => '=','keyval' => $key])) {
			if (UploadPic($_FILES['bukti'],$data->uploadfolder)) {
				echo "<script>alert('Data anda berhasil disimpan.');</script>";
			}else{
				echo "<script>alert('Terdapat kesalahan dalam menyimpan file gambar.');</script>";
			}

		} else {
			echo "<script>alert('Data Gagal disimpan.');</script>";
		}
	}

	$data->View('order/vupload.order.php',$data);
	*/
	// echo $key;



}

?>