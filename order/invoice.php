<?php 
session_start();
include_once $_SERVER['DOCUMENT_ROOT'].'/library/controller/order.controller.php';

if (isset($_SERVER['PATH_INFO'])) {
	@$url_segment = explode('/', trim($_SERVER['PATH_INFO'],'/'));
	$key = array_shift($url_segment);
	switch ($key) {
		case '':
			header('Location: http://salvina.id');
			break;
		case 'token':
			// Order::testoken();
			$_SESSION['ri_token'] = md5('Rit-000002');
			break;
		case 'pdf':
			if (isset($_GET['tr'])&&!empty($_GET['tr'])) {
				Order::ShowPDFInvoice($_GET['tr']);
			}
			break;
		case 'template':
			if (isset($_POST['token'])&&!empty($_POST['token'])) {
				Order::ResendingInvoiceViaClient($_POST['token']);
			}
			break;
		case 'sending':
			if (isset($_SESSION['ri_token'])&&!empty($_SESSION['ri_token'])) {
				if (isset($_GET['tr'])&&!empty($_GET['tr'])) {
					Order::cektoken($_GET['tr']);
					Order::SendingInvoiceViaClient($_GET['tr']);
				}
			}else{
				header('Location: http://salvina.id');	
			}
			break;
		default:
			header('Location: http://salvina.id');
			break;
	}
}else {
	if (isset($_GET['tr'])&&!empty($_GET['tr'])) {
		Order::ShowHtmlInvoice($_GET['tr']);
	}else{
		header('Location: http://salvina.id');
	}
}