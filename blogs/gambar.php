<?php 
include_once '../baseurl.php';
include_once HomeDir().'model/Artikel.php';
$artikel = new Artikel();

if (isset($_GET['v'])&&!empty($_GET['v'])) {

	if (preg_match('/.png/', $_GET['v'])) {
		header("Content-type:image/png");
		$val = HomeDir()."uploads/images/artikel/".$_GET['v'];
		readfile($val);
	}elseif (preg_match('/.jpg/', $_GET['v'])||preg_match('/.jpeg/', $_GET['v'])) {
		header("Content-type:image/jpeg");
		$val = HomeDir()."uploads/images/artikel/".$_GET['v'];
		readfile($val);
	}elseif (preg_match('/.gif/', $_GET['v'])) {
		// $artikel->OpenImage(HomeDir().'uploads/images/artikel/'.$_GET['v'],'image/gif');
		header("Content-type:image/gif");
		$val = HomeDir()."uploads/images/artikel/".$_GET['v'];
		readfile($val);
	}
}

